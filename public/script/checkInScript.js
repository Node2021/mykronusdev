//function to send details to PopUp.
		
		
		function sendDetails(place){
		   $("#loadingBar").show();
     	    timerControl("start");
			// var apikey = getApi()
		   $.ajax({
		         url:path + '/agileActionNew.do',
		        type:"POST",
		       
	 			dataType:'json',
	 			data:{action:"getCompanyDetailsForBusinessSolutions",place:place},
	 			success:function(result){
	 				
	 			   result = JSON.stringify(result);
	 			   var data = jQuery.parseJSON(result);
	 			    $("#loadingBar").hide();
	 			    timerControl("");
	 			    if(place=="wc" || place=="bc"){
		 			    //first fill the values of default(static values)
		 			    $("#BusinessCard1 input#authKey").val(data[0].authKey);
		 			   // $("#BusinessCard1 input#authKey").val("");
		 			   
		 			    $("#BusinessCard1 select#projName").html("");
		 			    var dataUI = '<option value="0">Select Project Name</option>';
		 			    var projList = eval(data[0].projList);
		 			     for(var i=0; i<projList.length; i++){
					         dataUI +="<option value=\""+projList[i].projId+"\" title=\""+replaceSpecialCharacter(projList[i].projectName)+"\">"+replaceSpecialCharacter(projList[i].projectName)+"</option>"
					     }
					    $("#BusinessCard1 select#projName").append(dataUI);
						
						$("#BusinessCard1 select#epicName").html("");
						var dataUI = '<option value="" pId ="0"></option>';
		 			    var epicList = eval(data[0].epicList);
		 			     for(var i=0; i<epicList.length; i++){
					         dataUI +="<option value=\""+epicList[i].custId+"\" pId =\""+epicList[i].projId+"\"  title=\""+replaceSpecialCharacter(epicList[i].epicName)+"\">"+replaceSpecialCharacter(epicList[i].epicName)+"</option>"
					     }
					    $("#BusinessCard1 select#epicName").append(dataUI);
						$("#BusinessCard1 select#epicName option").not('option[pId="0"]').hide();
					
						
						$("#BusinessCard1 input#projName").val("");
						$("#BusinessCard1 input#epicName").val("");
						
						$("#BusinessCard1 input#domain").val(root);
						$("#BusinessCard1 input#epicId").val("");
						
						$("#BusinessCard1 input#act").val("createStory");
						$("#BusinessCard1 input#loginName").val(data[0].userLoginName);
						$("#BusinessCard1 textarea#comment").val('');
						
						if(place=='bc' && $("input#Department").val().trim()!='')
						  $("#BusinessCard1 textarea#comment").val('Department : '+$("input#Department").val());
						if(place=='bc' && $("input#Date").val().trim()!='')
						  $("#BusinessCard1 textarea#comment").val($("#BusinessCard1 textarea#comment").val()+' | Order Date : '+$("input#Date").val());  
						if(place=='bc' && $("input#phone").val().trim()!='')
						  $("#BusinessCard1 textarea#comment").val($("#BusinessCard1 textarea#comment").val()+' | Phone Number : '+$("input#phone").val());
						
						if(place=='bc' && $("input#email").val().trim()!='')
						  $("#BusinessCard1 textarea#comment").val($("#BusinessCard1 textarea#comment").val()+" | Approving Manager's Email : "+$("input#email").val());
					    //dynamic values
						$("#BusinessCard1  textarea#orderNew").val($("input#order").val());
						$("#BusinessCard1  input#rc_num").val($("input#rcNum").val());
						$("#BusinessCard1  input#Date_New").val($("input#Date").val())
						$("#BusinessCard1  input#DepartmentNew").val($("input#Department").val())
						$("#BusinessCard1  input#phoneNew").val($("input#phone").val())
						$("#BusinessCard1  input#emailNew").val($("input#email").val())
						commonFunction();
					}else if(place=="cc"){
						//first fill the values of default(static values)
		 			    $("#BusinessCard8 input#authKey").val(data[0].authKey);
		 			   // $("#BusinessCard3 input#authKey").val(apikey);
		 			    $("#BusinessCard8  input#domain").val(root);
						$("#BusinessCard8  input#act").val("InsertComment");
						$("#BusinessCard8  input#loginName").val(data[0].userLoginName);
						$("#BusinessCard8  select#projName").val("");
		 			    $("#BusinessCard8  input#projCode").val("");
		 			    $("#BusinessCard8  textarea#projDesc").val("");
		 			    $("#BusinessCard8  input#projApi").val("");
		 			     var dataUI = '<option value="0">Select Project Name</option>';
		 			    var projList = eval(data[0].projList);
		 			     for(var i=0; i<projList.length; i++){
					         dataUI +="<option value=\""+projList[i].projId+"\" title=\""+replaceSpecialCharacter(projList[i].projectName)+"\">"+replaceSpecialCharacter(projList[i].projectName)+"</option>"
					     }
					    
					    $("#BusinessCard8 select#projName").append(dataUI);
						commonFunctionForConversations();
					}else if(place=="ta"){
						//first fill the values of default(static values)
		 			    $("#BusinessCard9 input#authKey").val(data[0].authKey);
		 			   // $("#BusinessCard3 input#authKey").val(apikey);
		 			    $("#BusinessCard9  input#domain").val(root);
						$("#BusinessCard9  input#act").val("createTask");
						$("#BusinessCard9  input#loginName").val(data[0].userLoginName);
						$("#BusinessCard9  select#projName").val("");
		 			   
		 			    $("#BusinessCard9  textarea#projDesc").val("");
		 			    $("#BusinessCard9  input#projApi").val("");
		 			     var dataUI = '<option value="0">Select Project Name</option>';
		 			    var projList = eval(data[0].projList);
		 			     for(var i=0; i<projList.length; i++){
					         dataUI +="<option value=\""+projList[i].projId+"\" title=\""+replaceSpecialCharacter(projList[i].projectName)+"\">"+replaceSpecialCharacter(projList[i].projectName)+"</option>"
					     }
					    
					    $("#BusinessCard9 select#projName").append(dataUI);
					    commonFunctionForTask();
					}else if(place=="ws"){
						//first fill the values of default(static values)
		 			    $("#BusinessCard3 input#authKey").val(data[0].authKey);
		 			   // $("#BusinessCard3 input#authKey").val(apikey);
		 			    $("#BusinessCard3  input#domain").val(root);
						$("#BusinessCard3  input#act").val("createProject");
						$("#BusinessCard3  input#loginName").val(data[0].userLoginName);
						$("#BusinessCard3  input#projName").val("");
		 			    $("#BusinessCard3  input#projCode").val("");
		 			    $("#BusinessCard3  textarea#projDesc").val("");
		 			    $("#BusinessCard3  input#projApi").val("");
		 			    
						commonFunctionForWorkspace();
					}else if(place=="msg"){
						//first fill the values of default(static values)
		 			    $("#BusinessCard10 input#authKey").val(data[0].authKey);
		 			   // $("#BusinessCard3 input#authKey").val(apikey);
		 			    $("#BusinessCard10  input#domain").val(root);
						$("#BusinessCard10  input#act").val("sendGroupMessage");
						$("#BusinessCard10  input#loginName").val(data[0].userLoginName);
						$("#BusinessCard10  input#groupName").val("");
		 			    $("#BusinessCard10  textarea#Message").val("");
		 			    $("#BusinessCard10  textarea#messageApi").val("");
		 			    
						commonFunctionForMessage();
					}else if(place=="cu"){
					//first fill the values of default(static values)
		 			    $("#BusinessCard4 input#authKey").val(data[0].authKey);
		 			    //$("#BusinessCard4 input#authKey").val(apikey);
		 			    $("#BusinessCard4  input#domain").val(root);
						$("#BusinessCard4  input#act").val("createUser");
						$("#BusinessCard4  input#loginName").val(data[0].userLoginName);
						$("#BusinessCard4  input#firstName").val("");
		 			    $("#BusinessCard4  input#lastName").val("");
		 			    $("#BusinessCard4  input#email").val("");
		 			    $("#BusinessCard4  input#userId").val("");
		 			    $("#BusinessCard4  input#createUserApi").val("");
		 			    
						commonFunctionForCreateUser();
					}else if(place=="au"){
					
						$("#BusinessCard5  input#authKey").val(data[0].authKey);
						//$("#BusinessCard5  input#authKey").val(apikey);
		 			    $("#BusinessCard5  input#domain").val(root);
						$("#BusinessCard5  input#act").val("associateUser");
						$("#BusinessCard5  input#loginName").val(data[0].userLoginName);
						
						$("#BusinessCard5  input#projAdmins").val("");
		 			    $("#BusinessCard5  input#teamMembers").val("");
		 			    $("#BusinessCard5  input#observers").val("");
		 			    $("#BusinessCard5  input#associateUserApi").val("");
		 			    
		 			    
		 			    $("#BusinessCard5 select#projName").html("");
		 			    var dataUI = '<option value="0">Select Project Name</option>';
		 			    var projList = eval(data[0].projList);
		 			     for(var i=0; i<projList.length; i++){
					         dataUI +="<option value=\""+projList[i].projId+"\" title=\""+replaceSpecialCharacter(projList[i].projectName)+"\">"+replaceSpecialCharacter(projList[i].projectName)+"</option>"
					     }
					    
					    $("#BusinessCard5 select#projName").append(dataUI);
					    
					    //$("#BusinessCard5  input#projName").val("");
					    commonFunctionForAssociateUser();
					}else if(place=="st"){
		 			    //first fill the values of default(static values)
		 			    $("#BusinessCard6 input#authKey").val(data[0].authKey);
		 			   // $("#BusinessCard6 input#authKey").val(apikey);
		 			    
		 			    $("#BusinessCard6 select#projName").html("");
		 			    var dataUI = '<option value="0">Select Project Name</option>';
		 			    var projList = eval(data[0].projList);
		 			     for(var i=0; i<projList.length; i++){
					         dataUI +="<option value=\""+projList[i].projId+"\" title=\""+replaceSpecialCharacter(projList[i].projectName)+"\">"+replaceSpecialCharacter(projList[i].projectName)+"</option>"
					     }
					    $("#BusinessCard6 select#projName").append(dataUI);
						
						$("#BusinessCard6 select#epicName").html("");
						var dataUI1 = '<option value="" pId ="0"></option>';
		 			    var epicList = eval(data[0].epicList);
		 			     for(var i=0; i<epicList.length; i++){
					         dataUI1 +="<option value=\""+epicList[i].custId+"\" pId =\""+epicList[i].projId+"\"  title=\""+replaceSpecialCharacter(epicList[i].epicName)+"\">"+replaceSpecialCharacter(epicList[i].epicName)+"</option>"
					     }
					    $("#BusinessCard6 select#epicName").append(dataUI1);
						//$("#BusinessCard6 select#epicName option").not('option[pId="0"]').hide();
						
						
						//$("#BusinessCard6 input#projName").val("");
						//$("#BusinessCard6 input#epicName").val("");
						
						$("#BusinessCard6 input#domain").val(root);
						$("#BusinessCard6 input#epicId").val("");
						
						$("#BusinessCard6 input#act").val("createStory");
						$("#BusinessCard6 input#loginName").val(data[0].userLoginName);
						$("#BusinessCard6 textarea#comment").val('');
						
						$("#BusinessCard6 input#taskName").val("");
						$("#BusinessCard6 input#taskStartDate").val("");
						$("#BusinessCard6 input#taskEndDate").val("");
						
						$("#BusinessCard6 textarea#instructions").val("");
						$("#BusinessCard6 input#participants").val("")
			
						if(place=='bc' && $("input#Department").val().trim()!='')
						  $("#BusinessCard6 textarea#comment").val('Department : '+$("input#Department").val());
						if(place=='bc' && $("input#Date").val().trim()!='')
						  $("#BusinessCard6 textarea#comment").val($("#BusinessCard6 textarea#comment").val()+' | Order Date : '+$("input#Date").val());  
						if(place=='bc' && $("input#phone").val().trim()!='')
						  $("#BusinessCard6 textarea#comment").val($("#BusinessCard6 textarea#comment").val()+' | Phone Number : '+$("input#phone").val());
						
						if(place=='bc' && $("input#email").val().trim()!='')
						  $("#BusinessCard6 textarea#comment").val($("#BusinessCard6 textarea#comment").val()+" | Approving Manager's Email : "+$("input#email").val());
					    //dynamic values
						$("#BusinessCard6  textarea#orderNew").val($("input#order").val());
						$("#BusinessCard6  input#rc_num").val($("input#rcNum").val());
						$("#BusinessCard6  input#Date_New").val($("input#Date").val())
						$("#BusinessCard6  input#DepartmentNew").val($("input#Department").val())
						$("#BusinessCard6  input#phoneNew").val($("input#phone").val())
						$("#BusinessCard6  input#emailNew").val($("input#email").val())
						commonFunctionForAgileAPI();
					}else if(place=="aKey"){
		 			    $("#BusinessCard7 input#domain").val(root);
						$("#BusinessCard7 input#act").val("fetchAuthKey");
						$("#BusinessCard7 input#companyId").val("");
						$("#BusinessCard7 input#systemAdminId").val("");
						commonFunctionForAuthKeyAPI();
					}
	 			}	
	 			
	 				  
 		 });	
		
			
		}

		function sendFormData(){
			document.getElementById("BusinessCard1").submit();
		}
		
		function projChange(){
		    var projId = $("#BusinessCard1 select#projName option:selected").prop("value");
		    $("#BusinessCard1 select#epicName option").hide();
		    $("select#epicName option[pId=0]").prop('selected','true').show();
		    $("#BusinessCard1 input#epicId").val("");
		    $("#BusinessCard1 select#epicName option[pId="+projId+"]").show();
		    commonFunction();
		}
		
		function epicChange(){
			var custId = $("#BusinessCard1 select#epicName option:selected").prop("value");
		    $("#BusinessCard1 input#epicId").val(custId);
		    commonFunction();
		}
		
		 /*------------------ special character replace method --------------------------*/
 
 function replaceSpecialCharacter(input){
    input = input.replaceAll( 'CHR(39)','&#96;');
    input = input.replaceAll( 'CH(58)','&#180;');
    input = input.replaceAll( 'CHR(40)','&quot;');
    input = input.replaceAll( 'CH(40)','&quot;');
    input = input.replaceAll( 'CH(39)','&quot;');
    input = input.replaceAll( 'CH(50)','\n');
    input = input.replaceAll( 'CHR(50)','\n');
    input = input.replaceAll( 'CH(57)','\n');
    input = input.replaceAll( 'CH(51)','&quot;');
    input = input.replaceAll( 'CH(70)','\\');
    input = input.replaceAll( 'CH(52)','&#39;');
    input = input.replaceAll( 'CH(54)','\t');
    input = input.replaceAll( "CH(101)","&#37;"); 
    input = input.replaceAll( "CHR(26)","&#58;"); 
    input = input.replaceAll( "CH(70)","&#92;");
    input = input.replaceAll( "chr(55)","&#32;");
    
   
    return input;
 }
		
		
		function commonFunction(){
		    		    
			var domainName = $("#BusinessCard1 input#domain").val().trim();
			var authKey = $("#BusinessCard1 input#authKey").val().trim();
			var act = $("#BusinessCard1 input#act").val().trim();
			var loginName = $("#BusinessCard1 input#loginName").val().trim();
			
			
			var projectName = $("#BusinessCard1 select#projName option:selected").text().trim();
			/* var epicName = $("#BusinessCard1 select#epicName option:selected").text().trim();
			*/
			
			//var projectName = $("#BusinessCard1 input#projName").val().trim();
			var epicName = $("#BusinessCard1 input#epicName").val().trim();
			
			var epicId = $("#BusinessCard1 input#epicId").val().trim();
			var featureName = $("#BusinessCard1 input#featureName").val().trim();
			var featureId = $("#BusinessCard1 input#featureId").val().trim();
			var comment = $("#BusinessCard1 textarea#comment").val().trim();
			
			var sprintgroup = $("#BusinessCard1 input#sprintgroup").val().trim();
			var sprint = $("#BusinessCard1 input#sprint").val().trim();
			var startDate = $("#BusinessCard1 input#startDate").val().trim();
			var endDate = $("#BusinessCard1 input#endDate").val().trim();
			var rc_number = $("#BusinessCard1 input#rc_num").val().trim();
			var orderNew = $("#BusinessCard1 textarea#orderNew").val().trim();
			$("#BusinessCard1 input#formEmail").val(loginName);
			var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&projName="+projectName+"&epicName="+epicName+"&epicId="+epicId+"&featureName="+featureName+
					"&featureId="+featureId+"&storyName="+orderNew+"&customId="+rc_number+"&comment="+comment+
					"&sprintGroupName="+sprintgroup+"&sprintGroupType=active"+
					"&sprintName="+sprint+"&sprintStartDate="+startDate+"&sprintEndDate="+endDate+"";
			$("#BusinessCard1 textarea#api").val(UI);
		}
		
		
		
				
		function commonFunctionForWorkspace(){
		    		    
			var domainName = $("#BusinessCard3 input#domain").val().trim();
			var authKey = $("#BusinessCard3  input#authKey").val().trim();
			var act = $("#BusinessCard3  input#act").val().trim();
			var loginName = $("#BusinessCard3  input#loginName").val().trim();
			var projectName = $("#BusinessCard3  input#projName").val();
			var projectCode = $("#BusinessCard3  input#projCode").val();
			var projectDesc = $("#BusinessCard3  textarea#projDesc").val();
			var projectType = $("#BusinessCard3  select#projType option:selected").attr("value");
			
			//https://newtest.colabus.com/AppAuth?act=projectCreation&userId=951&companyId=2&projectName=ForApi&projectHashTag=#forapi&projectDescription=meantforurls&userIds=601&projectType=public
			var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&projectName="+projectName+"&projectHashTag="+projectCode+"&projectDescription="+projectDesc+"&projectType="+projectType+"";
			$("#BusinessCard3  textarea#projApi").val(UI);
		}
		
	function commonFunctionForMessage(){
		var domainName = $("#BusinessCard10 input#domain").val().trim();
		var authKey = $("#BusinessCard10  input#authKey").val().trim();
		var act = $("#BusinessCard10  input#act").val().trim();
		var loginName = $("#BusinessCard10  input#loginName").val().trim();
		var groupName = $("#BusinessCard10  input#groupName").val();
		var Message = $("#BusinessCard10  textarea#Message").val();
		var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&groupName="+groupName+"&message="+Message+"";
		
	
		$("#BusinessCard10  textarea#messageApi").val(UI);
	}
	function commonFunctionForConversations(){
		
		var domainName = $("#BusinessCard8 input#domain").val().trim();
		var authKey = $("#BusinessCard8  input#authKey").val().trim();
		var act = $("#BusinessCard8  input#act").val().trim();
		var loginName = $("#BusinessCard8  input#loginName").val().trim();
		
		var projName = $("#BusinessCard8 select#projName option:selected").text().trim();
		//var projName = $("#BusinessCard5  input#projName").val().trim();
		var feedId = $("#BusinessCard8  input#feedId").val().trim();
		var menuType = $("#BusinessCard8  select#menuType option:selected").val().trim();
		if(menuType=="activityFeed"){
			$(".CustId").css({'display':'none'});
			$(".IdeaName").css({'display':'none'});
			$(".EpicName").css({'display':'none'});
		}else if(menuType=="wsIdea"){
			$(".CustId").css({'display':'block'});
			$(".IdeaName").css({'display':'block'});
			$(".EpicName").css({'display':'none'});
		}else if(menuType=="agile"){
			$(".CustId").css({'display':'block'});
			$(".EpicName").css({'display':'block'});
			$(".IdeaName").css({'display':'none'});
		}else{
			$(".CustId").css({'display':'none'});
			$(".IdeaName").css({'display':'none'});
			$(".EpicName").css({'display':'none'});
		}
		
		
		
		//https://newtest.colabus.com/AppAuth?act=projectCreation&userId=951&companyId=2&projectName=ForApi&projectHashTag=#forapi&projectDescription=meantforurls&userIds=601&projectType=public
		//var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&projectName="+projName+"&feedId="+feedId+"&menuType="+menuType+"&comments="+comments+"";
		if(menuType=="activityFeed"){
			var comments = $("#BusinessCard8  textarea#comments").val().trim();
			var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&projectName="+projName+"&feedId="+feedId+"&menuType="+menuType+"&comments="+comments+"";
		}else if(menuType=="wsIdea"){
			var CustId = $("#BusinessCard8  input#custId").val().trim();
			var IdeaName = $("#BusinessCard8  input#ideaName").val().trim();
			var comments = $("#BusinessCard8  textarea#comments").val().trim();
			var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&projectName="+projName+"&feedId="+feedId+"&menuType="+menuType+"&CustId="+CustId+"&ideaName="+IdeaName+"&comments="+comments+"";
		}else if(menuType=="agile"){
			var CustId = $("#BusinessCard8  input#custId").val().trim();
			var EpicName = $("#BusinessCard8  input#epicName").val().trim();
			var comments = $("#BusinessCard8  textarea#comments").val().trim();
			var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&projectName="+projName+"&feedId="+feedId+"&menuType="+menuType+"&CustId="+CustId+"&epicName="+EpicName+"&comments="+comments+"";
		}
		$("#BusinessCard8  textarea#convApi").val(UI);
	
	}
		
	function commonFunctionForTask(){
		
		var domainName = $("#BusinessCard9 input#domain").val().trim();
		var authKey = $("#BusinessCard9  input#authKey").val().trim();
		var act = $("#BusinessCard9  input#act").val().trim();
		var loginName = $("#BusinessCard9  input#loginName").val().trim();
		var projName = $("#BusinessCard9 select#projName option:selected").text().trim();
		var taskStartDate = $("#BusinessCard9  input#taskStartDate").val().trim();
		var taskEndDate = $("#BusinessCard9  input#taskEndDate").val().trim();
		var title = $("#BusinessCard9  input#title").val().trim();
		var participants = $("#BusinessCard9  input#participants").val().trim();
		var taskType = $("#BusinessCard9 select#taskType option:selected").val().trim();
		if(taskType == "Document" && taskType == "WorkspaceDocument"){
			$(".folderName").css({'display':'block'});
			var folderName = $("#BusinessCard9  input#folderName").val().trim();
		}else{
			$(".folderName").css({'display':'none'});		    
		}
		
		var sourceName = $("#BusinessCard9  input#sourceName").val().trim();
		var sourceCustId = $("#BusinessCard9  input#sourceCustId").val().trim();
		var taskStatusApi = $("#BusinessCard9 select#taskStatusApi option:selected").val().trim();
		var estimatedHour = $("#BusinessCard9  input#estimatedHour").val().trim();
		var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&projectName="+projName+"&start="+taskStartDate+"&end="+taskEndDate+"&title="+title+"&selectedTaskUserEmailIds="+participants+"&taskType="+taskType+"&folderName="+folderName+"&sourceName="+sourceName+"&sourceCustId="+sourceCustId+"&taskStatusApi="+taskStatusApi+"&estimatedHour="+estimatedHour+""
		
		$("#BusinessCard9  textarea#taskApi").val(UI);
	}
	function autofillProjectTagName(obj){
        var projNameVal = $(obj).val();
	    var res;
        if (!projNameVal) {
           return false;
	    } else {
	        res = projNameVal.toLowerCase();
	        res = res.replace(/[^a-z0-9]|\s+|\r?\n|\r/gmi, " ");
	        res = res.replace(/[\W_]+/g,"");
	    }
	    $("#BusinessCard3  input#projCode").val(res);
    }	
	
   function enterprojectTagName(obj){
        var projTagNameVal=$(obj).val();
        var tagres;
        if (!projTagNameVal) {
           return false;
	    } else {
	        tagres = projTagNameVal.toLowerCase();
	        tagres = tagres.replace(/[^a-z0-9]|\s+|\r?\n|\r/gmi, " ");
	        tagres = tagres.replace(/[\W_]+/g,"");
	    }
	    $("#BusinessCard3  input#projCode").val(tagres);
    }
   
   function saveWrkSpaceProject() {
		
	    var api = $("#BusinessCard3  textarea#projApi").val();
	    
	     $.ajax({
		        url: path+"/Business_card",
		        type:"POST",
	 			dataType:'json',
	 			data:{act:"createProject",api:api},//---local 435//-- newtest 440
	 			success:function(result){
			      var result = eval(result);
			      if(result[0].response=='success'){
			        alert(getValues(companyAlerts,"Alert_PrjCreated"));
			      }else{
			        alert(result[0].reason);
			      }
			      
				}
		   });
	    
	} 
   
      function saveUserData() {
		
	    var api = $("#BusinessCard4  textarea#createUserApi").val();
	    
	     $.ajax({
		        url: path+"/Business_card",
		        type:"POST",
	 			dataType:'json',
	 			data:{act:"createUser",api:api},//---local 435//-- newtest 440
	 			success:function(result){
			      var result = eval(result);
			      if(result[0].response=='success'){
			        alert(getValues(companyAlerts,"Alert_UsrCreatedSuccess"));
			      }else{
			        alert(result[0].reason);
			      }
			      
				}
		   });
	    
	} 
   		
		function commonFunctionForCreateUser(){
		    		    
			var domainName = $("#BusinessCard4 input#domain").val().trim();
			var authKey = $("#BusinessCard4  input#authKey").val().trim();
			var act = $("#BusinessCard4  input#act").val().trim();
			var loginName = $("#BusinessCard4  input#loginName").val().trim();
			
			var firstName = $("#BusinessCard4  input#firstName").val();
			var lastName = $("#BusinessCard4  input#lastName").val();
			var email = $("#BusinessCard4  input#email").val();
			var userId = $("#BusinessCard4  input#userId").val();
			var role =  $("#BusinessCard4  select#role option:selected").attr("value");
			var permission =  $("#BusinessCard4  select#permission option:selected").attr("value");
			var privilege =  $("#BusinessCard4  select#privilege option:selected").attr("value");
			var inviteMessage = $("#BusinessCard4  textarea#inviteMessage").val();
			
			//https://newtest.colabus.com/AppAuth?act=projectCreation&userId=951&companyId=2&projectName=ForApi&projectHashTag=#forapi&projectDescription=meantforurls&userIds=601&projectType=public
			var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&firstName="+firstName+"&lastName="+lastName+"&email="+email+"&userId="+userId+"&role="+role+"&permission="+permission+"&privilege="+privilege+"&inviteMessage="+inviteMessage+"";
			$("#BusinessCard4  textarea#createUserApi").val(UI);
		}
		
  
   function fillData(obj){
	 $("#BusinessCard4 input#userId").val($(obj).val());
   }
  
  function autoFillTask(obj){
	$("#BusinessCard6 input#taskName").val($(obj).val());
  }
  
	function commonFunctionForAssociateUser(){
	    		    
		var domainName = $("#BusinessCard5 input#domain").val().trim();
		var authKey = $("#BusinessCard5  input#authKey").val().trim();
		var act = $("#BusinessCard5  input#act").val().trim();
		var loginName = $("#BusinessCard5  input#loginName").val().trim();
		
		var projName = $("#BusinessCard5 select#projName option:selected").text().trim();
		//var projName = $("#BusinessCard5  input#projName").val().trim();
		var projAdmins = $("#BusinessCard5  input#projAdmins").val();
		var teamMembers = $("#BusinessCard5  input#teamMembers").val();
		var observers = $("#BusinessCard5  input#observers").val();
		
		//https://newtest.colabus.com/AppAuth?act=projectCreation&userId=951&companyId=2&projectName=ForApi&projectHashTag=#forapi&projectDescription=meantforurls&userIds=601&projectType=public
		var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&projName="+projName+"&projAdmins="+projAdmins+"&teamMembers="+teamMembers+"&observers="+observers+"";
		$("#BusinessCard5  textarea#associateUserApi").val(UI);
	}
		
      function associateUserData() {
		
	    var api = $("#BusinessCard5  textarea#associateUserApi").val();
	    
	     $.ajax({
		        url: path+"/Business_card",
		        type:"POST",
	 			dataType:'json',
	 			data:{act:"associateUser",api:api},//---local 435//-- newtest 440
	 			success:function(result){
			      var result = eval(result);
			      if(result[0].response=='success'){
			        alert(getValues(companyAlerts,"Alert_UsrAsso"));
			      }else{
			        alert(result[0].reason);
			      }
			      
				}
		   });
	    
	} 
  
  		
		function commonFunctionForAgileAPI(){
		    		    
			var domainName = $("#BusinessCard6 input#domain").val().trim();
			var authKey = $("#BusinessCard6 input#authKey").val().trim();
			var act = $("#BusinessCard6 input#act").val().trim();
			var loginName = $("#BusinessCard6 input#loginName").val().trim();
			
			/*
			var projectName = $("#BusinessCard6 select#projName option:selected").text().trim();
			var epicName = $("#BusinessCard6 select#epicName option:selected").text().trim();
			*/
			
			var projectName = $("#BusinessCard6 select#projName").val().trim();
			//var epicName = $("#BusinessCard6 select#epicName").val().trim();
			
			var epicId = $("#BusinessCard6 input#epicId").val().trim();
			var featureName = $("#BusinessCard6 input#featureName").val().trim();
			var featureId = $("#BusinessCard6 input#featureId").val().trim();
			var comment = $("#BusinessCard6 textarea#comment").val().trim();
			
			var sprintgroup = $("#BusinessCard6 input#sprintgroup").val().trim();
			var sprint = $("#BusinessCard6 input#sprint").val().trim();
			var startDate = $("#BusinessCard6 input#startDate").val().trim();
			var endDate = $("#BusinessCard6 input#endDate").val().trim();
			var rc_number = $("#BusinessCard6 input#rc_num").val().trim();
			var orderNew = $("#BusinessCard6 textarea#orderNew").val().trim();
			var taskName = $("#BusinessCard6 input#taskName").val().trim();
			var instructions = $("#BusinessCard6 textarea#instructions").val().trim();
			var participants = $("#BusinessCard6 input#participants").val().trim();
			var taskStartDate = $("#BusinessCard6 input#taskStartDate").val().trim();
			var taskEndDate = $("#BusinessCard6 input#taskEndDate").val().trim();
			
			
			$("#BusinessCard6 input#formEmail").val(loginName);
			var UI = "https://"+domainName+"/cAuth?authKey="+authKey+"&act="+act+"&loginName="+loginName+"&projName="+projectName+"&epicName="+epicName+"&epicId="+epicId+"&featureName="+featureName+
					"&featureId="+featureId+"&storyName="+orderNew+"&customId="+rc_number+"&comment="+comment+
					"&sprintGroupName="+sprintgroup+"&sprintGroupType=active"+
					"&sprintName="+sprint+"&sprintStartDate="+startDate+"&sprintEndDate="+endDate+
					"&taskName="+taskName+"&instructions="+instructions+"&participants="+participants+"&taskStartDate="+taskStartDate+"&taskEndDate="+taskEndDate+"";
			$("#BusinessCard6 textarea#agilapi").val(UI);
		}
   
    
      		
		function commonFunctionForAuthKeyAPI(){
		    		    
			var domainName = $("#BusinessCard7 input#domain").val().trim();
			var act = $("#BusinessCard7 input#act").val().trim();
			var companyId = $("#BusinessCard7 input#companyId").val().trim();
			var systemAdminId = $("#BusinessCard7 input#systemAdminId").val().trim();
			
			var UI = "https://"+domainName+"/cAuth?act="+act+"&companyId="+companyId+"&systemAdminId="+systemAdminId+"";
			$("#BusinessCard7 textarea#api").val(UI);
		}
   
    
    
    		
    function sendAgileAPIData() {
		
	    var api = $("#BusinessCard6 textarea#api").val();
	    
	     $.ajax({
		        url: path+"/Business_card",
		        type:"POST",
	 			dataType:'json',
	 			data:{act:"createStoryTask",api:api},//---local 435//-- newtest 440
	 			success:function(result){
			      var result = eval(result);
			      if(result[0].response=='success'){
			        alert(getValues(companyAlerts,"Alert_UsrAsso"));
			      }else{
			        alert(result[0].reason);
			      }
			      
				}
		   });
	    
	} 
  
		
/*		
		function callUploadDocumentFunctionality(result){
			var jsonData = JSON.parse(result); 
			var url ="http://newtest.colabus.com/AppAuth?act=agileDocumentUpload&action=agileStory&place=bizSolutions&storyName="+storyName+"&userId="+userId+"&storyId="+storyId+"";
			//var url ="http://localhost:8080/colabusApp/AppAuth?act=agileDocumentUpload&action=agileStory&place=bizSolutions&storyName="+storyName+"&userId="+userId+"&storyId="+storyId+"";
			if(jsonData){
  				var json = eval(jsonData);
		       	if(jsonData.length>0){
					for(var i=0;i<jsonData.length;i++){
						if(jsonData[i].response == 'success'){
							var answer = confirm('Would you like to attach the document to the created story');
							if (answer) {
								window.open(url);
							} else {
							   //
							}
						}else {
							alert(jsonData[i].response);
						  }
						}
					}
			} 	
			
		}
	*/	
	
	
	//-------------------- document upload functions added by DJ -----------------> 
	function uploadOfDocuments(){
	 alert(getValues(companyAlerts,"Alert_CreteStory"));
	}	
	
	function callUploadDocumentFunctionality(result){
			var jsonData = JSON.parse(result); 
			
			if(jsonData){
  				var json = eval(jsonData);
		       	if(jsonData.length>0){
					for(var i=0;i<jsonData.length;i++){
						if(jsonData[i].response == 'success'){
							var answer = confirm('Would you like to attach the document to the created story');
							if (answer) {
							    $('#myModal2').find('#uploadEpicId').val(storyId);
							    $('#myModal2').find('#uploadUserId').val(userId);
							    $('#myModal2').modal('show'); 
							} else {
							   //
							}
						}else {
							alert(jsonData[i].reason);
						  }
						}
					}
			} 
		}
		
   function TaskDocUpload(){
     	$('#transparentDiv').show();
        $('#WsUploadOptnsTask').slideToggle("slow");
   }	
   
   function submitAgileRepositoryFileForm(e){
		var formname = e.form.name;
		var uploadForm = document.getElementById(formname);
		uploadForm.target = 'upload_agile_target';
		var browserName  = navigator.appName;
		//uploadForm.action = 'http://localhost:8080/colabusApp/AgileRepositoryFileUpload?uploadPlace=BC&uploadUserId='+$('#uploadUserId').val()+'';
		uploadForm.action = 'https://newtest.colabus.com/AgileRepositoryFileUpload?uploadPlace=BC&uploadUserId='+$('#uploadUserId').val()+'';
		
			if(browserName == 'Microsoft Internet Explorer'){
				$('#detectBrowser').val('ie');
				uploadForm.submit();
			}
			else{
				var fileSize = e.files[0].size;
				$('#size').val(fileSize);
	    		if(fileSize >= 10000000) {
	    		    alertFun(getValues(companyAlerts,"Alert_filesizeLimit"),'warning');
	    		 }else{
	    			uploadForm.submit();
	    		}
			}
		 		
	}
	
	function uploadedAgileDoc(){
        epicId = $('#uploadEpicId').val();
        $.ajax({
				url: path+"/Business_card",
		        type:"POST",
	 			dataType:'json',
	 			data:{act:"listStoryDocs",ideaId:epicId},
	 			success:function(result){
					//$('#epicListCommonDiv').html(prepareStoryDocUIHeader());
					result = JSON.stringify(result);
					$('#epicListCommonDiv').html(prepareStoryDocUI(jQuery.parseJSON(result)));
				}
		  }); 
   }	

	

 function prepareStoryDocUI(data){
    
    var taskUI='';
    var imgCss="";
   if(data){
      var json = eval(data);
         var j = 1;
	     for(var i=0; i<json.length; i++){
	        j++;
	        //imgCss="";
			//if(userId != json[i].createdById){
			//	imgCss="display:none;";
			//}
	        
	        if(json[i].type=='Document'){
	           taskUI+="<div id=\"agileDoc_"+json[i].id+"\" class=\"assignedTaskPageClass docListViewRowCls\" >";  
			   taskUI+="  <div class=\"docListViewBorderCls\" style=\"padding-top: 8px;font-size:12px;margin:0px;\">";
			   taskUI+="      <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:30%;margin-top: 6px;float:left;\" ><input type=\"text\" class='defaultExceedCls' title=\""+json[i].title+"\" name=\"titleDoc_"+json[i].id+"\" id=\"titleDoc_"+json[i].id+"\" value=\""+json[i].title+"\" style=\"border:0px;background:transparent;width:95%;\" readOnly=\"readOnly\" /></div>";
	           taskUI+="      <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:35%;margin-top: 6px;float:left;\" >";
	           taskUI+="         <img src=\""+path+"/images/repository/"+json[i].documentImg+"\" style=\"cursor: pointer;height:28px;width:28px;float:left;max-width:15%;\" onclick=\"openViewDownloadPopup("+json[i].id+");\" />";
	           taskUI+="         <span class=\"docName defaultExceedCls\" id=\"spanDoc_"+j+"\" onclick=\"openViewDownloadPopup("+json[i].id+");\"  style='float: left;height: 18px;max-width: 70%;overflow: hidden;cursor:pointer;margin-top:4px;' title=\""+json[i].Name+"\">"+json[i].Name+"</span>";
	           taskUI+="         <input type=\"hidden\" name=\"attachDocId_"+j+"\" id=\"attachDocId_"+j+"\" value=\""+json[i].id+"\" /><input type=\"hidden\" name=\"repoDocId_"+j+"\" id=\"repoDocId_"+j+"\" value=\""+json[i].docid+"\" />";
	           
	           taskUI+="         <div class=\"hideOptionDiv docViewRdownload\" id=\"epicDownloadOption_"+json[i].id+"\" style=\"padding-left: 1px; margin-top: -12px;display:none;\">";
			   taskUI+="            <div style=\"float: left; margin-top: 10px; margin-left: -19px;\"><img src=\""+path+"/images/repository/arrowleft.png\"></div>";
			   taskUI+="            <div class=\"optionList\" style=\"float:left;margin-left: 7px; width: 85%;height: 28px;border-bottom: 1px solid #C1C5C8;\">";
			   taskUI+="                <a class=\"\" onclick=\"viewAgileDoc('"+json[i].docid+"','"+json[i].documentLocation+"','"+json[i].documentType+"');\" onmouseover=\"onMouseOverDiv(this)\" onmouseout=\"onMouseOutDiv(this)\"><div class=\"OptionsFont View_cLabelHtml\" style=\"margin-top: 4px;\">View</div></a> </div>";
			   taskUI+="            <div style=\"float: left;border-bottom:0px;margin-left: 7px;\" onmouseout=\"onMouseOutDiv(this)\" onmouseover=\"onMouseOverDiv(this)\" onclick=\"downloadAgileFile("+json[i].docid+",'"+json[i].documentLocation+"')\" class=\"optionList\"><div class=\"OptionsFont Download_cLabelHtml\">Download</div></div>";
			   taskUI+="         </div> ";
	           
	           
	           taskUI+="      </div>";
	           taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 15%;margin-top:6px;\" vaign=\"middle\"><span class='textExceedCss' title=\""+json[i].createdDate+"\">"+json[i].createdDate+"</span></div>";
			   taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 15%;margin-top:6px;\" vaign=\"middle\"><span class='textExceedCss' title=\""+json[i].createdBy+"\">"+json[i].createdBy+"</span></div>";
			   taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 5%;margin-top:6px;\" vaign=\"middle\">";
			   taskUI+="          <img class='sbRemoveIcons Delete_cLabelTitle'  src=\""+path+"/images/close.png\" width=\"11\" height=\"11\" style=\"cursor:pointer;margin-left:12px;"+imgCss+"\" onClick=\"delteDocLink('"+json[i].id+"',"+json[i].epicId+");\"/>";
			   taskUI+="      </div>";			
			   taskUI+="  </div>";			
			   taskUI+="</div>";
	        }else{
	           taskUI+="<div id=\"epicLink_"+json[i].id+"\" class=\"assignedTaskPageClass docListViewRowCls\" >";  
			   taskUI+="  <div class=\"docListViewBorderCls\" style=\"padding-top: 8px;font-size:12px;margin:0px;\">";
			   taskUI+="      <div id=\"titleTd_"+json[i].id+"\" class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:30%;margin-top: 6px;float:left;\" ><input type=\"text\" class='defaultExceedCls' title=\""+json[i].title+"\" name=\"title_"+json[i].id+"\" id=\"title_"+json[i].id+"\" value=\""+json[i].title+"\" style=\"border:0px;background:transparent;width:95%;\" readOnly=\"readOnly\" /></div>";
	           taskUI+="      <div id=\"linkTd_"+j+"\"  class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:35%;margin-top: 6px;float:left;\" >";
	           taskUI+="         <span class=\"docLink defaultExceedCls\" id=\"spanLinkIdea_"+json[i].id+"\" style='float: left;height: 18px;max-width: 80%;overflow: hidden;' onclick=\"openLink4Project(this ); \" title='"+json[i].Name+"'>"+json[i].Name+"</span>";
	           taskUI+="         <input type=\"text\" name=\"spanLink_"+json[i].id+"\" id=\"spanLink_"+json[i].id+"\" value=\""+json[i].Name+"\" style=\"display:none;border:0px;background:transparent;width:83%;float:left;\"  readOnly=\"readOnly\"/>";
	           taskUI+="      </div>";
	           taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 15%;margin-top:6px;\" vaign=\"middle\"><span class='textExceedCss' title=\""+json[i].createdDate+"\">"+json[i].createdDate+"</span></div>";
			   taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 15%;margin-top:6px;\" vaign=\"middle\"><span class='textExceedCss' title=\""+json[i].createdBy+"\">"+json[i].createdBy+"</span></div>";
			   taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 5%;margin-top:6px;\" vaign=\"middle\">";
			   taskUI+="          <img src=\""+path+"/images/close.png\" class='sbRemoveIcons Delete_cLabelTitle' width=\"11\" height=\"11\" style=\"cursor:pointer;margin-left:12px;"+imgCss+"\" onClick=\"funAttachLinkDelete('"+json[i].id+"',"+json[i].epicId+");\"/>";
			   taskUI+="      </div>";			
			   taskUI+="  </div>";			
			   taskUI+="</div>";
			
	        }
		}
     
   }else{
      taskUI+="<span>No data found.</span>";
   }
   
   return taskUI;
 }	
 
 function delteDocLink(epicDocId,epicId){
        var f = confirm("Do you want to delete ?");
        if(f){
            var uploadUserId = $('#uploadUserId').val();
            $.ajax({
			   	url: path + "/Business_card",
				type:"POST",
				data:{act:"deleteEpicDoc",uploadUserId:uploadUserId,epicDocId:epicDocId},
				success:function(result){
		              $("div#epicListCommonDiv").find("#agileDoc_"+epicDocId).remove();
                      
                   }
			});
 		}			
   }
   
   function updateStoryDocs(){
        var docId="";
           $("div#epicListCommonDiv").find("div[id^=agileDoc_]").each(function(){
              docId = docId + $(this).attr('id').split('_')[1]+',';
           });
        var linkId = "";
           $("div#epicListCommonDiv").find("div[id^=epicLink_]").each(function(){
              linkId = linkId + $(this).attr('id').split('_')[1]+',';
           });
          
          if(docId!=''){
            docId = docId.substring(0,docId.length-1);
          } 
          if(linkId!=''){
            linkId = linkId.substring(0,linkId.length-1);
          } 
           
        $.ajax({
				url: path+"/Business_card",
		        type:"POST",
	 			dataType:'text',
	 			data:{act:"updateStoryDocsForBC",linkId:linkId,docId:docId},
	 			success:function(result){
	 			    if(result=='success'){
	 			       alert(getValues(companyAlerts,"Alert_UpSuccess"));
	 			       $('#myModal2').modal('hide'); 
	 			    }else{
	 			       alert(getValues(companyAlerts,"Alert_fail"));
	 			    }
				}
		  }); 
   }
 
  function funAttachLinkDelete(linkId,epicId){
		
		var f = confirm("Do you want to delete ?");
        if(f){
            var uploadUserId = $('#uploadUserId').val();
            $.ajax({
			   	url: path + "/Business_card",
				type:"POST",
				data:{act:"deleteEpicLink",uploadUserId:uploadUserId,linkId:linkId},
				success:function(result){
		              $("div#epicListCommonDiv").find("#epicLink_"+linkId).remove();
                   }
			});
 		}	
		
 }		
		

 	function cancelAttachTaskLevelLink(){
        $('#LinkZone').hide();
 	    if($("[id^=taskLink_]").length < 0){
 		  $("[id^=docImgDiv_]").hide();
 	    }
    	$('#storylevelTitle').val('');
		$('#storyLevelLink').val('');
		$('#myModal2').find('.modal-body').css('height','67vh');
    }
 
	 function attachTaskLevelLink(){
        var idd=$("#uploadDocTaskId").val();	
		$("#WsUploadOptnsTask").slideToggle("slow");
		$('input#storylevelTitle,input#storyLevelLink').val('');
        $('#myModal2').find('.modal-body').css('height','55vh');
        $('#LinkZone').css('display','block');
     	
   }	
   
   function isValidURL(url){
	    var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	    if(RegExp.test(url)){
	        return true;
	    }else{
	        return false;
	    }
	}
	
	function AttachAgileLink(){
		if( $("#storyLevelLink").val().trim() == '' ){
			//alertFun(getValues(companyAlerts,"Alert_enterLink"),'warning');
			alert(getValues(companyAlerts,"Alert_enterLink"));
			$("#storyLevelLink").focus();
			return false;
		}
		var key = $("#storyLevelLink").val().trim();
		if (!isValidURL(key)) {
	       //alertFun(getValues(companyAlerts,"Alert_invalidUrl"),'warning');
	       alert(getValues(companyAlerts,"Alert_UrlInvalid"));
	       return false;
	    }
		var title=$("#storylevelTitle").val().trim();
		
		var storyId = $("input#uploadEpicId").val().trim();
		var userId = $("input#uploadUserId").val().trim();
	    
		if(title==""){
		   	title="";
	      }
	    $.ajax({
				url: path+"/Business_card",
		        type:"POST",
	 			dataType:'json',
	 			data:{act:"insertEpicLink",ideaId:storyId,link:key,title:title,apiUserName:userId},
       			success:function(result){
       				result = JSON.stringify(result);
		              if(result == '[]'){
		              	alert(getValues(companyAlerts,"Alert_Attached"));
		              }
		              else {
		              	$('#epicListCommonDiv').html(prepareStoryDocUI(jQuery.parseJSON(result)));
		              	$('input#storylevelTitle,input#storyLevelLink').val('');
		              }
		  	   }
         });
   }