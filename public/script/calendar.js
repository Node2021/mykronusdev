   	var Path;
   	var Labels;
   	var UserId;
   	var myTask;
	var assgndTsk;
	var historyTsk;
	var lighttpdpath;
	var userType;
	var view;
	var totalMinute="0";
	var day0;
	var day1;
	var day2;
	var day3;
	var day4;
	var day5;
	var day6;
	var selectTitle="";
	var prevSDate='';
 	var prevEDate='';  
	var tHour = '0';
	var tMinute='0';
	var confirmId ='';
	 var confirmIdmin ='';
	 var flag1='0';
	 var flag1_min='0';
	var resetDateValidate='0';
	var resetDateVar='0';
	var prevDateStartSave='';
	var prevDateEndSave='';
	var prevHourVal='0';
	var prevMinuteVal='0';
	var workingMinutes = '';
	var aMinute = "";
	var totalPrevHour="0";
	var totalPrevMinute="0";
	var resetDateValidate1="1";
	var alertCheckSprint="0";
	var sprintHourVal="";
	var sprintMinuteVal="";
	var textSprintDiv="";
	var textTaskDiv="";
	var updateDates="";
	var setFlowId="";
	var doc_projId=	"";	
	var doc_projName="";
	var moreValue="";
	var selectedFlowImage=0;	
	var tempID=0;
	var wFTaskDateXml="<WFTaskDates>";		/* variable for getWFDateXml method */
	var fullFileName='';
	var flowStatusTab="";
	var taskDependencyId=""; 
	var updateOldDepTaskId="";
	var updateNewDepId="";	
	var updateRemoveDepId="";
	
	function setPath(path,uId){
		Path = path;
		UserId=uId;
	}
	function setLabels(labels){
		Labels=labels
	} 
	function setLightTpdPath(lightTpdpath){
		lighttpdpath=lightTpdpath;
	}
	function setUserRegType(userRegType){
		userType=userRegType;
	}
	//	Added common functionality of calendar and list view.

function DropDown(el) {			/*Custom Dropdown Functionality */
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
}
DropDown.prototype = {
    initEvents : function() {
        var obj = this;
 
        obj.dd.on('click', function(event){
           $(this).toggleClass('active');
           obj.dd.on('mouseleave', function(){
           		$('.wrapper-dropdown-2').removeClass('active');
           		$('.idea-dropdown-5').removeClass('active');
            });
           	return false;
        });
 		obj.opts.on('click',function(){
 			$("#userProject,#wfProject").prop("disabled", true);
 			var opt = $(this);
        	var divId = $(this).closest('div').attr('id');
        	if(divId == 'dd5' || divId == 'dd8') {
				priorityId = opt.find('a').attr('value');
            }
            if(divId == 'dd7') {
            	modelId = opt.find('a').attr('value');
            }
            if(divId == 'docType') {
				projId=opt.find('a').attr('value');
			}
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
            if(divId == 'dd7') {    // To select workflow template in workflow task popup  
            	mid = opt.find('a').attr('value');
            	//alert(mid);
            	$('#taskWFDetails').html("");
            	parent.$("#loadingBar").show();
				parent.timerControl("start");
				cntntVal='';
				projId = opt.find('a').attr('projectId');
				reqProjectId=projId;			
            	$('select#userProject option[id="'+projId+'"],select#wfProject option[id="'+projId+'"]').prop("selected", "selected");
				
				mid = modelId;
				var wid = $('#taskWFDetails').width();
				$.ajax({
		            url: Path+'/calenderAction.do',
		            type: "POST",
		            data: {fileNameId: mid,act:'loadWFTemplateNew',w:wid},
		            error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							},   
		            success: function (data) {
		           parent.checkSessionTimeOut(data);
		            	var wFHtml = data.split('^&*');
		            	//alert(data);
		            	$('#taskWFDetails').html(wFHtml[0]);
		            	//$('#wfShowTask').remove();
		            	//$('#wFTemplateContainer').children('#wfShowTask').remove();
		                //$('#wFTemplateContainer').append(wFHtml[1]);
		                //$('#wFTemplateContainer').append(wFHtml[2]);
		                $('#crumbs ul li a').css({'width':'100px','font-size':'14px','padding-left':'20px'});
		                $('.wFSDatePicker').val(stDate);
		                $('.wFEDatePicker').val(stDate);
		                $('input.hasTimePicker').timepicker({ 'timeFormat': 'H:i' });
		                
	                	cntntVal=0;	
	                	$('#mainTaskDiv_0').find('.contTitle').css('width','0px');
	                	$('.WFTaskActive').css('border','');
						//$('.taskDefinition').css('color','#ffffff');
						//$('#mainTaskDiv_0').css('background-color','rgb(255,255,196)'); 
						//$('#wFContents_0').find('.taskDefinition').css('color','#48494F');	
						$('#userListContainer').html("");
						templateSelectedFlag = true;
						$('#taskWFDetails').find('.taskRows').hide();
						tabActive($('#crumbs ul:first-child li:first-child a'));
						showTabDetails($('#crumbs ul:first-child li:first-child a'));
						
						var isiPad = navigator.userAgent.match(/iPad/i) != null;
            			if(!isiPad)
	                		scrollBar('taskWFDetails');
						var test = document.getElementById("taskWFDetails");
						test.addEventListener("click", selectStep, false);
		                parent.$('#loadingBar').hide();
		                parent.timerControl("");
		            	
		            }
				});
				$('#userListContainer').html("");
				$.ajax({ 
	         	  url: Path+'/calenderAction.do',
		      	  type:"POST",
		       	  data:{act:'fetchParticipants',projId:projId,userId:UserId},
		       	  error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
		          success:function(result){					
		          	parent.checkSessionTimeOut(result);
					$('#userListContainer').html(result);	
					userListResult=result;
		       		var isiPad = navigator.userAgent.match(/iPad/i) != null;
			        if(!isiPad)	{
		       			scrollBar('userListContainer');
		       		}	
		       	  }
		   		});
				
				
			 }
            if(divId == 'dd') {
            	var text = $('#'+divId+ ' span').text();
            	taskCalendar(text);
			}	
			if(divId=='ddList3')
             setSnEdates(opt.attr('id'));
            if(divId=='ddList4'){
             $('div#ddList3').children('ul').find('li').hide();
             $('div#ddList3').children('ul').find('li.sprintLi_'+opt.attr('id')).show();
             $('#taskSprintGroup').val(opt.attr('id'));
             $('div#ddList3').children('span').text(getValues('${calendarLabels}','Select'));
             $('#taskSprint').val("");
           }
        });
    },
    getValue : function() {
        return this.val;
    },
    getIndex : function() {
        return this.index;
    }
}
function initWFTaskImagePopup(tempId){	
			var sId = tempId;
			tempID = tempId;
			customLabels = Labels;
			if(sId!=null && sId!=''){
				//loadWFLevelContent();
				showTemplateDetails('view','Calendar');
            }else{
				parent.alertFun(getValues(customalertData,"Alert_SelectTemplate"),'warning');
				return false;
			}
}

function OpenCalenderListView(){                       /*-- For CalenderListView fuction start--*/
	$('#googleTaskImg').show();
	$('#colabusTaskImg').hide();
	$('#myTaskSection').attr('onClick','showMyTaskList()');
	$('#assignedTaskContainer,#historyContainer').show();
	$('#historyFilter').hide();
	$('#sortInWs').show();
	$('#searchBox').val('');
	$('#selectDropDownDiv').show();
	$('#selectTaskType').show();
	$('#calendarSortDiv').slideUp();
	$('#defaultSort').attr('selected','selected');
	$('#defaultTaskSort').attr('selected','selected');
 	var height = $(window).height();
	var sortDivHeight=$("#headerTopicDiv").height();
	var CalTabDivHeight=$("#CalenderListViewTabDiv").height();
	var CalHeaderDivHeight=$("#historyheaders").height();
	var dataHeight=height-sortDivHeight-CalTabDivHeight-CalHeaderDivHeight-45;
	$("#mytaskdatalist").css('height',dataHeight);
	$("#assignedtaskdatalist").css('height',dataHeight);
	$("#historytaskdatalist").css('height',dataHeight-32);
	$('#mytaskdatalist').css('display','block');
    $("#mainCalender").hide();
    $("#MyTasklistView").show();
    $("#mytaskTab").css('color','#FFFFFF');
    $("#assignedTab").css('color','#A7A7A7');
    $("#historyTab").css('color','#A7A7A7');
    $('#historyTaskImg').css('opacity','0.6');
    $('#assignedTaskImg').css('opacity','0.6');
    $("#breadCrumName").html('&nbsp;> '+getValues(Labels,'MY_TASKS'));
    $("#CalenderListViewTabDiv").css('display','block');
    $("#breadCrumBar").css('height','75px');
    moreValue='myTasks';
    parent.$("#loadingBar").show();
    parent.timerControl("start");
    limit = 0;
    myTaskScroll = true;
    $.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'FetchMyTaskListViewData',limit:0},
		   beforeSend: function (jqXHR, settings) {
		        xhrPool.push(jqXHR);
		    },	
		   error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){
		   parent.checkSessionTimeOut(result);
		   		$("#mytaskdatalist").html(result);		
				$(".taskSub, .taskTypeImg").each(function(){
					var title = $(this).attr('title').replaceAll("CHR(41)"," ");
					$(this).attr('title',title);
				});
				var newIds="";
	            var idArray=taskNotType.split(',');
	            var isiPad = navigator.userAgent.match(/iPad/i) != null;
	            if(!isiPad)
	            	myTaskListScroll("","",searchWord); 
	            else {
	            	$('#mytaskheaders').css('width','100%');
		            $("#mytaskdatalist").scroll(function(){
	     				if($("#mytaskdatalist").scrollTop() >= ($("#MytaskList").height() - $("#mytaskdatalist").height()) && myTaskScroll == true)
	                    {	
	                    	myTaskIpadScroll("","",searchWord);
	                    }
	                });    
	            }if($('[id^=myTask_]').length==0){$('#MytaskList').append('<div id="innerDiv6" align="center" style="color:#ffffff;margin-top:20px;font-size:15px;font-family:icon;">No Task Available</div>');}
	           
	            $('#myTaskImg').css('opacity','1');	
	            parent.$("#loadingBar").hide();
	            parent.timerControl("");
				sortTaskType = 'myTasks';
				$('#searchBox').val('');
				$('#noteSearch').attr('src',Path+'/images/Idea/search.png').css('top','20px');		
			}
     });
 }


/*
var tflag=false;
function viewTask(task_id,obj,divId) {		// Edit Assigned Task 
	update='yes';			
	var id = obj.id;$("#participantMail").css("margin-top","0px");
	myTaskId=task_id;
	parId = divId;
	parent.$('#loadingBar').show();
	parent.timerControl("start");	
	$("#uploadDocTaskId").val(task_id);	
	$("#doctaskType").val(divId);
	if(divId == 'assignedTasks' || divId == 'myTasks' || divId == 'mySprint'){
	$("#moreButton").show();

	   tflag=true;
	   $("#taskDocCreateImg").show();
	   $(".uploadTaskIcon").show();
	   $('#createOrCancel').css('width','333x');
	//   $('#createOrCancel').css('width','200px');
	}else{
		$("#dependencyTask").hide();
	   $("#taskDocCreateImg").hide();
	   $(".uploadTaskIcon").hide();
	 //  $('#createOrCancel').css('width','230px');
	   $('#createOrCancel').css('width','180px');
	}
	
	if(divId == 'assignedTasks' || divId == 'assignedDocument' || divId == 'assignedWorkspaceDocument' || divId == 'assignedWorkFlow' || divId=='assignedWorkflow' || divId == 'assignedEvent' || divId == 'assignedIdea') {
		eId = myTaskId;
		$.ajax({ 
           url: Path+'/calenderAction.do',
       	   type:"POST",
	       data:{act:'fetchAssignedTaskDetails',taskId:task_id, taskType:divId},
	       success:function(result){	
	            parent.checkSessionTimeOut(result);
	            $("#eventPopup").show();
	            
	            $("#taskPopupSection").show();
				$('.transparent').show();
				parent.$("#transparentDiv").show();
				if(height >= 550)
					$('#eventPopup').css('height','550px');
				else {
					$('#eventPopup').css({'width':'850px','height':height+'px'});
					$('.eventPopup').css({'margin-top':'','margin-top':'-'+height/2+'px'});
					scrollBar('eventPopup');
				}
				if(parId != 'assignedWorkflow')	
					$('.wFTaskDateSave').hide();
				$("#unassgntaskDiv").css('display','none');
				$("#assgntaskDiv").css('display','block');
			//	$('.To').css("margin-left","113px");
	       		initCal();
				timePicker();
				$('#deleteTask').show().attr("onClick",'deleteEvent('+eId+',"'+divId+'")');
				$('#dd5 span').text(result.split('^&*')[0]);
	       		$('#eventDescription').val(result.split('^&*')[1]);
	       		$('#eventReminder').val(result.split('^&*')[2]);
	       		$('#remindThrough').val(result.split('^&*')[3]);
	       		var title = result.split('^&*')[4];
	       		var sDate = result.split('^&*')[5];
	       		tmpSDate = sDate;
	       		startDate = sDate;
	       		
	       		var eDate = result.split('^&*')[6];
	       		tmpEDate = eDate;
	       		endDate = eDate;
	       		prevDateStartSave=sDate;
 				prevDateEndSave=eDate;
	       		var sTime = result.split('^&*')[7];
	       		var eTime = result.split('^&*')[8];
	       		var projName = result.split('^&*')[9];
	       		var taskImages= result.split('^&*')[10];
	       		var place = result.split('^&*')[11];
	       		recType=result.split('^&*')[12];
	       		recPatternValue=result.split('^&*')[13];
	       		recEnd=result.split('^&*')[14];
	       		projId=result.split('^&*')[15];	
	       		$("#taskLevelprojectId").val(projId);			
	       		tHour=result.split('^&*')[16];
	       		aHour=result.split('^&*')[17];
	       		statImg=result.split('^&*')[18]; 
	       		var docId=result.split('^&*')[19];
	       		tMinute=result.split('^&*')[20];
	       		aMinute=result.split('^&*')[21];	
	       		 doc_projId=	result.split('^&*')[22];	
	       		 doc_projName=result.split('^&*')[23];	
	       		 var docIconStatus=result.split('^&*')[24];
	       		serverHour=tHour;
	       		prevHourVal=tHour;
	       		prevMinuteVal=tMinute;
	       		$('.actualHour').show();
	       		$('#actualWorkHour').val(aHour);
	       		$('#actualWorkMinute').val(aMinute);
	       		
	       		$("#totalMinute").val(tMinute);
	       		
	       		if(tHour==aHour && aMinute>tMinute){		
	       			$('#actualWorkHour').css('color','#FF0000');
	       			$('#actualWorkMinute').css('color','#FF0000');
	       			}
	       		else if(aHour > tHour ){					
	       			$('#actualWorkHour').css('color','#FF0000');
	       			$('#actualWorkMinute').css('color','#FF0000');
	       			}
	       		else{
	       			$('#actualWorkHour').css('color','#848484');
	       			$('#actualWorkMinute').css('color','#848484');
	       		}
	       		
	       		$('.Create').text(getValues(Labels,'Save'));
	       		$('.Create').attr('title',getValues(Labels,'Save'));
				$('#createEvent').removeClass();
	       		$('.createEvent').attr('onclick','updateAssignedTasks("'+divId+'");');
	       		$('#eventContainer').css('display','block');
				$('#projectUsers').css('display','block');
				$('#actHour').css('display','block');
				$('select#userProject').val(getValues(Labels,'All_Users'));
				//alert(divId);
				if(divId == 'assignedTasks' || divId == 'assignedIdea' || divId == 'assignedEvent'){
				    if(docIconStatus==""){
				       $("#docIconNameLevel").hide();
				       $("#eventName").css("width",'412px');
				    }else{
				       $("#eventName").css("width",'390px');
				       $("#eventName").css("float",'left');
				       $("#docIconNameLevel").show();
				    }
				    
				}
				if(divId == 'assignedDocument' || divId == 'assignedWorkspaceDocument') {
	       		    $("#pariticipateDiv").css('display','none');
		            $('#projectSection').css('display','none');
		            $('#participantEmail').css({'height':'173px','width':'98%'});
		           	$('#prjSection').css('display','none');
	       			$('#taskSection').css('display','block');
	       			$('#tskSection').css('display','block');
	       			$("#docType").css("overflow","");
	       			if(divId == 'assignedDocument'){
	       				$('select#userProject').val(getValues(Labels,'All_Users'));
	       				getProjectUsers(document.getElementById("userProject"));
		       			$("#docDownload").show();
		       			$("#docDownload").attr('onClick','downloadFile("'+docId+'")');
		       			$("#eventName").css('width','390px');
		       			$("#docDownload").attr('title','Download document');
		       			$("#docDownload").css({'margin-top':'11px','margin-left':'2px','cursor':'pointer'});
		       		}
		       		if(divId == 'assignedWorkspaceDocument'){
			       		if(doc_projName != '-' && doc_projName!='') {
							$('select#userProject').val(doc_projName);
							document.getElementById("userProject").value=doc_projName;
							getProjectUsers(document.getElementById("userProject"));
						}	
						if(doc_projName == getValues(Labels,'Select') || doc_projName == ''){
							$('select#userProject').val(getValues(Labels,'All_Users'));
							getProjectUsers(document.getElementById("userProject"));
						}
			       	}
	       			if(projName == '-' || projName == '') {
	       				$('#docType span').text(getValues(Labels,'Select'));
	       				$('select#userProject').val(getValues(Labels,'All_Users'));
	       			}
	       			else{ 	
	       					$('#docType span').text(projName);
	       			}
	       			
	       			var remainder=result.split('^&*')[2];
	       			var remainderType=result.split('^&*')[3];
		       		if(divId == 'assignedWorkspaceDocument' ||divId=="assignedDocument"){
				    	if(remainder=="minutes"){
				    		$('#eventReminder').val("30 minutes");
				    	}
				    	else if(remainder=="hour" || remainder=="Hour"){
				    	   $('#eventReminder').val("1 hour");
				    	}	
				        else if(remainder=="day" || remainder=="Day"){
				    	  $('#eventReminder').val("daily");
				    	}
				    	else if(remainder=="week" || remainder=="Week"){
				    	  $('#eventReminder').val("weekly");
				    	}
				    	else if(remainder=="Month" || remainder=="month"){			
				    	  $('#eventReminder').val("monthly");
				    	}
				    	else if(remainder=="year" || remainder=="Year"){
				    	  $('#eventReminder').val("yearly");
				    	}
				    	if(remainderType=="email" || remainderType=="Email"){
				    	  $('#remindThrough').val("by email");
				    	}	
				    }		
		       	
	       					
	       		}
	       		
	       		else if(divId == 'assignedTasks' || divId == 'assignedIdea' || divId == 'assignedWorkflow' ) {
		       		$("#pariticipateDiv").css('display','none');
			        $("#addParticipantTasks").css('height','173px');
			        $('#participantEmail').css('height','173px');
		        	$('.selectPlace').hide();
					$('#emailDiv').css('display','none');
		       		if(projName != '-' && projName!='') {
						$('select#project').val(projName);
						$('select#userProject').val(projName);
						getProjectUsers(document.getElementById("userProject"));
						document.getElementById("project").value=document.getElementById("userProject").value;
					}	
					if(projName == getValues(Labels,'Select') || projName == ''){
						$('select#project').val(getValues(Labels,'Select'));
						$('select#userProject').val(getValues(Labels,'All_Users'));
						getProjectUsers(document.getElementById("userProject"));
					}
					 
				}	
				else if(divId == 'assignedEvent') {
					$('#userListContainer').html('');$('#userListContainer').append('<div id="loadingB" align="center" style="margin-top:230px;overflow:hidden">loading...</div>');
					$('.estmHour').hide();
					$('#actualHourContentDiv').hide();
					initEventCal();
					$('#emailDiv').show();
			        $('.taskDate').hide();
			        $('.eventDateDiv').show();
			        $('#pariticipateDiv').show();
			        $('#taskHeaderSec').css('height','42px');
			        $('.calInstructions').hide();
			        $('#addParticipantTasks').css('height','176px');
			        $('#participantEmail').css('height','50px');
					$('#pariticipateDiv').css('padding-top','20px');
					$('#taskDetails').css('height','277px');
					$('.selectPlace').show();
					$('#pariticipateDiv').parent('div').css('display','block');
					$('#emailDiv').css('display','block');
					$('#eventPlace').val(place);
					$('#startEventDate').val(sDate);
					$('#endEventDate').val(eDate);
					$('#startEventTime').val(sTime);
					$('#endEventTime').val(eTime);
					$('#titleText').text(getValues(Labels,'Event'));
					$.ajax({ 
			           url: Path+'/calenderAction.do',
			       	   type:"POST",
				       data:{act:'fetchEventUsers',taskId:task_id},
				       success:function(result){
				      	parent.checkSessionTimeOut(result);
				      		$('#participantEmail').html(result);
				      		$('#participantEmailSection').append("<div id=\"eventCommentHeader\" class=\"eventNameLabel\" style=\"width: 73px;float: left;text-align: right;\">"+getValues(Labels,'Comments')+"</div>");
				      		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        				$.ajax({ 
					           url: Path+'/calenderAction.do',
					       	   type:"POST",
						       data:{act:'getEventComments',taskId:task_id},
						       success:function(result){
						      	parent.checkSessionTimeOut(result);
						      		$('#participantEmailSection').append(result);
						      		$('#eventCommentContainer').css({'max-height':'54px','width':'510px','padding-left':'10px'});
				      				$('#eventCommentHeader').css('width','65px');
					        		if(!isiPad)
				      					scrollBar('eventCommentContainer');
				      			}
						    });  		
				       		$('#participantEmail > div').each(function() {
								var id = $(this).attr('id');
								var userId = id.split('_')[1];
								$('#participant_'+userId).css('display','none');
							});
				       		if(!isiPad) {
				       			scrollBar('userListContainer');
								scrollBar('participantEmail');
							}
					if(projName != '-' && projName!='') {
						$('select#project').val(projName);
						$('select#userProject').val(projName);
						document.getElementById("project").value=document.getElementById("userProject").value;
						getProjectUsers(document.getElementById("userProject"));
					}	
					if(projName == getValues(Labels,'Select') || projName == ''){
						$('select#project').val(getValues(Labels,'Select'));
						$('select#userProject').val(getValues(Labels,'All_Users'));
						getProjectUsers(document.getElementById("userProject"));
					}
				       }
					});	
				}
				
				if(divId == 'assignedTasks'){
					$("#dependencyTask").show();
				//	$("#toDate").css("margin-left","24px");
					$('#dependencyTask').attr("onclick","updateListDependencyTask()");
					$('.createDependencyTask').attr("onclick","updateDependencyTaskList()");
				}
	       		$('#eventName').val(title);
				if(!eDate) {
        			eDate = sDate;
        		}
        		$('#titleImgs').attr('src',Path+'/images/calender/'+taskImages+'Black.png');
				$('#titleImgs').attr('title',taskImages);
				$('#headerTitles').text(taskImages);
				if(divId != 'assignedEvent') {
					$('.estmHour').show();
					$('#startDateDiv').val(sDate);
					$('#endDateDiv').val(eDate);
					$('#startTime').val(sTime);
					$('#endTime').val(eTime);
					$('#totalHour').val(tHour);
					$('#totalMinute').val(tMinute);
					$('#pariticipateDiv').parent('div').css('display','none');
					$.ajax({ 
			           url: Path+'/calenderAction.do',
			       	   type:"POST",
				       data:{act:'fetchTaskUsers',taskId:task_id, taskType:divId},
				       success:function(result){
				       parent.checkSessionTimeOut(result);
				       		$('#participantEmail').html(result);
				       		$('.ui-slider-handle.ui-state-default.ui-corner-all').css('background', 'url("images/handleDisable.png") repeat-x scroll 50% 50% rgba(0, 0, 0, 0)');
							$( ".ui-slider-handle" ).on( "mousedown", function(event) {
							   parent.alertFun("You can't update progress of the participants.",'warning');});
							$( ".userCompPerSlider" ).on( "click", function(event) {
							   parent.alertFun("You can't update progress of the participants.",'warning');});
               				if(projId == '0'){
								getAllUsers();//	$('#userListContainer').html('${allUsers}');
						    	$('.odd, .even').each(function() {
									var id = $(this).attr('id');
									var userId = id.split('_')[1];
									$('#participant_'+userId).css('display','none');
								});
								var isiPad = navigator.userAgent.match(/iPad/i) != null;
			       				if(!isiPad) {
									scrollBar('userListContainer');
									scrollBar('participantEmail');
								}
							}else{	
								$("select#userProject").find("option#"+projId).attr("selected", "selected");
								$.ajax({ 
							       url: Path+'/calenderAction.do',
							       type:"POST",
							       data:{act:'fetchParticipants',projId:projId,userId:UserId},
							       success:function(result){
							       		parent.checkSessionTimeOut(result);
							       		$('#userListContainer').html(result);
										$('.odd, .even').each(function() {
											var id = $(this).attr('id');
											var userId = id.split('_')[1];
											$('#participant_'+userId).css('display','none');
										});
										if($('[id^=participant_]:visible').length < 1){
											$('#userListContainer').append('<div id="innerDiv19" align="center"> No Participant</div>');
										}
										var isiPad = navigator.userAgent.match(/iPad/i) != null;
				        				if(!isiPad) {
				        					scrollBar('userListContainer');
											scrollBar('participantEmail');
										}
							       }
								});
							}
			       			$(document).click(function() {     // all dropdowns
								$('.wrapper-dropdown-2').removeClass('active');
							});   }
			       	   });
				}
				if(divId != 'assignedTasks' && divId != 'assignedEvent' && divId!='assignedDocument'){
					$("#userProject,#project").prop("disabled", true);
					
				}	
				if(divId == 'assignedTasks' || divId == 'assignedIdea' || divId == 'assignedEvent'){
							   tflag=true;
							   $("#taskDocCreateImg").show();
							   $(".uploadTaskIcon").show();
							   if(divId == 'assignedTasks'){
							   		$('#createOrCancel').css('width','333px');
							   }
							   else{
							   		$('#createOrCancel').css('width','200px');
							   }
						}else{
						   $("#taskDocCreateImg").hide();
						}
				parent.$('#loadingBar').hide();
				parent.timerControl("");}
		});
	}
	else if(divId == 'myTasks' || divId == 'myDocument' || divId == 'myWorkspaceDocument' || divId == 'myWorkFlow' || divId == 'myWorkflow' || divId == 'myEvent' || divId == 'myEvents' || divId== 'myIdea' || divId == 'mySprint') {
	 $.ajax({ 
	           url: Path+'/calenderAction.do',
	       	   type:"POST",
		       data:{act:'getMyTaskDetails',taskId:myTaskId,parId:divId},
		       success:function(result){
	            parent.checkSessionTimeOut(result);
	         	$("#eventPopup").hide();
	       		$("#taskPopupSection").hide();
       			$('.transparent').show();
       			parent.$("#transparentDiv").show();
       			$('#taskCalendar').append(result);
       			$('.ui-slider-handle.ui-state-default.ui-corner-all').css('background', 'url("images/handleDisable.png") repeat-x scroll 50% 50% rgba(0, 0, 0, 0)');
       			$( ".userCompPerSlider" ).on( "click", function(event) {
							   parent.alertFun("You can't update progress of the participants.",'warning');});
				$( ".userCompPerSlider" ).find(".ui-slider-handle").on( "mousedown", function(event) {
							   parent.alertFun("You can't update progress of the participants.",'warning');});
       			var tAmount = $('#taskAmount').val();
       			if(tAmount < 100)
       				$('#slider-range-min > a.ui-slider-handle').css('background', 'url("scripts/slider/css/ui-lightness/images/handle.png") repeat-x scroll 50% 50% rgba(0, 0, 0, 0)');
       			if(divId == 'myEvent' || divId == 'myEvents')	
       				$(".removeEventUser").hide();	
				if(height >= 550)
					$('.eventPopup').css('height','550px');
				else {
					$('.eventPopup').css({'height': height+'px','margin-top':'','margin-top':'-'+height/2+'px'});
				}
				if(divId == 'mySprint'){
					loadCustomLabel('TaskUI');
					$('.sprintUserContainer').css('width','715px');
					$('.sprintUserContainer').css('margin-left','20px');
					$('.userTitle').css('width','230px');
					$('.slider').css('width','235px');
					$('.completePer').css('width','50px');
					$('.userContainerSlide').css('width','150px');
					$('.taskAmount').css('margin-left','30px');
					$('.userStatus').css('width','155px');
					$('.userWorkingHour').css('text-align','left');
					$('.userComments').css('float','right');
					$('.usrComments').css('font-size','12px');
				}
				var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        	if(divId == 'myEvent' || divId == 'myEvents'){
	        		if(!isiPad){
       					scrollBar('postedComments');
       					scrollBar('eventUserContainer');
       					if(height < 550){
							$('.eventPopup').css('width','660px');
							scrollBar('myTaskPopup');
						}	
       				}$.ajax({ 
					           url: Path+'/calenderAction.do',
					       	   type:"POST",
						       data:{act:'getEventComments',taskId:myTaskId},
						       success:function(result){
						      	parent.checkSessionTimeOut(result);
						      		$('#postedComments').append(result);
						      		$('#eventCommentContainer').css({'max-height':'100px','width':'570px','padding-left':'10px','margin-left':'30px'});
				      				$('#eventCommentHeader').css('width','65px');
				      				$('[id^=commentTextarea_]').prop("disabled",false);
				      				if(!isiPad)
				      					scrollBar('eventCommentContainer');
				      			}
						    });
					//$("#taskDocCreateImg").hide();
					$("#updateOrCancel").css('width','28%');
					$("#updateTask").css('width','39%');
					$("#cancelTask").css('width','37%');
       			}	
       			else {	
	       			taskHour = $('#workingHours').val();
	       			if(!isiPad){
						commentsScrollBar();
						if(height < 550){
							$('.eventPopup').css('width','820px');
							scrollBar('myTaskPopup');
						}	
					}	
				}
				$(".taskSub").each(function(){
                	var title = $(this).attr('title').replaceAll("CHR(41)"," ").replaceAll("CHR(39)","'");
                	$(this).attr('title',title);
				});
				
				if(divId == 'myTasks' || divId == 'mySprint' || divId == 'myIdea' || divId == 'myEvent' || divId == 'myEvents'){
							   tflag=true;
							   $("#taskDocCreateImg").show();
							   $(".uploadTaskIcon").show();
							   $('#createOrCancel').css('width','200px');
						}else{
						   $("#taskDocCreateImg").hide();
						}
				parent.$('#loadingBar').hide();	
				parent.timerControl("");
			}
		});
	}
	else if(divId == 'myTask' || divId == 'myDocumentTask' || divId == 'myWsDocumentTask' || divId == 'myWorkflowTask' || divId == 'myworkflowTask' || divId == 'myEventTask' || divId == 'myIdeaTask' || divId == 'mySprintTasks' || divId == 'mySprintTask' || divId == 'assignedTask' || divId == 'assignedDocumentTask' || divId == 'assignedWsDocumentTask' || divId == 'assignedWorkflowTask' || divId == 'assignedworkflowTask' || divId == 'assignedEventTask' || divId == 'assignedIdeaTask' || divId == 'assignedSprint' || divId == 'assignedSprintTask') {
	     $.ajax({ 
	           url: Path+'/calenderAction.do',
	       	   type:"POST",
		       data:{act:'getHistoryMyTaskDetails',taskId:myTaskId,parId:divId},
		       success:function(result){
		       		parent.checkSessionTimeOut(result);
		            $("#historyeventPopup").hide();
	       			$('.transparent').show();
	       			parent.$("#transparentDiv").show();
	       			$("#taskCalendar").append(result);
	       			$('.ui-slider-handle.ui-state-default.ui-corner-all').css('background', 'url("images/handleDisable.png") repeat-x scroll 50% 50% rgba(0, 0, 0, 0)');
	       			$( ".ui-slider-handle" ).on( "mousedown", function(event) {
							   parent.alertFun("You can't update progress of the participants.",'warning');});
					$( ".userCompPerSlider" ).on( "click", function(event) {
							   parent.alertFun("You can't update progress of the participants.",'warning');});
	       			$("#historymyTaskPopup").show();
	       			isHistory = 'yes';
	       			parent.$('#loadingBar').hide();
	       			parent.timerControl("");
	       			var isiPad = navigator.userAgent.match(/iPad/i) != null;
	       			if(!isiPad){
	       				if(height < 400){
							$('#historymyTaskPopup').css({'width':'650px','height':height+'px'});
							$('.eventPopup').css({'margin-top':'','margin-top':'-'+height/2+'px'});
							scrollBar('historymyTaskPopup');
						}
						if(divId == 'assignedTasks' || divId == 'myTask' || divId == 'mySprint' || divId == 'myIdeaTask'){
							   tflag=true;
							   $("#taskDocCreateImg").show();
							   $(".uploadTaskIcon").show();
							   $('#createOrCancel').css('width','200px');
						}else{
						   $("#taskDocCreateImg").hide();
						}
						if(divId == 'mySprintTasks' || divId == 'mySprintTask'){
							loadCustomLabel('TaskUI');
							$('.sprintUserContainer').css('width','590px');
							$('.sprintUserContainer').css('margin-left','20px');
							$('.userTitle').css('width','165px');
							$('.slider').css('width','242px');
							$('.userCompPerSlider').css('width','170px');
							$('.userWorkingHour').css('width','50px');
							$('.userStatus').css('width','80px');
							$('.userComments').css('float','left');
							$('.usrComments').css('font-size','12px');
							$('.deleteUser').remove();
						}
	       				if(divId == 'myEventTask'){
		       				scrollBar('eventCommentContainer');
		        			scrollBar('eventUserContainer');
		        		}
		        		else
	       				commentsScrollBar();
	       			}
	       			$(".taskSub").each(function(){
	                	var title = $(this).attr('title').replaceAll("CHR(41)"," ").replaceAll("CHR(39)","'").replaceAll("CHR(40)","\"");
	                	$(this).attr('title',title);
					});
				}
	  });
	}
	
	
}   
                       */
                       /* 
 function getProjectUsers(obj){   // Get users of particular project  				
	var text = $(obj).val();			
     	if(text == getValues(Labels,'Select') || text == "" || text == "0") {
   	   	    projId=0;
   	   	  	getAllUsers();// $('#userListContainer').html('${allUsers}');
      	    $('select#userProject').val(getValues(Labels,'All_Users'));
      	    scrollBar('userListContainer');
          if(update == 'yes') {
        	$('.odd, .even, .updatePrtcpnt, .userContainer').each(function() {
				var id = $(this).attr('id');
				var userId = id.split('_')[1];
				$('#participant_'+userId).css('display','none');
			});
          }
          else{
          	$('div.participantDetails').each(function() {
			var id = $(this).attr('id');
			var userId = id.split('_')[1];
			$('#participant_'+userId).css('display','none');
  			});
  		  }	
        }
        else if(text == getValues(Labels,'All_Users')) {
       		projId = 0;
       		var optVal = $(obj).val();
       		$("select#project,select#wfProject").val(optVal);
       		$('#project option:selected').attr('selected', optVal);
	  		getAllUsers();
       		}
        else {	$('#userListContainer').html('');
        		$('#userListContainer').append('<div id="loadingB" align="center" style="margin-top:230px;overflow:hidden">loading...</div>');
				var optVal = $(obj).val();
        		var optId = $(obj).attr('id');
        		$("select#project,select#wfProject").val(optVal);
        		$("select#userProject").val(optVal);
        		projId=$("#"+optId+" option:selected").attr('id');
        	$.ajax({ 
		       url: Path+'/calenderAction.do',
		       type:"POST",
		       data:{act:'fetchParticipants',projId:projId,userId:UserId},
		       success:function(result){
	       		parent.checkSessionTimeOut(result);				
	       		if(parId != 'assignedSprint')
	       			$('#userListContainer,#ideaUsersDiv').html(result);
	       		else {
	       			$('#ideaUsersDiv').html(result);
	       		}	
	       		userListResult = result;
	       		parent.$('#loadingBar').hide();
	       		parent.timerControl("");
	       		if(update == 'yes') {
           			$('.odd, .even, .updatePrtcpnt, .participantDetails, .userContainer').each(function() {
						var id = $(this).attr('id');
						var userId = id.split('_')[1];
						$('#participant_'+userId).css('display','none');
					});if($('[id^=participant_]:visible').length < 1){$('#userListContainer').append('<div id="innerDiv15" align="center"> No Participant</div>');}
           		}
           		else{
            		$('div.participantDetails').each(function() {
						var id = $(this).attr('id');
						var userId = id.split('_')[1];
						$('#participant_'+userId).css('display','none');
	   				}); $('#innerDiv1').hide();
	   				if($('[id^=participant_]:visible').length < 1){
					$('#userListContainer').append('<div id="innerDiv15" align="center"> No Participant</div>');
						} 
					}
           			var isiPad = navigator.userAgent.match(/iPad/i) != null;
          			if(!isiPad) {	
          				scrollBar('userListContainer');
            			scrollBar('ideaUsersDiv');
           			}
               }
	     });
	}
}							*/
	/*
	function getAllUsers() {		//All User Details
			var popH=$("#userContainer").height();
			var mH=popH/2;
					$('#userListContainer').html('');$('#userListContainer').append('<div id=\"loadingB\" align=\"center\" style=\"margin-top:'+mH+'px;overflow:hidden;\">loading...</div>');
		$.ajax({ 
          url: Path+'/calenderAction.do',
      	  type:"POST",
       	  data:{act:'fetchAllUsers'},
          success:function(result){
          	parent.checkSessionTimeOut(result);
          	$('#userListContainer,#ideaUsersDiv').attr('align','left');
          	 $('#userListContainer,#ideaUsersDiv').css('height',popH);
			 $('#userListContainer,#ideaUsersDiv').css('margin-top','0px');
          	$('#userListContainer,#ideaUsersDiv').html(result);
          	userListResult= result;
       		parent.$('#loadingBar').hide();
       		parent.timerControl("");
       		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        if(!isiPad){
	          	scrollBar('userListContainer');     
	          	scrollBar('ideaUsersDiv');
	        }  	
       		if(update == 'yes') {
          		$('.odd, .even, .updatePrtcpnt').each(function() {
					var id = $(this).attr('id');
					var userId = id.split('_')[1];
					$('#participant_'+userId).css('display','none');
				});
            }
            else{
            	$('div.participantDetails').each(function() {
					var id = $(this).attr('id');
					var userId = id.split('_')[1];
					$('#participant_'+userId).css('display','none');
	   			});
	   		}if($('[id^=participant_]:visible').length < 1){$('#userListContainer').append('<div id="innerDiv18" align="center"> No Participant</div>');}
   		}
     });
}

*/
function updateAssignedTasks(type) {		/*Update assigned Tasks */
		contentData="completeData";
		place = '';
		taskType1 = type; 			
		if(type != 'assignedSprint') {
			title = $('#eventName').val();
			$('#eventName').css('border','');
			$('#startDateDiv').css('border','');
			$('#endDateDiv').css('border','');
			$('#startEventDate').css('border','');
			$('#endEventDate').css('border','');
			if((!title || title.match(/^\s*$/))){
				$('#eventName').css("border","1px solid red");
				return null;
			}	
			if(type == 'assignedEvent') {
				place = $('#eventPlace').val();
				start = $('#startEventDate').val();
				end = $('#endEventDate').val();
				startTime = $('#startEventTime').val();
				endTime = $('#endEventTime').val();
/*			
				if(!start) {
					$('#startDateDiv').css("border","1px solid red");
					return null;
				}
				if(!end) {
					$('#endDateDiv').css("border","1px solid red");
					return null;
				}*/
				place = $('#eventPlace').val();      //Not to comment for task incomplete functionality
				
		/*	if(!place || place.match(/^\s*$/)) {
				$('#eventPlace').css('border','1px solid red');
				return null;
			}	*/
				var sdArray=start.split('-');
				var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
				var edArray=end.split('-');
				var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
				var d1=new Date(sd);
				var d2=new Date(ed);
				if(d1 > d2){
				  parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
				  $('#startEventDate').css("border","1px solid red");
				  $('#endEventDate').css("border","1px solid red");
				  return false;
				}
				if(start == end) {
					var st = parseInt(startTime.replace(':', ''), 10); 
		        	var et = parseInt(endTime.replace(':', ''), 10);
					if(st >= et){
		               parent.alertFun(getValues(customalertData,"Alert_ValidEndTime"),'warning');
		               $('#endTime').css("border","1px solid red");
		               return null;
		            }
				}
/*				if($('.participantDetails').length <1 && $('.mCSB_container').find('.otherUserEmail' ).children().length<=0 ){
					parent.alertFun(getValues(customalertData,"Alert_Participant"),'warning');
					return false;	
				}
*/	//	INCOMPLETE TASK FUNCTIONALITY	
			if((!start) || (!end) || (!place) || ( $('.participantDetails').length <1 && $('.mCSB_container').find('.otherUserEmail' ).children().length<=0 ) || ($('#participantEmail').children('div').length<=0) ){
				if(contentData!="incompleteData"){
			     //	contentData="incompleteData";
					parent.confirmFun(getValues(companyAlerts,"Alert_SaveIncompleteData"),"delete","goToUpdateTask");
					return false;	
				}
				
			}
			
			}
			else {
				start = $('#startDateDiv').val();
				end = $('#endDateDiv').val();
				startTime = $('#startTime').val();
				endTime = $('#endTime').val();
/*			if(!start) {
					$('#startDateDiv').css("border","1px solid red");
					return null;
				}
				if(!end) {
					$('#endDateDiv').css("border","1px solid red");
					return null;
				}
*/				var sdArray=start.split('-');
				var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
				var edArray=end.split('-');
				var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
				var d1=new Date(sd);
				var d2=new Date(ed);
				if(d1 > d2){
				  parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
				  $('#startDateDiv').css("border","1px solid red");
				  $('#endDateDiv').css("border","1px solid red");
				  return false;
				}
				if(start == end) {
					var st = parseInt(startTime.replace(':', ''), 10); 
		        	var et = parseInt(endTime.replace(':', ''), 10);
					if(st >= et){
		                parent.alertFun(getValues(customalertData,"Alert_ValidEndTime"),'warning');
		               $('#endTime').css("border","1px solid red");
		               return null;
		            }
				}	
				if($.isNumeric($('#totalHour').val()) == false || $('#totalHour').val()<0){
	         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
	 		       $('#totalHour').css('border','1px solid red');
	 		       return null;
		        } else
		         	$('#totalHour').css('border','1px solid #BFBFBF');
		         	
		        if($.isNumeric($('#totalMinute').val()) == false){
	         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
	 		       $('#totalMinute').css('border','1px solid red');
	 		       return null;
		        } else
		         	$('#totalMinute').css('border','1px solid #BFBFBF');
		        
		        if($('#totalMinute').val()>59 || $('#totalMinute').val()<0){
		   		   parent.alertFun(getValues(customalertData,"Alert_MinValue"),'warning');
	 		       $('#totalMinute').css('border','1px solid red');
	 		       return null;
		   
		  		 } else
		         	$('#totalMinute').css('border','1px solid #BFBFBF');
		        
		        //Adding User is not mandatory (NEW FUNCTIONALITY INCOMPLETE TASK) .
				/*		         	 	
		      if($('#prtcpntSection').children('div:visible').length < 1){
					parent.alertFun(getValues(customalertData,"Alert_Participant"),'warning');
					return false;	
				}*/	
			}
		
			repeat = $('.eventReminder').val();
			description = $('#eventDescription').val();
			reminder = $('#eventReminder option:selected').text();
			remType = $('#remindThrough option:selected').text();
//   Remove the validation for user and date   (NEW FUNCTIONALITY INCOMPLETE TASK)         
			if((!start) || (!end) || ( $('#prtcpntSection').children('div:visible').length < 1 ) ){
				if(contentData!="incompleteData"){
			     	contentData="incompleteData";
					parent.confirmFun(getValues(companyAlerts,"Alert_SaveIncompleteData"),"delete","updateAssignedTask");
					return false;	
				}
				
			}

		}	
		else {
			title = $('#sprintTaskName > #eventName').val();
			$('#sprintTaskName > #eventName').css('border','');
			$('#sprintTaskDate > #startDateSprint').css('border','');
			$('#sprintTaskDate > #endDateSprint').css('border','');
			start = $('#sprintTaskDate > #startDateSprint').val();
			end = $('#sprintTaskDate > #endDateSprint').val();
			startTime = '00:00';
			endTime = '23:59';
			repeat = '';
			place = '';
			description = $('#sprintTaskDesc > #eventDescription').val();
			reminder = $('#ddList1 span').text();
			remType = $('#ddList2 span').text();
			if(!start) {
				$('#startDateSprint').css("border","1px solid red");
				return null;
			}
			if(!end) {
				$('#endDateSprint').css("border","1px solid red");
				return null;
			}
			if($('#ideaTaskParticipantsDiv > #participantEmail div').length == 0) {
				parent.alertFun(getValues(customalertData,"Alert_Participant"),'warning');
				return null;
			}			
		}
		if(type == "assignedDocument" || type == "assignedWorkspaceDocument"){
			var taskTypeName=$("#docType").children("span").html();	
				$("#docType").children().find("a").each(function() {
					  		if ($(this).html() == taskTypeName) {
					    		projId=$(this).attr("value");
					  		}
				});
		}
	//	prevDateStartSave=tmpSDate;
	//	prevDateEndSave=tmpEDate;
		
		if(tmpSDate != start || tmpEDate != end) {		
				updateDates="yes";	
				parent.confirmReset(getValues(customalertData,"Alert_Reset_Date"),'reset','resetAssignedDate','resetDateCancel');
		}else{
			
    		updateAssignedTask();
    	}	
}
function updateAssignedTask(){
		
		if(!startTime) {
			startTime = '00:00';
		}
		if(!endTime) {
			endTime = '23:59';
		}
		var event_id;
		var taskType = taskType1;
		if(taskType == getValues(Labels,'ASSIGNED_TASKS')) {
			taskType='assignedTasks';
			var backgroundColor='#F5CB02';
			var borderColor='#FBAD3D';
		}
		if(!title) {
			 $('#eventName').css("border","1px solid red");
			 return null;
		}
		var textColor = '#FFFFFF';
		var description = $('#eventDescription').val();
		if(!description) {
			description = '';
		}
		var updateStart = start;
		var updateEnd = end;
		if(reminder == getValues(Labels,'Select'))
			reminder = null;
		if(remType == getValues(Labels,'Select'))
			remType = null;
		if(update == 'yes') {				
			updateStart = updateStart.split('-');
        	updateStart = updateStart[2]+'-'+updateStart[0]+'-'+updateStart[1];
        	updateEnd = updateEnd.split('-');
        	updateEnd = updateEnd[2]+'-'+updateEnd[0]+'-'+updateEnd[1];
			$.extend(curr_event, {
  				title: title,
  				start: updateStart+' '+startTime,
  				end: updateEnd+' '+endTime,
  				description:description
			});
			
		}
		if(( parId != 'assignedEvent') && (tMinute!=$('#totalMinute').val() || tHour != $('#totalHour').val()) ){
			if(tHour != $('#totalHour').val() && tMinute!=$('#totalMinute').val()){	
			totalPrevHour=tHour;
			totalPrevMinute=tMinute;	
			updateDates="yes";		
				parent.confirmReset(getValues(customalertData,"Alert_Reset_Data"),'reset','resetDateConfirmHourMinute','resetDateCancel');
			}else if(tHour != $('#totalHour').val()){	
				totalPrevHour=tHour;
				totalPrevMinute=tMinute;
				updateDates="yes";
				parent.confirmReset(getValues(customalertData,"Alert_Reset_Data"),'reset','resetDateConfirm','resetDateCancel');
			}else if(tMinute!=$('#totalMinute').val()){
				totalPrevHour=tHour;
				totalPrevMinute=tMinute;
				updateDates="yes";
				parent.confirmReset(getValues(customalertData,"Alert_Reset_Data"),'reset','resetDateConfirmMin','resetDateCancelMin');
			}
		}
		else {
			$('#createEvent').attr('onclick','').unbind('click');
			parent.$('#loadingBar').show();
			parent.timerControl("start");
			if(resetDate == 1) {											
				$.ajax({
			   		url: Path+'/calenderAction.do',
			   		type:"POST",
			   		data:{act:'resetTaskDates',myTaskId:myTaskId, parId:parId},
			   		error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
			   		success:function(result){
			   			parent.checkSessionTimeOut(result);
			   		}
		   		});
			}	
			if((totalPrevHour!=tHour || totalPrevMinute!=tMinute)){
				if(updateDates=="yes"){
					doResetTaskDates();	
					}
				if(resetDateValidate!='1' || resetDateValidate1=="0" ){			
					getDateXml();	
				}
			}										
			$.ajax({ 
		       url: Path+'/calenderAction.do',
		       type:"POST",
		       data:{act:'updateTask',taskType: taskType,title:title,start:start,end:end,startTime:startTime,endTime:endTime,repeat:repeat,place:place,description:description,eId:eId,taskProject:projId,priorityId:priorityId,update:update,selectedTaskemailId:selectedTaskemailId,removeEmailId: removeEmailId,removeotherEmailId:removeotherEmailId,reminder:reminder,reminderType:remType,selectedOtherEmail:selectedOtherEmail,taskDateXml: taskDateXml,recType: recType, recPatternValue: recPatternValue, recEnd: recEnd,estimatedHour:tHour,estimatedMinute:tMinute,updateNewDepId:updateNewDepId,updateRemoveDepId:updateRemoveDepId},
		      error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
		       success:function(result){
		       removeotherEmailId='';
		       	parent.checkSessionTimeOut(result);
				var myTaskDatas = result.split('#@#@')[0];
				var assignedTaskDatas = result.split('#@#@')[1];
				var historyDatas = result.split('#@#@')[2];
				updateDates="no";
			if(view!="listV"){
					if(update == 'yes') {
						taskCalendar($('#dd span').text());
			        }
			    }    
				if(update == 'yes' && curr_event != null) {
					calendar.fullCalendar('updateEvent', curr_event);
					calendar.fullCalendar('refetchEvents');
					
				}
				calendarViewDatas(myTaskDatas,assignedTaskDatas,historyDatas);
				parent.$('#loadingBar').hide();
				parent.timerControl("");
				closePopup();
				if(sortTaskType == 'assignedTasks') {
					showAssignedTaskList();
				}	
				contentData="completeData";
				updateRemoveDepId="";
				updateNewDepId="";
				tempNewDepId="";
				}
			});
		}
}

function goToUpdateTask(){
	contentData="incompleteData";
	updateAssignedTasks(type);
	
}
function emptyTaskDetails() {      /* Empty the popup details while closing */
	selectedTaskemailId = '';
	selectedOtherEmail = '';
	$('#eventName').css('border','').val('');
	$('#startDateDiv').css('border','');
	$('#endDateDiv').css('border','').val('');
	$('#eventPlace').css('border','').val('');
	$('#endTime').css('border','');
	$('#eventDescription').val('');
	$('#startTime').val('');
	$('#endTime').val('');
	$('#eventParticipants').val('');
	$('div#participantEmail').empty();
	$('select#project').val(getValues(Labels,'Select'));
	$('#deleteTask').hide();
	//$('#createOrCancel').css('width','360px');
}
/*
function searchPage(obj) {			
	$('div.participants').hide();
	var name = $('#'+$(obj).attr('id')).val();
	if(!name) {
		$('div.participants').show();
		$('.searchUserImg').attr('src',Path+'/images/calender/search.png');
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad){
	    	if(parId == 'assignedSprint'){
        		scrollBar('ideaUsersDiv');
        	}		
        	else{
        		scrollBar('userListContainer');
        	}			
        }	
        hidePrtcpnts();
		return null;
	}
	else {
		$('.searchUserImg').attr('src',Path+'/images/remove.png').css({'width':'17px','height':'16px'}).attr('onclick','clearSearchUser(this)');
		$('div.participants').find('span.hidden_searchName:contains("'+name+'")').parents('div.participants').show();
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad){
	    	if(parId == 'assignedSprint')
        		$('#ideaUsersDiv').mCustomScrollbar("update");	
        	else {
        		$('#userListContainer').mCustomScrollbar("update");
        	}		
        }	
        hidePrtcpnts();
        return null;
	}
}
function clearSearchUser(obj){   // Clear Search Field  
	var id = $(obj).prev().attr('id');
	$('#'+id).val('');			
	$('.searchUserImg').attr('src',Path+'/images/calender/search.png');
	$('div.participants').show();
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
    if(!isiPad){
		if(parId == 'assignedSprint')
        	$('#ideaUsersDiv').mCustomScrollbar("update");	
       	else
       		$("#userListContainer").mCustomScrollbar("update");	
	}
	hidePrtcpnts();		
	return null;
}
function hidePrtcpnts(){				
	$('.odd, .even, .participantDetails, .updatePrtcpnt, .participantDetails').each(function() {
		var id = $(this).attr('id');
		var userId = id.split('_')[1];
		$('#participant_'+userId).css('display','none');
	});if($('[id^=participant_]:visible').length < 1){$('#userListContainer').append('<div id="innerDiv20" style="margin-top:-459px;" align="center"> </div>');}else{ $("[id^=innerDiv]").hide();}
}
*/
var workflow_id='';
/*
function updateMyTasks(taskStartDate,currentDate){	//Update mytasks 	
	
    $('#workingHours').css('border','');
	myComment = $('#myTaskComment').val();
	taskCompAmount =parseInt( $('#taskAmount').val());
	workingHours = $('#workingHours').val();
	workingMinutes = $('#workingMinutes').val();
	taskAction = $('#taskActionDropDown option:selected').val();
	tAction = 'Created';
	if(taskAction == 'Completed' || taskCompAmount >= 100){
		tAction = 'Completed';
		taskCompAmount = '100';
	}
	if(taskCompAmount == '100'){
	   tAction = 'Completed';
	}
	if(taskAction == 'Canceled'){
		parent.confirmFun(getValues(customalertData,"Alert_Task_Canceled"),"delete","cancelTask");
	}else{
		if(taskStartDate > currentDate){
		   parent.alertFun("You can't update the task because task start date is not matching to current date.",'warning');
		}else{
		   myTaskUpdate();
		}
	}	
}				*/
function saveData(){
	$.ajax({
	   		url: Path+'/calenderAction.do',
	   		type:"POST",
	   		data:{act:'updateWorkflowStatus', workflow_id: setFlowId},
	   		error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
	   		success:function(result){
	   			parent.checkSessionTimeOut(result);
	   			$("#asgndWorkflow_"+setFlowId).find("img").last().attr('src',''+Path+'/images/calender/Completed.png');
  				$("#asgndWorkflow_"+setFlowId).find("img").last().attr('title','Completed');
	   		}
   	    });
}
/*
function cancelTask() {
	tAction = 'Canceled';
	myTaskUpdate();
}*/
/*
function myTaskUpdate(){	
	var intRegex = /^\d+$/;
    var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
	if(parId != 'myEvent' && parId != 'myEvents'){
	    var str = workingHours;
	    var strMin= workingMinutes;
	     if(str!='' && str!='undefined'){
	       if(intRegex.test(str) || floatRegex.test(str)) {
		       if(str>24){
		          parent.alertFun(getValues(customalertData,"Alert_InvalidHour"),'warning');
		          return null;
		          setTimeout(function(){
		                $('#workingHours').focus();
		             },500);
		       }else{
		          $('#workingHours').css('border','1px solid #cccccc');
		      }
		   }else{	
 		       parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
 		       $('#workingHours').css('border','1px solid red');
 		       return null;
 		       setTimeout(function(){
                $('#workingHours').focus();
             },500);
		 }
		}else{
		   $('#workingHours').css('border','1px solid #cccccc');
		}
		if(strMin!='' && strMin!='undefined'){
	       if(intRegex.test(strMin) || floatRegex.test(strMin)) {
		       if(strMin>59){
		          parent.alertFun("Minute value should be between 0 to 59.",'warning');
		           $('#workingMinutes').css('border','1px solid red');
		          return null;
		          setTimeout(function(){
		                $('#workingMinutes').focus();
		             },500);
		       }else{
		          $('#workingMinutes').css('border','1px solid #cccccc');
		      }
		   }else{	
 		       parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
 		       $('#workingMinutes').css('border','1px solid red');
 		       return null;
 		       setTimeout(function(){
                $('#workingMinutes').focus();
             },500);
		 }
		}else{
		   $('#workingMinutes').css('border','1px solid #cccccc');
		}
		
		if(!($.isNumeric( $("#taskAmount").val() ))){
			parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
			return false;
		}   //commentDateUpdate();               
		if(tAction == 'Canceled'){
			cancelUpdate();
		}else{
			if ($('#userCompletedPercentage').val() == taskCompAmount && workingHours == 0 && myComment==''){
				closePopup();
				parent.$("#loadingBar").hide();
				parent.timerControl("");
				return null;
			}
			if(taskHour==str && taskCompAmount > 0 && str == '0' && strMin=='0') {			
				parent.confirmReset(getValues(customalertData,"Alert_ActualHour_Confirm"),'reset','continueUpdate','cancelUpdate');
			}
			else{
				cancelUpdate();
			}
		}
	}else{
		taskCompAmount = 0;
		workingHours = 0;
		workingMinutes = 0;
		cancelUpdate()
	}
}





function continueUpdate(){
	$( "#workingHours" ).focus();
	return null;
}

function cancelUpdate(){	
   var workflow_id="";
    if(parId == 'myWorkflow'){
      var level_doc = $('#level_doc').val();
      workflow_id=$("#workflow_id").val();
      if(level_doc!='' && level_doc=='Y' && $('#docImgDiv_'+UserId).is(':hidden')){
          parent.alertFun("Please attach required document to proceed.",'warning');
          showMyTaskUserOptions('userOptions_'+UserId+'','2');
          $('#uOptions_'+UserId).hide().slideToggle(1000);
          return false;
      }
    }
	var i=0;	
	$('#updateTask').attr('onclick','').unbind('click');
	parent.$('#loadingBar').show();
	parent.timerControl("start");
	if($('[id^=commentTextarea_]').is(':visible')) {
	$('[id^=commentTextarea_]').each(function() {
		if($(this).next().val()!=$(this).val()){
	  commentXml +="<id"+i+">"+$(this).attr('id').split('_')[2]+"</id"+i+">";
	  commentXml +="<Value"+i+">"+$(this).val()+"</Value"+i+">";i++;
	  }
	});
	
}
	commentXml +="</Comments>";
	$.ajax({
      	url: Path+'/calenderAction.do',
       	type:"POST",
		data:{act:'updateUserCompletion', taskId: myTaskId, taskType: parId, taskCompComm: myComment, tAction: tAction,commentLoop: i,commentFullXml: commentXml, taskCompAmount: taskCompAmount,workingHours: workingHours,workingMinutes: workingMinutes,latestCmntDate:latestCmntDate},
		success:function(result){
			parent.checkSessionTimeOut(result);
			if(taskAction == getValues(Labels,'Cancel')) {
				calendar.fullCalendar('removeEvents', myTaskId);
             	calendar.fullCalendar('refetchEvents');
			}
			commentXml='<Comments>';
			var res = result.split('@_@')[0];
			var myTaskDatas = result.split('@_@')[1];
			var replaceSpl=myTaskDatas.split("@@");
			$("#myTasksList > div").each(function(){
			var a= $(this).find("img").last().attr("title");
			var task_type=$(this).find("img").last().attr("id").split("_")[0];
			var b=$(this).find("img").last().attr("src");
			var tasksId=$(this).attr('id').split("_")[1];
			var taskName=$(this).attr('id').split("_")[0];
  				 $(replaceSpl).each(function(){  
  					 valueStatus=this.split("--");
  					if(tasksId==valueStatus[0] && valueStatus[2]==task_type ){	
  						if(a!=valueStatus[1]){	
  							$("#"+taskName+"_"+tasksId).find("img").last().attr('src',''+Path+'/images/calender/'+valueStatus[1]+'.png');
  							$("#"+taskName+"_"+tasksId).find("img").last().attr('title',valueStatus[1]);
  							$("#asgnd"+valueStatus[2]+"_"+tasksId).find("img").last().attr('src',''+Path+'/images/calender/'+valueStatus[1]+'.png');
  						    $("#asgnd"+valueStatus[2]+"_"+tasksId).find("img").last().attr('title',valueStatus[1]);
  							if(valueStatus[2]=="Tasks"){
  							$("#historyTask_"+tasksId).find("img").last().attr('src',''+Path+'/images/calender/'+valueStatus[1]+'.png');
  							$("#historyTask_"+tasksId).find("img").last().attr('title',valueStatus[1]);
  							}
  							else{
  							$("#history"+valueStatus[2]+"Task_"+tasksId).find("img").last().attr('src',''+Path+'/images/calender/'+valueStatus[1]+'.png');
  							$("#history"+valueStatus[2]+"Task_"+tasksId).find("img").last().attr('title',valueStatus[1]);
  							}
  						}     
                    }
               });
               
                       
            });
  			parent.$("#loadingBar").hide();
			parent.timerControl("");
			if(res == 'success'){
				if(sortTaskType == 'myTasks'){
					showMyTaskList();
				}	
				closePopup();
			}else{
				parent.alertFun(getValues(customalertData,"Alert_ErrorTask"),'warning');
				closePopup();
			}	
			if(parId="myWorkflow"){
			$.ajax({
		   		url: Path+'/calenderAction.do',
		   		type:"POST",
		   		data:{act:'checkWorkflowStatus',taskId: myTaskId, taskType: parId , workflow_id: workflow_id},
		   		success:function(result){
		   			parent.checkSessionTimeOut(result);			
		   			if(result=="Completed"){
		   				setFlowId=workflow_id;
		   				parent.confirmFun(getValues(customalertData,"Alert_WFStatusUpdate"),"delete","saveData");
		   				
		   			}
		   		}
	   	    });
	   	    
	   	    
	   	    
	}
			parent.$("#loadingBar").hide();
			parent.timerControl("");
		}
	});
}

*/
function timePicker(){				/*Initialize Timepicker and options*/		
  	 $('.hasTimepicker').timepicker({ 
         'step': 15,
         'timeFormat': 'H:i'
     });
 }
	
function calendarViewDatas(myTask,assgndTsk,historyTsk) {    /* Calendar view */		
	$('#myTasksList').empty();
	$('#assignedTasksList').empty();
	$('#historyListTasks').empty();
	myTask =  myTask.replaceAll("CHR(39)","'");
	myTask =  myTask.replaceAll("CHR(40)",'"');
	myTask =  myTask.replaceAll("CHR(50)",'\n');
	assgndTsk =  assgndTsk.replaceAll("CHR(39)","'");
	assgndTsk =  assgndTsk.replaceAll("CHR(40)",'"');
	assgndTsk =  assgndTsk.replaceAll("CHR(50)",'\n');
	historyTsk =  historyTsk.replaceAll("CHR(39)","'");
	historyTsk =  historyTsk.replaceAll("CHR(40)",'"');
	historyTsk =  historyTsk.replaceAll("CHR(50)",'\n');
	if(height < 590) {
		for(var i=0; i<3; i++) {
			$('#myTasksList').append(myTask.split('^&*')[i]);
			$('#assignedTasksList').append(assgndTsk.split('^&*')[i]);
			$('#historyListTasks').append(historyTsk.split('^&*')[i]);
		}
	}
	if(height < 725 && height > 594) {
		for(var i=0; i<4; i++) {
			$('#myTasksList').append(myTask.split('^&*')[i]);
			$('#assignedTasksList').append(assgndTsk.split('^&*')[i]);
			$('#historyListTasks').append(historyTsk.split('^&*')[i]);
		}
	}
	if(height < 850 && height > 725) {
		for(var i=0; i<5; i++) {
			$('#myTasksList').append(myTask.split('^&*')[i]);
			$('#assignedTasksList').append(assgndTsk.split('^&*')[i]);
			$('#historyListTasks').append(historyTsk.split('^&*')[i]);
		}
	}
	if(height > 850 ) {
		for(var i=0; i<6; i++) {
			$('#myTasksList').append(myTask.split('^&*')[i]);
			$('#assignedTasksList').append(assgndTsk.split('^&*')[i]);
			$('#historyListTasks').append(historyTsk.split('^&*')[i]);
		}
	}
	$(".myTask , .assignedTask, .taskSub").each(function(){
		var title = $(this).attr('title').replaceAll("CHR(41)"," ");
		$(this).attr('title',title);
	});
    parent.$('#loadingBar').hide();	
    parent.timerControl("");	
}	

/*Add Participants to the Task */		
/*function addParticipantEmail(obj) {			
	var addParticipantId = $(obj).attr('name');
	var projectUserCount = $('.userCheck').length;
	var title = $('#selectTask').val();
	var divCount = $('#participantEmail').length;
	if(addParticipantId != null && addParticipantId != 0 && addParticipantId == 'addParticipants') {
		//alert('in');
		$('.userCheck').each(function() {
			var partcipantId = $(this).prev().attr('id');
			var prtcpntId = $(this).prev().attr('value');
			var participantName = $('#'+partcipantId).text();
			if(participantName.length > 17)
				participantName = participantName.substring(0,16)+'...';
			var participantEmail = $('#'+partcipantId).attr('title');
	    	$('.addParticipant').hide();
	    	if(title == 'WorkFlow') {
	    		taskWorkFlowEmail(prtcpntId,partcipantId,participantName,participantEmail,cntntVal);
	    	}
			else {
				$('.createEvent').css('display','block');
				$('.createEvent1').css('display','none');
				var emailDiv = "";
				if(parId == 'assignedTasks' || parId == 'assignedIdea' || parId == 'assignedWorkflow' || parId == 'assignedDocument' || parId == 'assignedWorkspaceDocument' ) {
					if(selectedTaskemailId.indexOf(participantEmail+"_"+prtcpntId)<0){
						taskEmailDiv(prtcpntId,partcipantId,participantName,participantEmail);
					}
				}
				else if(parId == 'assignedSprint') {
						if(selectedTaskemailId.indexOf(prtcpntId)<0) {
							selectedTaskemailId=selectedTaskemailId+prtcpntId+',';
							taskSprintEmail(prtcpntId,partcipantId,participantName,participantEmail);
						}
					}
				else {
					if(selectedTaskemailId.indexOf(participantEmail+"_"+prtcpntId)<0){
						taskEmailDiv(prtcpntId,partcipantId,participantName,participantEmail);
						$('#userUnCheck_'+prtcpntId).removeClass('userCheck').addClass('userUnCheck');
						$('.createEvent').css('display','block');
						$('.createEvent1').css('display','none');
					}	
				}	
			}
			var isiPad = navigator.userAgent.match(/iPad/i) != null;
			if(!isiPad)
				scrollBar('userListContainer');
       	});
		if(projectUserCount == '0') {
			participantEmail = $(obj).attr('title');
			prtcpntId = $(obj).parent().attr('value');
			participantName = $('#particpantName_'+prtcpntId).text();
			if(participantName.length > 17) {
				participantName = participantName.substring(0,16)+'...';
			}
			partcipantId = $('#particpantName_'+prtcpntId).attr('id');
			if(parId == 'assignedSprint') {
			 selectedTaskemailId=selectedTaskemailId+prtcpntId+',';
			 taskSprintEmail(prtcpntId,partcipantId,participantName,participantEmail);
			} 
			else if(parId == 'workflow') 
			 taskWorkFlowEmail(prtcpntId,partcipantId,participantName,participantEmail,cntntVal);	
			else {
			 taskEmailDiv(prtcpntId,partcipantId,participantName,participantEmail);
			} 
		}
		progressBar();
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad)	{
			if($('#ideaTaskParticipantsDiv.mCustomScrollbar').length) {
				$("#ideaTaskParticipantsDiv").mCustomScrollbar("update");
			}
			else
				scrollBar('ideaTaskParticipantsDiv');
			if($('#participantEmail.mCustomScrollbar').length) {
				$("#participantEmail").mCustomScrollbar("update");
			}	
			else
				scrollBar('participantEmail');	
			if($('#wFParticipantEmail_'+cntntVal+'.mCustomScrollbar').length) {
				$('#wFParticipantEmail_'+cntntVal).mCustomScrollbar("update");
			}	
			else
				scrollBar('wFParticipantEmail_'+cntntVal);	
		}	
		$('#checkAll').removeClass().addClass('removeAll');	
		divCount++;
		$( ".ui-slider-handle" ).on( "mousedown", function(event) {
							   parent.alertFun("You can not update progress of participants.",'warning');});
		$( ".userCompPerSlider" ).on( "click", function(event) {
							   parent.alertFun("You can't update progress of the participants.",'warning');});
	}
	if(!addParticipantId == 'addParticipants' || !addParticipantId == 'addWFUsers' || addParticipantId =='addEventParticipant' ) {  /// Add Participants for Event 
		var uName='';
		var userEmail='';
		if(addParticipantId == 'addEventParticipant') {
			userEmail = $('#eventParticipants').val();
			uName = userEmail.split('@')[0];
		}	
		else {
			var usrEmailId = $('#'+addParticipantId).prev().attr('id');
			userEmail = $('#'+usrEmailId).val();
			uName = userEmail.split('@')[0];
		}
		$.ajax({ 
	   		url: Path+'/calenderAction.do',
       		type:"POST",
	 		data:{userEmail:userEmail,act:'checkParticipant'},
		 	success:function(result){
		 		parent.checkSessionTimeOut(result);
		 		if(result != 'other') {
		 			var userName = result.split('###@###');
		 			var userID = userName[0];
		 			if(addParticipantId == 'addEventParticipant') {
		 				if(selectedTaskemailId.indexOf(userEmail+"_"+userID)<0){
		 					selectedTaskemailId=selectedTaskemailId+userEmail+"_"+userID+',';
				 			var emailDiv = "<div id=\"email_"+userID+"\" style=\"width: 150px;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:10px;margin-bottom:10px;\"><div style=\"float: left;padding-left: 10px;padding-top:1px;\">"+userName[1]+"</div><img class=\"deleteEmail\" title=\""+getValues(Labels,'Delete')+"\" src=\""+Path+"/images/calender/emailDel.png\"  style=\"float: right;margin-top: 6px;margin-right: 5px;cursor:pointer;\" onclick=\"removeParticipant(this,'"+userEmail+"','"+userID+"');\"></div>";
							$('div#participantEmail').append(emailDiv);
							$("div#participantEmail").mCustomScrollbar("update");
						}	
						else {
							parent.alertFun(getValues(customalertData,"Alert_UserExists"),'warning');
							return null;
						}
					}
					else{
						if(selectedTaskemailId.indexOf(userEmail+"_"+userID)<0){
							var emailDiv = "<div class=\"wFEmail_"+userID+"\" style=\"width: 150px;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:10px;margin-bottom:10px;\"><div style=\"float: left;padding-left: 10px;color:#848484;padding-top:1px;\">"+userName[1]+"</div><img class=\"deleteEmail\" title=\""+getValues(Labels,'Delete')+"\" src=\""+Path+"/images/calender/emailDel.png\" style=\"float: right;margin-top: 6px;margin-right: 5px;cursor:pointer;\" onclick=\"removeParticipant(this,'"+userEmail+"','"+userID+"');\"></div>";
							var wFPrtcpntDiv = $('#'+addParticipantId).parent().next('div').attr('id');
							$('#'+wFPrtcpntDiv).append(emailDiv);
						}
						else {
							parent.alertFun(getValues(customalertData,"Alert_UserExists"),'warning');
							return null;
						}
					}
				}
				if(result == 'other') {
					if(!userEmail){
						$('#eventParticipants').css('border','1px solid red');
						return null;
					}
					else{
						$('#eventParticipants').val('');
						if( !isValidEmailAddress( userEmail ) ) {
							parent.alertFun(getValues(customalertData,"Alert_InvalidEmail"),'warning');
							$('#eventParticipants').css('border','1px solid red');
							return null;
						}
						else {
							if(selectedOtherEmail.indexOf(userEmail)<0){
								$('#eventParticipants').css('border','');
								selectedOtherEmail=selectedOtherEmail+userEmail+',';
								if(uName.length > 17)
									uName = uName.substring(0,16)+'...';
					 			var emailDiv = "<div class=\"otherUserEmail\" title=\""+userEmail+"\"   style=\"width: 155px;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:10px;margin-bottom:10px;\"><div style=\"float: left;padding-left: 10px;\">"+uName+"</div><img class=\"deleteEmail\" title=\""+getValues(Labels,'Delete')+"\" src=\""+Path+"/images/calender/emailDel.png\" style=\"float: right;margin-top: 6px;margin-right: 5px;cursor:pointer;\" onclick=\"removeOtherPrtcpnt(this,'"+userEmail+"');\"></div>";
								if($('#participantEmail > div > div.mCSB_container').length) {
									$('#participantEmail > div > div.mCSB_container').append(emailDiv);
									$("div#participantEmail").mCustomScrollbar("update");
								}	
								else{
									$('#participantEmail').append(emailDiv);
								}	
							}
							else {
								parent.alertFun(getValues(customalertData,"Alert_UserExists"),'warning');
								return null;
							}
						}	
					}
				}
		 		if(result == 'no'){
		 			parent.alertFun(getValues(customalertData,"Alert_UserUnAvail"),'warning');
		 			return null;
		 		}	
		 	}
		});
		
	}if($('[id^=participant_]:visible').length < 1){$('#userListContainer').append('<div id="innerDiv16" style="margin-top:-459px;" align="center"> No Participant</div>');}
}

function taskEmailDiv(prtcpntId,partcipantId,participantName,participantEmail){   
	selectedTaskemailId=selectedTaskemailId+participantEmail+"_"+prtcpntId+',';
	if(parId == 'assignedTasks' || parId == 'assignedIdea' || parId == 'assignedWorkflow' || parId == 'assignedDocument' || parId == 'assignedWorkspaceDocument' ){
		emailDiv = "<div  style=\"float:left;\" value=\""+prtcpntId+"\" id=\"emailPrtcpnt_"+prtcpntId+"\" class=\"updatePrtcpnt\">"
				+"<div align=\"left\" style=\"height:25px;overflow:hidden;float:left;color:#848484;width:120px;font-weight:normal;float:left;\" title=\""+$('#'+partcipantId).text()+"\">"+participantName+"</div>"
				+"<div align=\"left\" style=\"float:left\" title=\"0\">"
				+"<div style=\"float:left;\"><div class=\"demo\" style=\"float:left;width:180px;font-weight:normal;\"> "
				+"<div class=\"userCompPerSlider userCompPerSlider1\"  style=\"width: 133px;float:left;margin:6px 0px 0px 0px;background: none repeat scroll 0 0 #C1C5C6;color:#848484;\">0</div>"
				+"<div style=\"float:left;width:30px;text-align:right;\"><span style=\"float:none;width:30px;margin-left:10px;color:#848484;\" id=\"taskAmount\">0%</span></div>"
				+"</div>"
				+"</div></div>"
				+"<div style=\"float: left;height:20px;width:90px;font-weight:normal;color: #848484;\">"+getValues(Labels,'Created')+"</div>"
				+"<div style=\"float: left;height:30px;width:30px;font-weight:normal;color: #848484;\">0 h</div>"
				+"<div style=\"float: left;height:30px;width:45px;font-weight:normal;color: #848484;\">0 m</div>"
				+"<div id="+prtcpntId+" task_id=\"\" task_type=\"\" email=\""+participantEmail+"\" class=\"options\" onclick=\"showTaskUserOptions(this);\" style=\"float:left;margin-left:8px;width:20px;margin-top:0px;\"> <img id=\"taskUserOption_"+prtcpntId+"\" title=\""+getValues(Labels,'Options')+"\" class=\"options\" src=\"images/repository/Options.png\" style=\"float: right;height:17px;width:17px;padding-left:2px;margin-top:0;\"></div>"
				+" <div id=\"uOptions_"+prtcpntId+"\" class=\"hideOptionDiv\" style=\"width:160px; min-width:100px;max-height:105px;float: right;margin-top:-15px;right:0px;background-color:#FFFFFF;position:absolute;height:auto;"
				+" display:none;z-index: 5010;margin-right:40px;padding-left:5px;border-radius:5px;box-shadow: 0 1px 4px 0 rgba(168, 168, 168, 0.6);border:1px solid #A1A1A1;\">"
				+"</div>"
				+"</div>"
				+"<div id=\"userComments_"+prtcpntId+"\" class=\"commentsSec\" style=\"float: left;width:100%;cursor:pointer;font-weight:normal;\" ></div>"
				+"<div style=\"float: left;width:100%;cursor:pointer;font-weight:normal;display:none;margin-top:10px;\" class=\"documentSection\" id=\"documentSection_"+prtcpntId+"\"></div>";
		$('#participant_'+prtcpntId).hide();
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad)	{
			$('div#prtcpntSection').append(emailDiv);
			$("div.taskPrtcpntSection").mCustomScrollbar("update");
			$("div#userListContainer").mCustomScrollbar("update");
		}
		else 
			$('div#prtcpntSection').append(emailDiv);
	}	
	else {
		$('#participant_'+prtcpntId).hide();
		emailDiv = "<div id=\"email_"+prtcpntId+"\" class=\"participantDetails\" style=\"width: 150px;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:10px;margin-bottom:10px;\"><div style=\"float: left;padding-left: 10px;padding-top:1px;\" title=\""+$('#'+partcipantId).text()+"\">"+participantName+"</div><img class=\"deleteEmail\" title=\""+getValues(Labels,'Delete')+"\"  src=\""+Path+"/images/calender/emailDel.png\" style=\"float: right;margin-top: 6px;margin-right: 5px;cursor:pointer;\" onclick=\"removeParticipant(this,'"+participantEmail+"','"+prtcpntId+"');\"></div>";
		if($('#participantEmail > div > div.mCSB_container').length) {
			$('#participantEmail > div > div.mCSB_container').append(emailDiv);
			$("div#userListContainer").mCustomScrollbar("update"); 
		}	
		else	{
			$('#participantEmail').append(emailDiv);
		}	
	}
}*/
/*
function taskSprintEmail(prtcpntId,partcipantId,participantName,participantEmail){    //  Add Users in Assigned Sprint Task Popup  
	emailDiv = "<div  style=\"float:left;\" value=\""+prtcpntId+"\" id=\"emailPrtcpnt_"+prtcpntId+"\" class=\"updatePrtcpnt\">"
			+"<div align=\"left\" style=\"height:25px;overflow:hidden;float:left;color:#848484;width:120px;font-weight:normal;float:left;\" title=\""+$('#'+partcipantId).text()+"\">"+participantName+"</div>"
			+"<div align=\"left\" style=\"float:left\" title=\"0\">"
			+"<div style=\"float:left;\"><div class=\"demo\" style=\"float:left;width:185px;font-weight:normal;\"> "
			+"<div class=\"userCompPerSlider userCompPerSlider1\"  style=\"width: 135px;float:left;margin:6px 0px 0px 0px;background: none repeat scroll 0 0 #C1C5C6;color:#848484;\">0</div>"
			+"<div style=\"float:left;width:40px;text-align:right;\"><span style=\"float:none;width:30px;margin-left:10px;color:#848484;\" id=\"taskAmount\">0%</span></div>"
			+"</div>"
			+"</div></div>"
			+"<div style=\"float: left;height:20px;width:90px;font-weight:normal;color: #848484;margin-left:7px;\">"+getValues(Labels,'Created')+"</div>"
			+"<div style=\"float: left;height:20px;width:27px;font-weight:normal;color: #848484;\">0 h</div>"
			+"<div style=\"float: left;height:20px;width:40px;font-weight:normal;color: #848484;\">0 m</div>"
			//+"<img title=\"View Comments\" style=\"float: left;cursor: pointer;height:16px;width:16px;\" src=\"images/calender/comments.png\" onclick=\"viewNewUserCmnts('"+prtcpntId+"',this);\" />"
			//+"<div style=\"float:left;\"> <img src=\"images/Idea/userDelete.png\" style=\"float: right;height:16px;width:16px;padding-left:11px;\" onclick=\"removeNewParticipant(this,'"+participantEmail+"','"+prtcpntId+"');\"></div>"
			+"<div id=\""+prtcpntId+"\" task_id=\"\" task_type=\"assignedSprint\" email=\"\" class=\"options\" onclick=\"showSprintTaskUserOptions(this);\" style=\"float:left;margin-left:8px;width:20px;margin-top:0px;color: #848484;\"> <img id=\"taskUserOption_"+prtcpntId+"\" src=\"images/repository/Options.png\" class=\"options\" style=\"float: right;height:17px;width:17px;padding-left:2px;margin-top:0;\"></div>"
			+"<div id=\"uOptions_"+prtcpntId+"\" class=\"hideOptionDiv\" style=\"width:160px; min-width:100px;float: right;margin-top:-15px;right:0px;background-color:#FFFFFF;position:absolute;height:auto;"
			+"display:none;z-index: 5010;margin-right:40px;padding-left:5px;border-radius:5px;box-shadow: 0 1px 4px 0 rgba(168, 168, 168, 0.6);border:1px solid #A1A1A1;\">"
			+"</div>"
			+"</div>"
			+"<div id=\"userComments_"+prtcpntId+"\" class=\"commentsSec\" style=\"float: left;width:100%;cursor:pointer;font-weight:normal;\" ></div>"
			+"<div style=\"float: left;width:100%;cursor:pointer;font-weight:normal;display:none;margin-top:10px;\" class=\"documentSection\" id=\"documentSection_"+prtcpntId+"\"></div>";
	$('div#participant_'+prtcpntId).hide();
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	if(!isiPad)	{
		$('#ideaTaskParticipantsDiv > div > div.mCSB_container > #participantEmail').append(emailDiv);
		$("div#ideaTaskParticipantsDiv").mCustomScrollbar("update");
		$("div#ideaUsersDiv").mCustomScrollbar("update");
	}
	else 
		$('#ideaTaskParticipantsDiv > #participantEmail').append(emailDiv);
}*/
function taskWorkFlowEmail(prtcpntId,partcipantId,participantName,participantEmail,cntntVal){
		var partcipantId = cntntVal;
		if ($("[id^='mainTaskDiv_']").length < 1) {
				$('#userUnCheck_'+prtcpntId).removeClass('userCheck').addClass('userUnCheck');
				parent.alertFun(getValues(customalertData,"Alert_SelectTemplate"),'warning');
				$('.createEvent').css('display','block');
				$('.createEvent1').css('display','none');
			return false;
	 	}else{
	   		if($('#wFParticipantEmail_'+partcipantId+' #wFEmail'+partcipantId+'_'+prtcpntId).length) {
	   		}else{
	   				selectedTaskemailId=selectedTaskemailId+participantEmail+"_"+prtcpntId+',';
					var emailDiv = "<div id=\"wFEmail"+partcipantId+'_'+prtcpntId+"\" class=\"wFEmail_"+partcipantId+"\" title=\""+participantEmail+"\"  style=\"width: 175px;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:10px;margin-bottom:10px;\"><div style=\"float: left;padding-left: 10px;color:#848484;padding-top:1px;\" >"+participantName+"</div><img class=\"deleteEmail\" src=\""+Path+"/images/calender/emailDel.png\" title=\""+getValues(Labels,'Delete')+"\" style=\"float: right;margin-top: 6px;margin-right: 5px;cursor:pointer;\" onclick=\"removeWFParticipant(this,'"+participantEmail+"','"+prtcpntId+"','"+partcipantId+"');\"></div>";
					if($('#wFParticipantEmail_'+partcipantId+'.mCustomScrollbar').length) 
						$('#wFParticipantEmail_'+partcipantId+' > div > div.mCSB_container').append(emailDiv);
					else	
						$('#wFParticipantEmail_'+partcipantId).append(emailDiv);
					$('#participant_'+prtcpntId).hide();
			    	$('#userUnCheck_'+prtcpntId).removeClass('userCheck').addClass('userUnCheck');
					$('.createEvent').css('display','block');
					$('.createEvent1').css('display','none');	
					$('#wFParticipantEmail_'+partcipantId).mCustomScrollbar("update"); 
			}
		}
}	



	
function isValidEmailAddress(emailAddress) {		/* Validating email   */
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
}


function updateEpicTasks(taskId,idd){
		myTaskId = taskId;
		update='yes';
		parId = 'assignedSprint';
		divId = 'assignedSprint';
		tflag=true;
		
		$("#uploadDocTaskId").val(taskId);	
	    $("#doctaskType").val(divId);
		$('#sprintTaskEhourPopupDiv').remove();
		$('#sprintTaskDateHourDiv').remove();
		parent.$('#loadingBar').show();
		parent.timerControl("start");
		$.ajax({
			url: Path+'/calenderAction.do',
			type:"POST",
			data:{act:'getAssignedSprint',ideaID:idd,taskId:taskId},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				parent.checkSessionTimeOut(result);
				$('.transparent').show();
				parent.$("#transparentDiv").show();
				$('#eventPopup').append(result);
				$('.ui-slider-handle.ui-state-default.ui-corner-all').css('background', 'url("images/handleDisable.png") repeat-x scroll 50% 50% rgba(0, 0, 0, 0)');
				$('#eventPopup').css('display','block');
				$('#eventContainer').css('display','none');
				$('#taskUserListContainer').css({'box-shadow':'','border':'','border-left':'1px solid #BFBFBF'});
				
				startDate = $('#startDateSprint').val();
				endDate = $('#endDateSprint').val() ;
				if(height >= 550)
					$('#eventPopup').css('height','550px');
				else {
					$('#eventPopup').css('width','850px');
					$('#eventPopup').css('height',height+'px');
					$('.eventPopup').css('margin-top','');
					$('.eventPopup').css('margin-top','-'+height/2+'px');
					setTimeout(function(){scrollBar('eventPopup'),500});
				}
				$('#projectUsers').css('display','none');
				$('#taskDocCreateImg1').css('display','block');
				$('select#userProject').val(getValues(Labels,'All_Users'));
				$('#taskUserListContainer').show();
				parent.$('#loadingBar').hide();
				parent.timerControl("");
				var dd = new DropDown( $('#ddList1') );
			    var dd = new DropDown( $('#ddList2') );
			    var dd = new DropDown( $('#ddList3') );
			    var dd = new DropDown( $('#ddList4') );
			    tmpSDate=$('#startDateSprint').val();
	    		tmpEDate=$('#endDateSprint').val();
			    $(document).click(function() {
			       $('.wrapper-dropdown-2').removeClass('active');
			    });
			    initSprintCal1();
			    projId = $('#agileProjId').val();
			    if(projId == '0'){
					getAllUsers();			//$('#ideaUsersDiv').html('${allUsers}');
			    	$('.userContainer').each(function() {
						var id = $(this).attr('id');
						var userId = id.split('_')[1];
						$('#participant_'+userId).css('display','none');
					});
					var isiPad = navigator.userAgent.match(/iPad/i) != null;
       				if(!isiPad) {
						scrollBar('ideaUsersDiv');
						scrollBar('ideaTaskParticipantsDiv');
					}
					$('.usrComments').css('font-size','12px');
					loadCustomLabel('TaskUI');
				       
			    } else{	
					$.ajax({ 
				       url: Path+'/calenderAction.do',
				       type:"POST",
				       data:{act:'fetchParticipants',projId:projId,userId:UserId},
				       error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
				       success:function(result){
				       		parent.checkSessionTimeOut(result);
				       		$('#ideaUsersDiv').html(result);
				       		$('.userContainer').each(function() {
								var id = $(this).attr('id');
								var userId = id.split('_')[1];
								$('#participant_'+userId).css('display','none');
							});
							var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        				if(!isiPad) {
								scrollBar('ideaUsersDiv');
								scrollBar('ideaTaskParticipantsDiv');
							}
							$('.usrComments').css('font-size','12px');
							loadCustomLabel('TaskUI');
				        }
					});
				}
            	$('.prtcpntSection > div').each(function() {
					var id = $(this).attr('id');
					var userId = id.split('_')[1];
					$('#participant_'+userId).css('display','none');
				});
				$('.addParticipant').css('display','none');
				$('#estimatedHour').val($('#taskTotalEhour').val());
				$('#estimatedMinute').val($('#taskTotalEminute').val());
				tHour = $('#sprintEstHour').val();
				tMinute=$('#sprintEstMinute').val();
				
				var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        	if(!isiPad) {
					scrollBar('ideaUsersDiv');
					scrollBar('ideaTaskParticipantsDiv');
				}
				timePicker();
				initSprintCal1();
			}
		});
}
/*
function resetAssignedDate(){						
	$('#taskEhourPopupDiv').show();
	tHour = $('#totalHour').val();
	tMinute = $('#totalMinute').val();
	
	createHourPopup(start,end,tHour,tMinute);
   	$('#wFTaskDateSave').hide();				
   	resetDateValidate1="0";
   	return null;
}

*/
function commentsScrollBar() {
		$("div.prtcpntSection").mCustomScrollbar("destroy");
		$("div.prtcpntSection").mCustomScrollbar({
		  scrollButtons:{
			  enable:true
		  }
	 	 });		
	 	$('div.prtcpntSection .mCSB_container').css('margin-right','15px');
	  	$('div.prtcpntSection .mCSB_container').css('height','100%');
	    $('div.prtcpntSection .mCSB_scrollTools .mCSB_buttonUp').css({'background-position':'-80px 0px','opacity':'0.6'});
		$('div.prtcpntSection .mCSB_scrollTools .mCSB_buttonDown').css({'background-position':'-80px -20px','opacity':'0.6'});
		$('div.prtcpntSection .mCSB_scrollTools .mCSB_draggerRail').css('background','#B1B1B1');
		$('div.prtcpntSection .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','#6C6C6C');
	    $('div.prtcpntSection .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','#B1B1B1');
	    $('div.prtcpntSection .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','#B1B1B1');  
}
function progressBar() {				
	$('div.userCompPerSlider1').each(function() {
		var value = parseInt( $( this ).text(), 10 );
		$( this ).empty().slider({
			value: value,
			range: "min",
			animate: true,
			disabled: true ,
			orientation: "horizontal"
		});
		if(jQuery.browser.msie){$('#vas').css('height','20px');}
		$(this).removeClass('userCompPerSlider1');	
	});
}
contentDataSprint="complete";
var setIdSprint='';
function updateSprintTask(sprintTaskId){					
		 var selTaskEmailId = selectedTaskemailId.substring(0, selectedTaskemailId.length - 1);
		 var taskName = $("#taskEventName").val(); 
		 var startDate = $("#startDateSprint").val();
		 var endDate = $("#endDateSprint").val();			
		 var startTime = $("#taskStartTime").val();
		 var endTime = $("#taskEndTime").val();
		 var comments = $("#taskEventDescription").val(); 
		 var epicID=sprintTaskId;
		 setIdSprint=sprintTaskId;
		 var taskProjId=$('#agileProjId').val();
		 var taskId=$("#taskID").val(); 
		 var taskRemainder = $("#ddList1 span").text(); 
         var taskRemainderBy = $("#ddList2 span").text();
         var taskSprintGroupId=$("#taskSprintGroup").val(); 
		 var taskSprintId=$("#taskSprint").val(); 
		 var tmpSDate=$("#prevStartDate1").val(); 
		 var tmpEDate=$("#prevEndDate1").val(); 
		if(taskName == "" || taskName == null){
		    $('#taskEventName').css("border","1px solid red");
			return false;
		}else{
		    $('#taskEventName').css("border","1px solid #BFBFBF");
		}
	/*	if(startDate == "" || startDate == "Date" || startDate == null){
			$('#startDateSprint').css("border","1px solid red");
			return false;		
		}else{
		    $('#startDateSprint').css("border","1px solid #BFBFBF");
		}
		if(endDate == "" || endDate == "Date" || endDate == null){
			$('#endDateSprint').css("border","1px solid red");
			return false;		
		}else{
		    $('#endDateSprint').css("border","1px solid #BFBFBF");
		}
		if($('#ideaTaskParticipantsDiv > div > div > #participantEmail:visible').children().length <1){
			parent.alertFun(getValues(customalertData,"Alert_Participant"),'warning');
			return false;	
		}*/
		if((!startDate) || (!endDate) || $('#ideaTaskParticipantsDiv > div > div > #participantEmail:visible').children().length <1 ){
				if(contentDataSprint!="incompleteData"){
			    // 	contentDataSprint="incompleteData";
					parent.confirmFun(getValues(companyAlerts,"Alert_SaveIncompleteData"),"delete","goToUpdateSprintTask");
					return false;	
				}
				
			}
			var sdArray=startDate.split('-');
			var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
			var edArray=endDate.split('-');
			var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
			var d1=new Date(sd);
			var d2=new Date(ed);
			if(d1 > d2){   
						   parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
						   $('#startDateSprint').css("border","1px solid red"); $('#endDateSprint').css("border","1px solid red");
						   return false;
					   }
			else{
				  $('#startDateSprint').css("border","");$('#endDateSprint').css("border",""); 
			    }
		
	 var type=$("#type").val(); 
	 var typeId=$("#proj_ID").val();
	 var taskDateXml=$('#taskDateXml').val();				
	 var totalHour=$('#taskTotalEhour').val();
	 var totalMinute=$('#taskTotalEminute').val();
	 var budgetedHour=$('#estimatedHour').val();		
	 var budgetedMinute=$('#estimatedMinute').val();
	 sprintHourVal=budgetedHour;
	 sprintMinuteVal=budgetedMinute;
	 var hrStatus=testEstimatedHour();
	 var isiPad = navigator.userAgent.match(/iPad/i) != null;
	if(!selTaskEmailId)
		selTaskEmailId = ',';	
		
	if(hrStatus){
		$('#sprintTaskEhourPopupDiv').html('');
		if(totalHour!= sprintHourVal || totalMinute != sprintMinuteVal ){
				 if(resetDateValidate!='1'){
				// 		$('#sprintTaskEhourPopupDiv').html('');
						getDateXmlSprint();	
						taskDateXml	=$('#taskDateXml').val();
						totalHour=$("#taskTotalEhour").val();	
						totalMinute=$("#taskTotalEminute").val();			
					//	$("#taskTotalEhour").val(totalHour);
					//	$("#taskTotalEminute").val(totalMinute);
				}
					    doResetTaskDates();
						totalHour=$("#taskTotalEhour").val();	
						totalMinute=$("#taskTotalEminute").val();		
					
			}
		deletedUserID = deletedUserID.substring(0, deletedUserID.length - 1);
		parent.$("#loadingBar").show();
		parent.timerControl("start");
	   	$.ajax({
		   	url: Path+"/calenderAction.do",
			type:"POST",
			data:{act:"createTaskSprint", 
					  taskName:taskName, 
					  startDate:startDate,
					  endDate:endDate,
					  startTime:startTime,
					  endTime:endTime,
					  taskSprintGroupId:taskSprintGroupId,
					  taskSprintId:taskSprintId,
					  comments:comments,
					  selectedTaskEmailId:selTaskEmailId,
					  aTaskProj:taskProjId,
					  taskId:taskId,
					  ideaID:epicID,
					  projId:typeId,type:type,
					  deletedUserID:deletedUserID,
					  taskRemainder:taskRemainder,
					  taskRemainderBy:taskRemainderBy,
					  viewPlace:"Epic",
					  taskDateXml:taskDateXml,
					  prevStartDate:tmpSDate,
					  prevEndDate:tmpEDate,
					  totalHour:totalHour,
					  totalMinute:totalMinute,
					  budgetedHour:budgetedHour,
					  budgetedMinute:budgetedMinute,
					  isiPad:isiPad,
					  type:'History'
					 },
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
	            parent.checkSessionTimeOut(result);
	            $("#taskEventName").val("");
				$("#startDateSprint").val("");
				$("#endDateSprint").val("");
				$("#taskEventDescription").val("");
				$("#participantEmail").html("&nbsp;");
				$('#taskSprintGroup').val("");
				$('#taskSprint').val("");
				$('#taskID').val("");
				selectedTaskemailId=',';
		        deletedUserID=',';
		        taskDateXml="";
		        parent.$("#loadingBar").hide();
		        parent.timerControl("");
		        contentDataSprint="complete";
				closePopup();
				if(sortTaskType == 'assignedTasks') {
					showAssignedTaskList();
				}   
			}
			
		});
	}	  
 }
 function goToUpdateSprintTask(){
 	contentDataSprint="incompleteData";
 	updateSprintTask(setIdSprint);
 }
  function testEstimatedHour(){		
  	if(tHour != $('#taskTotalEhour').val() || tMinute!=$('#taskTotalEminute').val()){
		  	if(tHour != $('#taskTotalEhour').val() && tMinute!=$('#taskTotalEminute').val()){	
						parent.confirmReset(getValues(customalertData,"Alert_Reset_Data"),'reset','resetDateConfirmHourMinute','resetDateCancel');
						tHour=$('#taskTotalEhour').val();
						tMinute=$('#taskTotalEminute').val();
				/*		if(textTaskDiv!=""){		
							$("#taskEhourPopupDiv").html(textTaskDiv);
						}
						textSprintDiv = $('#sprintTaskDateHourDiv').html();
						$('#sprintTaskDateHourDiv').html('');*/
					}else if(tHour != $('#taskTotalEhour').val()){	
						parent.confirmReset(getValues(customalertData,"Alert_Reset_Data"),'reset','resetDateConfirm','resetDateCancel');
						tHour=$('#taskTotalEhour').val();
					}else if(tMinute!=$('#taskTotalEminute').val()){
						parent.confirmReset(getValues(customalertData,"Alert_Reset_Data"),'reset','resetDateConfirmMin','resetDateCancelMin');
						tMinute=$('#taskTotalEminute').val();
					}
					return false;
			}
			else{
				return true;
			}
/* var flag=true;
   var b_hr=$('#taskTotalEhour').val();
   var e_hr=$('#estimatedHour').val();
   var b_min=$('#taskTotalEminute').val();
   var e_min=$('#estimatedMinute').val();				
  if( e_hr!=0 && e_min!=0 && b_hr!=0 && b_min!=0){
    	if( b_hr!=e_hr && b_min!=e_min ){ 
	   		 confirmId=e_hr;
	   		 confirmIdmin=e_min;
	   		 parent.confirmReset(getValues(customalertData,"Alert_Reset_Data"),'reset','resetDateConfirmHourMinute','resetDateCancel');
	   		// parent.confirmReset("Your Budgeted minute and Estimated minute , Budgeted hour and Estimated hour is not matching do you want to override budgeted minute and hour?",'reset','confirmTaskHourMinuteDivClose','showEpicTaskBurndownPopup');
	   		 saveTaskFlag=true;
			 flag=false;
			 alertCheckSprint="1";
		}
    
    }else if(e_hr!=0 || e_min!=0){
	   if(b_hr!=e_hr || b_min!=e_min ){
	    
		    if(b_hr!=e_hr ){
			     confirmId=e_hr;			
			     parent.confirmReset(getValues(customalertData,"Alert_Unmatch_BudgetedHr"),'reset','confirmTaskHourDivClose','showEpicTaskBurndownPopup');
			     saveTaskFlag=true;
			     flag=false;
		     }
		     if(b_min!=e_min){
			     confirmIdmin=e_min;
			     parent.confirmReset("Your Budgeted minute and Estimated minute is not matching do you want to override budgeted minute?",'reset','confirmTaskMinuteDivClose','showEpicTaskBurndownPopup');
			     saveTaskFlag=true;
			     flag=false;
		     }
		     
	   }
   }else{
     saveTaskFlag=false;
   }				*/
/*   if(e_hr!=0 || e_min!=0){
	   if(b_hr!=e_hr || b_min!=e_min ){
	    
	    if(b_hr!=e_hr ){
	     confirmId=e_hr;			
	     parent.confirmReset(getValues(customalertData,"Alert_Unmatch_BudgetedHr"),'reset','confirmTaskHourDivClose','showEpicTaskBurndownPopup');
	     saveTaskFlag=true;
	     flag=false;
	     }
	     if(b_min!=e_min){
	     confirmIdmin=e_min;
	     parent.confirmReset("Your Budgeted minute and Estimated minute is not matching do you want to override budgeted minute?",'reset','confirmTaskMinuteDivClose','showEpicTaskBurndownPopup');
	     saveTaskFlag=true;
	     flag=false;
	     }
	     
	   }
   }else{
     saveTaskFlag=false;
   }
   
   */
 //  return flag;
 }
	
	

function deleteEvent(taskId,type) {			/*Delete Task */
	tskId = taskId;
	tskType = type;
	parent.confirmFun(getValues(customalertData,"Alert_Task_Delete"),"delete","confirmEventDel");
	calendar.fullCalendar('refetchEvents');
	$(".depTaskShowIcon").hide();
}
function confirmEventDel(){    /*Delete Task */
	parent.confirmReset(getValues(customalertData,"Alert_ConfirmationMail"),'reset','sendConfirmation','noConfirmation');
	
}
function sendConfirmation(){
	deleteTask('yes');
}
function noConfirmation(){
	deleteTask('no');
}
function deleteTask(confirmation){			
	parent.$('#loadingBar').show();			
	parent.timerControl("start");
	
	$.ajax({ 
          url: Path+'/calenderAction.do',
      	  type:"POST",
	      data:{taskId:tskId,taskType:tskType, act:'deleteTask',confirmation:confirmation},
	      error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
	      success:function(result){				
	      	 closePopup();
	       	 parent.checkSessionTimeOut(result);
	       	 eveId = result.split('#@#@')[0];
	       	 $(".depTaskShowIcon").hide();
	         $("#AssignedtaskList").find("[taskid='" + eveId + "']").parent('div').hide();
	       	 var assignedWFTaskDatas = result.split('#@#@')[1];
	       	 var myWFTaskDatas = result.split('#@#@')[2];
	       	 var historyWFDatas = result.split('#@#@')[3];
	       	 $('#myTasksList').html('');
	       	 $('#assignedTasksList').html('');
	       	 $('#historyListTasks').html('');
	       	 calendarViewDatas(myWFTaskDatas,assignedWFTaskDatas,historyWFDatas);
	       	 parent.$('#loadingBar').hide();
	       	 parent.timerControl("");
	       	 calendar.fullCalendar('removeEvents', eveId);
	         calendar.fullCalendar('refetchEvents');
	         
	        if($('.myTask').length == 0){$('#myTasksList').append('<div id="innerDiv3"  style="color:#ffffff;margin-top:25px;font-size:15px;font-family:icon;">No My Task</div>');}
	         if($('.assignedTask').length == 0){$('#assignedTasksList').append('<div id="innerDiv4"  style="color:#ffffff;margin-top:25px;font-size:15px;font-family:icon;">No Assigned Task</div>');}
			 if($('.historyTask').length == 0){$('#historyListTasks').append('<div id="innerDiv5"  style="color:#ffffff;margin-top:25px;font-size:15px;font-family:icon;">No History Task</div>');}
		}
   });
  	calendar.fullCalendar('refetchEvents');
}
/*	
function removeUpdateParticipant(obj,email,uId,taskId) {	// Remove Users while update 
	userEmailId = email;
	delUserId = uId;
	delTaskId = taskId;
	objVal = obj;
	parent.confirmFun(getValues(customalertData,"Alert_Delete_User"),"delete","clearCreateIdea");
}
	
function clearCreateIdea(){

	checkType='removeAll';
	$('#userUnCheck_'+delUserId).removeClass();
	$('#userUnCheck_'+delUserId).addClass('userUnCheck');
	$('#participant_'+delUserId).css('display','block');
	var deleteId = userEmailId+"_"+delUserId;
	removeEmailId = removeEmailId+deleteId+',';	
	$('#emailPrtcpnt_'+delUserId).remove();
	$('#userComments_'+delUserId+',#documentSection_'+delUserId+',#userLinks_'+delUserId).remove();
	
	$('#participant_'+delUserId).css('display','block');
	$('#wfUserListContainer').find('#participant_'+delUserId).css('display','block');
	if($('#participantEmail.mCustomScrollbar').length) {
		$("#participantEmail").mCustomScrollbar("update");
		$('#userListContainer').mCustomScrollbar("update");
	}
	if($('#wfParticipantEmail.mCustomScrollbar').length) {
		$("#wfParticipantEmail").mCustomScrollbar("update");
		$('#wfUserListContainer').mCustomScrollbar("update");
	}
	if($("[id^=participant_]").is(':visible')){$("[id^=inner]").hide();}else{ $("[id^=inner]").show();}
}
*/
function removeEventParticipant(obj,email,uId,taskId) {	/* Remove Users while update */
	userEmailId = email;
	delUserId = uId;
	delTaskId = taskId;
	objVal = obj;
	parent.confirmFun(getValues(customalertData,"Alert_Delete_User"),"delete","clearCreateEvent");
}
function clearCreateEvent(){
	checkType='removeAll';$("[id^=innerD]").hide();
	$('#userUnCheck_'+delUserId).removeClass().addClass('userUnCheck');
	$('#participant_'+delUserId).css('display','block');
	var deleteId = userEmailId+"_"+delUserId;
	removeEmailId = removeEmailId+deleteId+',';	
	var r='email_'+delUserId;
	$('#'+r).remove();
	$('#participant_'+delUserId).css('display','block');
			parent.$('#loadingBar').hide();
			parent.timerControl("");
		if($('#participantEmail.mCustomScrollbar').length) {
			$("#participantEmail").mCustomScrollbar("update");
			$('#userListContainer').mCustomScrollbar("update");
	    }
		if($("[id^=participant_]").is(':visible')){
			$("[id^=innerDiv]").hide();
		}else{
			$("[id^=innerDiv]").show();
		}
}	
	
  function removeTaskUpdateParticipant(userId) {			
  sprintUId = userId;
  	parent.confirmFun(getValues(customalertData,"Alert_Delete_User"),"delete","deleteSprintUser");
}
function deleteSprintUser(){
	$('#emailPrtcpnt_'+sprintUId).remove();
	$('#userComments_'+sprintUId).remove();
	 if(sprintUId){
       deletedUserID =deletedUserID+sprintUId+',';
     }
 
     $('#participant_'+sprintUId).css('display','block');
     if($('#ideaTaskParticipantsDiv.mCustomScrollbar').length) {
		$("#ideaTaskParticipantsDiv").mCustomScrollbar("update");
		$('#ideaUsersDiv').mCustomScrollbar("update");
	}
}

function removeOtherPrtcipant(obj,email){
	otherObj=$(obj).parent().attr('id');
	deleteId = email;
	parent.confirmFun(getValues(customalertData,"Alert_Delete_User"),"delete","clearCreateOtherEvent");
}
function clearCreateOtherEvent(){
	removeotherEmailId = removeotherEmailId+deleteId +',';$("#innerDiv").hide();
	$('#'+otherObj).remove();
	if($('#participantEmail.mCustomScrollbar').length) {
		$("#participantEmail").mCustomScrollbar("update");
		$('#userListContainer').mCustomScrollbar("update");
	}
}

function removeNewParticipant(obj,email,uId,taskId) {	/* Remove Users while update */
	checkType='removeAll';
	var id = $(obj).parent().parent().attr('id');
	$('#'+id).remove();
	$('#participant_'+uId).css('display','block');
	$('#particpantName_'+uId).css('display','block');
	var deleteId = email+"_"+uId;
	selectedTaskemailId = selectedTaskemailId.substring(0, selectedTaskemailId.indexOf(',' +deleteId)) + selectedTaskemailId.substring(selectedTaskemailId.indexOf(',' +deleteId) + deleteId.length + 1, selectedTaskemailId.length);
	$('#'+id).remove();
	$('#userComments_'+uId+',#documentSection_'+uId+',#userLinks_'+uId).remove();
	$('#participant_'+uId).css('display','block');
	$('#userUnCheck_'+uId).attr('class','userUnCheck');if($("[id^=participant_]").is(':visible')){$("[id^=inner]").hide();}else{$("[id^=inner]").show();}
	$('#wfParticipantEmail').find('#emailPrtcpnt_'+id).remove();
	if($('#ideaTaskParticipantsDiv.mCustomScrollbar').length) {
		$("#ideaTaskParticipantsDiv").mCustomScrollbar("update");
		$('#userListContainer').mCustomScrollbar("update");
	}
	if($('#participantEmail.mCustomScrollbar').length) {
		$("#participantEmail").mCustomScrollbar("update");
		$('#userListContainer').mCustomScrollbar("update");
	}
	if($('#wfParticipantEmail.mCustomScrollbar').length) {
		$("#wfParticipantEmail").mCustomScrollbar("update");
		$('#wfUserListContainer').mCustomScrollbar("update");
	}
}	

/*function checkParticipant(obj) {
	var classObj = $(obj).attr('class');
	if(classObj == 'userCheck') {
		$(obj).removeClass('userCheck');
		$(obj).addClass('userUnCheck');
		if($('div.participants .userCheck').length < 1) {
			$('.addParticipant').css('display','none');
			$('.createEvent').css('display','block');
			$('.createEvent1').css('display','none');
		}	
		else
			$('.addParticipant').css('display','block');		
	}	
	else {
		$(obj).removeClass('userUnCheck');
		$(obj).addClass('userCheck');
		$('.addParticipant').css('display','block');	
		$('.createEvent').css('display','none');
		$('.createEvent1').css('display','block');
	}
}*/
	
function scrollBar(id) {					
	if($('#'+id+'.mCustomScrollbar').length)
		$('#'+id).mCustomScrollbar("destroy");
  	$('#'+id).mCustomScrollbar({
	  scrollButtons:{
		  enable:true
	  }
    });		
    $('#'+id+' .mCSB_container').css('margin-right','15px');
    if(id=='participantEmail')
    	$('.content_1 .mCustomScrollBox .mCSB_scrollTools').css('margin','0%');
    $('#'+id+  ' .mCSB_scrollTools .mCSB_buttonUp').css({'background-position':'-80px 0px','opacity':'0.6'});
	$('#'+id+  ' .mCSB_scrollTools .mCSB_buttonDown').css({'background-position':'-80px -20px','opacity':'0.6'});
	$('#'+id+  ' .mCSB_scrollTools .mCSB_draggerRail').css('background','#B1B1B1');
	$('#'+id+  ' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','#6C6C6C');
    $('#'+id+  ' .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','#B1B1B1');
    $('#'+id+  ' .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','#B1B1B1');  
}

function taskIncludeClicked(obj){
		var cId = $(obj).attr('id');
		var flag = document.getElementById(cId).checked;
		if(!flag){
			//$(obj).parents('div.taskRows').hide().removeClass('wFactive').addClass('wFinactive');
			$(obj).parents('div.taskRows').show().removeClass('wFactive').addClass('wFinactive');
			var stepIndex = $(obj).attr('id').split('_')[1];
			$('#crumbs').find('a[stepIndex='+stepIndex+']').addClass('grayedOut').children('span').css('opacity','0.5');
			var disableLayer = '<div class="disableLayer" style="position: absolute; width: 100%; margin-top: 30px; background-color: rgb(255, 255, 255); opacity: 0.3; height: 76%; z-index: 1;"></div>';
			$('#wFContents_'+stepIndex).append(disableLayer);
		}else{
			$(obj).parents('div.taskRows').show().removeClass('wFinactive').addClass('wFactive');
			var stepIndex = $(obj).attr('id').split('_')[1];
			$('#crumbs').find('a[stepIndex='+stepIndex+']').removeClass('grayedOut').children('span').css('opacity','');
			$('#wFContents_'+stepIndex).find('div.disableLayer').remove();
		}
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad)
			$("div#taskWFDetails").mCustomScrollbar("update"); 
 }



 function showTaskHoursPopup() {									
 	if(parId == ''){
	 	if(tmpSDate==startDate && tmpEDate==endDate && tHour == $('#totalHour').val() ){
		}else{
		     	flag1=0;
		     }
		     
	     if(tmpSDate==startDate && tmpEDate==endDate && totalMinute == $('#totalMinute').val()){
	     }else{
	     	flag1_min=0;
	     }
	     
	     
	     
     }else if(parId == 'assignedSprint'){  			
     	if(tmpSDate==startDate && tmpEDate==endDate && tHour == $('#taskTotalEhour').val()){
     		flag1=1;
	     }else{
	     	flag1=0;
	     }
	      
	     if(tmpSDate==startDate && tmpEDate==endDate && tMinute == $('#taskTotalEminute').val()){
     		flag1_min=1;
	     }else{
	     	flag1_min=0;
	     }
	    
	     
     }
     else{
     	if(tmpSDate==startDate && tmpEDate==endDate && tHour == $('#totalHour').val() ){
     		flag1=1;
	     }else{
	     	flag1=0;
	     }
	     
	    
	     if(tmpSDate==startDate && tmpEDate==endDate && tMinute == $('#totalMinute').val()){
     		flag1_min=1;    
     		
	     }else{
	         
	     	flag1_min=0;
	     }
	     
	 
	     
     }
 	if(parId != 'assignedSprint') {
 		startDate=$('#startDateDiv').val();
	    endDate=$('#endDateDiv').val();
	    if(startDate == "" || startDate == "Date" || startDate == null){
			$('#startDateDiv').css("border","1px solid red");
			return false;		
		}else{
		    $('#startDateDiv').css("border","1px solid #BFBFBF");
		}
		if(endDate == "" || endDate == "Date" || endDate == null){
			$('#endDateDiv').css("border","1px solid red");
			return false;		
		}else{
		    $('#endDateDiv').css("border","1px solid #BFBFBF");
		}
			var sdArray=startDate.split('-');
			var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
			var edArray=endDate.split('-');
			var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
			var d1=new Date(sd);var d2=new Date(ed);
			if(d1 > d2){parent.alertFun(getValues(companyAlerts,"Alert_InvalidDate"),'warning');
					  $('#startDateDiv').css("border","1px solid red"); $('#endDateDiv').css("border","1px solid red");
					   return false;
						}
			else{	  $('#startDateDiv').css("border","");$('#endDateDiv').css("border","");  }
	     if($.isNumeric($('#totalHour').val()) == false){
         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
 		       $('#totalHour').css('border','1px solid red');
 		       return null;
         }
         else
         	$('#totalHour').css('border','1px solid #BFBFBF');
         	
         	
         if($.isNumeric($('#totalMinute').val()) == false){
         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
 		       $('#totalMinute').css('border','1px solid red');
 		       return null;
         }
         else
         	$('#totalMinute').css('border','1px solid #BFBFBF');	
         	
         if($('#totalMinute').val() > 59 || $('#totalMinute').val()<0){
			   		   parent.alertFun(getValues(companyAlerts,"Alert_MinValue"),'warning');
		 		       return null;
			   
  		 }
  		 else
         	$('#totalMinute').css('border','1px solid #BFBFBF');	
         	
         	
     }
     else {
     	startDate=$('#startDateSprint').val();
	    endDate=$('#endDateSprint').val();
	    if(startDate == "" || startDate == "Date" || startDate == null){
			$('#startDateSprint').css("border","1px solid red");
			return false;		
		}else{
		    $('#startDateSprint').css("border","1px solid #BFBFBF");
		}
		if(endDate == "" || endDate == "Date" || endDate == null){
			$('#endDateSprint').css("border","1px solid red");
			return false;		
		}else{
		    $('#endDateSprint').css("border","1px solid #BFBFBF");
		}
			var sdArray=startDate.split('-');
			var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
			var edArray=endDate.split('-');
			var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
			var d1=new Date(sd);var d2=new Date(ed);
			if(d1 > d2){parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
					  $('#startDateSprint').css("border","1px solid red"); $('#endDateSprint').css("border","1px solid red");
					   return false;
					   }
			else{	  $('#startDateSprint').css("border","");$('#endDateSprint').css("border","");  }
     }
     if(parId == 'assignedTasks' || parId == 'assignedDocument' || parId == 'assignedWorkflow' || parId == 'assignedSprint' || parId == 'assignedIdea' || parId ==  'assignedWorkspaceDocument') { 
		    if(parId == 'assignedSprint') {					
	     	 	var prevStartDate=$("#prevStartDate").val().trim(); 
				var prevEndDate=$("#prevEndDate").val().trim();
				if(prevStartDate!='' || prevEndDate!='' ){
		            if(prevStartDate==startDate && prevEndDate==endDate ){
		            	if(flag1 == 0 && flag1_min == 0){		
		            			parent.confirmReset(getValues(companyAlerts,"Alert_BudgetedMinHrMin"),'reset','resetDateConfirm','resetDateCancel');
		            	
		            	}
		            	else if(flag1 == 0){							
		            		parent.confirmReset(getValues(customalertData,"Alert_Unmatch_BudgetedHr"),'reset','resetDateConfirm','resetDateCancel');
		            	}
		                else if(flag1_min == 0){								
		            		parent.confirmReset(getValues(companyAlerts,"Alert_Unmatch_BudgetedMin"),'reset','resetDateConfirmMin','resetDateCancel');
		            	}
		            	else{		
		           /* 	if(textSprintDiv!=""){
		            			   $('#sprintTaskDateHourDiv').html(textSprintDiv);
		            			}	*/
			                  $('#sprintTaskEhourPopupDiv').show();
				             if($('#sprintTaskDateHourDiv').attr('class')){
				                
			                  }else{
			                     if(!isiPad){
				                  scrollBar('sprintTaskDateHourDiv');
				                 } 
				             }
				             if($('#sprintTaskDateHourDiv').find('div.mCSB_container').children('div.estimatedHourDiv').length<1){
				               loadStoryDate(parId);				
				             }else{
					             var estimatedHour=$('#estimatedHour').val(); 
					             var estimatedMinute=$('#estimatedMinute').val();
					             if(estimatedHour=='0'){
						             var datesLength;
						             if(!isiPad){
						               datesLength= $('#sprintTaskDateHourDiv .mCSB_container').children('div.estimatedHourDiv').length;
						             }else{
						               datesLength= $('#sprintTaskDateHourDiv').children('div.estimatedHourDiv').length;
						             }
						             var hourPerDay=parseInt(totalHr)/parseInt(datesLength);
						             hourPerDay=Math.round(hourPerDay);
						             if(hourPerDay==0){
						               hourPerDay=totalHr;
						             }
						             var i=0;
						             var j=datesLength-1;
						             var countHour=0;
						              $('.estimatedHourDiv').each(function() {
							                 if(i==j){
								                    var lastday=0;
								                    countHour=0;
								                    $('.estimatedHourDiv').each(function() {
								                      var hour = $(this).children('div').children('input.eHour').val().trim();
								                      if(hour==''){
													     hour='0';
													   }
								                      countHour=countHour+parseInt(hour);
								                    });
								                   lastday=parseInt(totalHr)-countHour;
								                    if(isNaN(lastday)){
								                        lastday=0;
								                    }
								                  $(this).children('div').children('input.eHour').val(lastday);
							                 }else{	
							                 if(isNaN(hourPerDay)){
							                	hourPerDay=0;
							                  }
							                       $(this).children('div').children('input.eHour').val(hourPerDay);
								                   countHour=countHour+hourPerDay;
								                   if(countHour>parseInt(totalHr)){
									                      hourPerDay=parseInt(totalHr)-[countHour-hourPerDay];
									                      if(hourPerDay<0){
									                        hourPerDay=0;
									                      }
								                   }else if(countHour==parseInt(totalHr)){
								                         hourPerDay=0;
								                   }else{
									                     var d=parseInt(totalHr)-countHour;
									                     if(d<hourPerDay){
									                        hourPerDay=d;
									                        countHour=parseInt(totalHr);
									                      }
								                   }
							                }
						                 i++;
						              });
					            }
					            
					            
					            if(estimatedMinute=='0'){
						             var datesLength;
						             if(!isiPad){
						               datesLength= $('#sprintTaskDateHourDiv .mCSB_container').children('div.estimatedHourDiv').length;
						             }else{
						               datesLength= $('#sprintTaskDateHourDiv').children('div.estimatedHourDiv').length;
						             }
						             var minutePerDay=parseInt(totalMin)/parseInt(datesLength);
						             minutePerDay=Math.round(minutePerDay);
						             if(minutePerDay==0){
						               minutePerDay=totalMin;
						             }
						             var i=0;
						             var j=datesLength-1;
						             var countMinute=0;
						              $('.estimatedHourDiv').each(function() {
							                 if(i==j){
								                    var lastday=0;
								                    countMinute=0;
								                    $('.estimatedHourDiv').each(function() {
								                      var minute = $(this).children('div').children('input.eMin').val().trim();
								                      if(minute==''){
													     minute='0';
													   }
								                      countMinute=countMinute+parseInt(minute);
								                    });
								                   lastday=parseInt(totalMin)-countMinute; 
								                   if(isNaN(lastday)){
								                   	  lastday=0;
								                   }
								                  $(this).children('div').children('input.eMin').val(lastday);
							                      }else{
							                      	if(isNaN(minutePerDay)){
							                      		minutePerDay=0;
							                      	}
							                       $(this).children('div').children('input.eMin').val(minutePerDay);
								                   countMinute=countMinute+minutePerDay;
								                   if(countMinute>parseInt(totalMin)){
									                      minutePerDay=parseInt(totalMin)-[countMinute-minutePerDay];
									                      if(minutePerDay<0){
									                        minutePerDay=0;
									                      }
								                   }else if(countMinute==parseInt(totalMin)){
								                         minutePerDay=0;
								                   }else{
									                     var d=parseInt(totalMin)-countMinute;
									                     if(d<minutePerDay){
									                        minutePerDay=d;
									                        countMinute=parseInt(totalMin);
									                      }
								                   }
							                }
						                 i++;
						              });
					            }
					           } 
			              }
			         }else{			
			             parent.confirmReset(getValues(customalertData,"Alert_Reset_Data"),'reset','resetDateConfirm','resetDateCancel');
			         }
			  }else{				
		            loadStoryDate(parId);
		      }
	     	}
    		else {
    			if(tmpSDate==startDate && tmpEDate==endDate){  
    				if(flag1 == '1' || flag1_min == '1'){					
	   					$.ajax({ 
					       url: Path+'/calenderAction.do',
					       type:"POST",
						   data:{act:'getAssignedDateDetails',myTaskId:myTaskId, parId:parId},
						   error: function(jqXHR, textStatus, errorThrown) {
					                checkError(jqXHR,textStatus,errorThrown);
					                $("#loadingBar").hide();
									timerControl("");
									}, 
						   success:function(result){
						   	parent.checkSessionTimeOut(result);
						   	$('#taskEhourPopupDiv').show();
						   	$('#taskDateHourDiv').html(result);			
						   	$('#taskDateSave').show();
						   	$('#wFTaskDateSave').hide();
						   	$('#checkUncheckDate').show();
						   	$('#taskDateHourDiv div.dTinactive').hide();
						   	var isiPad = navigator.userAgent.match(/iPad/i) != null;
		        			if(!isiPad)
						   		scrollBar('taskDateHourDiv');
						   }
				     	}); 
				    }
				    if(flag1 == '0' && flag1_min == '0'){
				   	  	parent.confirmReset(getValues(companyAlerts,"Alert_BudgetedMinHrMin"),'reset','assignedDateResetHourMinute','resetDateCancelHourMinute');
				   	}		
				    else if(flag1 == '0'){				
				    	parent.confirmReset(getValues(customalertData,"Alert_Unmatch_BudgetedHr"),'reset','assignedDateReset','resetDateCancel');
				    }
				    else if(flag1_min == '0'){			
				    	parent.confirmReset(getValues(companyAlerts,"Alert_Unmatch_BudgetedMin"),'reset','assignedDateResetMin','resetDateCancelMin');
				    }	
				}else if(prevStartDate==startDate && prevEndDate==endDate && serverHour != tHour){
			         	$('#taskEhourPopupDiv').show();	
			         	$('#taskDateHourDiv div.dTinactive').hide();
			    }
			    else {
			    	if(tmpSDate!=startDate || tmpEDate!=endDate || tHour == $('#totalHour').val()){
			    		updateDates=="yes";
			    		parent.confirmReset(getValues(customalertData,"Alert_Reset_Date"),'reset','assignedDateReset','resetDateCancel');
			    	}else{
		         		 parent.confirmReset(getValues(customalertData,"Alert_Reset_Data"),'reset','resetDateConfirm','resetDateCancel');
		         	}
		         	
		         	if(tmpSDate!=startDate || tmpEDate!=endDate || tMinute == $('#totalMinute').val()){
		         	
		         		
			    		parent.confirmReset(getValues(customalertData,"Alert_Reset_Date"),'reset','assignedDateReset','resetDateCancel');
			    	}else{		
		         		 parent.confirmReset(getValues(customalertData,"Alert_Reset_Data"),'reset','resetDateConfirmMin','resetDateCancel');
		         	}
		         	
		         	
		         	
		         		 
			    } 	
		     }  
	    }
	    else {
	     if(parId != 'assignedWorkflow'){
	     	$('#wFTaskDateSave').hide();
	     	$('#taskDateSave').show();
	     }
	     else{
	     	$('#wFTaskDateSave').show();
	     	$('#taskDateSave').hide();
	     }
	     $('#taskEhourPopupDiv').show();
	     tHour = $('#totalHour').val();
	     tMinute = $('#totalMinute').val();
	     if(flag1 == '0')
	     	createHourPopup(startDate,endDate,tHour,tMinute);
	     else
	     	$('#taskEhourPopupDiv').show();	
	     	
	     	
	     if(flag1_min == '0')
	     	createHourPopup(startDate,endDate,tHour,tMinute);
	     else
	     	$('#taskEhourPopupDiv').show();		
	     	
	    }
	}


	
 function resetDateConfirm(){				
 	$('div#ddList4').children('span').text(selectTitle);
 	$('div#ddList3').children('span').text(selectTitle);
    $('#taskDateHourDiv').empty();
    $('#taskEhourPopupDiv').show();
    $('#wFTaskDateSave').hide();
    $('#taskDateSave').show();
	prevSDate = startDate;
    prevEDate = endDate;
    if(parId == 'assignedSprint'){
    	tHour = $('#taskTotalEhour').val();
    	tMinute = $('#taskTotalEminute').val();
    	 startDate = $('#startDateSprint').val();
	  	 endDate = $('#endDateSprint').val();
    	
    	}
    else{
    	tHour = $('#totalHour').val();
    	}
    createHourPopup(startDate,endDate,tHour,tMinute);
    
    if(parId != 'assignedSprint'){				
	   	$.ajax({
	   		url: Path+'/calenderAction.do',
	   		type:"POST",
	   		data:{act:'resetTaskDates',myTaskId:myTaskId, parId:parId},
	   		error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
	   		success:function(result){
	   			parent.checkSessionTimeOut(result);
	   		}
	   	});
    }
 }
 function resetDateConfirmHourMinute(){
 	$('div#ddList4').children('span').text(selectTitle);
 	$('div#ddList3').children('span').text(selectTitle);
    $('#taskDateHourDiv').empty();
    $('#taskEhourPopupDiv').show();
    $('#wFTaskDateSave').hide();
    $('#taskDateSave').show();
	prevSDate = startDate;
    prevEDate = endDate;
    if(parId == 'assignedSprint'){
    	tHour = $('#taskTotalEhour').val();
    	tMinute = $('#taskTotalEminute').val();
    	 prevSDate = $('#startDateSprint').val();		
	  	 prevEDate = $('#endDateSprint').val();			
	  	 startDate = $('#startDateSprint').val();
	  	 endDate = $('#endDateSprint').val();			
	//  	 $('input#taskTotalEhour').val(confirmId);
//  	 $('input#taskTotalEminute').val(confirmIdmin); 
    }
    else{
    	tHour = $('#totalHour').val();
    	tMinute = $('#totalMinute').val();
    	}
    createHourPopup(startDate,endDate,tHour,tMinute);
 //   doResetTaskDates();			
 	
  /*  if(resetDateValidate='1'){
	  	$.ajax({
	   		url: Path+'/calenderAction.do',
	   		type:"POST",
	   		data:{act:'resetTaskDates',myTaskId:myTaskId, parId:parId},
	   		success:function(result){
	   			parent.checkSessionTimeOut(result);
	   		}
	   	});
   	}*/
 }
/*  function doResetTaskDates(){			
	 		$.ajax({
	   		url: Path+'/calenderAction.do',
	   		type:"POST",
	   		data:{act:'resetTaskDates',myTaskId:myTaskId, parId:parId},
	   		success:function(result){
	   			parent.checkSessionTimeOut(result);
	   		}
   	   });
 }
 */
 function resetDateConfirmMin(){				
 	$('div#ddList4').children('span').text(selectTitle);
 	$('div#ddList3').children('span').text(selectTitle);
    $('#taskDateHourDiv').empty();
    $('#taskEhourPopupDiv').show();
    $('#wFTaskDateSave').hide();
    $('#taskDateSave').show();
	prevSDate = startDate;
    prevEDate = endDate;
    if(parId == 'assignedSprint')
    	tMinute = $('#taskTotalEminute').val();
    else
    	tMinute = $('#totalMinute').val();
    createHourPopup(startDate,endDate,tHour,tMinute);
 /*  	$.ajax({
   		url: Path+'/calenderAction.do',
   		type:"POST",
   		data:{act:'resetTaskDates',myTaskId:myTaskId, parId:parId},
   		success:function(result){
   			parent.checkSessionTimeOut(result);
   		}
   	});*/
 }
 
 
 	
	
	

  function resetTDateConfirm(){
  	tHour = $('#totalHour').val();
  	tMinute = $('#totalMinute').val();
  	createHourPopup(startDate,endDate,tHour,tMinute);
  }
 var resetDate = 0;
 function assignedDateReset(){
 	resetDate=1;
 	$('#taskEhourPopupDiv').show();
    tHour = $('#totalHour').val();
    tMinute = $('#totalMinute').val();
 	createHourPopup(startDate,endDate,tHour,tMinute);
 }
 var resetDateMin = 0;
  function assignedDateResetMin(){
 	resetDateMIn=1;
 	$('#taskEhourPopupDiv').show();
    tMinute = $('#totalMinute').val();
 	createHourPopup(startDate,endDate,tHour,tMinute);
 }	
 var resetDateHourMinute=0;
 function assignedDateResetHourMinute(){
 	resetDateHourMinute=1;
 	$('#taskEhourPopupDiv').show();
    tHour = $('#totalHour').val();
    tMinute = $('#totalMinute').val();
 	createHourPopup(startDate,endDate,tHour,tMinute);
 }
/*   function createHourPopup(startDate,endDate,tHour,tMinute) {		
   	var minuteValue;
 	$('#taskDateHourDiv').html('');
 	tmpSDate = startDate;
 	tmpEDate = endDate;
 	var hoursUI = "";
     var strD=startDate.split("-");
	 var str_date=strD[2]+"-"+strD[0]+"-"+strD[1];
	 var strE=endDate.split("-");
	 var end_date=strE[2]+"-"+strE[0]+"-"+strE[1];
	 var tskDates = new Array();
	var tskMonth = new Array();
	var tskYear = new Array();
	var tskDays = new Array();
	var weekday=new Array(7);
	weekday[0]=day0;
	weekday[1]=day1;
	weekday[2]=day2;
	weekday[3]=day3;
	weekday[4]=day4;
	weekday[5]=day5;
	weekday[6]=day6;
	if(parId=='assignedSprint'){					
		totalHour = $('#taskTotalEhour').val();
		totalMinute=$('#taskTotalEminute').val();
		}
	else{	
		totalHour = $('#totalHour').val();
		totalMinute=$('#totalMinute').val();
		}
	var curHour=0;
	var curMinute=0;
	var start = new Date(str_date);
    var future = new Date(end_date);
    var range = []
    var  mil = 86400000 //24h
    var totalDay = (future-start)/mil +1;
    var hr_day=tHour/totalDay;
    var min_day=tMinute/totalDay;
    var hr_per_day=Math.round(hr_day);
    var min_per_day=Math.round(min_day);
    var totalMin=0;
    var flag=0;
     var flag_min=0;
    var j=future.getTime();
    if((totalHour>0) && (hr_per_day==0) ){
	   hr_per_day=totalHour;
	}
	if((totalMinute>0) && (min_per_day==0) ){
	   min_per_day=totalMinute;
	}
	for (var i=start.getTime(); i<=future.getTime();i=i+mil) {
			curHour = parseInt(curHour,10) + parseInt(hr_per_day,10);
			curMinute= parseInt(curMinute,10)+parseInt(min_per_day,10);
					
     		if(curHour < tHour) {
     			hr_per_day = hr_per_day;
     			if(i==j){
			  	 hr_per_day=tHour- (curHour - hr_per_day);
				} 
     		}
     		else {
     			if(flag == 0){				
      				curHour = parseInt(curHour,10) - parseInt(hr_per_day,10);
		       		hr_per_day = tHour - curHour;
		       		flag = 1;
      			}
	      		else{
	      			hr_per_day = 0;
	      		}	
	      	}
	     
	      		if(curMinute < tMinute) {
     				min_per_day = min_per_day;
	     			if(i==j){
				  	  min_per_day=tMinute- (curMinute - min_per_day);
					} 
     		    }
     		    else {
     				if(flag_min == 0){				
	      				curMinute = parseInt(curMinute,10) - parseInt(min_per_day,10);
			       		min_per_day = tMinute - curMinute;
			       		flag_min = 1;
      			}
	      		else{
	      			min_per_day = 0;
	      		}	
	      	}
			
	
	
	
	 //    	else{		
	 //    		if(curMin >= minuteValue){
	//     			min_per_day=min_per_day;
	//      			
	//      		}
	//      		else{	
	//      			min_per_day=minuteValue-curMin;
	  //    		}
	 //     	}
	      	
	      	
		 	tskDates[i] = new Date(i).getDate();
		    tskMonth[i] = new Date(i).getMonth()+1;
		    tskYear[i] = new Date(i).getFullYear();
		    tskDays[i] = new Date(i).getDay();
		    var month = tskMonth[i];
		    if(month < 10) {
		    	month = '0'+month;
		    }
		    if(tskDates[i] < 10) {
		    	tskDates[i] = '0'+tskDates[i];
		    }
		    var day = weekday[tskDays[i]]; 
		    if(parId != 'assignedSprint') {
			    $('#checkDates').show();
		    	$('#checkUncheckDate').show();
		    	hoursUI = "     <div class=\"estimatedHourDiv dTactive\" style=\"font-family:13px;\">"
			            +"<div style=\"width:50px;float:left;padding-top:8px;\" align=\"left\"><input id=\"check_"+i+"\"type=\"checkbox\" checked='true' onclick=\"dateIncludeClicked(this);\"/></div>"
						+"        <div style=\"width:100px;float:left;padding-top:5px;\" align=\"left\"><span class='taskDate' style='margin-left:10px;font-weight:normal;'>"+tskDates[i]+"/"+month+"/"+tskYear[i]+"</span></div>"
						+"        <div style=\"width:100px;float:left;padding-top:5px;\" align=\"left\"><span class='taskDay' style='margin-left:10px;font-weight:normal;'>"+day+"</span></div>"
						+"        <div style=\"width:70px;float:left;padding-top:5px;\" align=\"left\"><input type='text' class='eHour' style='margin-left:25px;width:50px;border-radius:3px;border:1px solid #cccccc;height:18px;color:#848484;' onblur=\"checkNumerals(this);\" value=\""+hr_per_day+"\" /></div>"
						+"        <div style=\"width:70px;float:left;padding-top:5px;\" align=\"left\"><input type='text' class='eMin' style='margin-left:25px;width:50px;border-radius:3px;border:1px solid #cccccc;height:18px;color:#848484;' onblur=\"checkNumerals(this);\"  value=\""+min_per_day+"\" /></div>"
						+"        <input type=\"hidden\" class=\"edCheckbox\" value=\"Checked\" />"
						+"     </div>";
				$('#wFTaskDateSave').attr('onclick','getWFDateXml(stepVal);');
			}		
			else if(parId == 'assignedSprint') {
				$('#checkDates').hide();
				$('#checkUncheckDate').hide();
				hoursUI = "     <div class=\"estimatedHourDiv dTactive\" style=\"font-family:13px;\">"
						+"        <div style=\"width:100px;float:left;padding-top:5px;\" align=\"left\"><span class='taskDate' style='margin-left:10px;font-weight:normal;'>"+month+"-"+tskDates[i]+"-"+tskYear[i]+"</span></div>"
						+"        <div style=\"width:100px;float:left;padding-top:5px;\" align=\"left\"><span class='taskDay' style='margin-left:10px;font-weight:normal;'>"+day+"</span></div>"
						+"        <div style=\"width:70px;float:left;padding-top:5px;\" align=\"left\"><input type='text' class='eHour' style='margin-left:25px;width:50px;border-radius:3px;border:1px solid #cccccc;height:18px;color:#848484;' onblur=\"checkNumerals(this);\"  value=\""+hr_per_day+"\" /></div>"
						+"        <div style=\"width:70px;float:left;padding-top:5px;\" align=\"left\"><input type='text' class='eMin' style='margin-left:25px;width:50px;border-radius:3px;border:1px solid #cccccc;height:18px;color:#848484;' onblur=\"checkNumerals(this);\"  value=\""+min_per_day+"\" /></div>"
						+"        <input type=\"hidden\" class=\"edCheckbox\" value=\"Checked\" />"
						+"     </div>";
			}
		    $('#taskDateHourDiv').append(hoursUI);		
	    	if((totalHour>0) && (hr_per_day==0) && totalHour == curHour){
      			flag = 1;	
      		}
      		if((totalMinute>0) && (min_per_day==0) && totalMinute == curMinute){
      			flag_min = 1;	
      		}	
	 }
  var isiPad = navigator.userAgent.match(/iPad/i) != null;
  if(!isiPad)
  	scrollBar('taskDateHourDiv');
  	
}
*/
/*

 function resetDateCancel(){
 	resetDate = 0;
 	$('#startDateDiv').val(tmpSDate); 
    $('#endDateDiv').val(tmpEDate);
    $('#totalHour').val(tHour);
    $('#totalMinute').val(tMinute);
    if(parId == 'assignedSprint') {
    	$('#taskTotalEhour').val(sprintHourVal);
  	    $('#taskTotalEminute').val(sprintMinuteVal);
    }
    return null;
 }
 */
  function resetDateCancelMin(){
 	resetDateMin = 0;
 	$('#startDateDiv').val(tmpSDate); 
    $('#endDateDiv').val(tmpEDate);
    $('#totalMinute').val(tMinute)
    return null;
 }
 function resetDateCancelHourMinute(){
    resetDate = 0;
    resetDateMin = 0;
 	$('#startDateDiv').val(tmpSDate); 
    $('#endDateDiv').val(tmpEDate);
    $('#totalHour').val(tHour);
    $('#totalMinute').val(tMinute)
    return null;
 
 }
 function dateIncludeClicked(obj){
		var cId = $(obj).attr('id');
		var flag = document.getElementById(cId).checked;
		if(!flag){
			$(obj).parents('div.estimatedHourDiv').hide().removeClass('dTactive').addClass('dTinactive').children('input.edCheckbox').val("Unchecked");
			$("#taskDateHourDiv").mCustomScrollbar("update");
		}else{
			$(obj).parents('div.estimatedHourDiv').show().removeClass('dTinactive').addClass('dTactive').children('input.edCheckbox').val("Checked");
			$("#taskDateHourDiv").mCustomScrollbar("update");
		}
	}
	
function dateEDclick(obj){
	var objId = $(obj).attr('id');
	if(objId == 'dht'){
		$('#taskDateHourDiv div.dTinactive').show();
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad)
			$("#taskDateHourDiv").mCustomScrollbar("update");
	}if(objId == 'hht'){
		$('#taskDateHourDiv div.dTinactive').hide();
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad)
			$("#taskDateHourDiv").mCustomScrollbar("update");
	}
}	

/*
 function hideTaskHourDiv(){
 	 estmHour=0;
 	 $('#sprintTaskEhourPopupDiv').css('display','none');
 	 $('#taskEhourPopupDiv').hide();
	 $('div.estimatedHourDiv').css('border','');
	hideDiv="1";
}*/
/*
 function getDateXml(){	
     taskDateXml = "<taskHour>";
     var i=1;
     flag1='1';
     flag1_min='1';
     estmHour=0;
     estmMinute=0;
  if(parId!='assignedSprint'){
		$('.estimatedHourDiv').each(function() {
			  var status = $(this).children('input.edCheckbox').val();
			  var date = $(this).children('div').children('span.taskDate').text().trim();
			  var day = $(this).children('div').children('span.taskDay').text().trim();
			  var hour = $(this).children('div').children('input.eHour').val().trim();
			  var minute=$(this).children('div').children('input.eMin').val().trim();
			  
		  if(!hour) {
		 	 hour = 0;
		  }
		  if(!minute) {
		 	 minute = 0;
		  }
		  if(status == 'Checked' || parId=='assignedSprint')
		  {		
			  	estmHour = parseInt(estmHour) + parseInt(hour);
			  	estmMinute=parseInt(estmMinute) + parseInt(minute);
		  }
		  else{
			  estmHour = parseInt(estmHour) + parseInt(hour);
			  estmMinute=parseInt(estmMinute) + parseInt(minute);
		  }		  
		  taskDateXml +="<taskDate id=\""+i+"\">"
		  taskDateXml +="<date>"+date+"</date>";
		  taskDateXml +="<day>"+day+"</day>";
		  taskDateXml +="<hour>"+hour+"</hour>";
		  taskDateXml +="<min>"+minute+"</min>";
		  taskDateXml +="<status>"+status+"</status>";				
		  if(parId == 'assignedTasks' || parId == 'assignedDocument' || parId == 'assignedWorkflow' || parId == 'assignedSprint' || parId == 'assignedIdea' || parId== 'assignedWorkspaceDocument') {
		  	taskDateXml +="<dateId>"+$(this).attr('id')+"</dateId>";
		  }
		  taskDateXml +="</taskDate>";
		  i++;
	 	});

 	}
 	else{
 		$('.estimatedHourDiv:visible').each(function() {
			  var status = $(this).children('input.edCheckbox:visible').val();
			  var date = $(this).children('div').children('span.taskDate:visible').text().trim();
			  var day = $(this).children('div').children('span.taskDay:visible').text().trim();
			  var hour = $(this).children('div').children('input.eHour:visible').val().trim();
			  var minute=$(this).children('div').children('input.eMin:visible').val().trim();
			  
		  if(!hour) {
		 	 hour = 0;
		  }
		  if(!minute) {
		 	 minute = 0;
		  }
		  if(status == 'Checked' || parId=='assignedSprint')
		  {		
			  	estmHour = parseInt(estmHour) + parseInt(hour);
			  	estmMinute=parseInt(estmMinute) + parseInt(minute);
		  }
		  else{
			  estmHour = parseInt(estmHour) + parseInt(hour);
			  estmMinute=parseInt(estmMinute) + parseInt(minute);
		  }		  
		  taskDateXml +="<taskDate id=\""+i+"\">"
		  taskDateXml +="<date>"+date+"</date>";
		  taskDateXml +="<day>"+day+"</day>";
		  taskDateXml +="<hour>"+hour+"</hour>";
		  taskDateXml +="<min>"+minute+"</min>";
		  taskDateXml +="<status>"+status+"</status>";				
		  if(parId == 'assignedTasks' || parId == 'assignedDocument' || parId == 'assignedWorkflow' || parId == 'assignedSprint' || parId == 'assignedIdea' || parId== 'assignedWorkspaceDocument') {
		  	taskDateXml +="<dateId>"+$(this).attr('id')+"</dateId>";
		  }
		  taskDateXml +="</taskDate>";
		  i++;
	 	});
 	}
 	
 	
	    if(estmMinute > 59 || estmMinute<0){
			   		   parent.alertFun("Minute value should be between 0 to 59.",'warning');
		 		//       $('#totalMinute').css('border','1px solid red');
		 		       return null;
			   
  		 }	
	 taskDateXml +="</taskHour>";				
	 $('#taskDateXml').val(taskDateXml);
	 var startDate=$('#startDateSprint').val();
     var endDate=$('#endDateSprint').val();
    if(parId == 'assignedSprint'){
		 $('#prevStartDate').val(startDate);
		 $('#prevEndDate').val(endDate);
		   $('input#taskTotalEhour').val(estmHour);
		  $('input#taskTotalEminute').val(estmMinute);
	 }
	 
	 $('#estimatedHour').val(estmHour);
	 $('#estimatedMinute').val(estmMinute);
	 $('#totalHour').val(estmHour);
	 $('#totalMinute').val(estmMinute);
	 tHour = estmHour;
	 tMinute=estmMinute;
	 totalHr=$('#estmatedHour').val();
	 totalMin=$('#estmatedMinute').val();
	 if(totalHr>0){
	   if(estmHour!=0 && estmHour!=totalHr){
	       confirmId=estmHour;
	       parent.confirmFun(getValues(customalertData,"Alert_Unmatch_BudgetedHr"),"close","confirmTaskHourDivClose");
	   }else if(estmHour!=0){
	     $('input#taskTotalEhour').val(estmHour);
	     hideTaskHourDiv(); 
	   }else{
	   	
	     hideTaskHourDiv();
	   }
	 }else{ 
	    $('input#taskTotalEhour').val(estmHour);
	    hideTaskHourDiv();
	 }
	 
	 if(totalMin>0){
	   if(estmMinute!=0 && estmMinute!=totalMin){
	       confirmIdmin=estmMinute;
	       parent.confirmFun("Your Budgeted minute and Estimated minute is not matching do you want to override budgeted minute?","close","confirmTaskMinuteDivClose");
	   }else if(estmMinute!=0){
	     $('input#taskTotalEminute').val(estmMinute);
	     hideTaskHourDiv(); 
	   }else{
	   	
	     hideTaskHourDiv();
	   }
	 }else{ 
	    $('input#taskTotalEminute').val(estmMinute);
	    hideTaskHourDiv();
	 }
	  resetDateValidate='1';				
// if(parId == 'assignedSprint'){
	// if($("#taskEhourPopupDiv").html()!=""){
	//		textTaskDiv = $("#taskEhourPopupDiv").html();
	///  }
	//$("#taskEhourPopupDiv").html('');
	//	$("#taskEhourPopupDiv").html('');
	//}
	 	 
	//$('#sprintTaskDateHourDiv').html(textSprintDiv);
	//if(textSprintDiv!=""){
	//	 textSprintDiv =  $('#sprintTaskDateHourDiv').html();	
	// }
	 
 }
 */
function getDateXmlSprint(){

	taskDateXml = "<taskHour>";
     var i=1;
     flag1='1';
     flag1_min='1';
     estmHour=0;
     estmMinute=0;
  //   $('#sprintTaskEhourPopupDiv').remove();
 // sprintTaskEhourPopupDiv)
   //  $('#sprintTaskDateHourDiv').is(':hidden')$('#sprintTaskDateHourDiv').removeClass(");
	//   $('#sprintTaskDateHourDiv').children('Div').removeClass("estimatedHourDiv");
//	$("#taskEhourPopupDiv").find('.estimatedHourDiv').each(function() {
	$('.estimatedHourDiv').each(function() {
		  var status = $(this).children('input.edCheckbox').val();
		  var date = $(this).children('div').children('span.taskDate').text().trim();
		  var day = $(this).children('div').children('span.taskDay').text().trim();
		  var hour = $(this).children('div').children('input.eHour').val().trim();
		  var minute=$(this).children('div').children('input.eMin').val().trim();
	  
	  if(!hour) {
	 	 hour = 0;
	  }
	  if(!minute) {
	 	 minute = 0;
	  }
	  if(status == 'Checked' || parId=='assignedSprint')
	  {		
		  	estmHour = parseInt(estmHour) + parseInt(hour);
		  	estmMinute=parseInt(estmMinute) + parseInt(minute);
	  }
	  else{
		  estmHour = parseInt(estmHour) + parseInt(hour);
		  estmMinute=parseInt(estmMinute) + parseInt(minute);
	  }		  
	  taskDateXml +="<taskDate id=\""+i+"\">"
	  taskDateXml +="<date>"+date+"</date>";
	  taskDateXml +="<day>"+day+"</day>";
	  taskDateXml +="<hour>"+hour+"</hour>";
	  taskDateXml +="<min>"+minute+"</min>";
	  taskDateXml +="<status>"+status+"</status>";				
	  if(parId == 'assignedTasks' || parId == 'assignedDocument' || parId == 'assignedWorkflow' || parId == 'assignedSprint' || parId == 'assignedIdea' || parId== 'assignedWorkspaceDocument') {
	  	taskDateXml +="<dateId>"+$(this).attr('id')+"</dateId>";
	  }
	  taskDateXml +="</taskDate>";
	  i++;
 	});
	    if(estmMinute > 59 || estmMinute<0){
			   		   parent.alertFun(getValues(customalertData,"Alert_MinValue"),'warning');
		 		//       $('#totalMinute').css('border','1px solid red');
		 		       return null;
			   
  		 }	
	 taskDateXml +="</taskHour>";				
	 $('#taskDateXml').val(taskDateXml);
	 var startDate=$('#startDateSprint').val();
     var endDate=$('#endDateSprint').val();
    if(parId == 'assignedSprint'){
		 $('#prevStartDate').val(startDate);
		 $('#prevEndDate').val(endDate);
		   $('input#taskTotalEhour').val(estmHour);
		  $('input#taskTotalEminute').val(estmMinute);
	 }
	 
	 $('#estimatedHour').val(estmHour);
	 $('#estimatedMinute').val(estmMinute);
	 $('#totalHour').val(estmHour);
	 $('#totalMinute').val(estmMinute);
	 tHour = estmHour;
	 tMinute=estmMinute;
	 totalHr=$('#estmatedHour').val();
	 totalMin=$('#estmatedMinute').val();
	 if(totalHr>0){
	   if(estmHour!=0 && estmHour!=totalHr){
	       confirmId=estmHour;
	       parent.confirmFun(getValues(customalertData,"Alert_Unmatch_BudgetedHr"),"close","confirmTaskHourDivClose");
	   }else if(estmHour!=0){
	     $('input#taskTotalEhour').val(estmHour);
	     hideTaskHourDiv(); 
	   }else{
	   	
	     hideTaskHourDiv();
	   }
	 }else{ 
	    $('input#taskTotalEhour').val(estmHour);
	    hideTaskHourDiv();
	 }
	 
	 if(totalMin>0){
	   if(estmMinute!=0 && estmMinute!=totalMin){
	       confirmIdmin=estmMinute;
	       parent.confirmFun(getValues(companyAlerts,"Alert_Unmatch_BudgetedHr"),"close","confirmTaskMinuteDivClose");
	   }else if(estmMinute!=0){
	     $('input#taskTotalEminute').val(estmMinute);
	     hideTaskHourDiv(); 
	   }else{
	   	
	     hideTaskHourDiv();
	   }
	 }else{ 
	    $('input#taskTotalEminute').val(estmMinute);
	    hideTaskHourDiv();
	 }
	 
	  resetDateValidate='1';	
	  
}
 function showEpicTaskBurndownPopup(){
 	showTaskHoursPopup();
 }
 /*
 function checkNumerals(obj){
                var intRegex = /^\d+$/;
		        var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
				var str = $(obj).val().trim();
		        if(str!=''){
			        if(intRegex.test(str) || floatRegex.test(str)) {
			           return true;
				    }else{	
		   		       parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
		   		       $(obj).css('border','1px solid red');
		   		       return null;
			   		}
			  }else{
			     return null;
			  }
  }
*/
function viewUserComments(taskId, type, user_id,obj) {		/* To view User Comments	*/
	$("#uOptions_"+user_id).hide();
	$('.linkSection').hide();			
	$('.documentSection').hide();
	$("#docIcon_"+user_id).attr('src','images/Idea/Document.png');
	$("#docIcon_"+user_id).attr('onclick','showTaskDocuments()');
	$('#cmtIcon_'+user_id+',.viewComments').attr('src',''+Path+'/images/calender/comments.png');
	
	$('#cmtIcon_'+user_id+',.viewComments').attr('src',''+Path+'/images/calender/comments.png');
	if ($('#userComments_'+user_id+' div.usrComments').is(':visible')) {
		$('#userComments_'+user_id).hide();
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad) {
	    	if($("#prtcpntContainer.prtcpntSection").find('div.mCSB_scrollTools').css('display') == 'block'){ 
	    		$("#prtcpntContainer.prtcpntSection").mCustomScrollbar("scrollTo","top");
	    		$("#prtcpntContainer.prtcpntSection").mCustomScrollbar("");	
	    	}
			if($("div.taskPrtcpntSection").find('div.mCSB_scrollTools').css('display') == 'block'){
				$("div.taskPrtcpntSection").mCustomScrollbar("scrollTo","top");
				$("div.taskPrtcpntSection").mCustomScrollbar("update");	 
			}
			if($("div#ideaTaskParticipantsDiv").find('div.mCSB_scrollTools').css('display') == 'block'){
				$("div#ideaTaskParticipantsDiv").mCustomScrollbar("scrollTo","top");
				$("div#ideaTaskParticipantsDiv").mCustomScrollbar("update");
			}
			
			if($("div#wfParticipantEmail").find('div.mCSB_scrollTools').css('display') == 'block'){
				$("div#wfParticipantEmail").mCustomScrollbar("scrollTo","top");
				$("div#wfParticipantEmail").mCustomScrollbar("update");
			}
			
		}	return null;
	}
	else {
		$('.commentsSec').hide();
		$('div[id^=cmtImgDiv_]').children('img').attr('src',''+Path+'/images/calender/comments.png');
		$('#cmtIcon_'+user_id+',#viewComments_'+user_id).attr('src',''+Path+'/images/Idea/comment1Toggle.png');
		
		parent.$("#loadingBar").show();
		parent.timerControl("start");
	   	$.ajax({
		   	url: Path+"/calenderAction.do",
			type:"POST",
			data:{act:"getUserComments",taskId: taskId, taskType: type, user_id : user_id},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				 parent.checkSessionTimeOut(result);
				 $('#userComments_'+user_id).html(result);
	            	if(parId == 'assignedTasks' || parId == 'assignedWorkflow' || parId == 'assignedDocument' || parId == 'assignedIdea' || parId == 'assignedIdea' || parId == 'assignedSprint' ) {
	            	$('#enterComment_'+user_id).hide();
	            	$('.usrComments').parent().css({'width':'480px','padding-left':'0px'});
	            }
	            if(isHistory=='yes') {
	            	$('#enterComment_'+user_id).hide();
	            	$('.usrComments').parent().css({'width':'575px','padding-left':'10px'});
	              }
	            if(user_id != UserId)
	            	$('#enterComment_'+user_id).hide();
	            	$('#userComments_'+user_id).show();
	           	
	            var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        	if(!isiPad) {
	        		if(parId == 'assignedTasks' || parId == 'assignedWorkflow' || parId == 'assignedDocument' || parId == 'assignedIdea') {
		            	$("div.taskPrtcpntSection").mCustomScrollbar("update");
		            	if($("div.taskPrtcpntSection").find('div.mCSB_scrollTools').css('display') == 'block'){ 
				        	$("div.taskPrtcpntSection").mCustomScrollbar("scrollTo","#userComments_"+user_id,{
		 						scrollInertia:0,
		 						callbacks:false
		 					});
						}	
		            }
		            if(parId == 'assignedSprint'){
		            	$("div#ideaTaskParticipantsDiv").mCustomScrollbar("update");
		            	if($("div#ideaTaskParticipantsDiv").find('div.mCSB_scrollTools').css('display') == 'block'){
			            	$("div#ideaTaskParticipantsDiv").mCustomScrollbar("scrollTo","#userComments_"+user_id,{
		 						scrollInertia:0,
		 						callbacks:false
		 					});
		 				}
		            }
		            else {
		            	$('#prtcpntContainer.prtcpntSection').mCustomScrollbar('update')
		            	if($("#prtcpntContainer.prtcpntSection").find('div.mCSB_scrollTools').css('display') == 'block'){ 
				        	$('#prtcpntContainer.prtcpntSection').mCustomScrollbar('scrollTo','#userComments_'+user_id,{
		 						scrollInertia:0,
		 						callbacks:false
		 					});
		 				}
					}
					setTimeout("$('.mCS_no_scrollbar').css('top','0px')",200);
		        }
		        else {
		        	var el = document.getElementById('userComments_'+user_id);
					el.scrollIntoView(true);
				}
				parent.$("#loadingBar").hide();
				parent.timerControl("");
				var i = $('.usrComments_'+user_id).length;
				if((type=="Task"||type=="WorkFlow" || type=="Document" || type=="Idea" || type=="Sprint" || type=="WorkspaceDocument") && (parId =="myTasks" || parId == "mySprint" || parId == "myIdea" || parId == "myWorkflow" || parId == "myDocument"  || parId == "myWorkspaceDocument") && user_id == UserId){
				$('[id^=commentTextarea_]').prop("disabled",false);
				}else{ $('[id^=commentTextarea_]').prop("disabled",true);}
				initCommentsCal(i);
			}
		});
	}
}

function userCommentDetail(obj,taskId,userId,type){			
    if ($(obj).parent().parent().next('div.userCmtDiv').is(":hidden")) { 
     parent.$("#loadingBar").show();
     parent.timerControl("start");
      $('div.userCmtDiv').html('').hide();
      $.ajax({
		   	url:Path+"/agileActionNew.do",
			type:"POST",
			data:{action:"loadUserCommentDetail",taskId:taskId,userId:userId,type:type},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
	            parent.checkSessionTimeOut(result);
	            $(obj).parent().parent().next('div.userCmtDiv').html(result);//-----------------..>>>>
	            loadCustomLabel('TaskUI');
                $(obj).parent().parent().next('div.userCmtDiv').show();
     //           $("[id^=emailPrtcpnt_]").css("width","98.6%");
                if(!isiPad){
                	if(isHistory){
                		scrollBar('prtcpntContainer');
                		$("[id^=userCmtDiv_]").css("float","none");
                		$('#prtcpntContainer.prtcpntSection:visible').mCustomScrollbar('update')
		            	if($("#prtcpntContainer.prtcpntSection:visible").find('div.mCSB_scrollTools').css('display') == 'block'){ 
				        	$('#prtcpntContainer.prtcpntSection:visible').mCustomScrollbar('scrollTo','#userComments_'+userId,{
		 						scrollInertia:0,
		 						callbacks:false
		 					});
		 				}
                	}
                	else{
		            	$("div#ideaTaskParticipantsDiv").mCustomScrollbar("update");
		            	if($("div#ideaTaskParticipantsDiv").find('div.mCSB_scrollTools').css('display') == 'block'){
			            	$("div#ideaTaskParticipantsDiv").mCustomScrollbar("scrollTo","#userComments_"+userId,{
		 						scrollInertia:0,
		 						callbacks:false
		 					});
		 				}
	            	}
	           }	
			   $('#completeComments').css('padding-left','');
               $('.usrComments').css('font-size','12px');  
               $('.usrComments > div').css('overflow','hidden');
               parent.$("#loadingBar").hide();
               parent.timerControl("");
			}
	  });
    }else{
        $(obj).parent().parent().next('div.userCmtDiv').hide();
        $(obj).parent().parent().next('div.userCmtDiv').html('');
        if(!isiPad){
        	if(parId == 'assignedSprint'){
            	$("div#ideaTaskParticipantsDiv").mCustomScrollbar("update");
            	if($("div#ideaTaskParticipantsDiv").find('div.mCSB_scrollTools').css('display') == 'block'){
	            	$("div#ideaTaskParticipantsDiv").mCustomScrollbar("scrollTo","#userComments_"+userId,{
 						scrollInertia:0,
 						callbacks:false
 					});
 				}
            }
            else {
            	$('#prtcpntContainer.prtcpntSection').mCustomScrollbar('update')
            	if($("#prtcpntContainer.prtcpntSection").find('div.mCSB_scrollTools').css('display') == 'block'){ 
		        	$('#prtcpntContainer.prtcpntSection').mCustomScrollbar('scrollTo','#userComments_'+userId,{
 						scrollInertia:0,
 						callbacks:false
 					});
 				}
			}
        }
    }
 }
 
 

function viewNewUserCmnts(user_id,obj) {
	if ($('#userComments_'+user_id+' div').is(':visible')) {
		$('#userComments_'+user_id+' div').hide();
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad) {
			$("div.prtcpntSection").mCustomScrollbar("update");
			$("div.taskPrtcpntSection").mCustomScrollbar("update");
			$("div#ideaTaskParticipantsDiv").mCustomScrollbar("update");
		}	
		return null;
	}
	else {
		$('.commentsSec').hide();
		$('.documentSection').hide();
		result="<div class=\"usrComments\" style=\"color:#848484;float:left;padding-left:175px;padding-top:10px;padding-bottom:10px;\">No previous comments for this task</div>";
		$('#userComments_'+user_id).html(result);
		$('#userComments_'+user_id).show();
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad) {
			$("div.prtcpntSection").mCustomScrollbar("update");
			$("div.taskPrtcpntSection").mCustomScrollbar("update");	
			$("div#ideaTaskParticipantsDiv").mCustomScrollbar("update");
		}
	}
}



	function taskRecurrence() {
		hideTaskHourDiv();
		$('#recurrencePopupDiv').show();
		$('#dailyClick').hide();
		$('#weeklyClick').hide();
		$('#monthlyClick').hide();
		$('#yearlyClick').hide();
		if(recType != '' && recType != 'null')
			$('#endCheck').css('display','block')
		if(recType == 'daily') {
			$('#dailyClick').show();
			$('#recDays').attr('checked','checked');
       		$('#recdd span').text("<bean:message key='Cal_Reminder.Daily' />");
       		recType = 'daily';
       	}	
       	else {
       		if(recType == 'weekly') {
	        	$('#weeklyClick').show();
	        	var d = recPatternValue.split('@')[1].split('_')[0];
					$('input#day_'+d).attr('checked','checked');
	        }	
	       	if(recType == 'daily') {
	       		$('#dailyClick').show();
	       		if(recPatternValue == 'weekdays') {
	       			$('div#weekDays').prev('input').attr('checked','checked');
	       		}
	       		else {
	       			$('#recWeekDay').val(recPatternValue);
	       		}
	       	}	
	       	if(recType == 'monthly') {
	       		$('#monthlyClick').show();
	       		var stDate = tmpSDate.replace(new RegExp("-","g"), "/");
	       		if(recPatternValue.split('_')[1]) {
	       			$('#recMnth').attr('checked','checked');
		       		$('#selectWeek').val(recPatternValue.split('@')[0]);
		       		$('#selectDay').val(recPatternValue.split('@')[1].split('_')[0]);
		       		$('#recMonthDetails1').val(recPatternValue.split('@')[1].split('_')[1]);
	       		}
	       		else {
	       			$('#recDayOfMonth').attr('checked','checked');
	       			$('#recDayofMnth').val(recPatternValue.split('@')[0]);
	       			$('#recMonthDetails').val(recPatternValue.split('@')[1]);
	       			$('#selectWeek').val(weekAndDay(stDate).split('^&*')[0]);
           			$('#selectDay').val(weekAndDay(stDate).split('^&*')[1]);
	       		}
	       	}	
	       	if(recType == 'yearly') {
	       		$('#yearlyClick').show();	
	       		var stDate = tmpSDate.replace(new RegExp("-","g"), "/");
	       		if(recPatternValue.split('_')[1]) {
	       			$('#recYearDetails').attr('checked','checked');
	       			$('#selectMonth').val(weekAndDay(stDate).split('^&*')[2]);
	       			$('#selectWeek1').val(recPatternValue.split('@')[0]);
		       		$('#selectDay1').val(recPatternValue.split('@')[1].split('_')[0]);
		       		$('#selectMonth1').val(recPatternValue.split('@')[1].split('_')[1]);
	       		}
	       		else {
	       			$('#recDayOfYear').attr('checked','checked');
	       			$('#recDayofMnth1').val(recPatternValue.split('@')[0]);
	       			$('#selectMonth').val(recPatternValue.split('@')[1]);
		       		$('#selectWeek1').val(weekAndDay(stDate).split('^&*')[0]);
		       		$('#selectDay1').val(weekAndDay(stDate).split('^&*')[1]);
		       		$('#selectMonth1').val(weekAndDay(stDate).split('^&*')[2]);
	       		}
	        }
	        $('#recdd span').text(recType);
        }
        $('#recPatternType').val(recType);
		$('#noOfOccurences').css('border','1px solid #BFBFBF');
	    $('#recEnd').css('border','1px solid #BFBFBF');
	    $('#recStart').val($('#startDateDiv').val());
	    $('#recEnd').val($('#endDateDiv').val());
	   	initRecurrenceCal();
	}
	
	
	
	
	function hideRecurrencePopup(){
		$('#recurrencePopupDiv').hide();
		$('#dailyClick').hide();
        $('#weeklyClick').hide();
        $('#monthlyClick').hide();
        $('#yearlyClick').hide();
	}
	
	
	
	
	function saveRecurrence(){
		var intRegex = /^\d+$/;
		value='';
		if(recType == 'daily'){
			if($('#recDays').is(':checked')) {
	   			if($('input#recWeekDay').val() == '') {
	   				$('input#recWeekDay').css('border','1px solid red');
	   				return null;
	   			}	
	   			else	
	   				recPatternValue = $('input#recWeekDay').val();
	   		}
	   		else
	   			recPatternValue = 'weekdays'; 
	   	}
	   	else if(recType == 'weekly') {
	   		var recValue = $('#recWeekDetails').val();
	   		recPatternValue = [];
			$('div#weeklyClick input[type=checkbox]').each(function() {
			   if ($(this).is(":checked")) {
			   	  recPatternValue.push($(this).attr('value'));
			   }
			});
			recPatternValue = recValue+'@'+recPatternValue;
		}	
		else if(recType == 'monthly') {
			value = $('#recDayofMnth').val();
			if($('#recDayOfMonth').is(":checked")) {
				var dayVal = $('#recDayofMnth').val();
				var noOfMonths = $('#recMonthDetails').val();
				recPatternValue = dayVal+'@'+noOfMonths;
			}
			else {
				var weekType = $('#selectWeek option:selected').val();
				var dayName = $('#selectDay option:selected').val();
				var noOfMnths = $('#recMonthDetails1').val();
				recPatternValue = weekType+'@'+dayName+'_'+noOfMnths;
			}
		}	
		else if(recType == 'yearly')	{
			value = $('#recDayofMnth1').val();
			if($('#recDayOfYear').is(":checked")) {
				var monthName = $('#selectMonth option:selected').val();
				var date = $('#recDayofMnth1').val();
				recPatternValue = date+'@'+monthName
			}
			else {
				var dayType = $('#selectWeek1 option:selected').val();
				var dayName = $('#selectDay1 option:selected').val();
				var monthName1 = $('#selectMonth1 option:selected').val();
				recPatternValue = dayType+'@'+dayName+'_'+monthName1
			}
		}	
		if(recType == 'monthly' || recType == 'yearly')	{
			if(intRegex.test(value)){ 
				if(value <= 31) {
					if(value <= 28) {
					}
					else {
						parent.confirmReset(getValues(customalertData,"Alert_Date_Info"),'reset','confirmDate','resetDate');
					}
				}
				else {
					if(!$('#recDayofMnth').val()) {
						$('#recDayofMnth').css('border','1px solid red');
					}else{
						parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
					    return null;
					}
				}
			}
			else {
				parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
				return null;
			}	
		 }
		confirmDate();
		hideRecurrencePopup();
}

	function recEndClick(obj){
		endId = $(obj).attr('id');
	}
	
	
	
	function downloadFile(docId){	/* Document DownLoad*/
		documentId = docId;
		parent.confirmFun(getValues(customalertData,"Alert_Download_Doc"),"close","downloadDocument");
	}
	function downloadDocument(){
		var url="";		
		if(parId == 'myDocument' || parId == 'assignedDocument')
			url = path+"/repositoryAction.do?action=downloadfile&docId="+documentId ;
		else
			url = path+"/workspaceAction.do?act=downloadfile&docId="+documentId+"&projId="+projId;
		window.open(url);
	}

	function veiwOrdownload(obj){
		var docId = $(obj).attr("id").split("_");
		var t=$(obj).offset().top;
	    var oh=$(window).height()-t;
	    if(oh > 380 ){
	      $("#dowloadOption_"+docId[1]).css('margin-top','0px');
	      $("#arrowDiv_"+docId[1]).css('margin-top','1px');
	    }else{
	       $("#dowloadOption_"+docId[1]).css('margin-top','-18px');
	       $("#dowloadOption_"+docId[1]).css('margin-right','32px');
	       $("#arrowDiv_"+docId[1]).css('margin-top','8px');
	    }
	    
	    if($("#dowloadOption_"+docId[1]).is(":hidden")){
	    	$('.hideOptionDiv').hide();
	    	$("#dowloadOption_"+docId[1]).slideToggle("slow");
	    	$("#varrowDiv_"+docId[1]).css('margin-top','8px');
	    }else{
	    	$("#dowloadOption_"+docId[1]).slideToggle("slow");
	    }	
	       
	}
	
	
	
	function viewDoc(url){
		window.open(url);
	}
	function onMouseOverDiv(obj){
		$(obj).addClass("mouseOvrClass");
	}
	function onMouseOutDiv(obj){
		$(obj).removeClass("mouseOvrClass");
	}
	function weekAndDay(str_date) {

	    var date = new Date(str_date),
	        days = ["<bean:message key='Cal_Day.Sunday'/>","<bean:message key='Cal_Day.Monday'/>","<bean:message key='Cal_Day.Tuesday'/>","<bean:message key='Cal_Day.Wednesday'/>",
	                "<bean:message key='Cal_Day.Thursday'/>","<bean:message key='Cal_Day.Friday'/>","<bean:message key='Cal_Day.Saturday'/>"],
	        months = ["<bean:message key='Cal_Month.January'/>","<bean:message key='Cal_Month.February'/>","<bean:message key='Cal_Month.March'/>","<bean:message key='Cal_Month.April'/>","<bean:message key='Cal_Month.May'/>","<bean:message key='Cal_Month.June'/>","<bean:message key='Cal_Month.July'/>","<bean:message key='Cal_Month.August'/>",
	        		"<bean:message key='Cal_Month.September'/>","<bean:message key='Cal_Month.October'/>","<bean:message key='Cal_Month.November'/>","<bean:message key='Cal_Month.December'/>"],        
	        prefixes = ["<bean:message key='Cal_First'/>", "<bean:message key='Cal_Second'/>", "<bean:message key='Cal_Third'/>", "<bean:message key='Cal_Fourth'/>", "<bean:message key='Cal_Last'/>"];
	
	    return prefixes[0 | date.getDate() / 7]+ '^&*' + days[date.getDay()] +'^&*'+ months[date.getMonth()];

	}
	
	
	
		function filterTask(obj){       /* Search Functionality  */
		$('div.task').hide();
		var kw = $(obj).val().toLowerCase();
		if(kw!=''){
			//$('#noteSearch').attr('src','${path}/images/remove.png').attr('onclick','clearSearchText(this)').css('top','18px');
			filterTaskName(kw);
			filterTaskPriority(kw);
			filterTaskOwner(kw);
			filterDate(kw);
		}else{
			$('#noteSearch').attr('src',''+Path+'/images/Idea/search.png').css('top','20px');
			$('div.task').show();
		}
		listViewScrollUpdate();
	}
	function filterTaskName(txt){
		$('div.task div div.rTasksName').find('span.hidden_searchTask:contains("'+txt+'")').parents('div.task').show();
	}
	function filterTaskPriority(txt){
		$('div.task div div.rTasksPriority').find('span.hidden_searchPriority:contains("'+txt+'")').parents('div.task').show();
	}
	function filterTaskOwner(txt){
		$('div.task div div.rTasksOwner').find('span.hidden_searchCreatedBy:contains("'+txt+'")').parents('div.task').show();
	}                      
	function filterDate(txt){
		$('div.task div div.rTasksDate').find('span.hidden_searchDate:contains("'+txt+'")').parents('div.task').show();
	}  
	function listViewScrollUpdate() {
		if(sortTaskType == 'myTasks' || sortTaskType == 'googleTasks'){
			var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        if(!isiPad)
				$('div#mytaskdatalist').mCustomScrollbar("update");
		}if(sortTaskType == 'assignedTasks') {
			var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        if(!isiPad)		
				$('div#assignedtaskdatalist').mCustomScrollbar("update");
		}else{ 
			var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        if(!isiPad)
				$('div#historytaskdatalist').mCustomScrollbar("update");
		}
	}	  
	
	
	function selectRecPattern(){
			recType = $('#recPatternType option:selected').val();
			$('#dailyClick').hide();
           	$('#weeklyClick').hide();
           	$('#monthlyClick').hide();
           	$('#yearlyClick').hide();
            var strD=tmpSDate.split("-");
			var str_date=strD[2]+"/"+strD[0]+"/"+strD[1];
			var strE=tmpEDate.split("-");
			var end_date=strE[2]+"/"+strE[0]+"/"+strE[1];
           	var d = new Date(str_date).getDay()+1;
           	var type='';
           	if(recType == 'select'){
           		$('#endCheck').css('display','none');
           		$('#recEnd').attr('disabled','disabled');
           		$('#noOfOccurences').attr('disabled','disabled');
           	}
           	else{
           		$('#endCheck').css('display','block');
           		$('#recEnd').attr('disabled',false);
            	$('#noOfOccurences').attr('disabled',false);
            	if(recType == 'weekly') {
	            	type='week(s)';
	           		$('#weeklyClick').show();
	           		$('input#day_'+d).attr('checked','checked');
	            }	
	           	else if(recType == 'daily') {
	           		type='day(s)';
	           		$('#dailyClick').show();
	           	}	
	           	else if(recType == 'monthly') {
	           		type='month(s)';
	           		$('#monthlyClick').show();
	           		$('#recdd1 span').text(weekAndDay(str_date).split('^&*')[0]);
	           		$('#recdd2 span').text(weekAndDay(str_date).split('^&*')[1]);
	           		$('#recDayofMnth').val(strD[1]);
	           	}	
	           	else if(recType == 'yearly') {
	           		type='year(s)';	
	           		$('#yearlyClick').show();	
	           		$('#recdd3 span').text(weekAndDay(str_date).split('^&*')[2]);
	           		$('#recdd4 span').text(weekAndDay(str_date).split('^&*')[0]);
	           		$('#recdd5 span').text(weekAndDay(str_date).split('^&*')[1]);
	           		$('#recdd6 span').text(weekAndDay(str_date).split('^&*')[2]);
	           		$('#recDayofMnth1').val(strD[1]);
	           	}
           	}
            $('span#recurrenceType').html(type);
	}
	/*var checkType='removeAll';
	function checkAllUsers(obj){
		$(obj).removeClass(checkType);
		if('div.participants :not(opacity 0.4)'){
			if(checkType == "removeAll"){
				checkType="checkAll";
				$('div.userUnCheck').each(function(){
						$('#'+this.id).removeClass('userUnCheck ').addClass('userCheck');
						$('.addParticipant').css('display','block');
						$('.createEvent').css('display','none');
						$('.createEvent1').css('display','block');
				});
			}
			else{
				checkType="removeAll";
				$('div.userCheck').each(function(){
					if($('#'+this.id).attr('class') == 'userCheck'){
						$('#'+this.id).removeClass('userCheck ').addClass('userUnCheck');
						$('.addParticipant').css('display','none');
						$('.createEvent').css('display','block');
						$('.createEvent1').css('display','none');
					}
				});
			}
		}
		$(obj).addClass(checkType);
	}	*/
	/*
	function changeSlider(e, o, so){
		$(o).css('border','medium none');
		checkNumerals(o);
		var val = $(o).val();
		$( so ).slider({
			range: 'min', 
			value: val,
			min: 1, 
			max: 100,
			slide: function( event, ui ) {
				$( '.cpc' ).val(ui.value);
			}
		});
		$( '#workingHours' ).prop('disabled',false);
		$( '#myTaskComment' ).prop('disabled',false);
	}
	
	*/
	
	function openMessagePopup(msgUsrId,obj){
		messageUserId = msgUsrId;
		$('#messageBox').show();
		$('#overlay').show();
		$("#uOptions_"+msgUsrId).slideToggle("slow");
		$('#messageTaskUser').html();
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
		var tData = '';
		$('.odd, .even, .updatePrtcpnt,.userContainer').each(function() {
			var id = $(this).attr('id');
			var userId = id.split('_')[1];
			if(userId != UserId){
				var name = $(this).children('div').attr('title');
				if(!name)
					name = $(this).children('div').children('div').attr('title');
					 if(isiPad) {
					 tData = tData + "<div style=\"width: 168px;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:10px;margin-bottom:10px;padding:3px\" class=\"tParticipants\" id=\"tUser_"+userId+"\">";
				tData = tData + "<div id=\"secndDivId_"+userId+"\" style=\"float: left; padding-left: 5px; padding-top: 1px; width: 116px;color:#848484;height:20px;overflow:hidden;\" title=\""+name+"\">"+name.substring(0,15)+"..."+"</div>";
				if(messageUserId == userId){
					tData = tData + " <input type=\"checkbox\" style=\"margin-right: 5px;height:16px\" checked=\"checked\" id=\"tUserCB_"+userId+"\" name=\"mParticipants\" class=\"mParticipants\" value=\""+userId+"\" title=\"\">";
				}else{
					tData = tData + " <input type=\"checkbox\" style=\"margin-right: 5px;height:16px\" id=\"tUserCB_"+userId+"\" name=\"mParticipants\" class=\"mParticipants\" title=\"\" value=\""+userId+"\" >";
			    }
			    tData = tData + " <img style=\"margin-top: 3px; margin-right: 2px; width: 16px;\" src=\"images/Idea/userDelete.png\"  id=\"tMsgUserDelete_"+userId+"\" title=\"Delete\" name=\"mParticipants\" class=\"mParticipants\" value="+userId+" onclick=\"DeleteMessageUser("+userId+")\"></div></div></div>";
			  
					 }else{
				tData = tData + "<div style=\"width: 165px;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:10px;margin-bottom:10px;padding:3px\" class=\"tParticipants\" id=\"tUser_"+userId+"\">";
				tData = tData + "<div id=\"secndDivId_"+userId+"\" style=\"float: left; padding-left: 5px; padding-top: 1px; width: 120px;color:#848484;height:20px;overflow:hidden;\" title=\""+name+"\">"+name.substring(0,15)+"..."+"</div>";
				if(messageUserId == userId){
					tData = tData + " <input type=\"checkbox\" style=\"margin-right: 5px;height:16px\" checked=\"checked\" id=\"tUserCB_"+userId+"\" name=\"mParticipants\" class=\"mParticipants\" value=\""+userId+"\" title=\"\">";
				}else{
					tData = tData + " <input type=\"checkbox\" style=\"margin-right: 5px;height:16px\" id=\"tUserCB_"+userId+"\" name=\"mParticipants\" class=\"mParticipants\" title=\"\" value=\""+userId+"\" >";
			    }
			    tData = tData + " <img style=\"margin-top: 3px; margin-right: 2px; width: 16px;\" src=\"images/Idea/userDelete.png\"  id=\"tMsgUserDelete_"+userId+"\" title=\"Delete\" name=\"mParticipants\" class=\"mParticipants\" value="+userId+" onclick=\"DeleteMessageUser("+userId+")\"></div></div></div>";
			  }
			}
		});
		
		$('#tUserContainer').html(tData);
		
	    if(!isiPad) {
		scrollBar('taskUserSection');
		$("#taskUserSection").mCustomScrollbar("update");
		}
	}
	
	var MsgUDeleteIds='';
	function DeleteMessageUser(uIds){
	   MsgUDeleteIds=uIds;
	   parent.$('#loadingBar').show();
	   parent.timerControl("start");
	   $("#tMsgUser_"+uIds).remove();
	   var isiPad = navigator.userAgent.match(/iPad/i) != null;
	   if(!isiPad) {
		$("#taskUserSection").mCustomScrollbar("update");
	   }
	   parent.alertFun(getValues(customalertData,"Alert_UsrRemove",'warning');
	   parent.$('#loadingBar').hide();
 	   parent.timerControl("");
	   return false;
	  
	}
	
	var checkedUserIdVal="";
	var msId="";
	var messageUserId = '';	
	var MsgUserIds='';
	function fetchMessageUserList(obj){
	 parent.$("#loadingBar").show();
	 parent.timerControl("start");
	  
	   var searchUserName=$("#searchMsguserId").val();
	   var mUserIds = [];
		$("input.mParticipants").each(function(obj){
			mUserIds.push($(this).val());
		});
		messageUserId = mUserIds.toString();
		MsgUserIds=messageUserId;
		if(obj=='company'){
		msId=messageId;
		}else{
		msId='0';
		}
		$.ajax({
       			url: Path+"/connectAction.do",
					type:"POST",
					data:{act:"fetchMessageUserList",searchUserName:searchUserName,MsgUserIds:MsgUserIds,msId:msId},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
						checkSessionTimeOut(result);
					var data=result.split('##@@##');
					if(data[1] == "failure"){
					   parent.alertFun(getValues(customalertData,"Alert_enterUsername",'warning');
					   parent.$('#loadingBar').hide();
					   parent.timerControl(" ");
					}
					
					else if(data[0] == ""){
					   parent.alertFun(getValues(customalertData,"Alert_noUserFound",'warning');
					   parent.$('#loadingBar').hide();
					   parent.timerControl(" ");
					}else{
					   $("#tUserContainer").show();
					   $("#tUserContainer").append(data[0]);
					   $("#searchMsguserId").val('');
					   var isiPad = navigator.userAgent.match(/iPad/i) != null;
					   if(!isiPad){
					      $("#taskUserSection").mCustomScrollbar("update");
					   }
					   
					   parent.$("#loadingBar").hide();
					   parent.timerControl("");
					  }
					}
       		});
	}
		function sendMessage(){
		var message=$('#message').val().trim();
		var mUserIds = [];
		$("input.mParticipants:checked").each(function(obj){
			mUserIds.push($(this).val());
		});
		messageUserId = mUserIds.toString();
		if(message.length==0){
			parent.alertFun(getValues(customalertData,"Alert_Msg"),'warning');
			$('#message').val('').focus();
		}
		else if(!messageUserId){
		   if($("#tUserContainer").find('.mCSB_container').html() == '' || $("#tUserContainer").find('.mCSB_container .mCSB_container').html() == ''){
		   		  parent.alertFun(getValues(customalertData,"Alert_SrhUsr"),'warning');
				  return false;
			}else{
			      parent.alertFun(getValues(customalertData,"Alert_CrtUsr"),'warning');
			      return false;
			}
		}else{
			parent.$('#loadingBar').show();
			parent.timerControl("");
			$.ajax({
      			url: Path+"/connectAction.do",
				type:"POST",
				data:{act:"savePersonalMessages",messageUserId: messageUserId,message:message},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
					  parent.checkSessionTimeOut(result);
					   if(result=="success"){
						 parent.alertFun(getValues(companyAlerts,"Alert_MsgSentSuccess",'warning');
						 parent.$("div#transparentDiv").hide();
					   }
					$('#message').val("");
					$('#messageBox').hide();
					$('#overlay').hide();
					parent.$("#transparentDiv").hide();
					parent.$('#loadingBar').hide();
					parent.timerControl("");
				}
			});	
		}
	}
	
	function closeMessageBox(){
		$('#message').val("");
		$('#messageBox').hide();
		$('#overlay').hide();
	}
	
/*	
	function attachLink(idd){
		$("#uOptions_"+UserId+"").slideToggle("slow");
		$('.commentsSec').hide();
		$('#documentSection_'+UserId+'').hide();
		$('input#linkTitle,input#linkLink').val('');
        $('#linkSection').css('display','block');
     	$('#linkTaskId').val(idd);		
		var type = parId.substring(2);
		var taskId=$('#linkTaskId').val(); 
		if($('#docImgDiv_'+UserId+'').is(':visible')){
			$('#docIcon_'+UserId+'').attr('src',Path+'/images/Idea/Document.png').attr('onclick','showTaskDocuments();');
		}
		if($('#cmtImgDiv_'+UserId+'').is(':visible')){
			$('#cmtIcon_'+UserId+'').attr('src',Path+'/images/calender/comments.png');
		}
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad) {
	    	if($("#prtcpntContainer.prtcpntSection").find('div.mCSB_scrollTools').css('display') == 'block'){ 
	    		$("#prtcpntContainer.prtcpntSection").mCustomScrollbar("scrollTo","top");
	    		$("#prtcpntContainer.prtcpntSection").mCustomScrollbar("update");	
	    	}
		}
  	}
  	
  	
*/  /*	
  	  	function funAttachLink(){
		if( $("#linkLink").val().trim() == '' ){
			parent.alertFun(getValues(customalertData,"Alert_enterLink"),'warning');
			$("#linkLink").focus();
			return false;
		}
		var key = $("#linkLink").val().trim();
	    if (!isValidURL(key)) {
	       parent.alertFun(getValues(customalertData,"Alert_invalidUrl"),'warning');
	       return false;
	    }
		var title1=$("#linkTitle").val().trim();
		var title = $("#linkTitle").val().trim();
		var link = $("#linkLink").val().trim();
		if(title==""){
		    title=link;
		    title1=link;
	    }
	    var taskId=$('#linkTaskId').val();   	
        var type = parId.substring(2);	
        parent.$("#loadingBar").show();
        parent.timerControl("start");
 		$.ajax({
  			url:Path+"/calenderAction.do",
			type:"POST",
			data:{act:"insertTaskLink",taskId:taskId,link:link,title:title,projId:projId,taskType:type},
			success:function(result){
         		parent.checkSessionTimeOut(result);
         		$('input#linkTitle,input#linkLink').val('');
         		$('#documentSection_'+UserId+'').html(result).show();
         		$('#docImgDiv_'+UserId+'').show();
         		$('#docIcon_'+''+UserId+'').attr('src','images/Idea/DocumentToggle.png');
         		loadCustomLabel('AttachLink');
         		var isiPad = navigator.userAgent.match(/iPad/i) != null;
			    if(!isiPad) {
			    	if($("#prtcpntContainer.prtcpntSection").find('div.mCSB_scrollTools').css('display') == 'block'){ 
			    		$("#prtcpntContainer.prtcpntSection").mCustomScrollbar("scrollTo","top");
			    		$("#prtcpntContainer.prtcpntSection").mCustomScrollbar("update");	
			    	}
				}
		        parent.$("#loadingBar").hide();
		        parent.timerControl("");
			}
  		});
	} 
	
	
	 function cancelAttachLink(){
    $('#linkSection').hide();
 	if($("[id^=taskLink_]").length < 0){
 		$("[id^=docImgDiv_]").hide();
 	}
    $('#linkTitle').val('');
	$('#linkLink').val('');
 }
 
 function isValidURL(url){
    var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    if(RegExp.test(url)){
        return true;
    }else{
        return false;
    }
}
*/
function cancelComment(){
	$('#userComments_'+UserId+'').hide();
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
    if(!isiPad){
    	$('#prtcpntContainer.prtcpntSection').mCustomScrollbar('update');
    	if($("#prtcpntContainer.prtcpntSection").find('div.mCSB_scrollTools').css('display') == 'block'){ 
       		$('#prtcpntContainer.prtcpntSection').mCustomScrollbar('scrollTo','#userComments_'+UserId+'',{
				scrollInertia:0,
				callbacks:false
			});
		}
		setTimeout("$('.mCS_no_scrollbar').css('top','0px')",500);
    }	
}


 function openLink4Project(obj) {
		    var urlLink = $(obj).attr('title');
		    var httpIndex = urlLink.indexOf("http://");
		    if(httpIndex!=0){
		    urlLink ='http://'+urlLink ;
		    }
		    popup(urlLink);
 } 
 
  function openLink4TaskLevelProject(obj) {
		    var urlLink = $(obj).attr('title');
		    var httpIndex = urlLink.indexOf("http://");
		    if(httpIndex!=0){
		    urlLink ='http://'+urlLink ;
		    }
		    window.open(urlLink);
 } 
 function popup(url) {
		    params = 'width=' + screen.width-1;
		    params += ', height=' + screen.height-1;
		    params += ', top=0, left=0'
		    params += ', fullscreen=no';
		    newwin = window.open(url, 'windowname4', params);
		    if (window.focus) {
		        newwin.focus()
		    }
		    return false;
 }
 
 
/*

function deleteTaskLink(taskLinkId,taskId){
  	confirmId=taskLinkId;
	parent.confirmFun(getValues(customalertData,"Alert_delete"),"delete","attachLinkDeleteConfirm");
  }
  function attachLinkDeleteConfirm(){
  	parent.$("#loadingBar").show();
	parent.timerControl("start");
  	$.ajax({
 		url:Path+"/calenderAction.do",
 		type:"POST",
 		data:{act:"deleteTaskLink",taskLinkId:confirmId},
 		success:function(result){
 			$('#taskLink_'+confirmId).remove();
 			if($("[id^=taskLink_]").length == 0 && $("[id^=taskDoc_]").length == 0  ){ 
 				$("[id^=docImgDiv_]").hide();
 			}
 			var isiPad = navigator.userAgent.match(/iPad/i) != null;
		    if(!isiPad)
		    	scrollBar('prtcpntContainer');
		    parent.$("#loadingBar").hide();
 		    parent.timerControl("");
 		}
 	});	
  }
  */
 /* 
  	function submitTaskRepositoryFileForm(e,uId){
		parent.$('#loadingBar').show();
		parent.timerControl("start");
		var formname = e.form.name;
		var uploadForm = document.getElementById(formname);
		uploadForm.target = 'upload_task_target';
		var browserName  = navigator.appName;
			if(browserName == 'Microsoft Internet Explorer'){
				$('#detectBrowser_'+uId).val('ie');
				uploadForm.submit();
			}
			else{
				var fileSize = e.files[0].size;
				$('#size_'+uId).val(fileSize);
	    		if(fileSize >= 10000000) {
	    		    parent.alertFun(getValues(customalertData,"Alert_filesizeLimit"),'warning');
	    		    parent.$("#loadingBar").hide();
 					parent.timerControl("");
	    		}else{
	    			uploadForm.submit();
	    		}
			}
	}
	
	*/
	/*
	function submitRepositoryFileForm(e,uId){
		parent.$('#loadingBar').show();
		parent.timerControl("start");
		$("#TaskLevellinkSection").hide();
		var formname = e.form.name;
		var uploadForm = document.getElementById(formname);
		uploadForm.target = 'upload_task_target1';
		var browserName  = navigator.appName;
			if(browserName == 'Microsoft Internet Explorer'){
				$('#detectBrowser_'+uId).val('ie');
				uploadForm.submit();
			}
			else{
				var fileSize = e.files[0].size;
				$('#size_'+uId).val(fileSize);
	    		if(fileSize >= 10000000) {
	    		    parent.alertFun(getValues(customalertData,"Alert_filesizeLimit"),'warning');
	    		    parent.$("#loadingBar").hide();
 					parent.timerControl("");
	    		}else{
	    			uploadForm.submit();
	    		}
			}
	}*/
	  function showTaskDocuments(){
 	$('.DocumentOptionDiv').show();
 	$('#linkSection').hide();
 	$('.commentsSec').hide();
 	$('#cmtIcon_'+''+UserId+'').attr('src',Path+'/images/calender/comments.png');
 	var docLocation = '';
 	var type = parId.substring(2);
 	if(type == 'Tasks')
 		type = 'Task';
 	if(projId==0)
 		docLocation = 'Repository';
 	else
 		docLocation = 'Workspace';	
 	$.ajax({
 		url:Path+"/calenderAction.do",
 		type:"POST",
 		data:{act:"fetchTaskDocuments",taskId:myTaskId,taskType: type,userId:UserId,docLocation:docLocation,type:'myTask'},
 		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
 		success:function(result){
 			checkSessionTimeOut(result);
 			result = result.replaceAll("CHR(39)","'");
 			$('#documentSection_'+''+UserId+'').html(result).show();
 			$('#docIcon_'+''+UserId+'').attr('src','images/Idea/DocumentToggle.png').attr('onclick','toggleTaskDocs(this);');
 			loadCustomLabel("AttachDoc");
 			$('input#projectId').val(projId);
 			$('input#taskOwner').val(UserId);
		 	var isiPad = navigator.userAgent.match(/iPad/i) != null;
		    if(!isiPad){
		    	$('#prtcpntContainer.prtcpntSection').mCustomScrollbar('update');
		    	if($("#prtcpntContainer.prtcpntSection").find('div.mCSB_scrollTools').css('display') == 'block'){ 
		       		$('#prtcpntContainer.prtcpntSection').mCustomScrollbar('scrollTo','#userComments_'+UserId+'',{
						scrollInertia:0,
						callbacks:false
					});
				}
				setTimeout("$('.mCS_no_scrollbar').css('top','0px')",500);
		    }
		}
	});	    
 }
 /*
   function uploadedDocNew(obj,docData){
		$('#uOptions_'+UserId+'').hide();
		docData = docData.replaceAll("CHR(39)","'");
		loadCustomLabel("AttachDoc");
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	   	fetchDocdata('upload');
		$('#docTaskContentSubDiv').mCustomScrollbar('update');
		$('#WsUploadOptns').slideToggle("slow");
		parent.$("#loadingBar").hide();
	 	parent.timerControl("");
		return null;
   }
   */
 /*  
   
	function uploadedDoc(obj,docData){
		$('#uOptions_'+UserId+'').hide();
		docData = docData.replaceAll("CHR(39)","'");
		parent.alertFun(getValues(customalertData,"Alert_DocumentUploaded"),'warning');
		$('#documentSection_'+''+UserId+'').show().html(docData);
		$('input#projectId').val(projId);
	 	$('input#taskOwner').val(UserId);
		loadCustomLabel("AttachDoc");
		if($('#docImgDiv_'+UserId).is(':visible')){
			$('#docIcon_'+UserId).attr('src',Path+'/images/Idea/DocumentToggle.png');
		}else{
			$('#docImgDiv_'+UserId).show();
		}
		if($('#cmtImgDiv_'+UserId).is(':visible')){
			$('#cmtIcon_'+UserId).attr('src',Path+'/images/calender/comments.png');
		}
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	   	if(!isiPad) {
	   		$('#prtcpntContainer.prtcpntSection').mCustomScrollbar('update');
	       	if($("#prtcpntContainer.prtcpntSection").find('div.mCSB_scrollTools').css('display') == 'block'){ 
	     		$('#prtcpntContainer.prtcpntSection').mCustomScrollbar('scrollTo','#userComments_'+UserId+'',{
					scrollInertia:0,
					callbacks:false
				});
			}
			setTimeout("$('.mCS_no_scrollbar').css('top','0px')",200);
	   	}
	   	else {
	     	var el = document.getElementById('userComments_'+UserId+'');
			el.scrollIntoView(true);
		}
		parent.$("#loadingBar").hide();
	 	parent.timerControl("");
		return null;
	}
	*/
function imageOnErrorReplace( obj ){
	$(obj).attr('src',""+lighttpdpath+"userimages/userImage.png");
}

	function checkThis(obj){			
		$('input.weeklyCheck').prop('checked', false);
		$(obj).prop('checked', true);
	}


 function loadNotificationsFun(id,divId){
 	myTaskId=id;
 	parId = divId;	
	$.ajax({ 
	           url: Path+'/calenderAction.do',
	       	   type:"POST",
		       data:{act:'getMyTaskDetails',taskId:id,parId:divId},
		       error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
		       success:function(result){
		         parent.checkSessionTimeOut(result);
	         	$("#eventPopup").hide();
	       		$("#taskPopupSection").hide();
       			$('.transparent').show();
       			parent.$("#transparentDiv").show();
       			$('#taskCalendar').append(result);			
       			if(divId == 'myEvent' || divId == 'myEvents')	
       				$(".removeEventUser").hide();	
				if(height >= 550)
					$('.eventPopup').css('height','550px');
				else {
					$('.eventPopup').css({'height':height+'px','margin-top':'','margin-top':'-'+height/2+'px'});
				}
				if(divId == 'mySprintTask'){
					loadCustomLabel('TaskUI');
					$('.sprintUserContainer').css('width','715px');
					$('.sprintUserContainer').css('margin-left','20px');
					$('.userTitle').css('width','230px');
					$('.slider').css('width','235px');
					$('.completePer').css('width','50px');
					$('.userContainerSlide').css('width','150px');
					$('.taskAmount').css('margin-left','30px');
					$('.userStatus').css('width','155px');
					$('.userWorkingHour').css('text-align','left');
					$('.userComments').css('float','right');
					$('.usrComments').css('font-size','12px');
				}
				var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        	if(divId == 'myEvent' || divId == 'myEvents'){
	        		if(!isiPad){
       					scrollBar('postedComments');
       					scrollBar('eventUserContainer');
       					if(height < 550){
							$('.eventPopup').css('width','660px');
							scrollBar('myTaskPopup');
						}	
       				}	
       			}	
       			else {	
	       			taskHour = $('#workingHours').val();
	       			if(!isiPad){
						commentsScrollBar();
						if(height < 550){
							$('.eventPopup').css('width','820px');
							scrollBar('myTaskPopup');
						}	
					}	
				}
				$(".taskSub").each(function(){
                	var title = $(this).attr('title').replaceAll("CHR(41)"," ").replaceAll("CHR(39)","'");
                	$(this).attr('title',title);
				});
				parent.$('#loadingBar').hide();	
				parent.timerControl("");
			}
	  });
 }
 
  
 function gotoListView(){
 	parent.$('#loadingBar').show();
	parent.timerControl("start");
 	parent.menuFrame.location.href = Path+'/colAuth?pAct=calListView';
 }
  function gotoAssigndListView(){
 	parent.$('#loadingBar').show();
	parent.timerControl("start");
 	parent.menuFrame.location.href = Path+'/colAuth?pAct=calListView&more=assignedTasks';
 }
 function gotoMytaskListView(){
 	parent.$('#loadingBar').show();
	parent.timerControl("start");
 	parent.menuFrame.location.href = Path+'/colAuth?pAct=calListView&more=myTasks';
 }
 function gotoHistoryListView(){
 	parent.$('#loadingBar').show();
	parent.timerControl("start");
 	parent.menuFrame.location.href = Path+'/colAuth?pAct=calListView&more=history'
 }
 function gotoCalendarView(){
 	parent.$('#loadingBar').show();
	parent.timerControl("start");
 	parent.menuFrame.location.href = Path+'/colAuth?pAct=calMenuView';
 	}
 

 function loadStoryDate(parId){
		    if(parId == 'assignedSprint') {  
		    var startDate=$('input#startDateSprint').val();
		    var endDate=$('input#endDateSprint').val();
		    var sprintGroup=$("#taskSprintGroup").val(); 
		    var sprint=$("#taskSprint").val();
		    var taskId=$('#taskID').val();
		    prevSDate = startDate;
		   	prevEDate = endDate;
		    parent.$("#loadingBar").show();
		    parent.timerControl("start");
		   	$.ajax({
			   	url: Path+"/calenderAction.do",
				type:"POST",
				data:{act:"getStoryDates",startDate:startDate,endDate:endDate,taskId:taskId ,sprintGroup:sprintGroup,sprint:sprint,parId:parId},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
		            parent.checkSessionTimeOut(result);		
		            $('#taskDateHourDiv').html(result);
		            $('#taskEhourPopupDiv').show();
		            $('#checkDates').hide();
		            $('#wFTaskDateSave').hide();
		            $('#checkUncheckDate').hide();
		            parent.$("#loadingBar").hide();
		            parent.timerControl("");
		            var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        		if(!isiPad)
		            	scrollBar('taskDateHourDiv');
				}
			});
		 }   
 	 	 else {
 	 	 	 var startDate = $('#startDateDiv').val();
 	 	 	 var endDate = $('#endDateDiv').val();
 	 	 	 parent.$("#loadingBar").show();
 	 	 	 parent.timerControl("start");
		   	 $.ajax({
			   	url: Path+"/calenderAction.do",
				type:"POST",
				data:{act:"getStoryDates",startDate:startDate,endDate:endDate,taskId:myTaskId,parId:parId},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
		            parent.checkSessionTimeOut(result);
		            $('#taskDateHourDiv').html(result);
		            $('#taskEhourPopupDiv').show();
		            $('#checkDates').hide();
		            $('#wFTaskDateSave').hide();
		            $('#checkUncheckDate').hide();
		            parent.$("#loadingBar").hide();
		            parent.timerControl("");
		            var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        		if(!isiPad)
		            	scrollBar('taskDateHourDiv');
				}
			});
 	 	 }  
  }
 
 
  function wFDateXml(k){
 	var TaskDateXml = "<taskHour id=\""+k+"\">";
	 var start = $('#wFStart_'+k).val();
 	 var end = $('#wFEnd_'+k).val();
 	 var strD=start.split("-");
	 var str_date=strD[2]+"/"+strD[0]+"/"+strD[1];
	 var strE=end.split("-");
	 var end_date=strE[2]+"/"+strE[0]+"/"+strE[1];
	var tskDates = new Array();
	var tskMonth = new Array();
	var tskYear = new Array();
	var tskDays = new Array();
	var weekday=new Array(7);
	weekday[0]=day0;
	weekday[1]=day1;
	weekday[2]=day2;
	weekday[3]=day3;
	weekday[4]=day4;
	weekday[5]=day5;
	weekday[6]=day6;
	tHour = $('#estmatedHour_'+k).val();
	tMinute = $('#estmatedMinute_'+k).val();
	var startDte = new Date(str_date);
	var futureDte = new Date(end_date);
	var range = []
	var  mil = 86400000 //24h
	var curHour=0;
	var curMinute=0;
	var totalDay = (futureDte-startDte)/mil +1;
	var hr_day=tHour/totalDay;
	var hr_per_day=Math.round(hr_day);
	
	var min_day=tMinute/totalDay;
	var min_per_day=Math.round(min_day);
	
	var flag=0;
	var flag_min=0;
	var j=futureDte.getTime();
    if((tHour>0) && (hr_per_day==0) ){
	   hr_per_day=tHour;
	}
	if((tMinute>0) && (min_per_day==0) ){
	   min_per_day=tMinute;
	} 
	var j=1;
     for (var i=startDte.getTime(); i<=futureDte.getTime();i=i+mil) {
     	curHour = parseInt(curHour,10) + parseInt(hr_per_day,10);
     	curMinute = parseInt(curMinute,10) + parseInt(min_per_day,10);
     	
    		if(curHour < tHour) {
    			hr_per_day = hr_per_day;
    			if(i==j){
		  	 hr_per_day= parseInt(tHour,10) - (parseInt(curHour,10) - parseInt(hr_per_day,10));
			} 
    		}
    		else {
    			if(flag == 0){
     				curHour = curHour - hr_per_day;
	       		hr_per_day = tHour - curHour;
	       		flag = 1;
      		}
      		else
      			hr_per_day = 0;
		}
		
		if(curMinute < tMinute) {
    			min_per_day = min_per_day;
    			if(i==j){
		  			 min_per_day= parseInt(tMinute,10) - (parseInt(curMinute,10) - parseInt(min_per_day,10));
				} 
    		}
    		else {
    			if(flag_min == 0){
     				curMinute = curMinute - min_per_day;
	       			min_per_day = tMinute - curMinute;
	       			flag_min = 1;
      		}
      		else
      			min_per_day = 0;
		}
		
		
		
	 	tskDates[i] = new Date(i).getDate();
	    tskMonth[i] = new Date(i).getMonth()+1;
	    tskYear[i] = new Date(i).getFullYear();
	    tskDays[i] = new Date(i).getDay();
	    var day = weekday[tskDays[i]]; 
	    var month = tskMonth[i];
	    if(month < 10) {
	    	month = '0'+month;
	    }
	    if(tskDates[i] < 10) {
	    	tskDates[i] = '0'+tskDates[i];
	    }
	  TaskDateXml +="<taskDate id=\""+j+"\">"
	  TaskDateXml +="<date>"+tskDates[i]+"/"+month+"/"+tskYear[i]+"</date>";
	  TaskDateXml +="<day>"+day+"</day>";
	  TaskDateXml +="<hour>"+hr_per_day+"</hour>";
	  TaskDateXml +="<min>"+min_per_day+"</min>";
	  TaskDateXml +="<status>Checked</status>";
	  TaskDateXml +="</taskDate>";
	  if((tHour>0) && (hr_per_day==0) ){
	  	flag = 1;
	  }
	  if((tMinute>0) && (min_per_day==0) ){
	  	flag_min = 1;
	  }
	  j++;
	}	
	TaskDateXml +="</taskHour>";
	return TaskDateXml;
 }
   function confirmTaskHourDivClose(){
  	 prevSDate = $('#startDateSprint').val();
  	 prevEDate = $('#endDateSprint').val();
  	 $('input#taskTotalEhour').val(confirmId); 
  	 hideTaskHourDiv();
 }
  function confirmTaskMinuteDivClose(){
  	 prevSDate = $('#startDateSprint').val();
  	 prevEDate = $('#endDateSprint').val();
  	 $('input#taskTotalEminute').val(confirmIdmin); 
  	 hideTaskHourDiv();
 }
  function confirmTaskHourMinuteDivClose(){
  	 prevSDate = $('#startDateSprint').val();
  	 prevEDate = $('#endDateSprint').val();
  	 $('input#taskTotalEhour').val(confirmId);
  	 $('input#taskTotalEminute').val(confirmIdmin); 
  	 hideTaskHourDiv();
 }
  function clearTaskDate(){
   $('.estimatedHourDiv').each(function() {
		$(this).children('div').children('input').val("");
	});
 }
 
//---------Added for Task creation in list view
function changeType(obj) { /*Change type of tasks(task, workflow, event) */		
	$('.addParticipant').hide();
	$('.createEvent').css('display','block');
	$('.createEvent1').css('display','none');	
	var image = $('#selectTask').val();
	headerText = image.toLowerCase();
	emptyTaskDetails();
	defaultCss(); //---------------->setting default css for the task creation popup
	templateSelectedFlag=false;
	if(headerText == 'workflow') {
	    $("#taskDocCreateImg").hide();
		mid='';
		modelId='';
		projId='';
		parId = 'workflow';
		$('#hiddenTempId').val('');    
		$('.calInstructions').text($("#taskWfMsg").val());
		$('#userSearch').val('');
		$('#workFlowName').val('');
		$('#searchUserImg').attr('src',''+Path+'/images/calender/search.png');
		$('#taskDetails').css('display','none');
		$('#taskWFDetails').empty();
		$('#wfShowTask').empty();
		$('#eventPopup').removeClass('eventPopup').addClass('workFlowPopup');
		$('#createOrCancel').css('width','200px');
		$('#dependencyTask').hide();
		$("#moreButton").hide();
		$('.wFContainer').css('height','');
		$('#createEvent #cancelEvent').css('width','75px');
		$('.addParticipant').css('diaplay','none');
		$('#addParticipantTasks').css('display','none');
		$('#taskDateSave').css('display','none');
		$('#wFTaskDateSave').css('display','block');
		$('input#workFlowName').css('');
		$('input#workFlowName').css('border','');
		$('#wfTemplatePopDiv').hide();
		$('.transparent').show();
		parent.$("#transparentDiv").show();
		$('#selTemplate').html(''); 
		parent.$('#loadingBar').show();
		parent.timerControl("start");
		$.ajax({
			url: Path+'/calenderAction.do',
       		type:"POST",
	 		data:{act:'createWorkFlowTask'},
	 		error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
			    parent.checkSessionTimeOut(result);
			    var wFTaskHtml = result;
			    setWorkflowCss();//------------------------>for workflow task changing the css
			    $('#eventPopup').css('display','block');
			    $('#workFlowContainer').css('display','block');
				//$('#selTemplate').append(wFTaskHtml);
				$('#wfTemplateListContainer').html(wFTaskHtml);
				$("#wfProject").prop("disabled", true);
	  			parent.$('#loadingBar').hide();
		        parent.timerControl("");}
		   });
	}
	if(headerText != 'workflow') {
	    $('#workFlowContainer').css('display','none');
	    $('#wFTaskDateSave').css('display','none');
	    $('#eventContainer').css('overflow','hidden');
		$('#userSearch').val('');
		$('#searchUserImg').attr('src',Path+'/images/calender/search.png');
		$("#eventContainer").find('.mCSB_scrollTools').css('display','none'); 
		
		$('#taskDateSave').css('display','block');
		$('#taskDetails').css('display','block');
		$('#addParticipantTasks').css('display','block');
		$('#dd7 span').text(getValues(Labels,'Select'));
	}
	if(headerText != 'event'){ 
	  $('.selectPlace').hide();
	  $('.estmHour').show();
	  $('#emailDiv').hide();
	}
	if(headerText == 'task'){	
		parId = 'task';
		tflag=true;
		$("#taskDocCreateImg").show();
		$('#actualHourContentDiv').css('display','block');
     	$('.calInstructions').text($("#taskMsg").val());
		$("#participantEmail").css('height','145px');
		$('#projectDiv').css('padding-top','0px');
		$('#eventPopup').removeClass('workFlowPopup').addClass('eventPopup');
		$('.eventDateDiv').hide();
		$('#endDateDiv').val(stDate);
		$('.taskDate').show();
		$("#userProject").prop("disabled",false);
		$('#pariticipateDiv').css('padding-top','10px');
		$('#addParticipantTasks').removeClass().addClass('addTaskEmail');
		$('#titleText').text(getValues(Labels,'Task'));
		$('#dependencyTask').show();
		$("#createOrCancel").css('width','333px');
	}
	if(headerText == 'event') {
	    //$("#taskDocCreateImg").hide();
		parId = 'Event';
		$("#mValueHidddenVal").val('5');
		$("#doctaskType").val('event');
	//	$("#createOrCancel").css('width','200px');
	    $("#createOrCancel").css('width','175px');
		$("#dependencyTask").hide();
        $('.calInstructions').text($("#taskEventMsg").val());
        $('#actualHourContentDiv').css('display','none');
		$('#emailDiv').show();
        $('.taskDate').hide();
        $('.estmHour').hide();
        $('#eventPopup').removeClass('workFlowPopup');
		$('#eventPopup').addClass('eventPopup');
		$('#addParticipantTasks').removeClass();
		$('#addParticipantTasks').addClass('addEventEmail');
		$('.eventDateDiv').show();
		$('#pariticipateDiv').css('padding-top','20px');
		$('#addParticipantTasks').css('height','173px');
		$("#participantEmail").css('height','115px');
		$('#taskDetails').css('height','277px');
//**********************OLD FUNCTIONALITY********************************************************		
	//	$('#startEventDate').val(stDate);
	//	$('#endEventDate').val(stDate);
		initEventCal();
		timePicker();
		$("#userProject").prop("disabled", false);
		$('.selectPlace').show();	
		$('#titleText').text(getValues(Labels,'Event'));
		$('#titleImg').attr('src',Path+'/images/calender/'+image+'Black.png');
	}
	$('#titleImg').attr('src',Path+'/images/calender/'+image+'Black.png');
 }

 function selectStep(evt) {        /* To Select a step in Workflow template in workflow task popup */
    var stepId = evt.target.id.split('_')[1];
    if(stepId && (stepId != cntntVal)){
	    cntntVal = stepId;
	    $('.taskRows').css('background-color','');
	    $('.taskRows').find('.contTitle').css('width','91.5%');
	    //$('.taskDefinition').css('color','#ffffff');
		//$('#mainTaskDiv_'+cntntVal).css('background-color','rgb(255,255,196)');
		$('#mainTaskDiv_'+cntntVal).find('.contTitle').css('width','0px');
		//$('#taskDefinition_'+cntntVal).css('color','#48494F');
		$.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
	       data:{act:'fetchParticipants',projId:projId,userId:'${userId}'},
	       error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
	       success:function(result){
	       	parent.checkSessionTimeOut(result);
	      	$('select#userProject option[id="'+projId+'"]').prop("selected", "selected");
			$('#userListContainer').html(result);
	     	parent.$('#loadingBar').hide();
	     	parent.timerControl("");
	     	$('.wFEmail_'+cntntVal).each(function() {
				var id = $(this).attr('id');
				var userId = id.split('_')[1];
				$('#participant_'+userId).css('display','none');
			});
			var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        if(!isiPad)	{
	      		scrollBar('userListContainer');
	      	}	
	      }
	  	});
	  	$('select#userProject').val(getValues('${calendarLabels}','All_Users'));
	  	
  	}
}
	
	
/*
function addTasks(){  				//Create Task functionality
	var hdrTxt = $('#selectTask').val();
	if(hdrTxt == 'WorkFlow') {
		addWFTask();
		return null;
	}
	else {
		title = $('#eventName').val();
		$('#eventName').css('border','');
		if(hdrTxt == 'Task'){
			$('#startDateDiv').css('border','');
			$('#endDateDiv').css('border','');
			$('#endTime').css('border','');
			start = $('#startDateDiv').val();
			end = $('#endDateDiv').val();
			startTime = $('#startTime').val();
			endTime = $('#endTime').val();
		}
		else {
			$('#startEventDate').css('border','');
			$('#endEventDate').css('border','');
			$('#endEventTime').css('border','');
			$('#eventPlace').css('border','');
			start = $('#startEventDate').val();
			end = $('#endEventDate').val();
			startTime = $('#startEventTime').val();
			endTime = $('#endEventTime').val();
		}
	    if(start == end) {
			var st = parseInt(startTime.replace(':', ''), 10); 
        	var et = parseInt(endTime.replace(':', ''), 10);
			if(st >= et)
            {
               parent.alertFun(getValues(customalertData,"Alert_ValidEndTime"),'warning');
               $('#endTime').css("border","1px solid red");
               return null;
            }
		}
		if(!startTime) {
			startTime = '00:00';
		}
		if(!endTime) {
			endTime = '23:59';
		}
		var event_id='';
		var taskName = $('#dd span').text();
		var taskType = $('#selectTask').val();
		if(taskName == getValues(Labels,'ASSIGNED_TASKS')) {
			if(hdrTxt == 'Event') {
				taskName='assignedEvent';
			}
			else
				taskName='assignedTasks';
			var backgroundColor='#F5CB02';
			var borderColor='#FBAD3D';
		}
		if(taskName == getValues(Labels,'MY_TASKS')) {
			if(hdrTxt == 'Event') {
				taskName='myEvent';
			}
			else
				taskName='myTasks';
			var backgroundColor='#2EB5E0';
			var borderColor='#49BFE6';
		}
		if(!title || title.match(/^\s*$/)) {
			 $('#eventName').css("border","1px solid red");
			 return null;
		}
			var sdArray=start.split('-');
			var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
			var edArray=end.split('-');
			var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
			var d1=new Date(sd);
			var d2=new Date(ed);
			if(d1 > d2){
			  parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
			 if(hdrTxt == 'Event') {
			 $('#startEventDate').css("border","1px solid red");
			 $('#endEventDate').css("border","1px solid red");
			 }else{
			 $('#startDateDiv').css("border","1px solid red");
			 $('#endDateDiv').css("border","1px solid red");
			 }
			return false;
			}
		if(hdrTxt == 'Event') {
			if(!start) {
				$('#startEventDate').css("border","1px solid red");
				return null;
			}	
			if(!end) {
				$('#endEventDate').css("border","1px solid red");
				return null;
			}
			place = $('#eventPlace').val();
			if(!place || place.match(/^\s*$/)) {
				$('#eventPlace').css('border','1px solid red');
				return null;
			}
		}
		if(hdrTxt == 'Task') {
			if(!start) {
				$('#startDateDiv').css("border","1px solid red");
				return null;
			}	
			if(!end) {
				$('#endDateDiv').css("border","1px solid red");
				return null;
			}
			var sdArray=start.split('-');
			var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
			var edArray=end.split('-');
			var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
			var d1=new Date(sd);
			var d2=new Date(ed);
			if(d1 > d2){
			  parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
			  $('#startDateDiv').css("border","1px solid red");
			  $('#endDateDiv').css("border","1px solid red");
			  return false;
			}
			if($.isNumeric($('#totalHour').val()) == false || $('#totalHour').val() < 0){
	         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
	 		       $('#totalHour').css('border','1px solid red');
	 		       return null;
		        }
		   if($.isNumeric($('#totalMinute').val()) == false){
	         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
	 		       $('#totalMinute').css('border','1px solid red');
	 		       return null;
		        } 
		        
		   if($('#totalMinute').val()>59 || $('#totalMinute').val()<0){
		   		   parent.alertFun("Minute value should be between 0 to 59.",'warning');
	 		       $('#totalMinute').css('border','1px solid red');
	 		       return null;
		   
		   }    
	    }else
     		$('#totalHour').css('border','1px solid #BFBFBF');
     		$('#totalMinute').css('border','1px solid #BFBFBF');
			var isiPad = navigator.userAgent.match(/iPad/i) != null;
          			if(!isiPad) {
          				if($('.participantDetails').length <1 && $('.otherUserEmail').length <1 ){		          			
			          			parent.alertFun(getValues(customalertData,"Alert_Participant"),'warning');
			          			return false;
	          			}
          			}else{
          				if($('#participantEmail').children('div').length<=0){
		          			parent.alertFun(getValues(customalertData,"Alert_Participant"),'warning');
		          			return false;
          			    }
          			}	
		var textColor = '#FFFFFF';
		repeat = $('.eventReminder').val();
		description = $('#eventDescription').val();
		if(!description) {
			var description = '';
		}
		var updateStart = start;
		var updateEnd = end;
		var reminder = $('#eventReminder').val();
		var remType = $('#remindThrough').val();
		tHour = $('#totalHour').val();
		tMinute=$('#totalMinute').val();
		if(reminder == getValues(Labels,'Select')) {
			reminder = null;
		}	
		if(remType == getValues(Labels,'Select')) {
			remType = null;
		}			
		if(taskDateXml == '' || taskDateXml == 'null' || taskDateXml == 'undefined' || estmHour != tHour || estmMinute != tMinute) {
			 tmpSDate = start;
		 	 tmpEDate = end;
		 	 var strD=tmpSDate.split("-");
			 var str_date=strD[2]+"/"+strD[0]+"/"+strD[1];
			 var strE=tmpEDate.split("-");
			 var end_date=strE[2]+"/"+strE[0]+"/"+strE[1];
			var tskDates = new Array();
			var tskMonth = new Array();
			var tskYear = new Array();
			var tskDays = new Array();
			var weekday=new Array(7);
			weekday[0]=day0;
			weekday[1]=day1;
			weekday[2]=day2;
			weekday[3]=day3;
			weekday[4]=day4;
			weekday[5]=day5;
			weekday[6]=day6;
			estmHour = $('#estmatedHour').val();
			tHour = $('#totalHour').val();
			
			estmMinute = $('#estmatedMinute').val();
			tMinute = $('#totalMinute').val();
			
			var startDte = new Date(str_date);
		    var futureDte = new Date(end_date);
		    var range = []
		    var  mil = 86400000 //24h in milliseconds
		    var minuteValue=$('#totalMinute').val();
		    var curHour=0;
		    var curMinute=0;
			var totalDay = (futureDte-startDte)/mil +1;
		    var hr_day=tHour/totalDay;
		    var hr_per_day=Math.round(hr_day);
		    var flag=0;
		    
		    
		    var min_day=tHour/totalDay;
		    var min_per_day=Math.round(min_day);
		    var flag_min=0;
		    var j=futureDte.getTime();
		    if((tHour>0) && (hr_per_day==0) ){
			   hr_per_day=tHour;
			}
			if((tMinute>0) && (min_per_day==0) ){
			   min_per_day=tHour;
			} 
		     taskDateXml = "<taskHour>";
		     for (var i=startDte.getTime(); i<=futureDte.getTime();i=i+mil) {
		     	curHour = parseInt(curHour,10) + parseInt(hr_per_day,10);
		     	curMinute = parseInt(curMinute,10) + parseInt(min_per_day,10);
	     		if(curHour < tHour) {
	     			hr_per_day = hr_per_day;
	     			if(i==j){
				  	 hr_per_day= parseInt(tHour,10) - (parseInt(curHour,10) - parseInt(hr_per_day,10));
					} 
	     		}
	     		else {
	     			if(flag == 0){
	      				curHour = curHour - hr_per_day;
			       		hr_per_day = tHour - curHour;
			       		flag = 1;
		      		}
		      		else
		      			hr_per_day = 0;
				}
				
				if(curMinute < tMinute) {
	     			min_per_day = min_per_day;
	     			if(i==j){
				  	 min_per_day= parseInt(tMinute,10) - (parseInt(curMinute,10) - parseInt(min_per_day,10));
					} 
	     		}
	     		else {
	     			if(flag_min == 0){
	      				curMinute = curMinute - min_per_day;
			       		min_per_day = tMinute - curMinute;
			       		flag_min = 1;
		      		}
		      		else
		      			min_per_day = 0;
				}
				
				tskDates[i] = new Date(i).getDate();
			    tskMonth[i] = new Date(i).getMonth()+1;
			    tskYear[i] = new Date(i).getFullYear();
			    tskDays[i] = new Date(i).getDay();
			    var day = weekday[tskDays[i]]; 
			    var month = tskMonth[i];
			    if(month < 10) {
			    	month = '0'+month;
			    }
			    if(tskDates[i] < 10) {
			    	tskDates[i] = '0'+tskDates[i];
			    }
		      taskDateXml +="<taskDate id=\""+i+"\">"
			  taskDateXml +="<date>"+tskDates[i]+"/"+month+"/"+tskYear[i]+"</date>";
			  taskDateXml +="<day>"+day+"</day>";
			  taskDateXml +="<hour>"+hr_per_day+"</hour>";
			  taskDateXml +="<min>"+min_per_day+"</min>";
	 		  taskDateXml +="<status>Checked</status>";
	 		  taskDateXml +="</taskDate>";
	 		  if((tHour>0) && (hr_per_day==0) ){
	 		  	flag = 1;
	 		  }
	 		  if((tMinute>0) && (min_per_day==0) ){
	 		  	flag_min = 1;
	 		  }
			}	
			taskDateXml +="</taskHour>";
	    }
	    if(hdrTxt == 'Task') {   // Add Task 
	    	tHour = $('#totalHour').val();
			parent.$('#loadingBar').show();
			$('#totalMinute').css('border','1px solid #BFBFBF');
			parent.timerControl("start");
			$.ajax({ 
		       url: Path+'/calenderAction.do',
		       type:"POST",
		       data:{act:'addtask',taskType:taskType,title:title,start:start,end:end,startTime:startTime,endTime:endTime,repeat:repeat,description:description,eId:eId,taskProject:projId,priorityId:priorityId,update:update,selectedTaskemailId: selectedTaskemailId,reminder:reminder,reminderType:remType,taskDateXml : taskDateXml,recPatternValue:recPatternValue,recEnd:recEnd ,recType:recType,estimatedHour:tHour,estimatedMinute:minuteValue,taskDependencyId:taskDependencyId},
		       success:function(result){
		       	parent.checkSessionTimeOut(result);
		       	if(view=='calendarV'){
			        event_id = result.split('#@#@')[1];
			        	start = start.split('-');
			        	start = start[2]+'-'+start[0]+'-'+start[1];
			        	end = end.split('-');
			        	end = end[2]+'-'+end[0]+'-'+end[1];
			  			start = start+" "+startTime;
						end = end+" "+endTime;
						var assignedTaskDatas = result.split('#@#@')[0];
			        	var myTaskDatas = result.split('#@#@')[2];
			        	var historyDatas = result.split('#@#@')[3];
			        	calendarViewDatas(myTaskDatas,assignedTaskDatas,historyDatas);
			        	parent.$('#loadingBar').hide();
			        	parent.timerControl("");			
					if(update == 'no') {			
						taskCalendar($('#dd span').text());
			        }
					if(update == 'yes') {	
						calendar.fullCalendar('updateEvent', curr_event);
						calendar.fullCalendar('refetchEvents');
					}	
				}				
				else if(view=='listV' && moreValue=='myTasks'){
					showMyTaskList();
				}
				else if(view=='listV' && moreValue=='assignedTasks'){
					showAssignedTaskList();
				}
				taskDependencyId="";
			  }
			});
			taskDependencyId="";
		}
		if(hdrTxt == 'Event') {   // Add Event 
			parent.$('#loadingBar').show();
			parent.timerControl("start");
			$.ajax({ 
		       url: Path+'/calenderAction.do',
		       type:"POST",
		       data:{act:'addEvent',title:title,start:start,end:end,startTime:startTime,endTime:endTime,place:place,description:description,eId:eId,taskProject:projId,priorityId:priorityId,update:update,selectedTaskemailId:selectedTaskemailId,selectedOtherEmail:selectedOtherEmail,reminder:reminder,reminderType:remType},
		       success:function(result){
		       	parent.checkSessionTimeOut(result);
		       	if(view=='calendarV'){
			        event_id = result.split('#@#@')[1];
			        	start = start.split('-');
			        	start = start[2]+'-'+start[0]+'-'+start[1];
			        	end = end.split('-');
			        	end = end[2]+'-'+end[0]+'-'+end[1];
			  			start = start+" "+startTime;
						end = end+" "+endTime;
						var assignedTaskDatas = result.split('#@#@')[0];
			        	var myTaskDatas = result.split('#@#@')[2];
			        	var historyDatas = result.split('#@#@')[3];
			        	$('#myTasksList').html('');
			        	$('#assignedTasksList').html('');
			        	$('#historyListTasks').html('');
			        	calendarViewDatas(myTaskDatas,assignedTaskDatas,historyDatas);
			        	parent.$('#loadingBar').hide();
			        	parent.timerControl("");
			        	
			        	if(update == 'no') {
							taskCalendar($('#dd span').text());
				        }
			//       if(update == 'no') {
			//			calendar.fullCalendar('renderEvent', {id:event_id, title: title, start: start, end: end, backgroundColor:backgroundColor,borderColor:borderColor, textColor:textColor,allDay : false ,timeFormat: 'H(:mm)',editable: true,taskType:taskName,ignoreTimezone: true,stick:true},true);
			//			calendar.fullCalendar('refetchEvents');
			//		}
					if(update == 'yes') {
						calendar.fullCalendar('updateEvent', curr_event);
						calendar.fullCalendar('refetchEvents');
					}
				}
				else if(view=='listV' && moreValue=='myTasks'){
					showMyTaskList();
				}
				else if(view=='listV' && moreValue=='assignedTasks'){
					showAssignedTaskList();
				}
			  }
			});
		}
	}closePopup();	
}
*/
/*
var wfContentData="complete";		
 function addWFTask() {   		//Create Workflow Tasks 
	
	    $('input#workFlowName').css('border','');
		title = $('#workFlowName').val();
		if(!title || title.match(/^\s*$/)) {
			$('input#workFlowName').css('border','1px solid red');
			return null;
		}
		
		  if($('[id^=estmatedMinute_]').val()>59 ){
		   		   parent.alertFun("Minute value should be between 0 to 59.",'warning');
	 		       $('[id^=estmatedMinute_]').css('border','1px solid red');
	 		       return null;
		   
		   }    
	    else{
     		$('[id^=estmatedMinute_]').css('border','1px solid #BFBFBF');
     		}
     		
     		
     		   if($.isNumeric($('[id^=estmatedMinute_]').val()) == false || $('[id^=estmatedMinute_]').val()<0){
	         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
	 		       $('[id^=estmatedMinute_]').css('border','1px solid red');
	 		       return null;
		        }
		    else{
     		  $('[id^=estmatedMinute_]').css('border','1px solid #BFBFBF');
     		} 
		
		
     		   if($.isNumeric($('[id^=estmatedHour_]').val()) == false || $('[id^=estmatedHour_]').val()<0){
	         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
	 		       $('[id^=estmatedHour_]').css('border','1px solid red');
	 		       return null;
		        } 
		       else{
     		       $('[id^=estmatedHour_]').css('border','1px solid #BFBFBF');
     		   }   
		  
		var projId = $('#wfProject option:selected').attr('id');    
		var modelId = $('#hiddenTempId').val();    
		var dStatus=validateRequiredFields();
		 $("div.wFactive").each(function (i) {
		 	var ids=$(this).attr('id');
	        var tId=ids.split('_')[1];
		    var wFDescription = $('#wFDescription_'+tId).val();
			var wFTitle = $('#taskDefinition_'+tId).parent().attr('title');
			var wFTaskId = 300;
			var wFStartDate = $('#wFStart_'+tId).val();
			wFStartDate = wFStartDate.split('-');
			wFStartDate = wFStartDate[2]+'-'+wFStartDate[0]+'-'+wFStartDate[1];
			var wFEndDate = $('#wFEnd_'+tId).val();
			wFEndDate = wFEndDate.split('-');
			wFEndDate = wFEndDate[2]+'-'+wFEndDate[0]+'-'+wFEndDate[1];
			var wFStartTime = $('#wFStartTime_'+tId).val();
			var wFEndTime = $('#wFEndTime_'+tId).val();
			if(!wFStartTime) {
				wFStartTime = '00:00';
			}
			if(!wFEndTime) {
				wFEndTime = '23:59';
			}
			var reminder = $('#remDrpDown_'+tId).val();
			var remType = $('#remTypeDrpDown_'+tId).val();
			var taskName = $('#dd span').text();
			var taskType = $('#selectTask').val();
			if(taskName == 'Assigned Tasks') {
				taskName='assignedWorkflow';
				var backgroundColor='#F5CB02';
				var borderColor='#FBAD3D';
			}
			if(taskName == getValues(Labels,'MY_TASKS')) {
				taskName='myWorkflow';
				var backgroundColor='#2EB5E0';
				var borderColor='#49BFE6';
			}
			var wFStart = wFStartDate+" "+wFStartTime;
			var wFEnd = wFEndDate+" "+wFEndTime;
			var textColor = '#FFFFFF';
			var taskDetails = $('#taskDateDetails_'+tId).val();
			if(taskDetails == ''){
				
				wFTaskDateXml=wFTaskDateXml+wFDateXml(tId);
				
			}
			else{
				wFTaskDateXml=wFTaskDateXml+taskDetails;
			}
	    });
	    
	    wFTaskDateXml +="</WFTaskDates>";
		if(dStatus == 'invalidDate'){ 
			parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
		    return false;
	     }
	     else{
	     	$('[id^=wFStart_]').css("border","");
			$('[id^=wFEnd_]').css("border","");
	     }
	     if(dStatus=='valid'){
			var taskXmlData = constructTaskXml();			
			var taskId = $('#taskId').val();
			var flowName = title;
			if(taskXmlData == '<taskData></taskData>'){
				parent.alertFun(getValues(customalertData,"Alert_InvalidWorkFlow"),'warning');
		    	return null;
			}
			else{
			
				parent.$('#loadingBar').show();
				parent.timerControl("start");
				$.ajax({
		            url: Path+"/calenderAction.do", 
		            type: "POST",
		            data: {act:'wfTaskInsert',refModelId:modelId ,priority:priorityId,taskId:taskId, taskXml: taskXmlData,status:"", taskFlowName:flowName, wFTaskDateXml: wFTaskDateXml,projectId:projId},  
		            success: function (result) {
		            	parent.checkSessionTimeOut(result);
		            	if(view=='calendarV'){
			            	var assignedTaskDatas = result.split('#@#@')[0];
				        	var myTaskDatas = result.split('#@#@')[1];
				        	var historyDatas = result.split('#@#@')[2];
				        	var workFlowId = result.split('#@#@')[3];
				        	$('#myTasksList').html('');
				        	$('#assignedTasksList').html('');
				        	$('#historyListTasks').html('');
				        	calendarViewDatas(myTaskDatas,assignedTaskDatas,historyDatas);
				        	taskCalendar($('#dd span').text());
			        	}
			        	else if(view=='listV' && moreValue=='myTasks'){
							showMyTaskList();
						}
						else if(view=='listV' && moreValue=='assignedTasks'){
							showAssignedTaskList();
						}
			        	parent.$('#loadingBar').hide();
			        	parent.timerControl("");
		            	closePopup();
		            }
	           });  
	         }   
          }else if(dStatus == 'invalidTime'){
          	   parent.alertFun(getValues(customalertData,"Alert_ValidEndTime"),'warning');
          }else{
          	parent.alertFun(getValues(customalertData,"Alert_Participant"),'warning');
          }
}	
 */
 var wfContentData="complete";
 function addWFTask() {   		/*Create Workflow Tasks */
	
	    $('input#workFlowName').css('border','');
		title = $('#workFlowName').val();
		if(!title || title.match(/^\s*$/)) {
			$('input#workFlowName').css('border','1px solid red');
			return null;
		}
		
		  if($('[id^=estmatedMinute_]').val()>59 ){
		   		   parent.alertFun(getValues(companyAlerts,"Alert_MinValue"),'warning');
	 		       $('[id^=estmatedMinute_]').css('border','1px solid red');
	 		       return null;
		   
		   }    
	    else{
     		$('[id^=estmatedMinute_]').css('border','1px solid #BFBFBF');
     		}
     		
     		
     		   if($.isNumeric($('[id^=estmatedMinute_]').val()) == false || $('[id^=estmatedMinute_]').val()<0){
	         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
	 		       $('[id^=estmatedMinute_]').css('border','1px solid red');
	 		       return null;
		        }
		    else{
     		  $('[id^=estmatedMinute_]').css('border','1px solid #BFBFBF');
     		} 
		
		
     		   if($.isNumeric($('[id^=estmatedHour_]').val()) == false || $('[id^=estmatedHour_]').val()<0){
	         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
	 		       $('[id^=estmatedHour_]').css('border','1px solid red');
	 		       return null;
		        } 
		       else{
     		       $('[id^=estmatedHour_]').css('border','1px solid #BFBFBF');
     		   }   
		  
		var projId = $('#wfProject option:selected').attr('id');    
		var modelId = $('#hiddenTempId').val();    
		var dStatus=validateRequiredFields();
		 $("div.wFactive").each(function (i) {
		 	var ids=$(this).attr('id');
	        var tId=ids.split('_')[1];
		    var wFDescription = $('#wFDescription_'+tId).val();
			var wFTitle = $('#taskDefinition_'+tId).parent().attr('title');
			var wFTaskId = 300;
			var wFStartDate = $('#wFStart_'+tId).val();
			wFStartDate = wFStartDate.split('-');
			wFStartDate = wFStartDate[2]+'-'+wFStartDate[0]+'-'+wFStartDate[1];
			var wFEndDate = $('#wFEnd_'+tId).val();
			wFEndDate = wFEndDate.split('-');
			wFEndDate = wFEndDate[2]+'-'+wFEndDate[0]+'-'+wFEndDate[1];
			var wFStartTime = $('#wFStartTime_'+tId).val();
			var wFEndTime = $('#wFEndTime_'+tId).val();
			if(!wFStartTime) {
				wFStartTime = '00:00';
			}
			if(!wFEndTime) {
				wFEndTime = '23:59';
			}
			var reminder = $('#remDrpDown_'+tId).val();
			var remType = $('#remTypeDrpDown_'+tId).val();
			var taskName = $('#dd span').text();
			var taskType = $('#selectTask').val();
			if(taskName == 'Assigned Tasks') {
				taskName='assignedWorkflow';
				var backgroundColor='#F5CB02';
				var borderColor='#FBAD3D';
			}
			if(taskName == getValues(Labels,'MY_TASKS')) {
				taskName='myWorkflow';
				var backgroundColor='#2EB5E0';
				var borderColor='#49BFE6';
			}
			var wFStart = wFStartDate+" "+wFStartTime;
			var wFEnd = wFEndDate+" "+wFEndTime;
			var textColor = '#FFFFFF';
			var taskDetails = $('#taskDateDetails_'+tId).val();
			if(taskDetails == ''){
				
				wFTaskDateXml=wFTaskDateXml+wFDateXml(tId);
				
			}
			else{
				wFTaskDateXml=wFTaskDateXml+taskDetails;
			}
	    });
	    
	    wFTaskDateXml +="</WFTaskDates>";
		if(dStatus == 'invalidDate'){ 
			parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
		    return false;
	     }
	     else{
	     	$('[id^=wFStart_]').css("border","");
			$('[id^=wFEnd_]').css("border","");
	     }
//********************************NEW FUNCTIONALITY******************************************************************************	     
	     if(dStatus == 'invalid' && wfContentData!="incompleteData"){ 
	     	parent.confirmFun(getValues(companyAlerts,"Alert_SaveIncompleteStatus"),"delete","addWrkFlowTask");
	     
	     	return false;
	     }
     
	     
	     if(dStatus=='valid' || dStatus=='invalid'){
			var taskXmlData = constructTaskXml();			
			var taskId = $('#taskId').val();
			var flowName = title;
			if(taskXmlData == '<taskData></taskData>'){
				parent.alertFun(getValues(customalertData,"Alert_InvalidWorkFlow"),'warning');
		    	return null;
			}
	
	 	else{
	 			parent.$('#loadingBar').show();
				parent.timerControl("start");
				$.ajax({
		            url: Path+"/calenderAction.do", 
		            type: "POST",
		            data: {act:'wfTaskInsert',refModelId:modelId ,priority:priorityId,taskId:taskId, taskXml: taskXmlData,status:"", taskFlowName:flowName, wFTaskDateXml: wFTaskDateXml,projectId:projId},
		            error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							},   
		            success: function (result) {
		            	parent.checkSessionTimeOut(result);
		            	if(view=='calendarV'){
			            	var assignedTaskDatas = result.split('#@#@')[0];
				        	var myTaskDatas = result.split('#@#@')[1];
				        	var historyDatas = result.split('#@#@')[2];
				        	var workFlowId = result.split('#@#@')[3];
				        	$('#myTasksList').html('');
				        	$('#assignedTasksList').html('');
				        	$('#historyListTasks').html('');
				        	calendarViewDatas(myTaskDatas,assignedTaskDatas,historyDatas);
				        	taskCalendar($('#dd span').text());
			        	}
			        	else if(view=='listV' && moreValue=='myTasks'){
							showMyTaskList();
						}
						else if(view=='listV' && moreValue=='assignedTasks'){
							showAssignedTaskList();
						}
			        	parent.$('#loadingBar').hide();
			        	parent.timerControl("");
		            	closePopup();
		            }
	           });  
	           wfContentData="complete";
	         }   
          }else if(dStatus == 'invalidTime'){
          	   parent.alertFun(getValues(customalertData,"Alert_ValidEndTime"),'warning');
          }else{
          	parent.alertFun(getValues(customalertData,"Alert_Participant"),'warning');
          }
}	
function addWrkFlowTask(){
		wfContentData="incompleteData";
		addWFTask();
}
 
 function removeWFParticipant(obj,email,uId,stepId) {   /* Delete Users from the workflow  */
	var id = $(obj).parent().attr('id');
	$('#'+id).remove();
	$('#participant_'+uId).css('display','block');
	$('#userUnCheck_'+uId).attr('class','userUnCheck');
	checkType='removeAll';
	if(!id) {
		var className = $(obj).parent().attr('class');
		$($(obj).parent().attr('class')).remove();
	}	
	var deleteId = email+"_"+uId;
	selectedTaskemailId = selectedTaskemailId.substring(0, selectedTaskemailId.indexOf(',' +deleteId)) + selectedTaskemailId.substring(selectedTaskemailId.indexOf(',' +deleteId) + deleteId.length + 1, selectedTaskemailId.length);
	if($('#wFParticipantEmail_'+stepId+'.mCustomScrollbar').length) {
		$('#wFParticipantEmail_'+stepId).mCustomScrollbar("update");
		$('#userListContainer').mCustomScrollbar("update");
	}	if($("[id^=participant_]").is(':visible')){$("[id^=inner]").hide();}else{$("[id^=inner]").show();}
}

 

/*function removeParticipant(obj,email,uId) {			// Delete User from Task  
	$('[id^=innerDiv]').hide();
	var id = $(obj).parent().attr('id');
	$('#'+id).remove();
	$('#participant_'+uId).css('display','block');
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	if(!isiPad)
		$("#userListContainer").mCustomScrollbar("update");
	if(!id) {
		var className = $(obj).parent().attr('class');
		$($(obj).parent().attr('class')).remove();
	}		
	var deleteId = email+"_"+uId;
	selectedTaskemailId = selectedTaskemailId.substring(0, selectedTaskemailId.indexOf(',' +deleteId)) + selectedTaskemailId.substring(selectedTaskemailId.indexOf(',' +deleteId) + deleteId.length + 1, selectedTaskemailId.length);
	if($('#participantEmail.mCustomScrollbar').length) {
		$("#participantEmail").mCustomScrollbar("update");
		$('#userListContainer').mCustomScrollbar("update");
	}
	checkType='removeAll';if($("[id^=participant_]").is(':visible')){$("[id^=innerDiv]").hide();}else{$("[id^=innerDiv]").show();}
}*/
 
function showWFTaskHoursPopup(i) {
 	stepVal = i;
 	var startDate=$('#wFStart_'+i).val();
    var endDate=$('#wFEnd_'+i).val();
    if(startDate == "" || startDate == "Date" || startDate == null){
		$('#wFStart_'+i).css("border","1px solid red");
		return false;		
	}else{
	    $('#wFStart_'+i).css("border","1px solid #BFBFBF");
	}
	if(endDate == "" || endDate == "Date" || endDate == null){
		$('#wFEnd_'+i).css("border","1px solid red");
		return false;		
	}else{
	    $('#wFEnd_'+i).css("border","1px solid #BFBFBF");
	}
	var sdArray=startDate.split('-');
	var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
	var edArray=endDate.split('-');
	var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
	var d1=new Date(sd);
	var d2=new Date(ed);
	if(d1 > d2){parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
			  $('#wFStart_'+i).css("border","1px solid red");
			  $('#wFEnd_'+i).css("border","1px solid red");
			  return false;
				}
	else{
			  $('#wFStart_'+i).css("border","");
			  $('#wFEnd_'+i).css("border","");
	 }
     tHour = $('#estmatedHour_'+i).val();
     tMinute = $('#estmatedMinute_'+i).val();
     if($.isNumeric(tHour) == false){
     	parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
		$('#estmatedHour_'+i).css('border','1px solid red');
		return null;
     }
     else{
     	$('#estmatedHour_'+i).css('border','1px solid #BFBFBF');
     	$('#taskEhourPopupDiv').show();
     	createHourPopup(startDate,endDate,tHour,tMinute);
     }
     
     
     
     if($.isNumeric(tMinute) == false){
     	parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
		$('#estmatedMinute_'+i).css('border','1px solid red');
		return null;
     }
     else{
     	$('#estmatedMinute_'+i).css('border','1px solid #BFBFBF');
     	$('#taskEhourPopupDiv').show();
     	createHourPopup(startDate,endDate,tHour,tMinute);
     }
     	
} 
 function validateRequiredFields(){
	  var status='valid';
	  $("div.wFactive").each(function (i) {
	   var idd=$(this).attr('id');
	   var ids=idd.split('_')[1];
	   var wFStartDate = $('#wFStart_'+ids).val();
	   var wFEndDate = $('#wFEnd_'+ids).val();
	   var wFStartTime = $('#wFStartTime_'+ids).val();
	   var wFEndTime = $('#wFEndTime_'+ids).val();
	   if($('#wFStart_'+ids).val()== "" || $('#wFStart_'+ids).val()== null || 
	 	$('#wFEnd_'+ids).val() == "" || $('#wFEnd_'+ids).val() == null ||
	 	$('#wFParticipantEmail_'+ids).find('div[id^=wFEmail]').length == 0)
	 	{	
	 	     status="invalid";
	 	}
	 	var sdArray=wFStartDate.split('-');
				var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
				var edArray=wFEndDate.split('-');
				var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
				var d1=new Date(sd);
				var d2=new Date(ed);
				if(d1 > d2){
				   status="invalidDate";
				  $('#wFStart_'+ids).css("border","1px solid red");
				  $('#wFEnd_'+ids).css("border","1px solid red");
				  return false;
				}
		if(wFStartDate == wFEndDate) {
			var st = parseInt(wFStartTime.replace(':', ''), 10); 
        	var et = parseInt(wFEndTime.replace(':', ''), 10);
			if(st >= et){
               $('#wFEndTime_'+ids).css("border","1px solid red");
               status="invalidEndTime";
               if($("div#taskWFDetails").find('div.mCSB_scrollTools').css('display') == 'block'){ 
	        	    $("div#taskWFDetails").mCustomScrollbar("scrollTo","#wFContents_"+ids,{
						 scrollInertia:0,
						 callbacks:false
				    });
			   }
               return false;
            }
            else{		
            	 $('#wFEndTime_'+ids).css("border","1px solid #BFBFBF");}
		}
	});
	 return status;
}


function scrollTemplate(id) {
	$("div#" + id).mCustomScrollbar({
		scrollButtons : {
			enable : true
		},
	});
	$('div#' + id + '.mCSB_container').css('margin-right', '15px');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_buttonUp').css(
			'background-position', '-80px 0px');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_buttonDown').css(
			'background-position', '-80px -20px');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_buttonUp').css('opacity', '0.6');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_buttonDown')
			.css('opacity', '0.6');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_draggerRail').css('background',
			'#B1B1B1');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css(
			'background', '#6C6C6C');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar')
			.css('background', '#B1B1B1');
	$(
			'div#'
					+ id
					+ ' .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar')
			.css('background', '#B1B1B1');

}	

function constructTaskXml(){		/*Construct xml for workflow Tasks */
		var wFTaskEmail="";
		var tData = "<taskData>";
	    jQuery("div.wFactive").each(function (i) {
	    	var ids=$(this).attr('id');
	        var tId=ids.split('_')[1];
	        var WFTaskMail="";
	        $(".wFEmail_"+tId).each(function(i) {
	        	var divId = $(this).attr('id').split('_')[1];
	        	if(WFTaskMail) {
	        		WFTaskMail = WFTaskMail+','+'N:'+divId+',';
	        	}
	        	if(!WFTaskMail){
	        		WFTaskMail = 'N:'+divId;
	        	}	
			});
			tData +="<taskSet id=\""+tId+"\">"
			tData +="<tInstructions>"+$('#wFDescription_'+tId).val()+"</tInstructions>";
			tData +="<tDefinition>"+$('#taskDefinition_'+tId).text()+"</tDefinition>";
			tData +="<tUsersId>"+WFTaskMail+"</tUsersId>";
			tData +="<tStartDate>"+$('#wFStart_'+tId).val()+"</tStartDate>";
			tData +="<tEndDate>"+$('#wFEnd_'+tId).val()+"</tEndDate>";
			tData +="<tStartTime>"+$('#wFStartTime_'+tId).val()+"</tStartTime>";
			tData +="<tEndTime>"+$('#wFEndTime_'+tId).val()+"</tEndTime>";
			tData +="<tReminder>"+$('#remDrpDown_'+tId).val()+"</tReminder>";
			tData +="<tReminderType>"+$('#remTypeDrpDown_'+tId).val()+"</tReminderType>";
			tData +="<tEstmHour>"+$('#estmatedHour_'+tId).val()+"</tEstmHour>";
			tData +="<tEstmMin>"+$('#estmatedMinute_'+tId).val()+"</tEstmMin>";
			tData +="<tLevelId>"+$('#hiddenLevelId_'+tId).val()+"</tLevelId>";
			tData +="<tLevelName>"+$('a[stepIndex='+tId+'] span').text()+"</tLevelName>";
			tData +="</taskSet>";
		});
		tData +="</taskData>"
		return tData;
	} 

var wFTaskDateXml="<WFTaskDates>";
  var j = 0;
 function getWFDateXml(k){
     var TaskDateXml = "<taskHour id=\""+k+"\">";
     var i=1;
     estmHour=0;
     estmMinute=0;
	 $('.estimatedHourDiv').each(function() {
	 	 var status = $(this).children('input.edCheckbox').val();
		 var date = $(this).children('div').children('span.taskDate').text().trim();
		 var day = $(this).children('div').children('span.taskDay').text().trim();
		 var hour = $(this).children('div').children('input.eHour').val().trim();
		 var minute = $(this).children('div').children('input.eMin').val().trim();
		 if(!hour || hour == "undefined") {
		 	hour = 0;
		 }
		 if(!minute || minute== "undefined") {
		 	minute = 0;
		 }
		 estmHour = parseInt(estmHour) + parseInt(hour);
		 estmMinute = parseInt(estmMinute) + parseInt(minute);
		 TaskDateXml +="<taskDate id=\""+i+"\">"
		 TaskDateXml +="<date>"+date+"</date>";
		 TaskDateXml +="<day>"+day+"</day>";
		 TaskDateXml +="<hour>"+hour+"</hour>";
		 TaskDateXml +="<min>"+minute+"</min>";
		 TaskDateXml +="<status>"+status+"</status>";
		 TaskDateXml +="</taskDate>";
		 i++;
		
 	});
	TaskDateXml +="</taskHour>";
	$('#taskDateDetails_'+k).val(TaskDateXml);
	$('#estmatedMinute_'+k).val(estmMinute);
	$('#estmatedHour_'+k).val(estmHour);
	hideTaskHourDiv();
 }	
   function wFDateXml(k){
 	var TaskDateXml = "<taskHour id=\""+k+"\">";
	 var start = $('#wFStart_'+k).val();
 	 var end = $('#wFEnd_'+k).val();
 	 var strD=start.split("-");
	 var str_date=strD[2]+"/"+strD[0]+"/"+strD[1];
	 var strE=end.split("-");
	 var end_date=strE[2]+"/"+strE[0]+"/"+strE[1];
	var tskDates = new Array();
	var tskMonth = new Array();
	var tskYear = new Array();
	var tskDays = new Array();
	var weekday=new Array(7);
	weekday[0]=day0;
	weekday[1]=day1;
	weekday[2]=day2;
	weekday[3]=day3;
	weekday[4]=day4;
	weekday[5]=day5;
	weekday[6]=day6;
	tHour = $('#estmatedHour_'+k).val();
	var startDte = new Date(str_date);
	var futureDte = new Date(end_date);
	var tMinute = $('#estmatedMinute_'+k).val();
	var range = []
	var  mil = 86400000 //24h
	var curHour=0;
	var curMinute=0;
	var totalDay = (futureDte-startDte)/mil +1;
	var hr_day=tHour/totalDay;
	var hr_per_day=Math.round(hr_day);
	
	
	var min_day=tMinute/totalDay;
	var min_per_day=Math.round(min_day);
	
	var flag=0;
	var flag_min=0;
	var j=futureDte.getTime();
    if((tHour>0) && (hr_per_day==0) ){
	   hr_per_day=tHour;
	}
	if((tMinute>0) && (min_per_day==0) ){
	   min_per_day=tMinute;
	} 
	var j=1;
     for (var i=startDte.getTime(); i<=futureDte.getTime();i=i+mil) {
     	curHour = parseInt(curHour,10) + parseInt(hr_per_day,10);
     	curMinute = parseInt(curMinute,10) + parseInt(min_per_day,10);
    		if(curHour < tHour) {
    			hr_per_day = hr_per_day;
    			if(i==j){
		  	 hr_per_day= parseInt(tHour,10) - (parseInt(curHour,10) - parseInt(hr_per_day,10));
			} 
    		}
    		else {
    			if(flag == 0){
     				curHour = curHour - hr_per_day;
	       		hr_per_day = tHour - curHour;
	       		flag = 1;
      		}
      		else
      			hr_per_day = 0;
		}
		
		
		if(curMinute < tMinute) {
    		min_per_day = min_per_day;
    		if(i==j){
		  	 min_per_day= parseInt(tMinute,10) - (parseInt(curMinute,10) - parseInt(min_per_day,10));
			} 
    	}
    	else {
    			if(flag_min == 0){
     				curMinute = curMinute - min_per_day;
		       		min_per_day = tMinute - curMinute;
		       		flag_min = 1;
      		    }
      		    else
      			    min_per_day = 0;
		}
		tskDates[i] = new Date(i).getDate();
	    tskMonth[i] = new Date(i).getMonth()+1;
	    tskYear[i] = new Date(i).getFullYear();
	    tskDays[i] = new Date(i).getDay();
	    var day = weekday[tskDays[i]]; 
	    var month = tskMonth[i];
	    if(month < 10) {
	    	month = '0'+month;
	    }
	    if(tskDates[i] < 10) {
	    	tskDates[i] = '0'+tskDates[i];
	    }
	    
	  TaskDateXml +="<taskDate id=\""+j+"\">"
	  TaskDateXml +="<date>"+tskDates[i]+"/"+month+"/"+tskYear[i]+"</date>";
	  TaskDateXml +="<day>"+day+"</day>";
	  TaskDateXml +="<hour>"+hr_per_day+"</hour>";
	  TaskDateXml +="<min>"+min_per_day+"</min>";
	  TaskDateXml +="<status>Checked</status>";
	  TaskDateXml +="</taskDate>";
	  if((tHour>0) && (hr_per_day==0) ){
	  	flag = 1;
	  }
	  if((tMinute>0) && (min_per_day==0) ){
	  	flag_min = 1;
	  }
	 
	  j++;
	}	
	TaskDateXml +="</taskHour>";
	return TaskDateXml;
 }
 function confirmDate(){
		 if(endId == 'endAfter') {
		 	if($('#noOfOccurences').val() > 0) {
		    	recEnd = $('#noOfOccurences').val()+' occurence(s)';
		    }		
		   	else {
		   		$('#noOfOccurences').css('border','1px solid red');
		   		return null;
		   	}	
	    }	
	    if(endId == 'endDate') {
	    	if($('#recEnd').val()) {
	   			var date = $('#recEnd').val().split('-');
	   			recEnd=date[2]+'-'+date[0]+'-'+date[1];
	   		}
		   	else {
		   		$('#recEnd').css('border','1px solid red');
		   		return null;
		   	}	
	    }
	    if(endId == 'noEnd') {
	    	recEnd= 'No End';
	    }
	   $('#recurrencePopupDiv').hide();
 }



 
//-------------------> workflow task new functions --------------------------->



function defaultCss(){
  
  var h=550;
  var w=830;
  $('#eventPopup').css('width',w+'px');
  $('#eventPopup').css('height',h+'px');
  $('#eventPopup').css('margin-left','-'+(w/2)+'px');
  $('#eventPopup').css('margin-top','-'+(h/2)+'px');
  $('#eventContainer').width('520px');
  $('#eventContainer').height(h-10);
  $('#taskWFDetails').width('100%');
  $('#userContainer').height(h - 40 - 17 - 30 );
  $('#userListContainer').css('height','100%');
}
function setWorkflowCss(){
  var h=parent.$('#menuFrame').height()-50;
  if(h >= 550)
    h=550;
  var w=parent.$('#menuFrame').width()-80;
  if(w >= 1060)
      w=1060;
  if(w<=930)
       w=930;    
  $('#eventPopup').css('width',w+'px');
  $('#eventPopup').css('height',h+'px');
  $('#eventPopup').css('margin-left','-'+(w/2)+'px');
  $('#eventPopup').css('margin-top','-'+(h/2)+'px');
  var w_userDiv=$('#projectUsers').width();
  $('#eventContainer').width(w-w_userDiv-35);
  $('#eventContainer').height(h-10);
  $('#taskWFDetails').width('99%');
  $('#taskWFDetails').height(h - 48 - 90 - 35-10 );
  $('#userContainer').height(h - 40 - 17 - 30 );
  $('#userListContainer').css('height','100%');
}

   var templateSelectedFlag = false;
 function checkTemplateSelected(){
   //alert(templateSelectedFlag);
   if(!templateSelectedFlag){
       var flow = $('#workFlowName').val().trim();
       if(flow !=''){
          generateTabWithoutTemplate();
       }  
     }
 }
 
 function generateTabWithoutTemplate(){
       var html=' <div id=\"crumbs\" style="padding-bottom: 1px;margin-top:5px;float:left;width:100%;" >';
          html+='   <img src="'+Path+'/images/addGroupUser.png" style="margin: 7px 0px 0px 15px; height: 20px; cursor: pointer; float: left;" onclick="addWFCustomLevel();" class="addWFCustomLevel" id="addNewLevel" title="'+ getValues(Labels,'WF_AddLevel')+'">';
          html+=' </div>';
         
         $('#taskWFDetails').html(html);
         prevIndex = 0;
         var lastRowStepCount =  $('#crumbs').children('ul:last').children('li').length;
         html= createWFTab(parseInt(prevIndex),'Step '+(parseInt(prevIndex)+1)+'',lastRowStepCount);
         $('#crumbs').children('ul:last').append(html);
         createWFTabDetailsHtml(parseInt(prevIndex));
         cntntVal=0;	
         templateSelectedFlag=true;
         $('#crumbs ul li a').css({'width':'100px','font-size':'14px','padding-left':'20px'});
 }
 
 function onEnterCheckTemplateSelected(event,obj){
    var code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if (code == 13) {
     if(!templateSelectedFlag){
       var flow = $('#workFlowName').val().trim();
       if(flow !=''){
          generateTabWithoutTemplate();
       }
     }
   }
}
 
 var prevIndex = 0;
 function addWFCustomLevel(){
    //var prevIndex = $('#crumbs').children('ul:last').children('li:last-child').children('a').attr('stepIndex');
    var lastRowStepCount =  $('#crumbs').children('ul:last').children('li').length;
    /*if(isNaN(prevIndex)){
         prevIndex = 0;
         var html='   <img src="'+Path+'/images/addGroupUser.png" style="margin: 7px 0px 0px 15px; height: 20px; cursor: pointer; float: left;" onclick="addWFCustomLevel();" class="addWFCustomLevel" id="addNewLevel" title="'+ getValues(Labels,'WF_AddLevel')+'">';
         $('#crumbs').html(html);
         html= createWFTab(0,'Step 1',0);
         $('#crumbs').children('ul:last').append(html);
         createWFTabDetailsHtml(0);
         cntntVal=0;	
         $('#crumbs ul li a').css({'width':'100px','font-size':'14px','padding-left':'20px'});
	 	 $('#taskWFDetails').find('.taskRows').hide();
		 tabActive($('#crumbs ul:first-child li:first-child a'));
		 showTabDetails($('#crumbs ul:first-child li:first-child a'));
         
    }else{ */
        prevIndex++;
        var tabHml = createWFTab(parseInt(prevIndex),'Step '+(parseInt(prevIndex)+1)+'',lastRowStepCount);
	    if(lastRowStepCount==0){
	      $('#crumbs').children('ul:last').append(tabHml);
	      createWFTabDetailsHtml(parseInt(prevIndex));
	    }else{
	        $('#crumbs').children('ul').find('a.activeTab').parent('li').after(tabHml);
	        createWFTabDetailsHtml(parseInt(prevIndex));
	      
	        var xml = generateCustomStepXml();
		    var html='   <img src="'+Path+'/images/addGroupUser.png" style="margin: 7px 0px 0px 15px; height: 20px; cursor: pointer; float: left;" onclick="addWFCustomLevel();" class="addWFCustomLevel" id="addNewLevel" title="'+ getValues(Labels,'WF_AddLevel')+'">';
		    $('#crumbs').html(html);
		     html = '';
		    var xmlData = xml.split('</wfLevel>');
		    if(xmlData.length >0){
				for(var i=0;i< xmlData.length-1;i++){
					 html= createWFTab(getValues(xmlData[i],"order"),getValues(xmlData[i],"name"),i);
					 $('#crumbs').children('ul:last').append(html);
				}
				$('#taskWFDetails').find('.taskRows').hide();
				tabActive($('#crumbs').children('ul').children('li').children('a[stepIndex='+prevIndex+']'));
				showTabDetails($('#crumbs').children('ul').children('li').children('a[stepIndex='+prevIndex+']'));
		   }
	    }  
	    $('#crumbs ul li a').css({'width':'100px','font-size':'14px','padding-left':'20px'});
	    var isiPad = navigator.userAgent.match(/iPad/i) != null;
		if(!isiPad)
			$('#taskWFDetails').mCustomScrollbar('update');
	    
	    
    //}
 }
 function createWFTab(tabIndex,tabName,tabTotCount){
	   //alert('tabIndex1:'+tabIndex);
	   var w = $('#crumbs').width()-15;
	   //alert(w);
	   var tabW = 145;
	   var tabsPerRow = Math.round(parseInt(w)/parseInt(tabW));
	  // alert(tabCount);
	   var tabHtml ="";
        if(tabIndex==0 || (tabTotCount % tabsPerRow)==0){
  		  tabHtml = "<ul style='float:left;clear:both;'></ul>";
  		  $('#crumbs').children('img.addWFCustomLevel').before(tabHtml);
  		}
		 tabHtml="  <li>";
		 tabHtml+="  <a href=\"#\" onclick=\"tabActive(this);showTabDetails(this);\" ondblclick=\"editWFCustomStep(this);\"  stepIndex=\""+tabIndex+"\" levelId=\"\" action=\"InProgress\" >";
		 tabHtml+="    <span style=\"width: 100%; float: left; height: 100%; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" title=\""+tabName+"\">"+tabName+"</span>";
		 tabHtml+="    <input type='text' value='' class='inputWFCustomStep' style='display:none;width:100%;border:1px solid #bfbfbf; height:20px;' onkeydown='setWFStepName(event,this);' onblur='checkWFStepName(this);'/>"; 
		 tabHtml+="  </a>";
		 tabHtml+=" </li>";
		
		return tabHtml;	
 }
	
function createWFTabDetailsHtml(tabIndex){
        //alert('tabIndex2:'+tabIndex);
  		var tabHtml="";
  		parent.$('#loadingBar').show();
		parent.timerControl("start");
			$.ajax({
		            url: Path+"/calenderAction.do", 
		            type: "POST",
		            data: {act:'createWFTabDetailsHtml',tabIndex:tabIndex ,tabTitle:"Step "+(tabIndex+1)},
		            error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							},   
		            success: function (result) {
		            	parent.checkSessionTimeOut(result);
		            	$('#taskWFDetails').append(result);
		            //	$('#mainTaskDiv_'+tabIndex).find('.wFSDatePicker').val(stDate);					/*Dates are optional as for incomplete task*/
				   //     $('#mainTaskDiv_'+tabIndex).find('.wFEDatePicker').val(stDate);
				        $('#mainTaskDiv_'+tabIndex).find('input.hasTimePicker').timepicker({ 'timeFormat': 'H:i' });
            
		            	$('#mainTaskDiv_'+tabIndex).find('.contTitle').css('width','0px');
				       	$('.WFTaskActive').css('border','');
						//$('.taskDefinition').css('color','#ffffff');
						//$('#mainTaskDiv_'+tabIndex).css('background-color','rgb(255,255,196)'); 
						//$('#wFContents_'+tabIndex).find('.taskDefinition').css('color','#48494F');	
						$('.taskRows').find('img[id^=deleteStep_]').show();				
				        $('#taskWFDetails').find('.taskRows').hide();
				        //alert( $('#taskWFDetails').find('#crumbs').children('ul').children('li').children('a[stepIndex='+tabIndex+'] > span').text());
						tabActive($('#crumbs').children('ul').children('li').children('a[stepIndex='+tabIndex+']'));
						showTabDetails($('#crumbs').children('ul').children('li').children('a[stepIndex='+tabIndex+']'));
		            	var isiPad = navigator.userAgent.match(/iPad/i) != null;
            			if(!isiPad)
	                		scrollBar('taskWFDetails');
		            	parent.$('#loadingBar').hide();
			        	parent.timerControl("");
		            }
	           });  
	}	
	
function createCheckUncheckTemplateHtml(){
  var html = "<div class=\"taskWFEnableDisabale wfTask\" id=\"wfShowTask\" style=\"visibility: visible; display: block;float:left;width:95%;padding-left:15px;\">";  
	   html+=  "<input type=\"radio\" name=\"taskEnableDisabale\" id=\"hht\" checked=\"checked\" onclick=\"taskEDclick(this);\" style=\"float:left;\" class=\"taskWFEnableDisabalechkbox\">";
	   html+=  "<span style=\"float:left;color:#848484;margin-left:5px;font-weight:normal;width:200px;height:15px;overflow:hidden;\"><label>"+ getValues(Labels,'Remove_Unchecked_Tasks')+"</label></span>  <input type=\"radio\" name=\"taskEnableDisabale\" id=\"dht\" style=\"margin-left: 20px;float:left;\" onclick=\"taskEDclick(this);\" class=\"taskWFEnableDisabalechkbox\">";
	   html+=  "<span style=\"float:left;color:#848484;margin-left:5px;font-weight:normal;width:200px;height:15px;overflow:hidden;\"><label>"+getValues(Labels,'Show_All_Tasks')+"</label></span>";
	   html+=  "</div>";
    $('#wFTemplateContainer').append(html);
}

function showTabDetails(obj){
   var stepIndex = $(obj).attr('stepIndex');
   //alert(stepIndex);
   cntntVal=stepIndex;	
   $('#taskWFDetails').find('.taskRows').hide();
   $('#mainTaskDiv_'+stepIndex).show(); 
   //$('.taskRows').css('background-color','');
   $('.taskRows').find('.contTitle').css('width','91.5%');
   //$('.taskDefinition').css('color','#ffffff');
   //$('#mainTaskDiv_'+stepIndex).css('background-color','rgb(255,255,196)');
   $('#mainTaskDiv_'+stepIndex).find('.contTitle').css('width','0px');
   //$('#taskDefinition_'+stepIndex).css('color','#48494F');

   	$('#userListContainer').html(userListResult);

   $('[id^=wFEmail]:visible').each(function(){
	   	var ids=$(this).attr('id').split("_");	
	   	$("#userListContainer").find("div#participant_"+ids[1]).hide();
	});
   var isiPad = navigator.userAgent.match(/iPad/i) != null;
   if(!isiPad)
      scrollBar('userListContainer');
}

function editWFCustomStep(obj){
   $(obj).children('span').hide();
   var stepName = $(obj).children('span').attr('title');
   $(obj).children('input').val(stepName).show();
   $(obj).children('input').focus();
   //$(obj).removeAttr('onclick');//tabActive(this);showTabDetails(this);

} 
var confirmWfObj=null;
function setWFStepName(event,obj){
  var code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
  if (code == 13) {
        var stepName = $(obj).val().trim();
        if(stepName==''){
           confirmWfObj= obj;
           parent.confirmReset(getValues(customalertData, "Alert_WFLevelEmpty"),'reset','editWFCustomLevelContinue','editWFCustomLevelCancel');
		   return false;
        }else{
           var stepName = $(obj).val().trim();
           $(obj).hide();
           $(obj).prev('span').attr('title',stepName).text(stepName).show();
           var stepIndex = $(obj).parent().attr('stepIndex');
           $('#taskDefinition_'+stepIndex).text(stepName);
        }
  }
  
}

function checkWFStepName(obj){
   
    var stepName = $(obj).val().trim();
     if(stepName==''){
        confirmWfObj= obj;
        parent.confirmReset(getValues(customalertData, "Alert_WFLevelEmpty"),'reset','editWFCustomLevelContinue','editWFCustomLevelCancel');
        return false;
     }else{
        var stepName = $(obj).val().trim();
        $(obj).hide();
        $(obj).prev('span').attr('title',stepName).text(stepName).show();
        var stepIndex = $(obj).parent().attr('stepIndex');
        $('#taskDefinition_'+stepIndex).text(stepName);
     }
}
function editWFCustomLevelContinue(){
  $(confirmWfObj).focus();
}
function editWFCustomLevelCancel(){
  $(confirmWfObj).hide();
  $(confirmWfObj).prev('span').show();
}
var confirmIndex='';
function deleteCustomStep(obj){
  confirmIndex = $(obj).attr('id').split('_')[1];
  parent.confirmFun(getValues(customalertData,"Alert_delete"),"delete","confirmDeleteCustomStep");
}
function confirmDeleteCustomStep(){
    $('#crumbs').find('a[stepIndex='+confirmIndex+']').parent('li').remove();
    $('#taskWFDetails').find('#mainTaskDiv_'+confirmIndex).remove();
    var xml = generateCustomStepXml();
    var html='   <img src="'+Path+'/images/addGroupUser.png" style="margin: 7px 0px 0px 15px; height: 20px; cursor: pointer; float: left;" onclick="addWFCustomLevel();" class="addWFCustomLevel" id="addNewLevel" title="'+ getValues(Labels,'WF_AddLevel')+'">';
    $('#crumbs').html(html);
     html = '';
    var xmlData = xml.split('</wfLevel>');
    if(xmlData.length >0){
		for(var i=0;i< xmlData.length-1;i++){
			 html= createWFTab(getValues(xmlData[i],"order"),getValues(xmlData[i],"name"),i);
			 $('#crumbs').children('ul:last').append(html);
		}
		$('#crumbs ul li a').css({'width':'100px','font-size':'14px','padding-left':'20px'});
	 	$('#taskWFDetails').find('.taskRows').hide();
		tabActive($('#crumbs ul:first-child li:first-child a'));
		showTabDetails($('#crumbs ul:first-child li:first-child a'));
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
		if(!isiPad)
		   $('#taskWFDetails').mCustomScrollbar('update');
   }
}

function generateCustomStepXml(){
  var levelXml="<xml>";
  $('#crumbs').find('li').each(function(){
       var l_name = $(this).find('span').text();
       var l_index = $(this).find('a').attr('stepIndex');
       levelXml +="<wfLevel>";
       levelXml +=" <name>"+l_name+"</name>";
	   levelXml +=" <order>"+l_index+"</order>";
	   levelXml +="</wfLevel>"
  });
  levelXml +="</xml>";
  return levelXml;
}



var wfCalendar;
function initwfdhtmlxdatecalNew(WFstart,WFEnd) {
            wfCalendar = new dhtmlXCalendarObject(["wFStart","wFEnd"]);
			wfCalendar.setDateFormat("%m-%d-%Y");
			$('.dhtmlxcalendar_container').css({'z-index':'11000','border-radius':'5px','border':'2px solid #BFBFBF','background-color':'#FFFFFF'});
			wfCalendar.hideTime();
			wfCalendar.attachEvent("onClick",function(date){
                //$('#taskTotalEhour').val('0');
            });
            var today = new Date();
			wfCalendar.setSensitiveRange(today, null);
			
	wfCalendar.setPosition('right');
	$('.dhtmlxcalendar_container').css('z-index','10000');
	$('.dhtmlxcalendar_container').css('border-radius','5px');
	$('.dhtmlxcalendar_container').css('border','2px solid #BFBFBF');
	$('.dhtmlxcalendar_container').css('background-color','#FFFFFF');
 }
 
 function setWFSens(id, k, sensVar) {
							// update range
		if (k == "min") {
		  wfCalendar.setSensitiveRange(byWFId(id).value, null);
		} else {
			if(byWFId(id).value)
				wfCalendar.setSensitiveRange(new Date(), byWFId(id).value);
			else
				wfCalendar.setSensitiveRange(new Date(), null);
		}
 }
 function byWFId(id) {
	return document.getElementById(id);
 }

function initWFSlider(){ 
	 $('div.userCompPerSlider').each(function() {
		var value = parseInt( $( this ).text(), 10 );
		$( this ).empty().slider({
			value: value,
			range: "min",
			animate: true,
			disabled: true ,
	   	orientation: "horizontal"
	   });
	});
	if(jQuery.browser.msie){$('#vas').css('height','20px');}
}
function createTaskInlist(){					
				initCal();
				var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();	    
				$('.Create').text(getValues(Labels,'Save'));
				$('.Create').attr('title',getValues(Labels,'Save'));
				var currentDate = new Date();
				var day = currentDate.getDate();			
				var month = currentDate.getMonth() + 1;				
				var year = currentDate.getFullYear();
				var hour = currentDate.getHours();
				var minute = currentDate.getMinutes();
				var stTime = hour+':'+minute;
				$("#dependencyTask").show();
				$("#taskDocCreateImg").show();
				$("#docIconNameLevel").hide();
				tflag=true;
			    $('#dependencyTask').attr("onclick","dependencyTaskPopUp()");
			//    $('.To').css("margin-left","23px");
				update = 'no';
				getAllUsers();
				$('.calInstructions').text($("#taskMsg").val());			
				$('#startDateDiv').val('');
				if(userType == 'web_standard')
					$('#selectTask > .Work_Flow').hide();
				else
					$('#selectTask > .Work_Flow').show();
	   			$("#eventPopup").show();
	   			$('#eventContainer').show();
	   			$('#actualHourContentDiv').show();
	   			$('#projectUsers').show();
	   			$('.estmHour').show();
	   			timePicker();
	   			$('#pariticipateDiv').show();			
				stDate = $.fullCalendar.formatDate(date, 'MM-dd-yyyy');
				var sendDate = $.fullCalendar.formatDate(date, 'yyyy/MM/dd');
				sendDate = new Date(Date.parse(sendDate.replace(/-/g,' ')))
				today = new Date();
				today.setHours(0,0,0,0);
				if (sendDate < today) {
					stDate = $.fullCalendar.formatDate(today, 'MM-dd-yyyy');
				} 
/***************NEW FUNCTIONALITY*******************************************************************************************************/								
		//		$('#startDateDiv').val(stDate);			
		//		$('#endDateDiv').val(stDate);
				
				$('#createEvent').removeClass().addClass('createEvent');
	   			$('#wFTaskDateSave').css('display','none');
	   			$('#taskDateSave').css('display','block');
	   			$('.createEvent').attr('onclick','addTasks();');
				$('.transparent').show();
				parent.$("#transparentDiv").show();
				$('select#project').val(getValues(Labels,"All_Users"));
				dayObj = $(this);
				var el = document.getElementById('createButton');
			    el.scrollIntoView(true);
			    initCal();
			    getAllProject();
			    if(height < 550){
				    var isiPad = navigator.userAgent.match(/iPad/i) != null;
		    		if(!isiPad)	{
						scrollBar('eventPopup');
					}	
				}
			    $("#dependencyTask").show();
			    $('#dependencyTask').attr("onclick","dependencyTaskPopUp()");
				$('#selectedTaskDiv').html('');
	}
/*	function dependencyTaskPopUp(){
		$("#dependencyCreateTask").css('display','block');
		$("#dependencyTaskListContainer").css('display','block');
		var title = $('#eventName').val();
		var start = $('#startDateDiv').val();
		var	end = $('#endDateDiv').val();
		$("#dependencyEventName").val(title);
		$("#depTaskStartDate").val(start);
		$("#depTaskEndDate").val(end);
		$("#depTaskList").append('<div id="loadingB" align="center" overflow:hidden">loading...</div>');
		$("#loadingB").css("margin-top",$("#depTaskList").height()/2);
		$.ajax({
			url:Path+"/calenderAction.do",
			type:"POST",
			data:{act:"dependencyTaskList"},
			success:function(result){
			$("#loadingB").remove();
			$("#depTaskList").html(result);
			var isiPad = navigator.userAgent.match(/iPad/i) != null;
				if(!isiPad){
		    		scrollBar('depTaskList');
	        	}	
				if(taskDependencyId!=""){
					var id=taskDependencyId.split(",");
					for(var i=0;i<id.length-1 ;i++){
						$("#depTask_"+id[i]).removeClass("taskUnSelected").addClass("taskSelected").css("background","").hide();
						if(!isiPad){
						}
					}
				}
				
			}
	  	 });
	
		
		
		
	}	
*/	
    var saveTaskDepCode="";
	function dependencyListTask(obj){
		var addTask="";
		var taskName=$('#dependencyTaskDropDown').find(":selected").text();
		var taskId=$('#dependencyTaskDropDown').find(":selected").val();
		if(	$("select#dependencyTaskDropDown option:selected").val()!="0"){
		/*	addTask=addTask+"<div id=\"\" style=\"margin-left:18px;width:96%;float:left;border-radius: 6px;height:30px;\">"
						   		+"<div class=\"task\" id=\"dependencyTask_"+taskId+"\" style=\"margin-top:6px;width: 400px;color:black;float:left;overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\"><span style=\"font-weight:100;font-size:15px;margin-top:3px;\">"+taskName+"</span></div>"
						   		+"<div style=\"float:right;\">"
						   			+"<img src=\""+Path+"/images/calender/userDelete.png\" id=\"\" style=\"margin-top:8px;cursor: pointer;\" onclick=\"removeDependencyTask("+taskId+",'create');\">"
								+"</div>"
							+"</div>";	
							*/
							
							addTask=addTask+"<div class=\"task\" title=\""+taskName+"\" onclick=\"\" id=\"dependencyTask_"+taskId+"\" style=\"height:30px;margin-left:19px;margin-top:6px;width: 562px;overflow: hidden; text-overflow: ellipsis; white-space: nowrap;color:black;float:left; \"><span  style=\"font-weight:initial;font-size:13px;margin-top:5px;float:left;margin-left:10px;overflow: hidden; text-overflow: ellipsis; white-space: nowrap;width:500px;\">"+taskName+"</span>"
					 							+"<img title=\"remove\" src=\""+Path+"/images/calender/userDelete.png\" id=\"\" style=\"margin-right: 5px;float:right;margin-top:8px;cursor: pointer;\" onclick=\"removeDependencyTask("+taskId+",'create');\">"
					 						+"</div>";
							
							
				$('#selectedTaskDiv').append(addTask);
				scrollBar('selectedTaskDiv');
				$("select#dependencyTaskDropDown option[value=" + taskId + "]").hide();
				saveTaskDepCode=saveTaskDepCode+addTask;
		}
//		$('select#dependencyTaskDropDown').find('option').val(taskId).hide();
	
	}
/*	function closeDependencyPopUp(){
	   $('#dependencyCreateTask').hide();
	   $("select#sortTasks").val('All');
	   $("select#sortInWs").val('sort');
	}*/
	var remDependencyId="";
	function removeDependencyTask(taskId,mode){
		if(mode=="create"){
			$("#depSelectedDiv_"+taskId).remove();
			$("#depSavedDiv_"+taskId).remove();
			$('#depTaskList').mCustomScrollbar("update");
			$("#depTask_"+taskId).removeClass("taskSelected").addClass("taskUnSelected").css("background","").show();
			$("#chkDep_"+taskId).addClass("userUnChecked");
		//	$("#depTaskList").find("#depTask_"+id).removeClass("taskSelected").addClass("taskUnSelected");
		}
		else if(mode=="update"){
		//	updateRemoveDepId=updateRemoveDepId+taskId+",";
			remDependencyId=remDependencyId+taskId+",";
			$("#depSavedDiv_"+taskId).remove();
			$('#selectedDependencyTaskContent').mCustomScrollbar("update");
			$("#depTaskList").find("#depTask_"+id).show();
			$("#depTask_"+taskId).removeClass("taskSelected").addClass("taskUnSelected").css("background","").show();
			$("#chkDep_"+taskId).addClass("userUnChecked");
			/*var splitTempNewDepId=tempNewDepId.split(',');
			for(var i=0;i< splitTempNewDepId.length ;i++){
				alert(splitTempNewDepId[i]);
			}*/
		}
		else{		
			$("#dependencyListTask_"+taskId).remove();
			$('#dependencyTaskList').mCustomScrollbar("update");
			$("select#dependencyUpdateTaskDropDown option[value=" + taskId + "]").show();
			var splitTempNewDepId=tempNewDepId.split(',');
			tempNewDepId="";
			for(var i=0;i< splitTempNewDepId.length-1 ;i++){
				if(splitTempNewDepId[i] != taskId){
					tempNewDepId=tempNewDepId+splitTempNewDepId[i]+",";
				}
			}
		}
	}
	function saveDependencyTasks(){
		var setId="";
		$('[id^=depSelectedDiv_]').each(function(){
			 setId = setId+$(this).attr('id').split("_")[1] +",";
		});
		taskDependencyId=setId;
		closeDependencyPopUp();
	}
	function listDependencyTask(){
		
		var listDepTask="";
		dependencyTaskUpdateDropDown();
		$.ajax({
			url:Path+"/calenderAction.do",
			type:"POST",
			data:{act:"getDependencyTaskListView",taskId:myTaskId},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
				var jsonData=jQuery.parseJSON(result);	
				for(var i=0; i < jsonData.length ;i++){
					 var taskId=jsonData[i].taskId;			
					 var taskName=jsonData[i].taskName;	
					 $('#dependencyListTaskPopup').show();
					 listDepTask=listDepTask+"<div class=\"task\" title=\""+taskName+"\" onclick=\"\" id=\"dependencyListTask_"+taskId+"\" style=\"height:30px;margin-left:19px;margin-top:6px;width: 562px;overflow: hidden; text-overflow: ellipsis; white-space: nowrap;color:black;float:left; \"><span  style=\"font-weight:initial;font-size:13px;margin-top:5px;float:left;margin-left:10px;overflow: hidden; text-overflow: ellipsis; white-space: nowrap;width:500px;\">"+taskName+"</span>"
					 							+"<img title=\"remove\" src=\""+Path+"/images/calender/userDelete.png\" id=\"\" style=\"margin-right: 5px;float:right;margin-top:8px;cursor: pointer;\" onclick=\"removeDependencyTask("+taskId+",'update');\">"
					 						+"</div>"
									
								+"</div>";
				updateOldDepTaskId=updateOldDepTaskId+taskId+",";	
				$("select#dependencyUpdateTaskDropDown option[value=" + taskId + "]").hide();
				}
		//	$("select#dependencyUpdateTaskDropDown option[value=" + taskId + "]").hide();
		    $('#dependencyTaskList').append(listDepTask);
		    
		    
		    if(result=="[]"){
		    	 $('#dependencyListTaskPopup').show();
		    }
		    if(updateNewDepId!=""){
		  	 	 var taskPerDependencyId=updateNewDepId.split(",");
		  	 	for(var i=0; i < taskPerDependencyId.length-1 ; i++){
			  	 	var taskName=$("select#dependencyUpdateTaskDropDown option[value=" + taskPerDependencyId[i] + "]").first().text();
			  	 	var	addTask="<div class=\"task\" title=\""+taskName+"\" onclick=\"\" id=\"dependencyListTask_"+taskPerDependencyId[i]+"\" style=\"height:30px;margin-left:19px;margin-top:6px;width: 562px;overflow: hidden; text-overflow: ellipsis; white-space: nowrap;color:black;float:left; \"><span  style=\"font-weight:initial;font-size:13px;margin-top:5px;float:left;margin-left:10px;overflow: hidden; text-overflow: ellipsis; white-space: nowrap;width:500px;\">"+taskName+"</span>"
						 							+"<img title=\"remove\" src=\""+Path+"/images/calender/userDelete.png\" id=\"\" style=\"margin-right: 5px;float:right;margin-top:8px;cursor: pointer;\" onclick=\"removeDependencyTask("+taskPerDependencyId[i]+",'');\">"
						 						+"</div>";
						$('#dependencyTaskList').append(addTask);
						addTask="";
						$("select#dependencyUpdateTaskDropDown option[value=" +taskPerDependencyId[i]+ "]").hide();
		  	 	}
	  	 	}	
	  	 	if(updateRemoveDepId!=""){
	  	 		var taskPerDependencyId=updateRemoveDepId.split(",");
		  	 	for(var i=0; i < taskPerDependencyId.length-1 ; i++){
		  	 		$("#dependencyTaskList").find("#dependencyListTask_"+taskPerDependencyId[i]).hide();
		  	 		
		  	 	}
	  	 	}
		    scrollBar('dependencyTaskList');
		   }
	  	 });
	  //	 scrollBar('dependencyTaskList');
	}
	function dependencyTaskUpdateDropDown(){
		parent.$("#transparentDiv").show();
		parent.$("#loadingBar").show();
		parent.timerControl("start");
			$.ajax({
			url:Path+"/calenderAction.do",
			type:"POST",
			data:{act:"dependencyTaskList"},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
			var jsonData=jQuery.parseJSON(result);	
				for(var i=0; i < jsonData.length ;i++){
					 var taskId=jsonData[i].id;			
					 var taskName=jsonData[i].taskName;	
					 var option = document.createElement("option");
			         document.getElementById("dependencyUpdateTaskDropDown").options.add(option);
		     	     option.text = taskName;
			         option.value = taskId;	
			         $("select#dependencyUpdateTaskDropDown option[value="+myTaskId+"]").hide();
			         parent.$("#loadingBar").hide();
				     parent.timerControl("");
			         $("#dependencyUpdateTaskDropDown").find("option").addClass("drpDwnOptions").css("width","543px");
			    }
		   }
	  	 }); 
	}
	var tempNewDepId="";	
	function dependencyUpdateListTask(obj){
		var updateDepTaskHtml="";
		var taskName=$('#dependencyUpdateTaskDropDown').find(":selected").text();
		var taskId=$('#dependencyUpdateTaskDropDown').find(":selected").val();
		tempNewDepId=tempNewDepId+taskId+",";
		if(	$("select#dependencyUpdateTaskDropDown option:selected").val()!="0"){
			updateDepTaskHtml=updateDepTaskHtml+"<div class=\"task\" title=\""+taskName+"\" onclick=\"\" id=\"dependencyListTask_"+taskId+"\" style=\"height:30px;margin-left:19px;margin-top:6px;width: 562px;overflow: hidden; text-overflow: ellipsis; white-space: nowrap;color:black;float:left; \"><span  style=\"font-weight:initial;font-size:13px;margin-top:5px;float:left;margin-left:10px;overflow: hidden; text-overflow: ellipsis; white-space: nowrap;width:500px;\">"+taskName+"</span>"
					 							+"<img title=\"remove\" src=\""+Path+"/images/calender/userDelete.png\" id=\"\" style=\"margin-right: 5px;float:right;margin-top:8px;cursor: pointer;\" onclick=\"removeDependencyTask("+taskId+",'');\">"
					 						+"</div>"
									
								+"</div>";
				$('#dependencyTaskList').append(updateDepTaskHtml);
				scrollBar('dependencyTaskList');
				$("select#dependencyUpdateTaskDropDown option[value=" + taskId + "]").hide();
		}
//		$('select#dependencyTaskDropDown').find('option').val(taskId).hide();
		 
	}
	function updateDependencyTaskList(){
		taskDependencyId="";
		updateNewDepId="";
	//	updateOldDepTaskId=updateOldDepTaskId.split(",");
		var taskId=$('#dependencyUpdateTaskDropDown').find(":selected").val();
	/*	$('[id^=dependencyListTask_]').each(function(){
			 taskDependencyId = $(this).attr('id').split("_")[1];
			 for(var i=0; i < updateOldDepTaskId.length-1 ; i++){
			 	if(updateOldDepTaskId[i]!=taskDependencyId){
			 		updateNewDepId=updateNewDepId+taskDependencyId+",";
			 	}
			 }		updateDependencyTaskList(
		});*/
		$('[id^=depSelectedDiv_]').each(function(){
			updateNewDepId=updateNewDepId+$(this).attr('id').split("_")[1]+",";
		
		});
	//	updateNewDepId=tempNewDepId;
		updateRemoveDepId=remDependencyId;	
		closeDependencyPopUp();
	//	tempNewDepId="";
	$("select#dependencyUpdateTaskDropDown option").show();
	}

function checkDependencyListTask(obj) {
		var classObj = $(obj).attr('class');
		var id=$(obj).attr("id").split("_")[1];
	//	var pIconId=$(obj).attr('type').trim();
		if(classObj == 'userChecked') {
		     $(obj).removeClass('userChecked').addClass('userUnChecked');
		     $("#depTask_"+id).css("background-color","");
			if($('div.userChecked').length<1){
				  $('img.addParticipantIcon').hide();
				  $('img.userCheck').addClass('userUncheck').removeClass('userCheck').attr('src',$path+'/images/Idea/uncheck.png');
	 		}	
		}else{
		    $(obj).removeClass('userUnChecked').addClass('userChecked');	
			$('img.addParticipantIcon').show();
			$("#depTask_"+id).css("background-color","#eeeeee");
		}	
 }
 var selectedTaskHtmlDiv="";
 function addTaskAsDependency(obj){
 	selectedTaskHtmlDiv="";
 	$('.userChecked ').each(function() {
 		var id=$(this).attr('id').split("_")[1];
 		var toDate=$("#toDate_"+id).text();
 		var fromDate=$("#fromDate_"+id).text();
 		var taskName=$("#taskName_"+id).text();
 		$("#depTaskList").find("#depTask_"+id).removeClass("taskUnSelected").addClass("taskSelected").hide();
 		$("#chkDep_"+id).removeClass("userChecked");
  		selectedTaskHtmlDiv=selectedTaskHtmlDiv+'<div id="depSelectedDiv_'+id+'" style="border-bottom: 1px solid #bfbfbf;border-radius: 5px;box-shadow: 1px 1px 5px #cccccc inset;height: 55px;  margin-bottom: 10px;  padding-bottom: 5px;padding-top: 5px;cursor: pointer; float: left;width: 98%;" >'
						        +' <div style="float:left;width:100%;height:35px;">'
					            +'   <div  align="left" id="selectedTaskName_'+id+'" style="width:450px;padding-left:5px; float: left;font-weight: normal;height: 30px;overflow: hidden;" title="'+taskName+'">'+taskName+'</div>'
								+"   <div  align='center' id='depTaskDelete_"+id+"' style='width: 20px;float:left;color: #848484;overflow: hidden;padding-left: 5px;'><img src='"+Path+"/images/Idea/userDelete.png' class='Delete_cLabelTitle' style='height: 18px;width: 18px;cursor:pointer;margin-top:7px;' onclick='removeDependencyTask("+id+",\"create\");'></div>"						        +' </div>'
								+' <div style="float:left; border-top:1px solid #BFBFBF;width:100%;font-size: 10px;">'
										+"<div  align='left' class=\"   \" style='color:black;float: left;  margin-top: 5px; width: 34px;'>"+getValues(Labels,'From')+"&nbsp;:</div>"
						 				+"<div id=\"fromSelectedDepDate_"+id+"\" align='left'  style=\"font-weight:normal;float:left;width:80px;margin-top: 5px;height:18px;overflow:hidden;padding-left:5px;\">"+fromDate+"</div>"
						 						//+"<span style=\"float: left; margin-top: 4px;\">|</span>"
						 				+"<div  align='left' class=\"   \" style='color:black;float: left;  margin-top: 5px; width: 22px;'>"+getValues(Labels,'To')+"&nbsp;:</div>"
						 				+"<div id=\"toSelectedDepDate_"+id+"\" align='left'  style=\"font-weight:normal;float:left;width:80px;margin-top: 5px;height:18px;overflow:hidden;padding-left:5px;\">"+toDate+"</div>"
					 			+"</div>"
						+'</div>';
 	});
 	$("#selectedDependencyTaskDiv").find('#selectedDependencyTaskContent').append(selectedTaskHtmlDiv);
 	scrollBar('selectedDependencyTaskContent');
 	$("#addTasksdependency").hide();
 }
 
 function searchDepPage(obj){
	var name = $(obj).val().toLowerCase();
	$('div.taskUnSelected').hide();
	if(!name) {
		$('div.taskUnSelected').show();
		$('.depSearchUserImg').attr('src',Path+'/images/calender/search.png');
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad){
        		scrollBar('depTaskList');
        }	
        hideDepTasks();
		return null;
	}
	else {
		$('.depSearchUserImg').attr('src',Path+'/images/remove.png').css({'width':'17px','height':'16px'}).attr('onclick','clearSearchDepTask(this)');
		$('div#depTaskList').find('span.taskNameHidden:contains("'+name+'")').parents('div.taskUnSelected').show();
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad){
        		$('#depTaskList').mCustomScrollbar("update");
        }	
        hideDepTasks();
        return null;
	}
 }
 function hideDepTasks(){				
	$('[id^=depTask_]').each(function() {
		var id = $(this).attr('id').split("_")[1];
		var taskId = id.split('_')[1];
		$('#depTask_'+taskId).css('display','none');
	});//if($('[id^=depTask_]:visible').length < 1){$('#depTaskList').append('<div id="innerDiv20" style="margin-top:-459px;" align="center"> </div>');}else{ $("[id^=innerDiv]").hide();}
}
function clearSearchDepTask(obj){   /* Clear Search Field  */
	var id = $(obj).prev().val('');
	$('.depSearchUserImg').attr('src',Path+'/images/calender/search.png');
	$('div.taskUnSelected').show()
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
    if(!isiPad){
		
       		$("#depTaskList").mCustomScrollbar("update");	
	}
	hideDepTasks();		
	return null;
}
/*
function updateListDependencyTask(){
		$("#dependencyCreateTask").css('display','block');
		$("#dependencyTaskListContainer").css('display','block');
		var title = $('#eventName').val();
		var start = $('#startDateDiv').val();
		var	end = $('#endDateDiv').val();
		$("#dependencyEventName").val(title);
		$("#depTaskStartDate").val(start);
		$("#depTaskEndDate").val(end);
		$("#depTaskList").html("");
		$("#depTaskList").append('<div id="loadingB" align="center" overflow:hidden">loading...</div>');
		$("#loadingB").css("margin-top",$("#depTaskList").height()/2);
		$("#selectedDependencyTaskDiv").find('#selectedDependencyTaskContent').html('');
		
	  	 
	  	 
		$.ajax({
			url:Path+"/calenderAction.do",
			type:"POST",
			data:{act:"dependencyTaskList",taskId:myTaskId},
			success:function(result){
					$("#depTaskList").html(result);		
					
					var isiPad = navigator.userAgent.match(/iPad/i) != null;
						if(!isiPad){
				    		scrollBar('depTaskList');
			        	}
			        	$.ajax({	
							url:Path+"/calenderAction.do",
							type:"POST",
							data:{act:"getDependencyTaskListView",taskId:myTaskId},
							success:function(result){
							 $("#selectedDependencyTaskDiv").find('#selectedDependencyTaskContent').append(result);
							 
							var isiPad = navigator.userAgent.match(/iPad/i) != null;
				    		if(!isiPad){
				 				scrollBar('selectedDependencyTaskContent');
				 			}
						 	if(updateNewDepId!=""){
							 	var newId=updateNewDepId.split(",");
							 	for(var i=0;i <newId.length-1;i++){
							 	var taskName=$("#taskName_"+newId[i]).text();
							 	var toDate=$("#toDate_"+newId[i]).text();
							 	var fromDate=$("#fromDate_"+newId[i]).text();
							 	var	selectedTaskHtmlDiv='<div id="depSelectedDiv_'+newId[i]+'" style="border-bottom: 1px solid #bfbfbf;border-radius: 5px;box-shadow: 1px 1px 5px #cccccc inset;height: 55px;  margin-bottom: 10px;  padding-bottom: 5px;padding-top: 5px;cursor: pointer; float: left;width: 98%;" >'
										        +' <div style="float:left;width:100%;height:35px;">'
									            +'   <div  align="left" id="selectedTaskName_'+newId[i]+'" style="width:450px;padding-left:5px; float: left;font-weight: normal;height: 30px;overflow: hidden;" title="'+taskName+'">'+taskName+'</div>'
												+"   <div  align='center' id='depTaskDelete_"+newId[i]+"' style='width: 20px;float:left;color: #848484;overflow: hidden;padding-left: 5px;'><img src='"+Path+"/images/Idea/userDelete.png' class='Delete_cLabelTitle' style='height: 18px;width: 18px;cursor:pointer;margin-top:7px;' onclick='removeDependencyTask("+newId[i]+",\"create\");'></div>"
												+' </div>'
												+' <div style="float:left; border-top:1px solid #BFBFBF;width:100%;font-size: 10px;">'
														+"<div  align='left' class=\"   \" style='color:black;float: left;  margin-top: 5px; width: 34px;'>"+getValues(Labels,'From')+"&nbsp;:</div>"
										 				+"<div id=\"fromSelectedDepDate_"+newId[i]+"\" align='left'  style=\"font-weight:normal;float:left;width:80px;margin-top: 5px;height:18px;overflow:hidden;padding-left:5px;\">"+fromDate+"</div>"
										 						//+"<span style=\"float: left; margin-top: 4px;\">|</span>"
										 				+"<div  align='left' class=\"   \" style='color:black;float: left;  margin-top: 5px; width: 22px;'>"+getValues(Labels,'To')+"&nbsp;:</div>"
										 				+"<div id=\"toSelectedDepDate_"+newId[i]+"\" align='left'  style=\"font-weight:normal;float:left;width:80px;margin-top: 5px;height:18px;overflow:hidden;padding-left:5px;\">"+toDate+"</div>"
									 			+"</div>"
										+'</div>';
										
									$("#selectedDependencyTaskDiv").find('#selectedDependencyTaskContent').append(selectedTaskHtmlDiv);
									var isiPad = navigator.userAgent.match(/iPad/i) != null;
						    		if(!isiPad){
						 				scrollBar('selectedDependencyTaskContent');
						 			}
							 	}
							 	
							 }	
						 		$("#selectedDependencyTaskContent").find('[id^=depSavedDiv_]').each(function() {
									var id = $(this).attr('id').split("_")[1];
									$("#depTask_"+id).hide().removeClass("taskUnSelected").addClass("taskSelected").css("background","");
								});
				 			
						   }
	  		 		});	
			        	
				
						
				
					
				
			}
		});
		
	}
	*/
	/*
	function showDepSearchNsort(){
		$('#calendarDepSortDiv').slideToggle();
	}		
	*/
	function sortDepTaskData(obj) {   /* Sort By Functionality for DependencyTask UI  */ 
		var sortTType = $('#sortInWs').find("option:selected").attr('value');
		var sortVal = $('#sortTasks').find("option:selected").attr('value'); 
		searchWord=$("#searchBox").val();
			parent.$("#loadingBar").show();
			parent.timerControl("start");	
				$.ajax({
					url:Path+"/calenderAction.do",
					type:"POST",
					data:{act:"dependencyTaskList",optionval:sortTType,searchWord:searchWord,sortVal:sortVal},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
						checkSessionTimeOut(result);
					$("#depTaskList").html(result);
					var isiPad = navigator.userAgent.match(/iPad/i) != null;
						if(!isiPad){
				    		scrollBar('depTaskList');
			        	}	
					$('[id^=depSavedDiv_]').each(function() {
						var id = $(this).attr('id').split("_")[1];
						$("#depTask_"+id).removeClass("taskUnSelected").addClass("taskSelected").css("background","").hide();
					});
					parent.$("#loadingBar").hide();
					parent.timerControl("");
						
					}
				});		
		/*if(sortVal!="" && sortVal!="All" ){
			$(".taskUnSelected").hide();
			$(".taskStatus").each(function(){
				var id=$(this).attr('id').split("_")[1];
				if($(this).text() == sortVal){
					$("#depTask_"+id).show();
				}
				var isiPad = navigator.userAgent.match(/iPad/i) != null;
			    if(!isiPad){
			       		$("#depTaskList").mCustomScrollbar("update");	
				}
				
			});
		}
		else{
			$(".taskUnSelected").show();
		}			*/
		
	}
	function showDepTask(id,parentEndDate){
	
			$('.hideEpicOptionDiv').hide();
			$('#depTaskPopUpDiv').html("");
			
		/*	 var html='<div style="display: block; margin-top: -88px; float: left; right: 25%; top: 0px; padding: 10px; width: 450px ! important;box-shadow: 0 0 5px 2px rgba(0, 0, 0, 0.35);" class=\"hideEpicOptionDiv\" id=\"depTaskPopDiv\">'    
				 +"<div  align=\"left\"  style=\"border-bottom:1px solid #a1a1a1;width:450px;padding-left:5px; float: left;font-weight: normal;height: 16px;\"><span style=\"width:47%;float:left;\">Sub Task</span><span style=\"width:23%;float:left\">From</span><span style=\"width:24%;float:left\">To</span></div>"
				 +'  <div id=\"depTaskContent\" style=\"background:; float:left; height: 90px; width: 100%; border-radius: 2px; font-family: tahoma; font-size: 13px;\"></div>'
				 +'  <div style="float: right; height: auto; position: relative; width: 30px; margin-right: 10px;" class="triangleDiv">'
				 +'   	<img src="'+Path+'/images/Idea/arrow.png" style="transform: rotate(90deg);-ms-transform: rotate(90deg); -webkit-transform: rotate(90deg); position: absolute; top: 2px;">'
				 +'   </div>'
				 +'</div>';
				 $('#depTaskPopUpDiv').html(html).show();*/
		//	$(obj).parents('div.fc-event').append(html).children('#depTaskPopDiv').show();
			$.ajax({
					url:Path+"/calenderAction.do",
					type:"POST",
					data:{act:"getDepTask",id:id},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
						checkSessionTimeOut(result);
						var ids=result.split(",");		
						$('.fc-event-icons').each(function(){				
							if($(this).find('img').attr("title")=="Task"){
								var depId=$(this).attr('depId');	
								for(var i=0; i< ids.length;i++){	
									if(depId==ids[i].split(":")[0].trim()){		
										var childDate=ids[i].split(":")[1];	
										var sdArray=parentEndDate.split('-');
										var edArray=childDate.split('-');
										var sd=sdArray[1]+'/'+sdArray[0]+'/'+sdArray[2];
										var ed=edArray[1]+'/'+edArray[0]+'/'+edArray[2];
										var d1=new Date(sd);
										var d2=new Date(ed);	
										if(d2 > d1){	
											$(this).parent(".fc-event-inner").css("background","#e67b09").css("border","#F5860F").parent('div').css("background","#e67b09").css("border","#F5860F").css("border-width","1px 0");
										
										}
										else{
											$(this).parent(".fc-event-inner").css("background","#067886").css("border","1px 0 #088090").parent('div').css("background","#067886").css("border","1px 0 #088090");
										}
									}  
								}
							}	
								
							
						});
													
			/*		$('#depTaskPopUpDiv').find("#depTaskContent").append(result);
				//	$('#depTaskPopDiv').css("margin-top",(offset.top-145)+'px');
					var isiPad = navigator.userAgent.match(/iPad/i) != null;
						if(!isiPad){
				    		scrollBar('depTaskContent');
			        	}	
					parent.$("#loadingBar").hide();
					parent.timerControl("");
					return ;	*/
					
					
					
					}      
				});
			
	}
	
/*	function attachTaskRepo(){
	    $("#TaskLevellinkSection").hide();
        var idd=$("#uploadDocTaskId").val();
        $('#ideaTaskLevelDocPopUp').css('display','block');
     	  var landH=$('#ideaDocPopUp').height();
		  var mainH=parent.$('#menuFrame').height();
		  if(mainH>landH){
		  }else{
			  $('#ideaTaskLevelDocPopUp').css({'width':'920px','margin-left':'-460px','top':'0%','margin-top':'0px'});
			  $('#ideaTaskLevelDocPopUp').css('height',mainH-10+'px');
			  popScroll('ideaDocPopUp');
		  }
          $('#TaskLeveldocIdeaId').val(idd);
 		  $('#ideaTransparent').css('display','block');
 		  parent.$('#transparentDiv').css('display','block'); 
	      loadCustomLabel("AttachDoc");
	      loadTaskLevelFolder(); 
     }
     
     function loadTaskLevelFolder(){
	 parent.$("#loadingBar").show();
	 parent.timerControl("start");
	 $('#TaskLevelattachSaveBtnDiv').hide();	
	 $.ajax({
       	url: Path+"/agileActionNew.do",
		type:"POST",
		data:{action:"loadAllFolder",from:"Agile"},
		success:function(result){
		        parent.checkSessionTimeOut(result);
		        result = result.replaceAll("CHR(39)","'");
		        var data=result.split('###@@###')[0];
				$('#TaskLevelfolderNames').text('');
			    $('#TaskLevelfolderDiv').html(data);
			 	popupHideIcon();
			 	if(!isiPad){
			 	   scrollBar('TaskLevelfolderDiv');
			 	   $('div#TaskLevelfolderDiv >.mCS-light >.mCSB_container').css('margin-right','15px');
			 	   $('div#TaskLevelfolderDiv >.mCS-light >.mCSB_scrollTools').css('margin-right','0px');
			 	}else{
				   $('div#ideaTaskLevelDocPopUp').find('.folderheader').css('width','100%');
				 }
			    parent.$("#loadingBar").hide();
			    parent.timerControl("");
		}
	 });
 }
 */
  /*   function attachDocumentforTaskLevelIdea(){
 	//   chkDocAttached="Y";
       parent.$("#loadingBar").show();
       parent.timerControl("start");
       var docIdList="";
       var taskId=$('#TaskLeveldocIdeaId').val();
       var taskOwner = $("#ownerId").val();
       $('div.docSelected').each(function(){
           var docId=$(this).attr('id').split('_');
           docIdList=docIdList+docId[1]+",";
       });
 	    var type = parId;
 	    var docTaskType=$("#doctaskType").val();
	    if(docTaskType == "event"){
	      menuType="event";
	    }else{
	      menuType="cal";
	    }
 		$.ajax({
	  	     url: Path+"/calenderAction.do",
	  	     type:"POST",
	  		 data:{act:"insertTaskLevelDocRepo",docSplitId:docIdList,taskId:taskId,taskType:type,projId:projId,taskOwner:taskOwner,menuType:menuType},
	  		 success:function(result){
	  			parent.checkSessionTimeOut(result);
				$('#ideaTaskLevelDocPopUp').hide()
				$('.options').show();
			    fetchDocdata('upload');
  		     }
	    }); 
     }
     */
     function cancelTaskLevelAttachDoc(){
	    $('#ideaTaskLevelDocPopUp').hide().css({'width':'900px','margin-left':'-450px','top':'50%','height':'520px','margin-top':'-260px'});
		$("#ideaTaskLevelDocPopUp").mCustomScrollbar("destroy");
	    $('#TaskLevelfolderNames').text('');
		$('#TaskLevelfolderDiv').html('');
        $('#attachSaveBtnDiv').hide();		
 		$('#ideaTransparent').hide();
 		parent.$('#transparentDiv').hide(); 
     }
     
  /*   
     function attachTaskLevelLink(){
        var idd=$("#uploadDocTaskId").val();	
		$("#WsUploadOptns").slideToggle("slow");
		$('input#TaskLevellinkTitle,input#TaskLevellinkLink').val('');
        $('#TaskLevellinkSection').css('display','block');
     	$('#TaskLevellinkTaskId').val(idd);		
		var type = parId.substring(2);
		var taskId=$('#uploadDocTaskId').val(); 
		$("#docTaskContentSubDiv").css('height','200px');
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad) {
	    		$("#docTaskContentSubDiv").mCustomScrollbar("update");	
		}
		
		
  	}
  	*/
  	
 /* 	function funAttachTaskLevelLink(){
		if( $("#TaskLevellinkLink").val().trim() == '' ){
			parent.alertFun(getValues(customalertData,"Alert_enterLink"),'warning');
			$("#TaskLevellinkLink").focus();
			return false;
		}
		var key = $("#TaskLevellinkLink").val().trim();
		 if (!isValidURL(key)) {
	       parent.alertFun(getValues(customalertData,"Alert_invalidUrl"),'warning');
	       return false;
	    }
		
	    
		var title1=$("#TaskLevellinkTitle").val().trim();
		var title = $("#TaskLevellinkTitle").val().trim();
		var link = $("#TaskLevellinkLink").val().trim();
		if(title==""){
		    title=link;
		    title1=link;
	    }
	    var taskId=$('#TaskLevellinkTaskId').val();   	
        var type = parId;	
        var projId= $("#taskLevelprojectId").val();
        parent.$("#loadingBar").show();
        parent.timerControl("start");
          var docTaskType=$("#doctaskType").val();
	    if(docTaskType == "event"){
	      menuType="event";
	    }else{
	      menuType="cal";
	    }
 		$.ajax({
  			url:Path+"/calenderAction.do",
			type:"POST",
			data:{act:"insertTaskLevelLink",taskId:taskId,link:link,title:title,projId:projId,taskType:type,menuType:menuType},
			success:function(result){
         		parent.checkSessionTimeOut(result);
         		$('input#TaskLevellinkTitle,input#TaskLevellinkLink').val('');
         		$("#TaskLevellinkSection").hide();
         		loadCustomLabel('AttachLink');
         		fetchDocdata('upload');
         		$("#docTaskContentSubDiv").css('height','273px');
         		var isiPad = navigator.userAgent.match(/iPad/i) != null;
			    if(!isiPad) {
			    		$("#prtcpntContainer.prtcpntSection").mCustomScrollbar("update");	
			    }
		        parent.$("#loadingBar").hide();
		        parent.timerControl("");
			}
  		});
	} 
	
	
	function cancelAttachTaskLevelLink(){
        $('#TaskLevellinkSection').hide();
 	    if($("[id^=taskLink_]").length < 0){
 		  $("[id^=docImgDiv_]").hide();
 	    }
    	$('#TaskLevellinkTitle').val('');
		$('#TaskLevellinkLink').val('');
		$("#docTaskContentSubDiv").css('height','273px');
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad) {
	    		$("#docTaskContentSubDiv").mCustomScrollbar("update");		
	    }
    }
*/    
/*	
	function docTaskclosePopup(){
	   $("#uploadTaskLevelDoc").hide();
	   $("#overlay").hide();
	   var taskId=$("#uploadDocTaskId").val();	
	   var docTaskType=$("#doctaskType").val();
	    if(docTaskType == "event"){
	      menuType="event";
	    }else if(docTaskType =="assignedSprint"){
	       menuType="sprint";
	    }
	    else{
	      menuType="cal";
	    }
	   $.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'FetchTaskLevelDocLinkStatus',taskId:taskId,menuType:menuType},
		   success:function(result){
		   parent.checkSessionTimeOut(result);
				    if(result==""){
				       $("#docIconNameLevel").hide();
				       $("#docIconNameLevel1").hide();
				       $("#eventName").css("width",'412px');
				       $("#taskEventName").css("width",'430px');
				    }else{
				       $("#eventName").css("width",'390px');
				       $("#eventName").css("float",'left');
				       $("#taskEventName").css("width",'400px');
				       $("#taskEventName").css("float",'left');
				       $("#docIconNameLevel").show();
				       $("#docIconNameLevel1").show();
				    }
		   }
		   });
}
*/
function depListViewPopUp() {			
	$('#dependencyListViewPopUp').css('display','block');
	var depLi="";
	parent.$("#loadingBar").show();
	parent.timerControl("start");
	$.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'FetchTaskContainDependency'},
		   error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){
		   parent.checkSessionTimeOut(result);
				    var jsonData=jQuery.parseJSON(result);
				    depLi+= "<ul>";				
				    for(i= 0 ;i < jsonData.length; i++){			
				    	depLi+= "  <li  id='dep_"+jsonData[i].id+"' >"
				    				+"   <div id=\"task_"+jsonData[i].id+"\" class=\"roundDiv\" style=\"width:100%;float:left;margin-bottom: 10px;font-weight: normal;font-weight: normal;\"> "
							        +"      <div id=\"blueband_"+jsonData[i].id+"\"  align=\"center\" class=\"epicOrangeBandCss\">"
							        +"				<img onclick=\"loadDepTaskInListView("+jsonData[i].id+")\" src=\""+Path+"/images/Idea/plus.png\" class=\"expandCollapseImg\">"
							        +"		</div>"
								//	+"      <div class=\"dd-handle\" >"
							   //     +"         <div>"
							        +"   <div>"
							        +"   <div style=\"height:8px;\" ></div>"
								 	+"   <div class=\"captionIdea\" >"
								 	+"         <div class=\"editableTxtArea\" id=\"editIdea_"+jsonData[i].id+"\"  style=\"margin-top:-3px;margin-left:20px;\"  >"+jsonData[i].taskName+"</div>"
								 	+"         <input type=\"hidden\" id='hiddenTitle_"+jsonData[i].id+"' value='"+jsonData[i].taskName+"' />"
									+"   </div>"
								    +"         </div>"
								//	+"       </div>"
									+"       <div class=\"sm2_actions\" id=\"Action_"+jsonData[i].id+"\" style=\"min-width:17%;height:auto;margin-top:0px !important;\">"
							        +"          <div style=\"float:left;cursor:pointer;margin-top: 5px;margin-left:4px;\" class=\"eventNameLabel From labelBold\">"+getValues(Labels,'From')+"&nbsp;:</div>"
						 			+"          <div  style=\"float: left;  margin-top: 5px; width: 85px;\">"+jsonData[i].endDate+"&nbsp;</div>"
								    +"          <div style=\"float:left;cursor:pointer;margin-top: 5px;\" class=\"eventNameLabel To labelBold\">"+getValues(Labels,'To')+"&nbsp;:</div>"
						 			+"          <div  style=\"margin-top: 5px; width: 85px;float:left;\">"+jsonData[i].startDate+"&nbsp;</div>"
						 			+"          <div style=\"float:left;cursor:pointer;margin-top: 5px;\" class=\"eventNameLabel labelBold\">Status&nbsp;:&nbsp;</div>"
								    +"              <img title=\"\" src=\""+Path+"/images/calender/"+jsonData[i].userCompletedStatus+".png\" id=\"\" style=\"margin-right: 5px;float:left;margin-top:6px;cursor: pointer;width:15px;\">"
								    +"        <div id=\"epicCreatedBy_"+jsonData[i].id+"\"  class=\"epicCreatedByDiv\" style=\"margin-top:5px; margin-bottom:5px;margin-left: 29px;font-family:tahoma;\">"
								    +"          <div style=\"float:left;cursor:pointer;\" class=\"eventNameLabel labelBold\">&nbsp;Created By&nbsp;:</div>"
								    +"              <span id=\"createdBy_"+jsonData[i].createdBy+"\"  style=\"margin-left:10px;\" >"+jsonData[i].createdBy+"</span>"
								    +"        </div>"		
								    +"   </div>"
								    +"   </div>"
				    			+"</li>";
				    		
				    	
				    }
				       depLi+="</ul>";
				    $("#depTaskContentListView").show().append(depLi);
				    scrollBar('depTaskContentListView');
				    parent.$("#loadingBar").hide();
					parent.timerControl("");
	  	 }
	  });
	
}
/*function loadDepTaskInListView(taskId){
	$.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'FetchSubTaskInListView',taskId:taskId},
		   success:function(result){
		   }
	});	   
}*/
function closeDependencyListViewPopUp(){
	$("#dependencyListViewPopUp").hide();
	 $("#depTaskContentListView").html("");
}
var setMargin=0;
function showDepTaskInListView(taskId,obj,taskListType){
	var depListHtml="";
	$(obj).attr("onclick","collapseDepList("+taskId+",this,'"+taskListType+"');event.stopPropagation();").find("img").attr('src',Path+'/images/Idea/sprintDetailCollapse.png');
	$.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'FetchSubTaskInListView',taskId:taskId},
		   error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){
			   checkSessionTimeOut(result);
		   	 var jsonData=jQuery.parseJSON(result);
		   	 		 depListHtml+= " <div class=\"taskListView\" style=\"float:left;display: none;width: 100%;\">	 "
										+" <div class=\"folderheader\" id=\"mytaskheaders\" style=\"width: 100%\"> "
										+" <div class=\"tasktype\" style=\"margin-top:5px;width:4%;margin-left: 10px;overflow: hidden;height: 17px;\">"+getValues(Labels,"Type")+"</div> "
										+" <div class=\"hSize\" style=\"width: 6%;margin-left:1%;overflow: hidden;height: 17px;\">"+getValues(Labels,"Priority")+"</div> "
										+" <div class=\"hName\" style=\"width: 24%;margin-left:1%;overflow: hidden;height: 17px;\">"+getValues(Labels,"Task")+"</div> "
										+" <div class=\"hLModified\" style=\"width: 21%;margin-left:1%;overflow: hidden;height: 17px;\">"+getValues(Labels,"Assigned_to")+"</div> "
										+"  <div class=\"hLModified\" style=\"width: 11%;margin-left:1%;overflow: hidden;height: 17px;\">"+getValues(Labels,"Created_by")+"</div> "
										+" <div class=\"hOwner\" style=\"width: 10%;margin-left:1%;overflow: hidden;height: 17px;\">"+getValues(Labels,"Start_date")+"</div> "
										+" <div class=\"hCreated\" style=\"width: 10%;margin-left:1%;overflow: hidden;height: 17px;\">"+getValues(Labels,"End_date")+"</div> "
										+"  <div class=\"hModified\" style=\"width: 5%;margin-left:1%;overflow: hidden;height: 17px;\">"+getValues(Labels,"Status")+"</div> "
										+" </div> ";
		   	 		 
		   	 		 
				    for(i= 0 ;i < jsonData.length; i++){
				        	depListHtml+="<div  id='deptask_"+jsonData[i].taskId+"' style=\"width:100%;\">"
				    				    +    "<div style=\"cursor:pointer;width:100%\" class=\"task\" id=\"myTask_"+jsonData[i].taskId+"\" onclick=viewTask("+jsonData[i].taskId+",this,'"+taskListType+"')>"
				    					+    "<div style=\"padding-top:6px\">"
				    					+	 "</div>"
				    					+    "<div class=\"rTasksType\" align=\"left\" vaign=\"middle\" style=\" width:5%;float:left;margin-left:10px;margin-top:5px;\"><img title=\"\" class=\"taskTypeImg\" style=\"float:left;margin-left:8px;\" src=\""+Path+"/images/calender/TaskBlack.png\" /></div>";
				    					if(jsonData[i].depId=="Yes"){
				    						depListHtml+="<div  class=\"rTasksPriority\" align=\"left\" vaign=\"middle\" style=\" width:4%;float:left;margin-top:7px;\"><div><img title=\"\" src=\""+jsonData[i].PriorityImage+"\"></span></div></div>";
				    						depListHtml+="<div id=\"showDepButton\" onclick=\"showDepTaskInListView("+jsonData[i].taskId+",this,'"+taskListType+"');event.stopPropagation();\" class=\"\" align=\"left\" vaign=\"middle\" style=\" width:3%;float:left;margin-top:7px;\"><div><img  title=\"dependency task\" style=\"width:20px;\" src=\""+Path+"/images/Idea/sprintDetailExpand.png\"></span></div></div>";
				    					}
				    					else{
				    						depListHtml+="<div  class=\"rTasksPriority\" align=\"left\" vaign=\"middle\" style=\" width:7%;float:left;margin-top:7px;\"><div><img title=\"\" src=\""+jsonData[i].PriorityImage+"\"></span></div></div>";
				    					}
				  			depListHtml+="<div  class=\"rTasksName\" align=\"left\" vaign=\"middle\" style=\" width: 25%;float:left;margin-top:6px;\" ><div style=\"width: 100%;overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\"><span class=\"taskSub\" title=\"\"></span>"+jsonData[i].taskName+"<span class=\"hidden_searchTask\" style=\"display:none\">"+jsonData[i].taskName+"</span></div></div>"
				    					+    "<div style=\"float:left;width:22%\" class=\"rTasksOwner\" dtaskid=\""+jsonData[i].taskId+"\" taskstatus=\"\" taskid=\"\" ><div><span style=\" color: #000000;font-weight:normal;height: 20px;overflow-x: hidden;float:left;margin-top:8px;\" title=\"\" class=\"feedExtraFont taskAT\">"+jsonData[i].assignedTo+"</span><span style=\"display:none\" class=\"hidden_searchCreatedBy\">"+jsonData[i].assignedTo+"</span>"
				    					+    "</div></div>"
				    					+	 "<div class=\"rTasksOwner\" align=\"left\" vaign=\"middle\" style=\" float: left; width: 12%;margin-top:6px;\"><span title=\""+jsonData[i].taskCreatedBy+"\">"+jsonData[i].taskCreatedBy+"</span><span class=\"hidden_searchCreatedBy\" style=\"display:none\">"+jsonData[i].taskCreatedBy+"</span></div>"
								        +    "<div class=\"rTasksDate\" align=\"left\" vaign=\"middle\" style=\" float: left; width: 11%;\">"+jsonData[i].startDate+"<span class=\"hidden_searchDate\" style=\"display:none\">"+jsonData[i].startDate+"</span><div>"+jsonData[i].startTime+"</div></div>"
								        +    "<div class=\"rTasksDate\" align=\"left\" vaign=\"middle\" style=\" float: left; width: 11%;\">"+jsonData[i].endDate+"<span class=\"hidden_searchDate\" style=\"display:none\">"+jsonData[i].endDate+"</span><div>"+jsonData[i].endTime+"</div></div>"
								        +    "<div class=\"rTasksStatus\" align=\"left\" vaign=\"middle\" style=\" width:5%;float:left;margin-top:5px;\"><img title=\""+jsonData[i].taskStatusImageTitle+"\" style=\"margin-left:15px;\" src=\""+jsonData[i].taskStatusImage+"\" /></div>"
								
				    					+"</div>"
				    		 		    + "</div>";
				    		 		    if(jsonData[i].depId=="Yes"){
				    		 		   	 depListHtml+="<div id=\"depTaskContainer_"+jsonData[i].taskId+"\" class=\"dependencyTaskContent\" align=\"left\" vaign=\"middle\" style=\" width:100%;height:auto;float:left;margin-left:5%;\">"
				    		 		    		+" </div> ";
				    		 		    }
									 depListHtml+=" </div> ";
									
								    		 		    
				    	
				    }					
				//     depListHtml+= "</ul>";			
				     if(moreValue=="assignedTasks"){					
 						$("#depTaskContainer_"+taskId).append(depListHtml).show();	
				      	$("#depTaskContainer_"+taskId).find(".taskListView").css("display","block");
				        $("#depTaskContainer_"+taskId).css("width","95%");
				     }
				     else if(moreValue=="myTasks"){		
				        $("#depTaskContainer_"+taskId).append(depListHtml).show();	
				     	$("#depTaskContainer_"+taskId).find(".taskListView").css("display","block");		
					 	$("#depTaskContainer_"+taskId).css("width","95%");
				     }
				 //    $("#depTaskContainer_"+taskId).css("margin-left",setMargin);
				     $("#eventPopup").mCustomScrollbar("update");
				     $("#depTaskContainer_"+taskId).find("#myDepTasklistView_"+taskId).css("display","block");
				   	 $("#AssignedtaskList").children("#depTaskContainer_"+taskId).find("#myDepTasklistView_"+taskId).css("display","block");
				    	
		   }
	});
}
function collapseDepList(taskId,obj,taskListType){
		$(obj).attr("onclick","showDepTaskInListView("+taskId+",this,'"+taskListType+"');event.stopPropagation();").find('img').attr('src',Path+'/images/Idea/sprintDetailExpand.png');
		$("#depTaskContainer_"+taskId).html("");
		//$("#depTaskContainer_"+taskId).find("#myDepTasklistView_"+taskId).css("display","none");
		if($("#AssignedtaskList").length!=0){
			$("#AssignedtaskList").children("#depTaskContainer_"+taskId).html("");
			$("#AssignedtaskList").children("#depTaskContainer_"+taskId).find("#myDepTasklistView_"+taskId).css("display","none");
		}
		$("#eventPopup").mCustomScrollbar("update");
}

     var glddocLocation="";
     var glddocTaskId="";
     function downloadTakLevelFile(docTaskId,docId,docLocation){	/* Document DownLoad*/
		documentId = docId;
		glddocLocation=docLocation;
		glddocTaskId=docTaskId;
		parent.confirmFun(getValues(customalertData,"Alert_Download_Doc"),"close","downloadDocument");
	}
	function downloadDocument(){
		var url="";	
		 if(glddocLocation == "Workspace"){
		   url = Path+"/workspaceAction.do?act=downloadfile&docId="+documentId+"&projId="+projId;
	     }else{
	        url = Path+"/repositoryAction.do?action=downloadfile&docId="+documentId ;
	     }		
	   window.open(url);
	   $("#dowloadOption_"+glddocTaskId).slideToggle("slow");
	}
	
	function viewTaskLevelDoc(docTaskId,url){
		window.open(url);
		$("#dowloadOption_"+docTaskId).slideToggle("slow");
	}
