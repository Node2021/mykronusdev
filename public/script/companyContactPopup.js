/**
 * @ author Abhishek
 */

    function addCompanyContact_old(){
    	//alert("addCompanyContact--");
    	  $('#transparentDiv').show();
		   
		   var UI ="<div id='userDetailpopUp' style=\"width:35%; height:87%; border:1px solid #a6a6a6; position: fixed; background: #fff; margin-left: 11%; border-radius: 7px;\">"
		    	   
		          +"<div style=\"width:100%; height:12%;\">"
		              +"<div style=\"height: 100%; border-bottom: 1px solid #A6A6A6; width: 95%; margin-left: 2%;\">"
		    			/* 	+"<div style=\"width:17%; height:100%;float:left;\">"
		    				  +"<img src='${path}/images/inviteuser.png'  width=\"78%\" height=\"85%\" style=\"float:right; padding-top: 10px;margin-right: 18%;\">"
		    				+"</div>" */
		    				+"<div style=\"width:98%; height:100%;float:left;\">"
		    					 + "<div class='proHeaderCss' style=\"width: 99%; margin-top: 9%; height:35%; font-size:20px;padding-left: 9%; \">ADD USER</div>"
		    				+"</div>"
		    				+"<div style=\"width: 2%; float: left; height: 60%; margin-top: 2%;\">"
		    					+"<img onclick='closeUserPopUp()' src=\""+path+"/images/close.png\" style=\"cursor:pointer;\">"
		    				+"</div>"
		             + "</div>"
		          +"</div>"
		      
		          +"<div id='usersDetailsDiv' style=\"width:100%; height:87%; overflow:auto;\">"
		              +"<form style=\"width:100%; height:100%;\" >"
		    		    +"<div class='tlabel tlabel3' >First Name</div>"
		                +"<input type='text' id='sysAdminUserFname' class='theight' placeholder='Enter First Name'><span style='position: absolute; color: red; margin-left: 1%;'>*</span>"
		    			+"<div class='tlabel tlabel3' >Last Name</div>"
		                +"<input type='text' id='sysAdminUserLname' class='theight' placeholder='Enter Last Name'>"
		    		    +"<div class='tlabel tlabel3' >Email</div>"
		                +"<input type='text' id='sysAdminUserEmail' class='theight' placeholder='Enter Email'><span style='position: absolute; color: red; margin-left: 1%;'>*</span>"
		    			+"<div class='tlabel tlabel3' >User id</div>"
		                +"<input type='text'  id='sysAdminUserId' class='theight' placeholder='Enter User Id'><span style='position: absolute; color: red; margin-left: 1%;'>*</span>"
		    			+"<div class='tlabel tlabel3' >Role</div>"
		    			+"<div class='tlabel styled-select' style='width:81%'><span style='color: red; margin-top: 2%; position: absolute; margin-left: 82%;'>*</span>"
		    			  +"<select id='userRole' class='drpDwnSelected tfont' style='width: 106%; height:4%;padding-left:0; border: 0 none !important; box-shadow: none; border-bottom: 1px solid #a6a6a6 !important;'>"
		    			    +"<option value='0' style='padding: 1%;'>Select Role</option>" 
		    			    +"<option value='4' style='padding: 1%;'>Project Administrator</option>"
			    			+"<option value='2' style='padding: 1%;'>System Administrator</option>"
			    			+"<option value='5' style='padding: 1%;'>User</option>"
		    		    +"</select>"
			    	   +"</div>"
		    
		    		    +"<div class='tlabel tlabel3' >Permission</div>"
		    		    +"<div class='tlabel styled-select' style='width:81%'><span style='color: red; margin-top: 2%; position: absolute; margin-left: 82%;'>*</span>"
		    		      +"<select id='userPermission' class='drpDwnSelected tfont' style='width: 106%; height:4%;border: 0 none !important; padding-left:0;  box-shadow: none; border-bottom: 1px solid #a6a6a6 !important;'>"
		    		        +"<option value='0' style='padding: 1%;'>Select Permission</option>"
		    		        +"<option value='1' style='padding: 1%;'>Permit Full Access to Contacts</option>"
			    		    +"<option value='2' style='padding: 1%;'>Restrict Contacts to Project Teams</option>"
			    		    +"<option value='3' style='padding: 1%;'>No Access to Contacts</option>"
		    		      +"</select>"
		    		    +"</div>"
	
		    			+"<div class='tlabel tlabel3' >Custom message</div>"
		    			+"<textarea id='userTextareaComments' rows=\"7\"  class='tarea' style='width: 81%; margin-left: 10%; margin-top: 5%;box-shadow: 0 1px 4px 0 rgba(168, 168, 168, 0.9) inset; border: 1px solid #abadb3;padding: 1%;' placeholder=''>"
		    			+"I have created an account for you on Colabus, the next generation collaboration hub. Now we can work together more effectively, anytime, anywhere!</textarea>"
		    		
		    	        +"<div class='createBtn' style=' margin-right: 9%;' onclick='SaveAdminUserData();' id='changeDynamic'>CREATE</div>"
		    	       
		              +"</form>"
		          +"</div>"
		       +"</div>";
		            
		     $('#connectData').show().html(UI);
		     popUpScrollBar('usersDetailsDiv');   
		     loadEmailKeyUp();
    }
	
    
    function loadEmailKeyUp(){	
        $('div#usersDetailsDiv input#sysAdminUserEmail').keyup(function(){
    		autofillUsername(this);
    	});
    	}
    	
    function autofillUsername(emailObj){
    	var emailVal = $(emailObj).val();
    	$('div#usersDetailsDiv input#sysAdminUserId').val(emailVal);
    }
    
	function closeUserPopUp(){
   		$('#userDetailpopUp , #transparentDiv').hide();   
    }	
    
    
	/*
    @ Saving the User credentials
      checking the Data before Save
	*/
	 function SaveAdminUserData(){
	// alert("SaveAdminUserData---");
		    
			var fname = $('#sysAdminUserFname').val();
			var lname = $('#sysAdminUserLname').val();
			var email =	$('#sysAdminUserEmail').val();
			var userid = $('#sysAdminUserId').val();
			
			var roleid = $("select#userRole option:selected").val();
			var roleName = $("select#userRole option:selected").text();
			var contactPermission = $("select#userPermission option:selected").val();
			var comments = $("#userTextareaComments").val();
			var type = 'Uinsert';
			var emailReg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			var updateUid = $('#userId').val();
			var userStatus = $('.userStatusImg').attr('value');
			
		
			
			if(fname.trim() == ""){
			 	alertFun(getValues(customalertData,"Alert_EnterFname"),'warning');
				return false;
			}
			if(checkSplChar(fname) == true || checkSplChar(lname) == true){
			 	alertFun(getValues(customalertData,"Alert_NameChar"),'warning');
				return false;
			}	
			if(email.trim() == "" || email.trim() == null){
				alertFun(getValues(customalertData,"Alert_EmailEmpty"),'warning');
			 	return false;
			}		
			if(!emailReg.test(email) ) {
			 	alertFun(getValues(customalertData,"Alert_EmailInvalid"),'warning');
				return false;
			}	
			if(userid.trim() == ""){
			 	alertFun(getValues(customalertData,"Alert_UserIdEmpty"),'warning');
				return false;
			}
			if(checkSplCharforUserId(userid) == true){
			 	alertFun(getValues(customalertData,"Alert_UserIdChar"),'warning');
				return false;
			}	
			/* if($('#userId').val() == 0){
				if ($('#passwordGenerated').text() == '') {
		        	alertFun(getValues(customalertData,"Alert_GeneratePwd"),'warning');
		        	return false;
		        }		
			} */
			
			if(roleid == 0){
			 	alertFun(getValues(companyAlerts,"Alert_SelectRole"),'warning');
				return false;
			}
			
			if(contactPermission == 0){
			 	alertFun(getValues(companyAlerts,"Alert_SelectPermission"),'warning');
				return false;
			}
			
			
			$("input[name=existUsers]").click(function () 
				{
			
				    var previousValue = $(this).attr('value');
				    if(previousValue == 'nuser'){
				    	$('#loadExistsUsers').hide();
				    	$('#userDataEmailIdsDivider').hide();
				    	tTest='Y'
				    }else{
				    	$('#loadExistsUsers').show();
				    	$('#userDataEmailIdsDivider').show();
				    	tTest='N';
				    
				    }
					checkStatus=previousValue;
				});
				
				//alert(checkStatus+"--- "+userRegisterType);
			if(checkStatus =='euser' && userRegisterType == 'web_standard' ){
			//alert("register type ");
			if(checkedUserId.length==0 && pTest !='ok'){
				 $.ajax({
					url: path+"/connectAction.do",
					type:"POST",
					data:{act:"getEmailIdExistence",userEmail:email,userid:userid},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
						checkSessionTimeOut(result);
					if(result.split("$$$$")[0] == '' && result.split("$$$$")[1] !='user exists'){
						createNewUser(fname,lname,email,userid,"",roleid,contactPermission,comments,roleName,type,updateUid,userStatus,userRegisterType);
				
					}else{
					  if(result.split("$$$$")[1] =='user exists'){
					         alertFun(getValues(customalertData,"Alert_UserIdExist"),'warning');
							 $('#loadingBar').hide();
							 timerControl("");
					  }else{
					    pTest='ok';
						var dat=result.split("$$$$")[1];
						if(dat.length>0 && tTest == 'Y'){
							alertFun(getValues(customalertData,"Alert_UserIdExist"),'warning');
								$('#loadingBar').hide();
							    timerControl("");
						}
					
					$("#userDataEmailIds").show();
					$("#loadExistsUsers").show();
					$("#userDataEmailIdsDivider").show();
					$("#loadExistUsersData").html(result.split("$$$$")[0]);
					$("#existUsers1").prop("checked", true)
					$('#existUsers2').removeAttr('checked');
				
					 }
				   }
				}
					
				});
				
				}else{
					if(checkedUserId.length==0){

						parent.alertFun(getValues(companyAlerts,"Alert_SelUsrInvite"),'warning');
				
					}else{
					
						parent.$('#loadingBar').show();
						parent.timerControl("start");
                      
					$.ajax({
						url: path+"/connectAction.do",
						type:"POST",
						data:{act:"updateNewCompanyUser",checkedUserId:checkedUserId,limit:limit,index:index},
						error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
						success:function(result){
							checkSessionTimeOut(result);
							pTest='';
							$('#systemAdmin_4').hide();
							checkedUserId='';
							tTest='Y';
							loadCompanyContactData(notFolid, "");
								  $("#connectDataMainDiv").html(result.split('$$')[0]);
								  
								   $('#searchSort').show();
									alertFun(getValues(customalertData,"Alert_UserInvited"),'warning');
									$('.systemAdmin').hide();
									$("#userDataEmailIds").hide();
									$("#loadExistsUsers").hide();
									$("#userDataEmailIdsDivider").hide();
									uRoleId= '${userRoleId}';
								   if(uRoleId == '2' || uRoleId == '1'){
								 	   $("#insertUsers").show();
								 	}else{
								 	   $("#insertUsers").hide();
								 	}
									$('#back').hide();	
									$('#userTextareaComments').val('');
									$('#userId').val(0);
									$('#overlay').hide();	
									$("#existUsers1").prop("checked", true)
									$('#existUsers2').removeAttr('checked');					
			    					
								$('.sysAdminUser').unbind('click');
								$('.uStatusClass').unbind('click');
								parent.$('#loadingBar').hide();
								parent.timerControl("");									
							}	
						});
					}
				}
				}
				else{
				//alert("else");
			  //  alert(fname+"--"+lname+"--"+email+"--"+userid+"--"+""+"--"+roleid+"--"+contactPermission+"--"+comments+"--"+roleName+"--"+type+"--"+updateUid+"--"+userStatus+"--"+userRegisterType);		
				createNewUser(fname,lname,email,userid,"",roleid,contactPermission,comments,roleName,type,updateUid,userStatus,userRegisterType);
					
				}
					
	 }
  
function createNewUser(fname,lname,email,userid,password,roleid,contactPermission,comments,roleName,type,updateUid,userStatus,userRegisterType){
			$('#loadingBar').show();
			timerControl("start");
		    
	    	$.ajax({
				url: path+"/connectAction.do",
				type:"POST",
				data:{act:"insertNewCompanyUser",
						  fname:fname,
						  lname:lname,
						  email:email,
						  userid:userid,
						  password:password,
						  roleid:roleid,
						  contactPermission:contactPermission,
						  comments:comments,
						  roleName:roleName,
						  type:type,
						  updateUid:updateUid,
						  userStatus:userStatus,
						  index:index,
						  limit:limitAct,
						  userRegisterType:userRegisterType
						 },
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						},	 
				success:function(result){
					checkSessionTimeOut(result);
					//alert("result--"+result);
						pTest='';
						if(result == 'failure'){
								checkStatus='nuser';
								tTest='Y';
							
							alertFun(getValues(customalertData,"Alert_UserIdExist"),'warning');
							
						}else{
								checkStatus='euser';
								checkedUserId='';
								pTest='';
							 //  $("#connectDataMainDiv").html(result);
								var result2 = result.split("~##@@##~")[1];
								result=result.split("~##@@##~")[0];
								
								jsonData=jQuery.parseJSON(result);
								result=prepareUI();
								$("div#connectDataMainDiv").html('').append(result);
								$('#connectData').html('').hide();
								$('#transparentDiv').hide();
								
							   //loadCompanyContactData(notFolid, "");
							  
							   $('#searchSort').show();
							   $("#existUsers1").prop("checked", true)
								$('#existUsers2').removeAttr('checked');
								alertFun(getValues(customalertData,"Alert_UserInvited"),'warning');
								$('.systemAdmin').hide();
								$("#userDataEmailIds").hide();
								$("#loadExistsUsers").hide();
								$("#userDataEmailIdsDivider").hide();
								uRoleId= '${userRoleId}';
							   if(uRoleId == '2' || uRoleId == '1'){
							 	   $("#insertUsers").show();
							 	}else{
							 	   $("#insertUsers").hide();
							 	}
								$('#back').hide();	
								$('#userTextareaComments').val('');
								$('#userId').val(0);
								$('#overlay').hide();						
				
							}
							$('.sysAdminUser').unbind('click');
							$('.uStatusClass').unbind('click');
							$('#loadingBar').hide();
							result2= result2.split("@@")[1];
							//alert("result2---"+result2);	
								$.ajax({
				url: path+"/connectAction.do",
				type:"POST",
				data:{act:"xmppNewCompanyUser",
						  fname:fname,
						  lname:lname,
						  email:email,
						  newUserId:result2,
							 },
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						},	 
				success:function(result){
				//alert("result---"+result);
				}
				   });			
						}	
						
					});

		}

function ContactTabData(type){	
    if(type == 'comGroup'){
       window.location.href = path+"/Redirect.do?pAct="+type; 
	}else if(type == 'comContact'){
	   window.location.href = path+"/Redirect.do?pAct="+type; 
	}
}  