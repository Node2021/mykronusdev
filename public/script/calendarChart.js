//chart functionality starts here
var tType="";
var chartDetailType="";
	function openMyTaskChart(type,taskType){
		cancelTaskCreation();
		$("body").css("overflow", "hidden");
	    tType=taskType;
	    //var height = $(window).height();
		var sortDivHeight=$("#headerTopicDiv").height();
		var dataHeight=$(window).height()-$("#header_outer").innerHeight()-$("#tabMainDiv").innerHeight();
		var calTop=parseInt($("#rowTab").css("height").replace(/[^\d.]/g,''));
		$("#mytaskDashboard").css('height',dataHeight);
		$("#mytaskDashboard").css("margin-top","-"+calTop+"px");
	    $("#CalenderListViewTabDiv").hide();
	   // $("#calendarContainer").hide();
	    $("#taskContentDiv").hide();
		$("#rowTab").hide();
	    if(taskType == 'my'){
	       $("#MyTasklistView").hide();
	       $("#calendarContainer").hide();
	       $("#mytaskdatalist").hide();
	       $("#calViewData").show();
	       //$("#myTaskChart").hide();
	    }else{
	       $("#AssignedlistView").hide();
	       $("#assignedtaskdatalist").hide();
	    }
	  
	    
	    $("#gotoListImg").show();
	    $("#breadCrumName").html('&nbsp;> Dashboard');
	    $("#mytaskDashboard").show();
	    
	    if(type=='bar'){
		    parent.$('#loadingBar').show();
		  	parent.timerControl("start");
		    $("#mytaskDashboardPie").hide();
		    $("#mytaskDashboardBar").show();
		     $.ajax({
		 		url:path+"/calenderAction.do",
		 		type:"POST",
		 		data:{act:"fetchmyTaskChartData",chartType:type},
		 		error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
		 		success:function(result){				
		 		    parent.checkSessionTimeOut(result);
		 		   
		 		    var mainArray = [ ['Dates', 'Completed Task', 'In Progress Task', 'Not Completed Task']];
				    var subArray; 
		    	    $(result).find("labelData").each(function(i){
				    	var completeTask = $(this).find("label_completedTask").text();
				    	var pendingTask = $(this).find("label_pendingTask").text();
				    	var incompleteTask =$(this).find("label_incompleteTask").text();
				    	var days = $(this).find("label_finalDay").text();
						subArray = new Array();
						subArray.push(days);
						subArray.push(parseInt(completeTask));
						subArray.push(parseInt(pendingTask));
						subArray.push(parseInt(incompleteTask));
						mainArray.push(subArray);
				   });
				
		 		   var data = google.visualization.arrayToDataTable(mainArray);
		           new google.visualization.ColumnChart(document.getElementById('mytaskDashboardBar')).
		              draw(data,
		                 {title:"Bar chart - My task progress status",
		                  vAxis: {title: "Count"},
		                  hAxis: {title: "Dates"}}
		            );
		            parent.$('#loadingBar').hide();
		  	        parent.timerControl("");
		 		}
		 	});
	  }
	  else if(type== 'pie'){
	     parent.$('#loadingBar').show();
	  	 parent.timerControl("start");
	     $("#mytaskDashboardPie").show();
	     $("#mytaskDashboardBar").hide();
	     // Create and populate the data table.
	     $.ajax({
	 		url:path+"/calenderAction.do",
	 		type:"POST",
	 		data:{act:"fetchmyTaskChartData",chartType:type},
	 		error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
	 		success:function(result){
	 		     parent.checkSessionTimeOut(result); 
	 		     var dataList=result.split('#@#');
	 		     var data1 = new google.visualization.DataTable();
		         data1.addColumn('string', 'Topping');
		         data1.addColumn('number', 'Slices');
		         data1.addColumn('string', 'taskId');
		         data1.addRows([
				  	['Completed Task',parseInt(dataList[0]),dataList[3]],				  
				    ['In Progress Task',parseInt(dataList[1]),dataList[4]],
				    ['Not Completed Task',parseInt(dataList[2]),dataList[5]]
			     ]);

                // Set chart options
                 var options = {'colors': ['#279230', '#FF9900', '#DC3912']};

                // Instantiate and draw our chart, passing in some options.
			     var chart = new google.visualization.PieChart(document.getElementById('mytaskDashboardPie'));
			    
			    //Chart onclick function starts
			     function myTaskChartDetail() {
			        chartDetailType="myTask"
			        parent.$("#loadingBar").show();
                    parent.timerControl("start");
          			var selectedItem = chart.getSelection()[0];
         			 if (selectedItem) {
         			 	var taskStatusHeader = data1.getValue(selectedItem.row, 0);
         			 	var taskIds = data1.getValue(selectedItem.row, 2);
         			 	
            			$.ajax({
				          url:path+"/calenderAction.do",
				          type:"POST",
				          data:{act:"loadTaskChartDetails",taskStatusHeader:taskStatusHeader,taskIds:taskIds,chartDetailType:chartDetailType},
				          error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
				          success:function(result){
				          	parent.checkSessionTimeOut(result);
	          				$('#chartMyTaskDetailDiv').css('display','block');
	          				$("#myTaskDataDiv").html(result);
	            			$("#myTkHeader").html(taskStatusHeader+' Details');
		                    $('#ideaTransparent').css('display','block');
	 		                parent.$('#transparentDiv').css('display','block');
	            			var landH=$('#chartMyTaskDetailDiv').height();
							var mainH=parent.$('#menuFrame').height();
							$('#myTaskDataDiv').css('height',landH-90+'px');
						//	chartDetailScroll('myTaskDataDiv');
						    popUpScrollBar('myTaskDataDiv');
						   }
						});
         			 }
         			parent.$("#loadingBar").hide();
                    parent.timerControl("");
        		 }

       			 google.visualization.events.addListener(chart, 'select', myTaskChartDetail);   
       			 
       			 
       			 
       			 //Chart onclick function ends
			    
			     chart.draw(data1, options);
			     parent.$('#loadingBar').hide();
				 parent.timerControl("");
 		     }
 	    });
 	   }
    }
    
    function openAssignedTaskChart(type,taskType){
        tType=taskType;
	    var height = $(window).height();
		var sortDivHeight=$("#headerTopicDiv").height();
		var dataHeight=height-sortDivHeight-30;
		$("#mytaskDashboard").css('height',dataHeight);
	    $("#CalenderListViewTabDiv").hide();
	   if(taskType == 'my'){
	       $("#MyTasklistView").hide();
	       $("#mytaskdatalist").hide();
	    }else{
	       $("#AssignedlistView").hide();
	       $("#assignedtaskdatalist").hide();
	    }
	    $("#gotoListImg").show();
	    $("#breadCrumName").html('&nbsp;> Dashboard');
	    $("#mytaskDashboard").show();
	    
	    if(type=='bar'){
		    parent.$('#loadingBar').show();
		  	parent.timerControl("start");
		    $("#assignedtaskDashboardPie").hide();
		    $("#assignedtaskDashboardBar").show();
		    $.ajax({
		 		url:path+"/calenderAction.do",
		 		type:"POST",
		 		data:{act:"fetchassignedTaskChartData",chartType:type},
		 		error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
		 		success:function(result){
		 		    parent.checkSessionTimeOut(result);
			 		var mainArray = [ ['Dates', 'Completed Task', 'In Progress Task', 'Not Completed Task']];
					var subArray; 
			    	$(result).find("labelData").each(function(i){
				    	var completeTask = $(this).find("label_completedTask").text();
				    	var pendingTask = $(this).find("label_pendingTask").text();
				    	var incompleteTask =$(this).find("label_incompleteTask").text();
				    	var days = $(this).find("label_finalDay").text();
						subArray = new Array();
						subArray.push(days);
						subArray.push(parseInt(completeTask));
						subArray.push(parseInt(pendingTask));
						subArray.push(parseInt(incompleteTask));
						mainArray.push(subArray);
					});
				
			 		var data = google.visualization.arrayToDataTable(mainArray);
			        new google.visualization.ColumnChart(document.getElementById('assignedtaskDashboardBar')).
			            draw(data,
			                 {title:"Bar chart - Assigned task progress status",
			                  vAxis: {title: "Count"},
			                  hAxis: {title: "Dates"}}
			            );
			            parent.$('#loadingBar').hide();
			  	        parent.timerControl("");
			 		}
			 		
		 	});
	 }
	 else if(type=='pie'){
	     parent.$('#loadingBar').show();
	     parent.timerControl("start");
	     $("#assignedtaskDashboardPie").show();
	     $("#assignedtaskDashboardBar").hide();
	     // Create and populate the data table.
	     $.ajax({
	 		url:path+"/calenderAction.do",
	 		type:"POST",
	 		data:{act:"fetchassignedTaskChartData",chartType:type},
	 		error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
	 		success:function(result){
	 		    parent.checkSessionTimeOut(result);
 		        var dataList=result.split('#@#');
 		        
 		        var data1 = new google.visualization.DataTable();
			    data1.addColumn('string', 'Topping');
			    data1.addColumn('number', 'Slices');
			    data1.addColumn('string', 'taskId');
		        data1.addRows([
				  	['Completed Task',parseInt(dataList[0]),dataList[3]],				  
				    ['In Progress Task',parseInt(dataList[1]),dataList[4]],
				    ['Not Completed Task',parseInt(dataList[2]),dataList[5]]
			     ]);

      // Set chart options
		      var options = {'colors': ['#279230', '#FF9900', '#DC3912']};
		
		      // Instantiate and draw our chart, passing in some options.
		       var chart = new google.visualization.PieChart(document.getElementById('assignedtaskDashboardPie'));
		       
		        //Chart onclick function starts
			     function assignedTaskChartDetail() {
			       chartDetailType="assignedTask";
			        parent.$("#loadingBar").show();
                    parent.timerControl("start");
          			var selectedItem = chart.getSelection()[0];
         			 if (selectedItem) {
         			 	var taskStatusHeader = data1.getValue(selectedItem.row, 0);
         			 	var taskIds = data1.getValue(selectedItem.row, 2);
         			 	$.ajax({
				          url:path+"/calenderAction.do",
				          type:"POST",
				          data:{act:"loadTaskChartDetails",taskStatusHeader:taskStatusHeader,taskIds:taskIds,chartDetailType:chartDetailType},
				          error: function(jqXHR, textStatus, errorThrown) {
					                checkError(jqXHR,textStatus,errorThrown);
					                $("#loadingBar").hide();
									timerControl("");
								  }, 
				          success:function(result){
				          	parent.checkSessionTimeOut(result);
	          				$('#chartMyTaskDetailDiv').css('display','block');
	          				$("#myTaskDataDiv").html(result);
	            			$("#myTkHeader").html(taskStatusHeader+' Details');
		                    $('#ideaTransparent').css('display','block');
	 		                parent.$('#transparentDiv').css('display','block');
	            			var landH=$('#chartMyTaskDetailDiv').height();
							var mainH=parent.$('#menuFrame').height();
							$('#myTaskDataDiv').css('height',landH-90+'px');
							chartDetailScroll('myTaskDataDiv');
						   }
						});
         			 }
         			parent.$("#loadingBar").hide();
                    parent.timerControl("");
        		 }

       			 google.visualization.events.addListener(chart, 'select', assignedTaskChartDetail);   
       			 
       			 
       			 
       			 //Chart onclick function ends
		       
		       chart.draw(data1, options);
		       parent.$('#loadingBar').hide();
			   parent.timerControl("");
		 	 }
		});
 	  }
 	  
 	  //chartscroll();
    }
	function goToCalendarListView(){
	    if(tType == 'my'){
	       $("#MyTasklistView").show();
	       $("#mytaskdatalist").show();
	       $("#breadCrumName").html('&nbsp;> My Tasks');
	    }else{
	       $("#AssignedlistView").show();
	       $("#assignedtaskdatalist").show();
	       $("#breadCrumName").html('&nbsp;> Assigned Tasks');
	    }
	    $("#CalenderListViewTabDiv").show();
	    
	    $("#gotoListImg").hide();
	    $("#mytaskDashboard").hide();
	    
	}
	
	function chartscroll(){
		 $("div#mytaskDashboard").mCustomScrollbar({
			scrollButtons:{
				enable:true
			},
			advanced:{
	           autoExpandVerticalScroll:true,
	           autoScrollOnFocus:false
	       }
		}); 
	}
	
	function closeTaskDetailPopup(){
	     $('#chartMyTaskDetailDiv').css('display','none');
		 $('#ideaTransparent').css('display','none');
	 	 parent.$('#transparentDiv').css('display','none');
	
	 }
	  function chartDetailScroll(scrollDivId){
			$("div#"+scrollDivId).mCustomScrollbar({
					scrollButtons:{
						enable:true
					},
					
				});
				
		        $('div#'+scrollDivId+' >.mCS-light >.mCSB_container').css('margin-right','15px');
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools').css('margin-right','5px');
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
			    $('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
	            $('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
					
	}
	//chart functionality starts here
	function openLink(docName){
		var urlLink = docName;
		    var httpIndex = urlLink.indexOf("http://");
		    if(httpIndex!=0){
		      httpIndex = urlLink.indexOf("https://");
		      if(httpIndex!=0){
		         urlLink ='http://'+urlLink ;
		      }   
		    }
		    popup(urlLink);
	}
	
	function popup(url) {
		    params = 'width=' + screen.width-1;
		    params += ', height=' + screen.height-1;
		    params += ', top=0, left=0'
		    params += ', fullscreen=no';
		    newwin = window.open(url, 'windowname4', params);
		    if (window.focus) {
		        newwin.focus()
		    }
		    return false;
		}   