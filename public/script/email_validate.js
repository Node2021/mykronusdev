// Email Validation Javascript
// copyright 23rd March 2003, by Stephen Chapman, Felgall Pty Ltd

// You have permission to copy and use this javascript provided that
// the content of the script is not changed in any way.

function validateEmailJs(addr,man,db) {
if (addr == '' && man) {
   if (db) alert(getValues(companyAlerts,"Alert_EmailMandatory"));
   return false;
}
if (addr == '') return true;
var invalidChars = '\/\'\\ ";:?!()[]\{\}^|';
for (i=0; i<invalidChars.length; i++) {
   if (addr.indexOf(invalidChars.charAt(i),0) > -1) {
      if (db) alert(getValues(companyAlerts,"Alert_EmailInvalidChar"));
      return false;
   }
}
for (i=0; i<addr.length; i++) {
   if (addr.charCodeAt(i)>127) {
      if (db) alert(getValues(companyAlerts,"Alert_EmailContainAscii"));
      return false;
   }
}

var atPos = addr.indexOf('@',0);
if (atPos == -1) {
   if (db) alert(getValues(companyAlerts,"Alert_EmailContainn"));
   return false;
}
if (atPos == 0) {
   if (db) alert(getValues(companyAlerts,"Alert_EmailNotStartWith"));
   return false;
}
if (addr.indexOf('@', atPos + 1) > - 1) {
   if (db) alert(getValues(companyAlerts,"Alert_EmailContainOnlyOne"));
   return false;
}
if (addr.indexOf('.', atPos) == -1) {
   if (db) alert(getValues(companyAlerts,"Alert_EmailDomainName"));
   return false;
}
if (addr.indexOf('@.',0) != -1) {
   if (db) alert(getValues(companyAlerts,"Alert_PeriodFollow"));
   return false;
}
if (addr.indexOf('.@',0) != -1){
   if (db) alert(getValues(companyAlerts,"Alert_EmailPrecede"));
   return false;
}
if (addr.indexOf('..',0) != -1) {
   if (db) alert(getValues(companyAlerts,"Alert_PeriodAdjacent"));
   return false;
}
var suffix = addr.substring(addr.lastIndexOf('.')+1);
	
if (suffix.length < 2 || suffix != 'com' && suffix != 'net' && suffix != 'org' && suffix != 'edu' && suffix != 'int' && suffix != 'mil' && suffix != 'gov' && suffix != 'biz' && suffix != 'aero' && suffix != 'name' && suffix != 'coop' && suffix != 'info' && suffix != 'pro' && suffix != 'museum' && suffix != 'in' && suffix != 'jobs' && suffix != 'mobi' && suffix != 'eu' && suffix != 'travel' && suffix != 'ac' && suffix != 'ad' && suffix != 'ae' && suffix != 'af' && suffix != 'ag' && suffix != 'ai' &&   suffix != 'al' && suffix != 'am' && suffix != 'an' && suffix != 'ao' && suffix != 'aq' && suffix != 'ar' && suffix != 'as' && suffix != 'at' && suffix != 'au' && suffix != 'aw' && suffix != 'az' && suffix != 'ax' && suffix != 'bl' && suffix != 'ba' && suffix != 'bb' && suffix != 'bd' && suffix != 'be' && suffix != 'bf' && suffix != 'bg' && suffix != 'bh' && suffix != 'bi' && suffix != 'bj' && suffix != 'bm' && suffix != 'bn' &&
            suffix != 'bo' && suffix != 'br' && suffix != 'bs' && suffix != 'bt' && suffix != 'bv' && suffix != 'bw' && suffix != 'by' && suffix != 'bz' && suffix != 'ca' && suffix != 'cc' && suffix != 'cd' && suffix != 'cf' && suffix != 'cg' && suffix != 'ch' && suffix != 'ci' && suffix != 'ck' && suffix != 'cl' && suffix != 'cm' && suffix != 'cn' && suffix != 'co' && suffix != 'cr' && suffix != 'cu' && suffix != 'cv' && suffix != 'cx' && suffix != 'cy' && suffix != 'cz' && suffix != 'de' && suffix != 'dj' && suffix != 'dk' && suffix != 'dm' && suffix != 'do' && suffix != 'dz' && suffix != 'ec' && suffix != 'ee' && suffix != 'eg' && suffix != 'eh' && suffix != 'er' && suffix != 'es' && suffix != 'et' && suffix != 'fi' && suffix != 'fj' && suffix != 'fk' && suffix != 'fm' && suffix != 'fo' && suffix != 'fr' && suffix != 'ga' && suffix != 'gd' && suffix != 'ge' && suffix != 'gf' && suffix != 'gg' && suffix != 'gh' && suffix != 'gi' && suffix != 'gl' && suffix != 'gm' && suffix != 'gn' && suffix != 'gp' && suffix != 'gq' && suffix != 'gr' && suffix != 'gs' && suffix != 'gt' && suffix != 'gu' && suffix != 'gv' && suffix != 'gy' && suffix != 'hk' && suffix != 'hm' && suffix != 'hn' && suffix != 'im' && suffix != 'in' && suffix != 'io' && suffix != 'iq' && suffix != 'ir' && suffix != 'is' && suffix != 'it' && suffix != 'je' && suffix != 'jm' && suffix != 'jo' && suffix != 'jp' && suffix != 'ke' && suffix != 'kg' && suffix != 'kh' && suffix != 'ki' && suffix != 'km' && suffix != 'kn' && suffix != 'kp' &&
            suffix != 'kr' && suffix != 'kw' && suffix != 'ky' && suffix != 'kz' && suffix != 'la' && suffix != 'lb' && suffix != 'lc' && suffix != 'li' && suffix != 'lk' && suffix != 'lr' && suffix != 'ls' && suffix != 'lt' && suffix != 'lu' && suffix != 'lv' && suffix != 'ly' && suffix != 'ma' && suffix != 'mc' && suffix != 'md' && suffix != 'mg' && suffix != 'mh' && suffix != 'mk' && suffix != 'ml' && suffix != 'mm' && suffix != 'mn' && suffix != 'mo' && suffix != 'mp' && suffix != 'mq' && suffix != 'mr' && suffix != 'ms' && suffix != 'mt' && suffix != 'mu' && suffix != 'mv' && suffix != 'mw' && suffix != 'mx' && suffix != 'my' && suffix != 'mz' && suffix != 'na' && suffix != 'nc' && suffix != 'ne' && suffix != 'nf' && suffix != 'ng' && suffix != 'ni' && suffix != 'nl' && suffix != 'no' && suffix != 'np' && suffix != 'nr' && suffix != 'nu' && suffix != 'nz' && suffix != 'om' && suffix != 'pa' && suffix != 'pe' && suffix != 'pf' && suffix != 'pg' && suffix != 'ph' && suffix != 'pk' && suffix != 'pl' && suffix != 'pm' && suffix != 'pn' && suffix != 'pr' && suffix != 'ps' && suffix != 'pt' && suffix != 'pw' && suffix != 'py' && suffix != 'qa' && suffix != 're' && suffix != 'ro' && suffix != 'rw' && suffix != 'ru' && suffix != 'sa' && suffix != 'sb' && suffix != 'sc' && suffix != 'sd' && suffix != 'se' && suffix != 'sg' && suffix != 'sh' && suffix != 'si' && suffix != 'sj' && suffix != 'sk' && suffix != 'sl' && suffix != 'sm' && suffix != 'sn' && suffix != 'so' && suffix != 'sr' && suffix != 'st' &&
            suffix != 'sv' && suffix != 'sy' && suffix != 'sz' && suffix != 'tc' && suffix != 'td' && suffix != 'tf' && suffix != 'tm' && suffix != 'tn' && suffix != 'to' && suffix != 'tt' && suffix != 'tz' && suffix != 'th' && suffix != 'tj' && suffix != 'tk' && suffix != 'tp' && suffix != 'tr' && suffix != 'ua' && suffix != 'ug' && suffix != 'uk' && suffix != 'um' && suffix != 'us' && suffix != 'uy' && suffix != 'uz' && suffix != 'va' && suffix != 'vc' && suffix != 've' && suffix != 'vi' && suffix != 'vg' && suffix != 'vn' && suffix != 'vu' && suffix != 'ws' && suffix != 'wf' && suffix != 'ye' && suffix != 'yt' && suffix != 'yu' && suffix != 'za' && suffix != 'zm' && suffix != 'zw'
           ) {   if (db) alert(getValues(companyAlerts,"Alert_InvalidPrimaryDomain"));
   return false;
}
return true;
}




