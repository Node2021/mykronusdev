

var googleUser = {};
var client_id='362610831353-7psdndqeroq03agb7mvk2tnp627v1nle.apps.googleusercontent.com';
function startApp() {
   
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id:  client_id ,  //This will change depent Upon Googl e account
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('customBtn'));
    });
  };



  function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function(googleUser) {
            var profile = googleUser.getBasicProfile(); 
            var id_token = googleUser.getAuthResponse().id_token;
            console.log(id_token)
            googleSignIn(id_token);
        }, function(error) {
          console.log(error);
        });
  }

  function googleSignIn(id_token){
    $.get('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='+id_token)
        .then(function (response) {
            var idToken = id_token;
            var iss = response.iss;
            var at_hash = response.at_hash;
            var aud = response.aud;
            var sub = response.sub;
            var email_verified = response.email_verified;
            var azp = response.azp;
            var email = response.email;
            var iat = response.iat;
            var exp = response.exp;
            var given_name = response.given_name;
            var family_name = response.family_name;
            var alg = response.alg;
            var kid = response.kid;
            if(aud == client_id && email_verified == "true"){
                $('#loadingBar').show();
                timerControl("start");
                verifyUser(sub,email,given_name,family_name,'google');
            }


        })
        .catch(function (error) {
            alert(error);
        })
        .then(function () {
            // always executed
        });
  }

  // facebook login

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '250245839953109',//250245839953109 -- mykronusdev id      ////'2874179639494798' -- newmykronus id,  ////'4920668388008412',
      cookie     : true,
      xfbml      : true,
      version    : 'v9.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
        //display user data
        getFbUserData();
    }
});

function fbLogin() {
  FB.login(function (response) {
      if (response.authResponse) {
          // Get and display the user profile data
          getFbUserData();
      } else {
          document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
      }
  }, {scope: 'email'});
}

function getFbUserData(){
  FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
  function (response) {
        var fbId = response.id;
        var fbEmail = response.email;
        var fbFirst_name = response.first_name;
        var fbLast_name = response.last_name;
        verifyUser(fbId, fbEmail, fbFirst_name, fbLast_name, "facebook");

    });
}




  function verifyUser(id, email, first_name, last_name, loginAction){
    var localOffsetTime=getTimeOffset(new Date());
    let updateTaskHours = {
      "firstName":first_name,
      "lastName":last_name,
      "token":id,
      "type":loginAction,
      "email":email,
      "localoffsetTime":localOffsetTime
    }
    
    console.log("up---->"+JSON.stringify(updateTaskHours));
    
    $.ajax({
      url:apiPath+"/"+myk+"/v1/googlelogin",
      type:"post",
      dataType:"json",
      contentType:"application/json",
      data:JSON.stringify(updateTaskHours),
      error: function(jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR,textStatus,errorThrown);
                    $("#loadingBar").hide();
            timerControl("");
      },
      success:function(result){
        var jsondata = JSON.stringify(result);
        var userlogininfo = JSON.parse(jsondata);
        var a = userlogininfo.roleId;
        var cname = userlogininfo.companyId;
        window.sessionStorage.user = JSON.stringify(result);
        ///window.sessionStorage.traillogin = "Y";
        window.location.href = hostname+"/postlogin?pAct=home&rid="+a+"&cname="+cname+"";
      }
    });
  
               
  }